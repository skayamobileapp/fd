<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  
  </head>
  <body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>
      <!-- Breadcrumb -->
      <!-- <div class="breadcrumb-bar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-12 col-12">
              <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Terms and Conditions</li>
                </ol>
              </nav>
              <h2 class="breadcrumb-title">Terms and Conditions</h2>
            </div>
          </div>
        </div>
      </div> -->
      <!-- /Breadcrumb -->
      
      <!-- Page Content -->
      <div class="content">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
                    <h1>Why First Doctor?</h1>
                    <div class="join-us-content">

                        <p class="text-justify">FirstDoctor’s primary aim is to bridge the gap between healthcare providers and patients
using technology. In doing so, we endeavour to make the lives of professional healthcare
providers easy. Doctors running single practices or polyclinics are often encumbered with
limited resources to handle their practices. We help them get more bang for their buck
through our unique and customer-focussed tech solutions.
</p>
                      <p class="text-justify"> Our proprietary medical practice management software is the result of two years of extensive
research and analysis. It helps doctors schedule and plan appointments, take care of billing,
streamline office workflow and automate routine tasks to free up precious time and resources.
The result? A profitable practice and satisfied patients.</p>

                      <p class="text-justify">Patient information management occurs with the help of a digital PHR (Patient Health
Records) that are secured on a platform and accessible to authorized personnel, including the
patient’s family and her team of caregivers. The information provides a more clear picture of

the patient’s health and improves the ability to effectively monitor patient outcomes and for
efficient intervention when needed.</p>

                      <p class="text-justify">Our hospital management system has been created for efficiently managing the resources of a
small or mid-sized hospitals. This system helps in allotting resources, keeping track of
financials and business intelligence through collating and analysis of data that helps in
enhancing profitability and provides a high level of patient care.
</p>                      
                    </div>
                </div>
                
               
                <div class="col-xs-12">
                  <div class="join-us-content">
                    <p class="text-justify">We believe that in a country like India, there is a serious loss of parity between care givers
and receivers. To create an efficient system, we need to bring better connectivity of patients,
physicians, pharmacies and secondary healthcare providers. A technological platform is the
only way to create a comprehensive and holistic ecosystem to bring all the stakeholders of the
healthcare system together.</p>                 

                    <p class="text-justify">FirstDoctor endeavours to be a comprehensive, technology-enabled and data-driven
healthcare ecosystem that encompasses all aspects of healthcare &amp; hygiene, including
happiness &amp; wellness, sports &amp; fitness, spas &amp; salons. We believe that along with reactive
healthcare, proactive and preventive healthcare is vital to reduce the burden on our country’s
health system. The gospel of lasting health and wellness needs to be preached and practised
by all to remain happy and healthy in the pursuit of the joys of life. </p>                         
                  </div>             
                </div>
        </div>

      </div>    
      <!-- /Page Content -->
   
      <?php include('footer.php'); ?>
    </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="assets/js/script.js"></script>
    
  </body>
</html>