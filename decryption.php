<?php

function decryption($number){

	// Store the cipher method 
$ciphering = "AES-128-CTR"; 

$options = 0; 

// Non-NULL Initialization Vector for decryption 
$decryption_iv = '1234567891011121'; 
  
// Store the decryption key 
$decryption_key = "cryptionKey"; 
  
// Use openssl_decrypt() function to decrypt the data 
$decryption=openssl_decrypt ($number, $ciphering,  
        $decryption_key, $options, $decryption_iv);

return $decryption;
}
?>