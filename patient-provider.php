<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      h4{
        size: 6px;
      }
      p{
        font-size: 13px;
      }

    </style>
  
  </head>
  <body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>
      
      <!-- Page Content -->
      <div class="content">
        <div class="container">
          <h1 class="text-center primary-title">For Patients </h1>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/service_phr.svg" width="80px">
                    <h4 style="color: #3c663e;">Personal Health Record (PHR)</h4>
                    <p>Digital medical records stored on a safe secure platform allows access to team of
caregivers for better patient care.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col pink">
                    <img src="img/service_post_care_treatment.svg" width="80px">
                    <h4 style="color: #ff3655;">Post Treatment Care</h4>
                    <p>Easy access to care for convalescing patients, after primary treatment. Choose the
right caregivers for your diverse needs.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col orange">
                    <img src="img/service_ai.svg" width="80px">
                    <h4 style="color: #ed8e0e;">AI-powered Wellness</h4>
                    <p>AI solutions help in analysing patient condition for better diagnoses and create
treatment strategies. Can also be used for material management in hospitals.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col navy-blue">
                    <img src="img/service_data_analytics.svg" width="80px" >
                    <h4 style="color: #5f329b;">Data Analytics</h4>
                    <p>Big Data is leveraged to analyse patient behaviour and other aspects that guide the
better functioning of the healthcare ecosystem.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col orange">
                    <img src="img/service_clinics_hospitals.svg" width="80px">
                    <h4 style="color: #ed8e0e;">Clinics &amp; Hospitals</h4>
                    <p>Easy search of top clinic and hospitals, quick appointments and confirmations. No
need to stand in waiting rooms and queues.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col navy-blue">
                    <img src="img/service_medical_supplies.svg" width="80px">
                    <h4 style="color: #5f329b;">Medical Supplies</h4>
                    <p>The FirstDoctor platform connects vendors of medical supplies with patients and
healthcare providers.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/service_diagnostic.svg" width="80px">
                    <h4 style="color: #3c663e;">Diagnostic Services</h4>
                    <p>Accredited path labs on the FirstDoctor platform for quick testing and reporting at
affordable rates.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col pink">
                    <img src="img/service_nextgen_testing.svg" width="80px">
                    <h4 style="color: #ff3655;">Gene testing</h4>
                    <p>Next-generation gene testing services for predictive health testing and counselling on
diet and lifestyle changes for lasting health and wellness.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="new-services-col navy-blue">
                        <img src="img/service_pharmacy.svg" width="80px">
                        <h4 style="color: #5f329b;">Pharmacy</h4>
                        <p>Accredited pharmacies will help provide required medication to patients to doorstep.</p>
                    </div>
                </div>  
                <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="new-services-col pink">
                            <img src="img/service_blood_bank.svg" width="80px">
                            <h4 style="color: #ff3655;">Blood Banks</h4>
                            <p>Accredited blood banks connected to the First Doctor platform will get notifications on requirements for easy and quick access and delivery. Real-time reporting and stock details will help in optimum utilization of resources.</p>
                        </div>
                    </div>     
                    <div class="col-sm-6 col-md-4 col-lg-3">
                            <div class="new-services-col orange">
                                <img src="img/service_emergency.svg" width="80px">
                                <h4 style="color: #ed8e0e;">Paramedic &amp; emergency</h4>
                                <p>To avert adverse health events, paramedic services will be connected to First Doctor and will respond swiftly in cases of emergency.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-3">
                                <div class="new-services-col green">
                                    <img src="img/service_homecare.svg" width="80px">
                                    <h4 style="color: #3c663e;">Home Care</h4>
                                    <p>Accredited caregivers like nurses, physiotherapists and physicians will connect with patients that require home care services on demand.</p>
                                </div>
                            </div>                                                                 
        </div>
        </div>

      </div>    
      <!-- /Page Content -->
   
      <?php include('footer.php'); ?>
    </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="assets/js/script.js"></script>
    
  </body>
</html>