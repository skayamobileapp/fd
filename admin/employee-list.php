<?php
include('../connection/conn.php');
include('session_check.php');
$select = mysqli_query($conn,"SELECT e1.id, e1.name, e1.email, e1.phone, e1.designation, e2.name as manager FROM employee_details e1 INNER JOIN employee_details as e2 ON e1.reporting_manager=e2.id");
$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {

  $view[$i]['id'] = $row['id'];
  $view[$i]['name'] = $row['name'];
  $view[$i]['email'] = $row['email'];
  $view[$i]['phone'] = $row['phone'];
  $view[$i]['designation'] = $row['designation'];
  $view[$i]['manager'] = $row['manager'];
  $i++;

}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		
			<?php include('navbar.php'); ?>

			<?php include('sidebar.php'); ?>
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">List of Employees <a href="add-employee.php" class="btn btn-primary btn-lg pull-right">Add Employee</a></h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item active">Employees</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Employee Name</th>
					                               <th>Email</th>
					                               <th>Phone Number</th>
					                               <th>Designation</th>
					                               <th>Reporting Manager</th>
												</tr>
											</thead>
											<tbody>
												<?php
									            for ($i=0; $i <count($view) ; $i++) {
									              ?>
												<tr>
													<td><h2 class="table-avatar"><?php echo $view[$i]['name'] ; ?></h2>
													</td>
													<td><?php echo $view[$i]['email'] ; ?></td>
													<td><?php echo $view[$i]['phone']; ?></td>
													<td><?php echo $view[$i]['designation'] ; ?></td>
													<td class="text-right"><?php echo $view[$i]['manager']; ?></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>			
					</div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		
    </body>
</html>