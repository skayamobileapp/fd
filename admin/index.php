<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$sql   = "select * from doctor_details";
$result = $conn->query($sql);
$doctorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($doctorList, $row);
}

$sql   = "select * from patient_details";
$result = $conn->query($sql);
$patientList = array();
while ($row = $result->fetch_assoc()) {
    array_push($patientList, $row);
}

$sql   = "select * from lab_details";
$result = $conn->query($sql);
$hospitalList = array();
while ($row = $result->fetch_assoc()) {
    array_push($hospitalList, $row);
}

$sql   = "select * from pharmacy_registration";
$result = $conn->query($sql);
$drugList = array();
while ($row = $result->fetch_assoc()) {
    array_push($drugList, $row);
}

if ($_POST) {
  $id_doc = $_POST['doctor'];
  $id_pat = $_POST['patient'];
  $id_lab = $_POST['lab'];
  $id_pharma = $_POST['pharma'];
  $message = $_POST['msg'];

  $sql = "INSERT INTO notifications(message, doctor_flag, patient_flag, phama_flag, read_flag)VALUES('$message','$id_doc','$id_pat','$id_pharma','$id_lab')";
  $result = mysqli_query($conn,$sql);
  if ($result) {
    echo "<script>alert('Notification Sent Successfully')</script>";
  }
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="../fd_logo.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('navbar.php'); ?>
			

			<?php include('sidebar.php'); ?>
			
			<!-- Page Wrapper -->
            <div class="page-wrapper">
			
                <div class="content container-fluid">
					
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Welcome Admin!</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item active">Dashboard</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

					<div class="row">
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon text-primary border-primary">
											<i class="fe fe-users"></i>
										</span>
										<div class="dash-count">
											<h3><?php echo count($doctorList); ?></h3>
										</div>
									</div>
									<div class="dash-widget-info">
										<h6 class="text-muted">Doctors</h6>
										<div class="progress progress-sm">
											<div class="progress-bar bg-primary w-50"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon text-success">
											<i class="fe fe-credit-card"></i>
										</span>
										<div class="dash-count">
											<h3><?php echo count($patientList); ?></h3>
										</div>
									</div>
									<div class="dash-widget-info">
										
										<h6 class="text-muted">Patients</h6>
										<div class="progress progress-sm">
											<div class="progress-bar bg-success w-50"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon text-danger border-danger">
											<i class="fe fe-money"></i>
										</span>
										<div class="dash-count">
											<h3>485</h3>
										</div>
									</div>
									<div class="dash-widget-info">
										
										<h6 class="text-muted">Appointment</h6>
										<div class="progress progress-sm">
											<div class="progress-bar bg-danger w-50"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon text-warning border-warning">
											<i class="fe fe-folder"></i>
										</span>
										<div class="dash-count">
											<!-- <h3>$62523</h3> -->
										</div>
									</div>
									<div class="dash-widget-info">
										
										<h6 class="text-muted">Revenue</h6>
										<div class="progress progress-sm">
											<div class="progress-bar bg-warning w-50"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Send Notifications</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item active">Select To Send Notification</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

                <div class="card">
                  <form action="" method="POST">
                  	<div class="content container-fluid">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group" style="padding: 20px;">
                        <label>
                        </label><br>
                           <label class="checkbox-inline">
                            <input type="checkbox" name="doctor" id="Telcheck1" value="1">
                            <span class="check-radio" ></span>Doctor</label>
                           
                           <label class="checkbox-inline">
                             <input type="checkbox" name="patient" id="Telcheck2" value="1"><span class="check-radio"></span>Patient
                           </label>

                           <label class="checkbox-inline">
                             <input type="checkbox" name="lab" id="Telcheck3" value="1"><span class="check-radio">
                             </span>Diagnostic Lab</label>

                             <label class="checkbox-inline">
                             <input type="checkbox" name="pharma" id="Telcheck4" value="1"><span class="check-radio">
                             </span>Pharmacy</label>
                           </div>
                            <div class="form-group col-sm-6">
                              <textarea class="form-control" name="msg" placeholder="Enter Notification Message"></textarea>
                            </div>
                            <div class="bttn-group">
                              <button type="submit" class="btn btn-primary btn-lg pull-right" name="save" id="save">Send</button>
                            </div>
                        </div>
                      </div>
                  </div>
                    </form>
                  </div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<script src="assets/plugins/raphael/raphael.min.js"></script>    
		<script src="assets/plugins/morris/morris.min.js"></script>  
		<script src="assets/js/chart.morris.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		
    </body>
</html>