<?php
include('../connection/conn.php');
include('session_check.php');
// $onload = 0;
$employee=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "SELECT * FROM employee_details where id='$id' ";
    $result   = $conn->query($sql);
    $employee = $result->fetch_assoc();
   }

if($_POST)
{
    if($_GET['id'])
    {
      $id = $_GET['id'];
      $name = $_POST['name'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $designation = $_POST['designation'];
      $manager = $_POST['manager'];
      $type = $_POST['type'];
      if ($type == 'Employee') {
            $ref= sprintf("%'.05d", $id);
              $ref_number = "EMP"."".$ref;
        }
        else
        {
          $ref= sprintf("%'.05d", $id);
          $ref_number = "INT"."".$ref;
        }

     $sql = "UPDATE employee_details SET name='$name', email='$email', phone='$phone', designation='$designation', reporting_manager='$manager', type='$type', reference_no='$ref_number' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Employee Details updated successfully');</script>";
        echo "<script>parent.location='employee_details.php'</script>";
      }
    }
    else
    {
      $name = $_POST['name'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $designation = $_POST['designation'];
      $manager = $_POST['manager'];
      $type = $_POST['type'];

      $sql = "INSERT INTO employee_details(name, email, phone, password, designation, reporting_manager, type, reference_no)VALUES('$name','$email','$phone','123456789','$designation','$manager', '$type', '')";
      $result  = $conn->query($sql);
      if ($result)
      {
        $type = $_POST['type'];
          if ($type == 'Employee') {
            $rid = mysqli_insert_id($conn);
            $ref= sprintf("%'.05d", $rid);
                    $ref_number = "EMP"."".$ref;
                    $update_query = "UPDATE employee_details SET reference_no='$ref_number' WHERE id='$rid' ";
                    mysqli_query($conn,$update_query);
          }
          else
          {
            $rid = mysqli_insert_id($conn);
            $ref= sprintf("%'.05d", $rid);
                    $ref_number = "INT"."".$ref;
                    $update_query = "UPDATE employee_details SET reference_no='$ref_number' WHERE id='$rid' ";
                    mysqli_query($conn,$update_query);
          }

          // Sms code
$mobileno = $phone;
$message = urlencode("Hello ".ucwords($name).", You have been successfully coordinated with FirstDoctor Team as ".$type." and here is your referral code. Use it for further reference. ".$ref_number." ");
$ch = curl_init();

$url = "http://simplysmsit.com/submitsms.jsp?user=HEALTH&key=a9f3bd5447XX&mobile=$mobileno&message=$message&senderid=FSTDOC&accusage=1";

// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

// grab URL and pass it to the browser
curl_exec($ch);

// close cURL resource, and free up system resources
curl_close($ch);

        echo "<script>alert('Employee added successfully');</script>";
        echo "<script>parent.location='employee_details.php'</script>";
      }
    }
}
$sql   = "select * from employee_details";
$result = $conn->query($sql);
$empList = array();
while ($row = $result->fetch_assoc()) {
    array_push($empList, $row);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
    
    <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    
    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    
    <!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
    
    <!-- Datatables CSS -->
    <link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
    
    <!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">

        <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
  
    <!-- Main Wrapper -->
        <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>

      <?php include('sidebar.php'); ?>
      <!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
        
          <!-- Page Header -->
          <div class="page-header">
            <div class="row">
              <div class="col-sm-12">
                <h3 class="page-title">Add Employee </h3>
                <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                  <li class="breadcrumb-item active"> Add Employee</li>
                </ul>
              </div>
            </div>
          </div>
          <!-- /Page Header -->
          
          <div class="row">
            <div class="col-sm-12">
              <div class="card">
                <div class="card-body">
                  <form action="" method="POST" id="myform">
                    <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                                <label class="">Employee Type<span class="text-danger"></span></label>
                                <select class="form-control selitemIcon" name="type" id="type">
                                <option value="">Select Type</option>
                                <option value="Employee" <?php if($employee['type']=='Employee'){ echo "selected";}?>>EMPLOYEE</option>
                                <option value="Intern" <?php if($employee['type']=='Intern'){ echo "selected";}?>>INTERN</option>
                              </select>
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                            <label>Employee Name <span class="text-danger">*</span></label>
                              <input type="text" name="name" class="form-control" id="name" autocomplete="off" value="<?php echo $employee['name'];?>" placeholder="Enter Employee Name">
                          </div>            
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                                <label>Email<span class="text-danger">*</span></label>
                                <input name="email" type="text" class="form-control" id="email" value="<?php echo $employee['email'];?>" autocomplete="off" placeholder="Enter Employee Email">
                        </div>                            
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label>Phone Number <span class="text-danger">*</span></label>
                                <input name="phone" type="text" class="form-control" id="phone" value="<?php echo $employee['phone'];?>" autocomplete="off" maxlength='10' placeholder="Enter Employee Phone Number">
                          </div>          
                        </div>                        
                    </div>
                    <div class="row">                
                      <div class="col-sm-6">
                          <div class="form-group">
                                <label> Designation <span class="text-danger">*</span></label>
                              <input name="designation" type="text" class="form-control" id="designation" value="<?php echo $employee['designation'];?>" autocomplete="off" placeholder="Enter Employee Designation">
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group">
                              <label class="">Reporting Manager<span class="text-danger"></span> *</label>
                                <select class="form-control selitemIcon" name="manager" id="manager">
                                <option value="">Select Employee Name</option>
                                  <?php
                                  for ($i=0; $i <count($empList); $i++) { ?>
                                      <option value="<?php echo $empList[$i]['id']; ?>" <?php if ($empList[$i]['id']==$employee['reporting_manager']){
                                        echo "selected=selected";
                                      } ?> >
                                        <?php echo $empList[$i]['name']; ?></option>
                                      <?php
                                  }
                                  ?>
                              </select>
                          </div>
                        </div>
                    </div>
                    
                    <div class="bttn-group pull-right">
                        <a class="btn btn-light" href="employee-list.php">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    </div>
                    </form>
                </div>
              </div>
            </div>      
          </div>
          
        </div>      
      </div>
      <!-- /Page Wrapper -->
    
        </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
    
    <!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <!-- Datatables JS -->
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/datatables.min.js"></script>
    
    <!-- Custom JS -->
    <script  src="assets/js/script.js"></script>
    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#myform").validate({
             rules : {

              name : {
                required : true,
                accept: true
            },
              email : "required",
              type : "required",
              designation : "required",
              manager : "required",
              city_id : "required",
              pincode : "required",
                
                  phone : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                }
         
            },
            messages : {

                name : {
               required : "<span> enter Employee name</span>",
               accept : "<span> enter letters only</span>"
               },
               
                email : "<span> enter email Id</span>",
                type : "<span> select employee type</span>",
                designation : "<span> enter designation</span>",
                manager : "<span> select reporting manager</span>",
                city_id : "<span> select city</span>",
                pincode : "<span> enter pincode </span>",
            
                 phone : {
                    required : "<span> enter phone number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
    
    </body>
</html>