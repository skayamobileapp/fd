<?php
include('../connection/conn.php');
include('session_check.php');
$select = mysqli_query($conn,"SELECT e.name as emp_name, t.date, t.hours, t.task_details FROM employee_details as e INNER JOIN time_track as t ON t.emp_name=e.id");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {

  $view[$i]['id'] = $row['id'];
  $view[$i]['emp_name'] = $row['emp_name'];
  $view[$i]['date'] = $row['date'];
  $view[$i]['hours'] = $row['hours'];
  $view[$i]['task_details'] = $row['task_details'];
  $i++;

}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		
			<?php include('navbar.php'); ?>

			<?php include('sidebar.php'); ?>
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">List of Time Track <a href="add-time-track.php" class="btn btn-primary btn-lg pull-right">Add Time Track</a></h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item active">Time Track</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->

					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>SL. NO</th>
						                            <th>Employee Name</th>
						                            <th>Date</th>
						                            <th>Hours</th>
						                            <th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
									            for ($i=0; $i <count($view) ; $i++) {
									              ?>
												<tr>
													<td><?php echo $i+1; ?></td>
													<td><h2 class="table-avatar"><?php echo $view[$i]['emp_name'] ; ?></h2>
													</td>
													<td><?php echo $view[$i]['date'] ; ?></td>
													<td><?php echo $view[$i]['hours']; ?></td>
							                	<td><a class="fa fa-edit" href="add-time-track.php?id="<?php echo $view[$i]['id']; ?>></a></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>			
					</div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		
    </body>
</html>