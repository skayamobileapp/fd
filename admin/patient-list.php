<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
date_default_timezone_set("Asia/Kolkata");

$select = mysqli_query($conn,"SELECT p.*, e.id as eid, e.title, e.start, e.status, e.amount, d.doctor_name from patient_details as p INNER JOIN events as e on p.id=e.patient_id INNER JOIN doctor_details as d ON d.id=e.doctor_id order by e.start ASC");
$CurrentDate = date('Y-m-d');

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['patient_name'] = $row['patient_name'];
  $view[$i]['doctor_name'] = $row['doctor_name'];
  $view[$i]['mobile_number'] = $row['mobile_number'];
  $view[$i]['start'] = date("d M Y", strtotime($row['start']));
  $view[$i]['address1'] = $row['address1'];
  $view[$i]['amount'] = $row['amount'];
  $view[$i]['date_of_birth'] = date("Y-m-d", strtotime($row['date_of_birth']));

  $i++;

}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		
			<?php include('navbar.php'); ?>

			<?php include('sidebar.php'); ?>
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">List of Patients</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item active">Patients</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Patient Name</th>
													<th>Age</th>
													<th>Address</th>
													<th>Phone</th>
													<th>Appt Visit</th>
													<th>Appt With</th>
													<th class="text-right">Paid</th>
												</tr>
											</thead>
											<tbody>
												<?php
									            for ($i=0; $i <count($view) ; $i++) { $age = $CurrentDate - $view[$i]['date_of_birth'];
									              ?>
												<tr>
													<td><h2 class="table-avatar"><?php echo $view[$i]['patient_name'] ; ?></h2>
													</td>
													<td><?php echo $age; ?></td>
													<td><?php echo $view[$i]['address1'] ; ?></td>
													<td><?php echo $view[$i]['mobile_number']; ?></td>
													<td><?php if($view[$i]['amount']=='0'){ echo "Not Visited"; } else{ echo $view[$i]['start'];} ?></td>
													<td><?php echo $view[$i]['doctor_name'] ; ?></td>
													<td class="text-right">₹<?php echo $view[$i]['amount']; ?>.00</td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
									</div>
								</div>
							</div>
						</div>			
					</div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		
    </body>
</html>