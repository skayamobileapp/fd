<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$date = date('Y-m-d');

 $select = "SELECT e.*, d.doctor_name, c.city FROM events_list e INNER JOIN doctor_details d ON d.id=e.id_doctor INNER JOIN cities c ON c.id=e.city WHERE event_fromdatetime > CURRENT_DATE order by event_fromdatetime ASC";

$resultcity = mysqli_query($conn,$select);

  $i=0;
  $events = array();

   while ($row=mysqli_fetch_assoc($resultcity))
  {
     
     $row['event_fromdatetime']   = date("d-m-Y", strtotime($row['event_fromdatetime']));
     $row['event_todatetime']   = date("d-m-Y", strtotime($row['event_todatetime']));
     
     $events[$i]['id'] = $row['id'];
     $events[$i]['event_name'] = $row['event_name'];
     $events[$i]['interest_no'] = $row['interest_no'];
     $events[$i]['event_desc'] = $row['event_desc'];
     $events[$i]['event_fromdatetime'] = $row['event_fromdatetime'] . " To <br> ". $row['event_todatetime'];
     $events[$i]['location'] = $row['location'] . ", " . $row['city'];
     $events[$i]['doctor_name'] = $row['doctor_name'];
     $events[$i]['fd_colaboration'] = $row['fd_colaboration'];
     $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		<?php include('navbar.php'); ?>

			<?php include('sidebar.php'); ?>
	
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">List of Events</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item active">Events</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Event Name</th>
						                              <th>Event Date</th>
						                              <th>Address</th>
						                              <th>Doctor Name</th>
						                              <th>Colloborate</th>
												</tr>
											</thead>
											<tbody>
												<?php
            
            for ($i=0; $i <count($events) ; $i++) {
              ?>
              <tr>
                <td><?php echo strtoupper($events[$i]['event_name']); ?></td>
                <!-- <td><?php echo $events[$i]['event_desc']; ?></td> -->
                
                <td><?php echo $events[$i]['event_fromdatetime']; ?></td>
                <td><?php echo $events[$i]['location']; ?></td>
                <td><?php echo $events[$i]['doctor_name']; ?></td>
                <td><?php 
                    if($events[$i]['fd_colaboration'] == 1){ 
                      echo "Collaborated";
                    }
                    else 
                    { 
                      echo "<a class='btn btn-primary' onclick = 'javascript: event_id($(this));return false;' href='set_fd_collaboration.php?id=". $events[$i]['id']."'>Collaborate</a>";
                    } ?>
                </td>

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>			
					</div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->

		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		<script type="text/javascript">
  function event_id(id)
  {
   var conf = confirm('Are you sure to collaborate with this event?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    </body>
</html>