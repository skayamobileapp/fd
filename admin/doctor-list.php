<?php
include('../connection/conn.php');
include('session_check.php');
$select = mysqli_query($conn,"SELECT d.*, ss.name FROM doctor_details AS d INNER JOIN sub_specialties AS ss ON d.sub_specialty=ss.id order by d.doctor_name ASC");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
    # code...
  $view[$i]['id'] = $row['id'];
  $view[$i]['doctor_name'] = $row['doctor_name'];
  $view[$i]['profile_status'] = $row['profile_status'];
  $view[$i]['mobile'] = $row['mobile'];
  $view[$i]['email'] = $row['email'];
  $view[$i]['name'] = $row['name'];
  $view[$i]['address'] = $row['address'];
  $view[$i]['approval_status'] = $row['approval_status'];
  $i++;

}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		<?php include('navbar.php'); ?>

			<?php include('sidebar.php'); ?>
	
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">List of Doctors</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item active">Doctors</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Doctor Name</th>
					                               <th>Contact NO</th>
					                               <th>Speciality</th>
					                               <th>Profile Status</th>
					                               <th>Approval Status</th>
					                               <th>Verify Doctor Details</th>
												</tr>
											</thead>
											<tbody>
												<?php
											for ($i=0; $i <count($view) ; $i++) {
												?>
												<tr>
													<td>
														<h2 class="table-avatar"><?php echo $view[$i]['doctor_name']; ?>
														</h2><br>
														<small><?php echo strtolower($view[$i]['email']); ?></small>
													</td>
													<td><?php echo $view[$i]['mobile']; ?></td>
													
													<td><?php echo $view[$i]['name'] ; ?></td>
													
													<td><div class="status-toggle">
															<input type="checkbox" id="status_1" class="check" <?php if($view[$i]['profile_status'] =='1'){ echo "checked"; } ?>>
															<label for="status_1" class="checktoggle">checkbox</label>
														</div>
													</td>
													
													<td>
														<div class="status-toggle">
															<input type="checkbox" id="status_1" class="check" <?php if($view[$i]['approval_status'] =='1'){ echo "checked"; } ?>>
															<label for="status_1" class="checktoggle">checkbox</label>
														</div>
													</td>
													<td><a href="verify-doctor-details.php?id=<?php echo $view[$i]['id']; ?>" class="btn btn-primary">Verify</a></td>
												</tr>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>			
					</div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		
    </body>
</html>