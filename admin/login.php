<?php
session_start();
include('../connection/conn.php');

if($_POST)
{
    // $userType = $_POST['user'];
    $email = $_POST['email'];
    $password = $_POST['password'];

//admin data fetching queries
   
      $sql = "SELECT * from admin_login where email='$email' and password='$password'";
      $result  = $conn->query($sql);
      $loginList = array();
      while ($row = $result->fetch_assoc())
      {
        $_SESSION['admin_login'] = $row;
        array_push($loginList, $row);
      }
    

    if(empty($loginList))
    {
      echo "<script>alert('Please enter the valid username and password');</script>";
      echo "<script>parent.location='login.php'</script>";
      exit;
    }
    else
    {
      echo "<script>parent.location='index.php'</script>";
      exit;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper login-body">
            <div class="login-wrapper">
            	<div class="container">
                	<div class="loginbox">
                    	<div class="login-left">
							<img class="img-fluid" src="../logo.png" alt="Logo">
                        </div>
                        <div class="login-right">
							<div class="login-right-wrap">
								<h1>Login</h1>
								<p class="account-subtitle">Access to our dashboard</p>
								
								<!-- Form -->
					          <form action="" method="POST" id="loginform">
									<div class="form-group">
										<input type="text" name="email" class="form-control" autocomplete="off" placeholder="Enter Email" required>
									</div>
									<div class="form-group">
										<input name="password" type="password" class="form-control" autocomplete="off" placeholder="Enter Password" required>
									</div>
									<div class="form-group">
										<button class="btn btn-primary btn-block" type="submit" name="submit">Login</button>
									</div>
								</form>
								<!-- /Form -->
								
								<div class="text-center forgotpass"><a href="#">Forgot Password?</a></div>
								<div class="login-or">
									<span class="or-line"></span>
									<span class="span-or">or</span>
								</div>
								  
								<!-- Social Login -->
								<div class="social-login">
									<span>Login with</span>
									<a href="#" class="facebook"><i class="fa fa-facebook"></i></a><a href="#" class="google"><i class="fa fa-google"></i></a>
								</div>
								<!-- /Social Login -->
								
								<!-- <div class="text-center dont-have">Don’t have an account? <a href="register.html">Register</a></div> -->
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		
    </body>
</html>