<?php
include('../connection/conn.php');
// include('session_check.php');
// $onload = 0;
$doctor=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "SELECT d.doctor_name, d.email, d.mobile, d.address, d.pincode, d.experiance, d.qualification, d.qualification_ug, d.qualification_ug_file, d.qualification_pg, d.qualification_pg_file, d.institute, d.ima_no, d.licence, d.license2, d.license_file, d.license_file2, d.approval_status, ss.name, s.specialty, m.name as stream FROM doctor_details AS d INNER JOIN stream AS m ON d.stream=m.id INNER JOIN specialties AS s ON d.specialty=s.id INNER JOIN sub_specialties AS ss ON d.sub_specialty=ss.id WHERE d.id='$id' ";
    $result   = $conn->query($sql);
    $doctor = $result->fetch_assoc();
    // print_r($doctor);\
   }

if($_POST)
{
    if($_GET['id'])
    {
      $id = $_GET['id'];
      $approval = $_POST['approval'];

     $sql = "UPDATE doctor_details SET approval_status='$approval' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Doctor Approval status added successfully');</script>";
        echo "<script>parent.location='doctor_approval_list.php'</script>";
      }
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
		<?php include('navbar.php'); ?>

			<?php include('sidebar.php'); ?>
	
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">
				
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Verify Doctor Details</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item"><a href="doctor-list.php">Doctors</a></li>
									<li class="breadcrumb-item active">Verify Doctor Details</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<form action="" method="POST" id="myform">
                    <div class="row">
                        <div class="col-sm-6">
                          <h4> Doctor Name : <u><?php echo $doctor['doctor_name'];?></u></h4>                         
                        </div>
                        <div class="col-sm-6">
                          <h4> Doctor Mobile : <u style=""><?php echo $doctor['mobile'];?></u></h4>                         
                        </div>
                    </div>
                    <div class="row">                      
                        <div class="col-sm-6">
                              <h4>Doctor Email_Id : <u style=""> <?php echo $doctor['email'];?></u> </h4>
                        </div>              
                        <div class="col-sm-6">
                          <h4>Doctor Address : <u style=""><?php echo $doctor['address'];?></u></h4>
                        </div>
                    </div>
                    <div class="row">                      
                        <div class="col-sm-6">
                              <h4>Stream : <u style=""> <?php echo $doctor['stream']; ?></u> </h4>
                        </div>              
                        <div class="col-sm-6">
                          <h4>Specialty : <u style=""><?php echo $doctor['specialty'];?></u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                              <h4>Sub Specialty : <u style=""> <?php echo $doctor['name']; ?></u> </h4>
                        </div>              
                        <div class="col-sm-6">
                          <h4>Experiance : <u style=""><?php echo $doctor['experiance'];?> Years</u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                              <h4>Institution / University : <u style=""> <?php echo $doctor['institute']; ?></u> </h4>
                        </div>              
                        <div class="col-sm-6">
                          <h4>IMA Number : <u style=""><?php echo $doctor['ima_no'];?></u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                              <h4>Highest Educational Qualification : <u style=""> <?php echo $doctor['qualification']; ?></u> </h4>
                        </div>              
                        <div class="col-sm-6">
                          <h4>UG Qualification Name : <u style=""><?php echo $doctor['qualification_ug'];?> </u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>UG Qualification Certificate: <a href="<?php echo $doctor['qualification_ug_file']; ?>" target="_blank"><?php echo $doctor['qualification_ug_file']; ?></a> </h4>
                        </div>
                        <embed src="<?php echo '../uploads/'.$doctor['qualification_ug_file']; ?>" type="application/pdf" width="90%" height="350px" />
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>PG Qualification Name : <u style=""><?php echo $doctor['qualification_pg'];?> </u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>PG Qualification Certificate: <a href="<?php echo $doctor['qualification_pg_file']; ?>" target="_blank"><?php echo $doctor['qualification_pg_file']; ?></a> </h4>
                        </div>
                        <embed src="<?php echo '../uploads/'.$doctor['qualification_pg_file']; ?>" type="application/pdf" width="90%" height="350px" />
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>License 1 : <u style=""><?php echo $doctor['licence'];?> </u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>License File 1: <a href="<?php echo $doctor['license_file']; ?>" target="_blank"> <?php echo $doctor['license_file']; ?></a> </h4>
                        </div>
                        <embed src="<?php echo '../uploads/'.$doctor['license_file']; ?>" type="application/pdf" width="90%" height="350px" />
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>License 2 : <u style=""><?php echo $doctor['license2'];?> </u></h4>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                          <h4>License File 2: <a href="<?php echo $doctor['license_file2']; ?>" target="_blank"> <?php echo $doctor['license_file2']; ?></a> </h4>
                        </div>
                        <embed src="<?php echo '../uploads/'.$doctor['license_file2']; ?>" type="application/pdf" width="90%" height="350px" />
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label><br>
                            Doctor Approval (Check to approve doctor if verification is done.)<span class="error">*</span></label>
                          </div>
                        </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="checkbox-inline"><br>
                            <input type="checkbox" name="approval" id="approval" value="1" <?php if($doctor['approval_status'] == '1') { echo "checked=checked";}?>><span class="check-radio" ></span>Doctor's Approval status
                            </label>
                        </div>
                      </div>
                    </div>

                    <div class="bttn-group">
                        <a class="btn btn-light" href="doctor-list.php">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    </div>
                    </form>
								</div>
							</div>
						</div>			
					</div>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		
    </body>
</html>