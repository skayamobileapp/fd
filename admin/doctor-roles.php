<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$type1List = array();
$sql   = "SELECT tr.*, hr.id_role, hr.id_doctor_type FROM doctor_type_roles AS tr INNER JOIN doctor_has_roles AS hr ON hr.id_role=tr.id WHERE id_doctor_type='1'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    array_push($type1List, $row);
}

$type2List = array();
$sql   = "SELECT tr.*, hr.id_role, hr.id_doctor_type FROM doctor_type_roles AS tr INNER JOIN doctor_has_roles AS hr ON hr.id_role=tr.id WHERE id_doctor_type='2'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    array_push($type2List, $row);
}

$type3List = array();
$sql   = "SELECT tr.*, hr.id_role, hr.id_doctor_type FROM doctor_type_roles AS tr INNER JOIN doctor_has_roles AS hr ON hr.id_role=tr.id WHERE id_doctor_type='3'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    array_push($type3List, $row);
}

$type4List = array();
$sql   = "SELECT tr.*, hr.id_role, hr.id_doctor_type FROM doctor_type_roles AS tr INNER JOIN doctor_has_roles AS hr ON hr.id_role=tr.id WHERE id_doctor_type='4'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    array_push($type4List, $row);
}

$type5List = array();
$sql   = "SELECT tr.*, hr.id_role, hr.id_doctor_type FROM doctor_type_roles AS tr INNER JOIN doctor_has_roles AS hr ON hr.id_role=tr.id WHERE id_doctor_type='5'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
    array_push($type5List, $row);
}

$sql   = "select * from doctor_type_roles";
$result = $conn->query($sql);
$rolesList = array();
while ($row = $result->fetch_assoc()) {
    array_push($rolesList, $row);
}


if ($_POST) {

  $type1 = $_POST['type1'];
  if ($type1) {
      mysqli_query($conn, "DELETE FROM doctor_has_roles WHERE id_doctor_type='1'");
    for($i=0; $i<count($type1); $i++){
      $dt = $type1[$i]['id_role'];
      $sql = "INSERT INTO doctor_has_roles(id_doctor_type, id_role)VALUES('1', '$dt')";
      $result= mysqli_query($conn,$sql);
    }
  }
  $type2 = $_POST['type2'];
  if ($type2) {
      mysqli_query($conn, "DELETE FROM doctor_has_roles WHERE id_doctor_type='2'");
    for($i=0; $i<count($type2); $i++){
      $dt = $type2[$i]['id_role'];
      $sql = "INSERT INTO doctor_has_roles(id_doctor_type, id_role)VALUES('2', '$dt')";
      mysqli_query($conn,$sql);
    }
  }
  $type3 = $_POST['type3'];
  if ($type3) {
      mysqli_query($conn, "DELETE FROM doctor_has_roles WHERE id_doctor_type='3'");
    for($i=0; $i<count($type3); $i++){
      $dt = $type3[$i]['id_role'];
      $sql = "INSERT INTO doctor_has_roles(id_doctor_type, id_role)VALUES('3', '$dt')";
      mysqli_query($conn,$sql);
    }
  }
  $type4 = $_POST['type4'];
  if ($type4) {
      mysqli_query($conn, "DELETE FROM doctor_has_roles WHERE id_doctor_type='4'");
    for($i=0; $i<count($type4); $i++){
      $dt = $type4[$i]['id_role'];
      $sql = "INSERT INTO doctor_has_roles(id_doctor_type, id_role)VALUES('4', '$dt')";
      mysqli_query($conn,$sql);
    }
  }
  $type5 = $_POST['type5'];
  if ($type5) {
      mysqli_query($conn, "DELETE FROM doctor_has_roles WHERE id_doctor_type='5'");
    for($i=0; $i<count($type5); $i++){
      $dt = $type5[$i]['id_role'];
      $sql = "INSERT INTO doctor_has_roles(id_doctor_type, id_role)VALUES('5', '$dt')";
      mysqli_query($conn,$sql);
    }
  }
              echo "<script>alert('Roles added successfully')</script>";
              echo "<script>parent.location='doctor-roles.php'</script>";
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>First Doctor</title>
		
		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="../fd_logo.png">
		
		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
		
		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="assets/css/feathericon.min.css">
		
		<link rel="stylesheet" href="assets/plugins/morris/morris.css">
		
		<!-- Main CSS -->
        <link rel="stylesheet" href="assets/css/style.css">
		
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('navbar.php'); ?>
			

			<?php include('sidebar.php'); ?>
			
			<!-- Page Wrapper -->
            <div class="page-wrapper">
			
                <div class="content container-fluid">
					
					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col-sm-12">
								<h3 class="page-title">Add Roles to Doctor</h3>
								<ul class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
									<li class="breadcrumb-item active">Doctors Roles</li>
								</ul>
							</div>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
                <section class="col-12">          
	            <div class="content container-fluid">
               
               <!-- <h3>Subscriptions</h3>                -->

                <div class="card col-12">
                  <form action="" method="POST">
                    	<br>
                    <div class="row">
                      <div class="col-sm-2">
                        <h5>Menu / Roles</h5>
                      </div>
                      <div class="col-sm-2">
                        <h5>BASIC</h5>
                      </div>
                      <div class="col-sm-2">
                        <h5>PREMIUM</h5>
                      </div>
                      <div class="col-sm-2">
                        <h5>SUBLIME</h5>
                      </div>
                      <div class="col-sm-2">
                        <h5>ULTRA</h5>
                      </div>
                      <div class="col-sm-2">
                        <h5>PRISTINE</h5>
                      </div>
                    </div>
                    <hr>
                    <?php for($i=0; $i<count($rolesList); $i++){ ?>
                  <div class="row">
                    <div class="col-sm-2">
                        <h5><?php echo ucwords($rolesList[$i]['role_type']); ?></h5>
                    </div>
                      <div class="col-sm-2">
                        <div class="form-group">

                           <label class="checkbox-inline">
                            <input type="checkbox" name="type1[]" id="type1" value="<?php echo $rolesList[$i]['id']; ?>" 
                            <?php for($j=0; $j<count($type1List); $j++) {
                              if ($rolesList[$i]['id']==$type1List[$j]['id_role']) {
                                echo "checked";
                              }
                            }?>
                            >
                            <span class="check-radio" ></span></label>
                            
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                           
                           <label class="checkbox-inline">
                             <input type="checkbox" name="type2[]" id="type2" value="<?php echo $rolesList[$i]['id']; ?>"
                             <?php for ($k=0; $k<count($type2List); $k++) { 
                               if ($rolesList[$i]['id']==$type2List[$k]['id_role']) {
                                 echo "checked";
                               }
                             }?>
                             ><span class="check-radio"></span></label>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">

                           <label class="checkbox-inline">
                             <input type="checkbox" name="type3[]" id="type3" value="<?php echo $rolesList[$i]['id']; ?>"
                             <?php for ($l=0; $l<count($type3List); $l++) { 
                               if ($rolesList[$i]['id']==$type3List[$l]['id_role']) {
                                 echo "checked";
                               }
                             }?>
                             >
                             <span class="check-radio"></span></label>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                             <label class="checkbox-inline">
                             <input type="checkbox" name="type4[]" id="type4" value="<?php echo $rolesList[$i]['id']; ?>"
                             <?php for ($m=0; $m<count($type4List); $m++) { 
                               if ($rolesList[$i]['id']==$type4List[$m]['id_role']) {
                                 echo "checked";
                               }
                             }?>
                              ><span class="check-radio"></span></label>
                        </div>
                      </div>
                      <div class="col-sm-2">
                        <div class="form-group">
                             <label class="checkbox-inline">
                             <input type="checkbox" name="type5[]" id="type5" value="<?php echo $rolesList[$i]['id']; ?>"
                             <?php for ($n=0; $n<count($type5List); $n++) { 
                               if ($rolesList[$i]['id']==$type5List[$n]['id_role']) {
                                 echo "checked";
                               }
                             }?>
                             ><span class="check-radio"></span></label>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                      <div class="bttn-group">
                        <button type="submit" class="btn btn-primary btn-lg pull-right" name="save" id="save">Save</button>
                      </div>
                    </form>
                  </div>
                </div>
        </section>
					
				</div>			
			</div>
			<!-- /Page Wrapper -->
		
        </div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
        <script src="assets/js/jquery-3.2.1.min.js"></script>
		
		<!-- Bootstrap Core JS -->
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slimscroll JS -->
        <script src="assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<script src="assets/plugins/raphael/raphael.min.js"></script>    
		<script src="assets/plugins/morris/morris.min.js"></script>  
		<script src="assets/js/chart.morris.js"></script>
		
		<!-- Custom JS -->
		<script  src="assets/js/script.js"></script>
		<script type="text/javascript">
      function get(){
        var c = $("#type1").val();
        console.log(c);
      }
    </script>
    </body>
</html>