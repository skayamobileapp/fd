<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
// $select ="";
if(isset($_POST['search']))
{

  $doctorName = strtoupper($_POST['doctor_name']);
  $speciality = $_POST['speciality'];
  $city = $_POST['city'];
      $citysql = "";
  if($city=='') {
       $citysql = " AND c.city like '%$city%'";
  }

  $select = "SELECT d.id, d.doctor_name, d.photo, d.email, d.mobile, d.experiance, d.address, s.specialty, c.city FROM doctor_details d INNER JOIN specialties s ON d.specialty=s.id INNER JOIN cities c ON d.city=c.id WHERE s.specialty like '%$speciality%' $citysql";
  $result = mysqli_query($conn, $select);
  $i = 0;
  $view = array();
  while ($row = mysqli_fetch_assoc($result)) {
    $view[$i]['id'] = $row['id'];
    $view[$i]['doctor_name'] = $row['doctor_name'];
    $view[$i]['photo'] = $row['photo'];
    $view[$i]['email'] = $row['email'];
    $view[$i]['address'] = $row['address'];
    $view[$i]['specialty'] = $row['specialty'];
    $view[$i]['experiance'] = $row['experiance'];
    $view[$i]['mobile'] = $row['mobile'];

    $i++;
  }
}
$sql   = "select * from specialties";
$result = $conn->query($sql);
$specialities = array();
while ($row = $result->fetch_assoc()) {
    array_push($specialities, $row);
  }

  $sql = "SELECT id, city from cities order by city";
$result = mysqli_query($conn, $sql);
  $i = 0;
  $cityList = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($cityList, $row);
  }
  $count=0;
$pid = $_SESSION['patient_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Fancybox CSS -->
		<link rel="stylesheet" href="../assets/plugins/fancybox/jquery.fancybox.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('main-sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0">Search Doctor</h4>
                  
								</div>
								<div class="card-body">
								<form action="" method="POST" id="form">
                    <div class="row">
	                  <div class="col-sm-6">
	                    <div class="form-group">
	                          <label class="">Select City<span class="error"></span></label>
	                        <select name="city" id="city" class="form-control selitemIcon">
	                                <option value="">SELECT CITY</option>
	                                <?php
	                                for ($i=0; $i<count($cityList); $i++) {  ?>
	                                <option value="<?php echo $cityList[$i]['id']; ?>" <?php if($city==$cityList[$i]['id']) echo "selected";?> ><?php echo $cityList[$i]['city']; ?></option>
	                                <?php
	                                }
	                                ?>
	                            </select>
	                    </div>
	                  </div>
	                  <div class="col-sm-6">
	                    <div class="form-group">
	                        <label>Select Speciality</label><span class="error"></span>
	                        <select name="speciality" id="speciality" class="form-control selitemIcon">
	                                <option value="">SELECT SPECIALITY</option>
	                                <?php
	                                for($i=0; $i<count($specialities); $i++){ ?>
	                                  <option value="<?php echo $specialities[$i]['specialty']; ?>" <?php if($speciality == $specialities[$i]['specialty']){ echo "selected=selected"; } ?>><?php echo $specialities[$i]['specialty']?></option>
	                                <?php
	                                }

	                                ?>
	                            </select>
	                    </div>
	                  </div>
	              </div>
	              <div class="row">
	                  <div class="col-sm-12">
	                    <div class="form-group">
	                      <label></label><div>
	                        <input type="submit" class="btn btn-primary btn-lg float-right" title="Search" name="search" value="Search">
	                      </div>
                    </div>
                  </div>
                </div>
              </form>
									
								</div>
							</div>


					<?php for($i=0; $i<count($view); $i++){
                            $roleid = $view[$i]['id'];?>
							<!-- Doctor Widget -->
							<div class="card">
								<div class="card-body">
									<div class="doctor-widget">
										<div class="doc-info-left">
											<div class="doctor-img">
												<a href="#">
													<img src="../uploads/<?php echo $view[$i]['photo']; ?>" class="img-fluid" alt="">
												</a>
											</div>
											<div class="doc-info-cont">
												<h4 class="doc-name"><a href="#"><?php echo ucwords($view[$i]['doctor_name']); ?></a></h4>
												<p class="doc-speciality"><?php echo ucfirst($view[$i]['address']); ?></p>
												<h5><font class="doc-department"><?php echo ucwords($view[$i]['specialty']); ?></font>
												 <br>
													<span>Experiance : <?php echo $view[$i]['experiance']; ?>  Years</span> </h5>
												<div class="rating">
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star"></i>
													<span class="d-inline-block average-rating">(17)</span>
												</div>
												<div class="clinic-details">
													<p class="doc-location"><i class="fas fa-map-marker-alt"></i> India</p>
												</div>
											</div>
										</div>
										<div class="doc-info-right">
											<div class="clinic-booking">
												<i class="fas fa-mobile"></i> :
												<span> <?php echo $view[$i]['mobile']; ?> </span>
												<br>
												<i class="fa fa-envelope"></i> :
												<span> <?php echo $view[$i]['email']; ?> </span>
<!-- 												<a class="view-pro-btn" href="doctor-profile.html">View Profile</a> --> <br><br>
												<a class="apt-btn" href="book-appointment.php?id=<?php echo $roleid; ?>">Book Appointment</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
							<!-- /Doctor Widget -->

						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Datetimepicker JS -->
		<script src="../assets/js/moment.min.js"></script>
		<script src="../assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Fancybox JS -->
		<script src="../assets/plugins/fancybox/jquery.fancybox.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../select2/js/select2.js" ></script>
	    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>