<?php
include('../connection/conn.php');
include('session_check.php');

$pid = $_SESSION['patient_details']['id'];

$sql   = "SELECT a.* FROM doctor_details as a , events as b where a.id=b.doctor_id and b.patient_id='$pid' group by a.id";
          $result = $conn->query($sql);
          $doctorList = array();
          while ($row = $result->fetch_assoc()) {
          array_push($doctorList, $row);
        }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <?php include('navbar.php'); ?>
    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('sidebar.php'); ?>
        
        <section class="col-sm-8 col-lg-9">
            <div class="main-container"> 
            <!-- <form action="" method="" id="myform"> -->
               <h3 class="clearfix">Chat With Doctor</h3>       
                <div class="card">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group fg-float">
                          <input type="text" name="search" class="form-control user-search" placeholder="Search" id="search" onkeyup="getpatient()" autocomplete="off">
                          </div>
                          <div class="users-list">                          
                          <div id="result">
                            <?php
                            for($i=0; $i<count($doctorList); $i++)
                            {
                              $count1=0;
                              $notesql= "SELECT * FROM messages where patient_id='$pid' AND sent_by='doctor' AND patient_flag='0' AND doctor_id='".$doctorList[$i]['id']."' ";
                                $result=mysqli_query($conn, $notesql);
                                $count1=mysqli_num_rows($result);
                              ?>

                              <div class="chat-user-profile" onclick="get_prev(this);" id="<?php echo $doctorList[$i]['id']; ?>" >
                              <span id="notification-count" class="notification-count">
                                <?php if($count1>0) { echo $count1; } ?></span>
                                <div class="user-image">
                                  <?php if(empty($doctorList[$i]['photo'])){?>
                                  <img src="https://ui-avatars.com/api/?background=0D8ABC&color=fff&name=<?php echo $doctorList[$i]['doctor_name']; ?>">
                                <?php } else { ?>
                                  <img src="../uploads/<?php echo $doctorList[$i]['photo']; ?>">
                                  <?php
                                }
                                ?>
                                </div>
                                <div class="user-name">Dr. <?php echo strtoupper($doctorList[$i]['doctor_name']); ?></div>
                                <div><?php if(empty($doctorList[$i]['mobile'])){ echo "N/A";} else { echo strtoupper($doctorList[$i]['mobile']); } ?>
                                  <i style="color: green;"><?php echo $doctorList[$i]['activity']; ?></i>
                                </div>
                              </div>
                              <?php
                            }
                            ?>
                                <input type="hidden" name="doctor_id" id="doctor_id" value="" required>
                        </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="message-container">
                          <h3 class="clearfix"></h3>
                          <div class='scrollbar' id='style-5'>
                            <div  id="previous"></div>
                          </div>
                        </div>
                        <div class="message-form">
                        <div class="form-group fg-float">
                          <div class="fg-line">
                            <input type="text" class="form-control" name="answer" id="answer" placeholder="Type a message..." autocomplete="off" required/>
                          </div>
                        </div>
                        <div class="bttn-group">
                          <button class="btn btn-primary btn-lg" name="save" id="save">Send</button>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
            <!-- </form> -->
                </div>
        </section>
      </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/main.js"></script>
  
        <script type="text/javascript">

        function get_prev(id){
          var did = $(id).attr("id");
          var pid = '<?php echo $pid; ?>';
          console.log(did);
          console.log(pid);
          $("#doctor_id").val(did);

          $.ajax({
            url: 'get_previous.php',
            data:{
                  'did': did,
                  'pid': pid,
        },
            success: function(result){
            $("#previous").html(result);
          }
        });
        }
      </script>
      <!-- <script type="text/javascript">
    
        function getpatient(){
          var find = $('input').val();
          console.log(find);

          $.ajax({
            url: 'get_patients.php',
            data:{
                  'find': find,
        },
            success: function(result){
            $("#result").html(result);
          }
        });
        }
    </script> -->
    <script type="text/javascript">
    $("#save").on('click',function(){
    var aid = $('#answer').val();
    var pid = '<?php echo $pid; ?>';
    var did = $("#doctor_id").val();
    console.log(did);
    console.log(pid);
    console.log(aid);
      $.ajax({
        // type: 'POST',
        url: 'send_message.php',
        data:{
          'aid': aid,
          'did': did,
          'pid': pid,
        },
        success: function(result){
        $("#previous").html(result);
        $('#answer').val(' ');
      }
        });
      });
    </script>

      <script src="../select2/js/select2.js" ></script>
          <script src="../select2/js/select2-init.js" ></script>
  </body>
</html>