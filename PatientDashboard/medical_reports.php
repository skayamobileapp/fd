<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$eventId = $_GET['id'];

$select1 = mysqli_query($conn,"SELECT p.id, p.doctor_name, e.id, e.title, e.start FROM doctor_details AS p INNER JOIN events AS e ON e.doctor_id = p.id WHERE e.id='$eventId' ");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $doctor_name = $row1['doctor_name'];
  $doctorId = $row1['doctor_id'];
  }
$prescrip = mysqli_query($conn,"SELECT d.type, p.drug_name, p.duration, p.repeat_same, p.time_of_the_day, p.to_be_taken, p.patient_id, p.appointment_on FROM prescrip AS p INNER JOIN drug_type AS d ON p.drug_type=d.id WHERE p.event_id='$eventId'");
  $i=0;
  while ($row = mysqli_fetch_assoc($prescrip)) {
    $fetch[$i]['type'] = $row['type'];
    $fetch[$i]['drug_name'] = $row['drug_name'];
    $fetch[$i]['duration'] = $row['duration'];
    $fetch[$i]['repeat_same'] = $row['repeat_same'];
    $fetch[$i]['time_of_the_day'] = $row['time_of_the_day'];
    $fetch[$i]['to_be_taken'] = $row['to_be_taken'];
    $fetch[$i]['appointment_on'] = $row['appointment_on'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $lab = mysqli_query($conn,"SELECT lds.lab_name, ltn.test_name, lds.mobile, lds.email FROM labdetails ld INNER JOIN lab_details lds ON ld.lab_name=lds.id INNER JOIN lab_test_name ltn ON ld.test_name=ltn.id WHERE ld.event_id = '$eventId' ");

  $i=0;
  while ($row = mysqli_fetch_assoc($lab)) {
    $Lab[$i]['lab_name'] = $row['lab_name'];
    $Lab[$i]['test_name'] = $row['test_name'];
    $Lab[$i]['date'] = $row['date'];
    $Lab[$i]['email'] = $row['email'];
    $Lab[$i]['mobile'] = $row['mobile'];
    $i++;
  }

  $allergy = mysqli_query($conn,"SELECT a.*, atype.allergy_type AS altype, alist.allergy_name AS alname FROM allergies a INNER JOIN allergy_types atype ON atype.id=a.allergy_type INNER JOIN allergy_list alist ON a.allergy_name=alist.id WHERE a.event_id = '$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($allergy)) {
    $Allergy[$i]['altype'] = $row['altype'];
    $Allergy[$i]['alname'] = $row['alname'];
    $Allergy[$i]['date'] = $row['date'];
    $Allergy[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $vital = mysqli_query($conn,"SELECT * FROM vitals WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($vital)) {
    $vitals[$i]['height'] = $row['height'];
    $vitals[$i]['weight'] = $row['weight'];
    $vitals[$i]['bp'] = $row['bp'];
    $vitals[$i]['sugar'] = $row['sugar'];
    $vitals[$i]['blood'] = $row['blood'];
    $vitals[$i]['bmi'] = $row['bmi'];
    $i++;
  }

  $file = mysqli_query($conn,"SELECT * FROM fileUpload WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($file)) {
    $files[$i]['file_name'] = $row['file_name'];
    $files[$i]['uploaded_file'] = $row['uploaded_file'];
    $i++;
  }

  $referDoc = mysqli_query($conn,"SELECT d.doctor_name, d.mobile, d.email FROM doctor_details AS d INNER JOIN refer_table AS rt ON d.id=rt.refer_to WHERE event_id ='$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($referDoc)) {
    $Refer[$i]['doctor_name'] = $row['doctor_name'];
    $Refer[$i]['mobile'] = $row['mobile'];
    $Refer[$i]['email'] = $row['email'];
    $i++;
  }

  
  $complaint = mysqli_query($conn,"SELECT * FROM complaints WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($complaint)) {
    $complaints[$i]['complaint'] = $row['complaint'];
    $i++;
  }

  $observe = mysqli_query($conn,"SELECT * FROM observations WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($observe)) {
    $observes[$i]['obervation'] = $row['obervation'];
    $i++;
  }

  $diagnose = mysqli_query($conn,"SELECT * FROM diagnosis WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($diagnose)) {
    $diagnosis[$i]['diagnosis'] = $row['diagnosis'];
    $i++;
  }

  $appointment = mysqli_query($conn,"SELECT * FROM prescriptions WHERE event_id='$eventId' ");
  while ($row = mysqli_fetch_assoc($appointment)) {
    $appoint_date = $row['appoint_date'];
    $nxt_appoint = $row['nxt_appoint'];
    $desc = $row['more_prescrip'];
  }
$count=0;
$pid = $_SESSION['patient_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>

<body>     
    <nav class="navbar navbar-default dashboard-navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          
          <a class="navbar-brand" href="#">First Doctor</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right main-nav">
            <li><a href="index.php" class="dashboard ">Dashboard</a></li>
            <li><a href="card_renewal.php" class="precaution">Card Renewal</a></li>
            <li><a href="reports.php" class="medical-report active">Medical Reports</a></li>
             <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Find Providers <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="find_lab.php">Diagnostic Labs</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="find_pharma.php">Pharmacy Stores</a></li>
                </ul>                
            </li>
            <li><a href="find_doctor.php" class="find-doctor">Find Doctor</a></li>
            <li><a href="view_notifications.php" class="notifications-link"><sup id="notification-count"><?php if($count>0) { echo $count; } ?></sup> </a></li>
                    <li><a href="../index.php">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('sidebar.php'); ?>
        <section class="col-sm-8 col-lg-9">
          <div class="main-container">
           <h3 class="clearfix">Prescription details <a href="reports.php" class="btn btn-primary pull-right btn-lg">Back</a></h3>
           <div class="patient-summary-row">
            <div class="patient-summary-head">
              <h4><?php echo "Doctor Name : ". strtoupper($doctor_name); ?><a href="prescriptions_pdf.php?id=<?php echo $eventId; ?>" class="pull-right">Download Prescriptions</a></h4>
            </div>
              <div class="patient-summary-container">
                <div class="card">
                 <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item active">
                    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Prescriptions</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Lab</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#references" role="tab" data-toggle="tab">Allergies</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#references1" role="tab" data-toggle="tab">Vitals</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#references2" role="tab" data-toggle="tab">Files</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#refer" role="tab" data-toggle="tab">Refer doctor</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#complaints" role="tab" data-toggle="tab">Complaints</a>
                  </li><li class="nav-item">
                    <a class="nav-link" href="#observations" role="tab" data-toggle="tab">Observations</a>
                  </li><li class="nav-item">
                    <a class="nav-link" href="#diagnosis" role="tab" data-toggle="tab">Diagnosis</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="profile">
                  <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Drug Type</th>
                            <th>Drug Name</th>
                            <th>Duration in Days</th>
                            <th>Repeat</th>
                            <th>Timings</th>
                            <th>Take</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($fetch); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo $fetch[$i]['type']; ?></td>
                            <td><?php echo $fetch[$i]['drug_name']; ?></td>
                            <td><?php echo $fetch[$i]['duration']; ?></td>
                            <td><?php echo ucfirst($fetch[$i]['repeat_same']); ?></td>
                            <td><?php echo ucfirst($fetch[$i]['time_of_the_day']); ?></td>
                            <td><?php echo ucfirst($fetch[$i]['to_be_taken']); ?></td>
                          </tr>
                          <?php }
                          ?>
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                    <div class="col-sm-10">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <label>Notes by Dr. <?php echo " ". strtoupper($doctor_name); ?></label>
                          <p><?php echo $desc; ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="buzz">
                    <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Lab Name</th>
                            <th>Test Name</th>
                            <th>Contact Number</th>
                            <th>Email Id</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($Lab); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo strtoupper($Lab[$i]['lab_name']); ?></td>
                            <td><?php echo strtoupper($Lab[$i]['test_name']); ?></td>
                            <td><a href="tel:<?php echo $Lab[$i]['mobile']; ?>"><?php echo $Lab[$i]['mobile']; ?></a></td>
                            <td><a href="email:<?php echo $Lab[$i]['email']; ?>"><?php echo strtoupper($Lab[$i]['email']); ?></a></td>
                          </tr>
                          <?php }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="references">
                    <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Allergy Type</th>
                            <th>Allergy Name</th>

                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($Allergy); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo $Allergy[$i]['altype']; ?></td>
                            <td><?php echo $Allergy[$i]['alname']; ?></td>
                          </tr>
                          <?php }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="references1">
                    <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>Height in cm</th>
                            <th>Weight in Kg's</th>
                            <th>BP</th>
                            <th>Sugar</th>
                            <th>Blood</th>
                            <th>BMI Calculation</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($vitals); $i++) {
                        ?>
                          <tr>
                            <td><?php echo $vitals[$i]['height']; ?> cm</td>
                            <td><?php echo $vitals[$i]['weight']; ?> kg</td>
                            <td><?php echo $vitals[$i]['bp']; ?></td>
                            <td><?php echo $vitals[$i]['sugar']; ?></td>
                            <td><?php echo $vitals[$i]['blood']; ?></td>
                            <td><?php echo $vitals[$i]['bmi']; ?></td>
                          </tr>
                          <?php }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="references2">
                    <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>File Name</th>
                            <th>File</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($files); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo ucfirst($files[$i]['file_name']); ?></td>
                            <td><a href="../uploads/<?php echo $files[$i]['uploaded_file']; ?>" target="_blank"><?php echo $files[$i]['uploaded_file']; ?></a></td>
                          </tr>
                          <?php }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="refer">
                    <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Doctor Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($Refer); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo strtoupper($Refer[$i]['doctor_name']); ?></td>
                            <td><a href="tel:<?php echo $Refer[$i]['mobile']; ?>"><?php echo $Refer[$i]['mobile']; ?></a></td>
                            <td><a href="email:<?php echo $Refer[$i]['email']; ?>"><?php echo strtoupper($Refer[$i]['email']); ?></a></td>
                          </tr>
                          <?php }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="complaints">
                   <h3> </h3>
                          <?php
                        for($i=0; $i<count($complaints); $i++) {

                         echo $complaints[$i]['complaint'];
                        }
                          ?>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="observations">
                   <h3> </h3>
                          <?php
                        for($i=0; $i<count($observes); $i++) {

                         echo $observes[$i]['obervation'];
                        }
                          ?>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="diagnosis">
                   <h3> </h3>
                          <?php
                        for($i=0; $i<count($diagnosis); $i++) {

                         echo $diagnosis[$i]['diagnosis'];
                        }
                          ?>
                  </div>
                  <form action="" method="post" id="form" enctype="multipart/form-data">

                  
                      
                <!-- <div class="bttn-group">
                    <a href="index.php" class="btn btn-link">Cancel</a>
                    <button class="btn btn-primary btn-lg" name="save" id="save">Save</button>
                </div> -->
            </form>
                </div>
              </div>               
           </div>
         </div>
       </div>
   </section>
      </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>      
</body>

</html>