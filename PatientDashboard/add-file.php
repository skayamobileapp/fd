<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$pid = $_SESSION['patient_details']['id'];

$fileDetails=[];

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from patient_files where id='$id' ";
    $result   = $conn->query($sql);
    $fileDetails = $result->fetch_assoc();
   }

if($_POST)
{
      $file_type = $_POST['file_type'];
      $created_date = date('Y-m-d',strtotime($_POST['created_date']));
      $notes = $_POST['notes'];

      $Profile = $_FILES['photo']['name'];
      $img_tmp =$_FILES['photo']['tmp_name'];
      move_uploaded_file($img_tmp,"../uploads/".$Profile);

      $sql = "INSERT INTO patient_files (file_type, created_date, notes, file, id_patient)VALUES('$file_type', '$created_date', '$notes', '$Profile','$pid')";

    $insert = mysqli_query($conn,$sql);
    if ($insert)
      {
        echo "<script>alert('File Added successfully');</script>";
        echo "<script>parent.location='my-files.php'</script>";
        exit;
      }

  if($_GET['id'])
    {
      $id = $_GET['id'];
      $file_type = $_POST['file_type'];
      $created_date = date('Y-m-d',strtotime($_POST['created_date']));
      $notes = $_POST['notes'];

      $Profile = $_FILES['photo']['name'];
      if ($Profile==" ") {
        $Profile = $fileDetails['photo'];
      }
      else
      {
        $Profile = $_FILES['photo']['name'];
        $img_tmp =$_FILES['photo']['tmp_name'];
        move_uploaded_file($img_tmp,"../uploads/".$Profile);
      }

      $sql = "UPDATE patient_files SET file_type='$file_type', created_date='$created_date', notes='$notes', file='$Profile' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('File Updated successfully');</script>";
        echo "<script>parent.location='my-files.php'</script>";exit;
      }
    }
}

if($fileDetails['created_date']=='') {
$fileDetails['created_date'] = date('Y-m-d');
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('main-sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="my-files.php">My Files</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?php if (!empty($fileDetails['id'])) { echo "Edit";} else { echo "Add"; } ?> File</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php if($fileDetails['id']){echo "Edit"; } else{ echo "Add"; }?> File</h4>
									<div class="row form-row">
										
										<div class="col-md-6">
											<div class="form-group">
												<label>File Type <span class="text-danger">*</span></label>
                        <select name='file_type' id='file_type' class='form-control selitemIcon' >
                             <option value='Medicine' <?php if($fileDetails['file_type']=='Medicine'){ echo "selected=selected";}?> >Medicine</option>

                             <option value='Prescription' <?php if($fileDetails['file_type']=='Prescription'){ echo "selected=selected";}?> >Prescription</option>
                             <option value='Lab Report' <?php if($fileDetails['file_type']=='Lab Report'){ echo "selected=selected";}?> >Lab Report</option>
                             <option value='Scanning File' <?php if($fileDetails['file_type']=='Scanning File'){ echo "selected=selected";}?> >Scanning File</option>
                             <option value='Others' <?php if($fileDetails['file_type']=='Others'){ echo "selected=selected";}?> >Others</option>
                          </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label> Date <span class="text-danger">*</span></label>
												<input type="text" class="form-control datetimepicker" name="created_date" id="created_date" maxlength="20" autocomplete="off" value="<?php echo $fileDetails['created_date']; ?>" >
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Notes <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="notes" autocomplete="off" value="<?php echo $fileDetails['notes']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Upload File <span class="text-danger">*</span></label>
												<input type="file" class="form-control" name="photo">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="my-files.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Dropzone JS -->
		<script src="../assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {
              file_type : "required",
              created_date : "required",
              notes : "required"
         
            },
            messages : {
               
                file_type : "<span> select File type</span>",
                created_date : "<span> select date</span>",
                notes : "<span> enter notes</span>"
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

		 <script type="text/javascript">
$( document ).ready(function() {

	 var idstate = '<?php  echo $clinicDetails['state_id'];?>';
	 if(idstate!='' && idstate!= null) {
		getCities();
	 }
    // getCities();
    // getspecialty();
});

        function getCities(){
          var id = $("#state_id").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#city_id").html(result);
              var idcityselected = '<?php  echo $clinicDetails['city_id'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#city_id").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

       

        function getPincode(){
          var id = $("#city_id").val();
          console.log(id);

          $.ajax({url: "getpincode.php?id="+id, success: function(result){
            $("#pincode").html(result);

             var pincodeselected = '<?php  echo $clinicDetails['pincode'];?>';
              if(pincodeselected!='' && pincodeselected!= null) {
                 $("#pincode").val(pincodeselected);
              }
          }
        });
          
        }
    </script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>