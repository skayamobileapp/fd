<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$DoctorId = $_GET['id'];

$pid = $_SESSION['patient_details']['id'];

// $doctor_data = [];
$sql = "select d.*, s.name from doctor_details as d INNER JOIN sub_specialties as s on d.sub_specialty=s.id where d.id ='$DoctorId'";
$result = $conn->query($sql);
$doctor_data = $result->fetch_assoc();

// $id = $_SESSION['patient_details']['id'];
// $name = $_SESSION['patient_details']['patient_name'];
// $mobile = $_SESSION['patient_details']['mobile_number'];
// $id = $_SESSION['patient_details']['id'];


$select = mysqli_query($conn,"SELECT * FROM patient_details WHERE id = '$pid'");
if ($row = mysqli_num_rows($select)) 
{
    while ($row = mysqli_fetch_array($select))
    {
        //from database fields
        $fname = $row['first_name'];
        $mname = $row['middle_name'];
        $lname = $row['last_name'];
        $fullname = $fname." ".$mname." ".$lname;
        $phone = $row['patient_phone_number'];
        $mobile_number = $row['mobile_number']; 
        $DateOfBirth = $row['date_of_birth'];
        $email = $row['email'];
    }
}
// if ($_POST) {

//   $DoctorId = $_GET['id'];
//   $id = $_SESSION['patient_details']['id'];
//   $name = $_SESSION['patient_details']['patient_name'];
//   $mobile = $_SESSION['patient_details']['mobile_number'];
//   $purpose = $_POST['purpose'];
//   $adate = $_POST['date'];
//   $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, patient_name, mobile, height, weight, bp, sugar, bmi, status) VALUES('$purpose', '$adate', '$adate', '$id', '$name', '$mobile','NA','NA','NA','NA', 'NA', '0')";
//   $result = $conn->query($sql);
//   if ($result) {
//     echo "<script>alert('Appointment Booking successfull')</script>";
//     echo "<script>parent.location='find_doctor.php'</script>";
//   }
// }


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor | Patient</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    
    <link href="../css/jquery.datetimepicker.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../css/jquery-ui.css" rel="stylesheet">
    <link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>

<body>
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">

      <div class="row">
        <?php include('sidebar.php'); ?>
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Book Appointment</h3>
               <div class="card" style="width: 99%;">
                   <div class="row">
                  <div class="col-sm-12">
                <div class="category-container">
                    <div class="category-header-profile">
                        <img src="../uploads/<?php echo $doctor_data['photo']; ?>" alt=""/>
                        <h4><a href="#">Dr. <?php echo ucfirst($doctor_data['first_name'])." ".ucfirst($doctor_data['middle_name'])." ".ucfirst($doctor_data['last_name']); ?></a></h4>
                        <p class="grey-text"><strong><?php echo strtoupper($doctor_data['qualification']); ?></strong></p>                     
                    </div>  
                    <div class="clearfix info">
                        <p class="pull-left">Specialty: <strong class="black-text"><?php echo $doctor_data['name']; ?></strong></p>
                        <p class="pull-right">Exp: <strong class="black-text"><?php echo $doctor_data['experiance']; ?>&nbsp Years</strong></p>
                    </div>      
                    <p class="icon location">Clinic Address: <strong class="black-text"><?php echo $doctor_data['clinic_address'] ?></strong></p>
                    <p class="icon location">Address: <strong class="black-text"><?php echo $doctor_data['address'] ?></strong></p>
                    <p>&#9742; call: <a href="tel:<?php echo $doctor_data['mobile']; ?>"><?php echo $doctor_data['mobile'] ?></a></p>  
                    <p class="icon email"><a href="mailto:<?php echo $doctor_data['email']; ?>"><?php echo $doctor_data['email']; ?></a></p>        
                </div>
            </div>
        </div>
        <?php
       
          if ($_POST) {

                $DoctorId = $_GET['id'];
                $purpose = $_POST['purpose'];
                $slotid = $_POST['eventid'];
                $slot_query= "SELECT * FROM timeslots WHERE id='$slotid' ORDER BY date";
                $slot_result = $conn->query($slot_query);
                $row = $slot_result->fetch_assoc();
                $date = $row['date'];
                $time = $row['time'];
                $slot_id = $row['id'];
                $id_clinic = $row['id_clinic'];
                $datetime  = $date." ".$time;
                $etime = strtotime($row['time']);
                // $startTime = date("H:i", strtotime('-30 minutes', $etime));
                $endTime = date("H:i", strtotime('+30 minutes', $etime));
                $enddatetime = $date." ".$endTime;
                $patientId = $_SESSION['patient_details']['id'];
                $name = $_SESSION['patient_details']['patient_name'];
                $mobile = $_SESSION['patient_details']['mobile_number'];

                $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, id_time_slots, id_clinic, patient_name, mobile, height, weight, bp, sugar, bmi, status) VALUES('$purpose', '$datetime', '$enddatetime', '$DoctorId', '$patientId', '$slot_id', '$id_clinic', '$name', '$mobile','NA','NA','NA','NA', 'NA', '0')";
                $result = $conn->query($sql);
                if ($result) {
                  mysqli_query($conn,"UPDATE timeslots SET slot='n' WHERE id='$slotid' ");
                  echo "<script>alert('Appointment Booked On '+ '".date("d M Y h:i:a", strtotime($datetime))."')</script>";
                  // echo "<script>parent.location='find_doctor.php'</script>";
                }
              }
               // $slots = mysqli_query($conn, "SELECT * FROM timeslots WHERE doctor_id='$DoctorId' AND slot='y' AND date>=CURRENT_DATE AND time>CURRENT_TIME");
$slots = mysqli_query($conn, "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$DoctorId' AND t.slot='y' AND c.flag='1' AND CONCAT(t.date,' ',t.time)>=CURRENT_TIMESTAMP order by t.id ASC");

        $i = 0;
          $slotList = array();
          while ($row = mysqli_fetch_assoc($slots)) {
            $slotList[$i]['id'] = $row['id'];
            $slotList[$i]['date'] = $row['date'];
            $slotList[$i]['time'] = $row['time'];
            $slotList[$i]['clinic_name'] = $row['clinic_name'];
            $slotList[$i]['slot'] = $row['slot'];
            $i++;
          }

        ?>
<hr/>
          <form id="target" action="" method="POST">
            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group fg-float">
                    <div class="fg-line">
                      <input type="text" name="purpose" class="form-control" id="purpose" autocomplete="off" required>
                      <label class="fg-label">Appointment Reason<span class="error"> *</span></label>
                    </div>
                  </div>
                </div>
            </div>
         
          <h3><?php if($slotList){ echo "Avalailable slots : "; } else{ echo "No Slots Avalailable";} ?></h3>
              <div class="row">
                <div class='table-responsive theme-table v-align-top'>
                <table class="table">
        <?php for($i=0; $i<count($slotList); $i++){ ?>
                  <tr>
                    <td style="text-align: center;">
                      <?php echo date('d-M-Y',strtotime($slotList[$i]['date'])); ?>
                         <?php echo ' '.$slotList[$i]['time']; ?>
                    </td>
                    <td style="text-align: center;">
                         <?php echo $slotList[$i]['clinic_name']; ?>
                    </td>
                    <td style="text-align: center;">
                      <button type="button" class="btn btn-primary btn-lg"  onclick="selectedid(<?php echo $slotList[$i]['id']; ?>)" id='book'>Book</button>
                    </td>
                  </tr>
              <?php
                  }
              ?>
                </table>
              </div>
              </div>
                 <input type='hidden' name='eventid' id="eventid" value='' />

            </form>
          </div>
        </div>
      </section>
    </div>
  </div>
   <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>

<script type="text/javascript">

function selectedid(id) {
  $("#eventid").val(id);
  $("#target").validate({
    rules : {
        purpose : "required"
      },
    messages : {
        purpose : "<span> ENTER APPOINTMENT REASON</span>"
      }
  });
  var conf = confirm('ARE YOU SURE FOR THIS APPOINTMENT');
   if(conf==true){
    $("#target").submit();
  }
}
</script>

</body>

</html>