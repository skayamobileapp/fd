<?php
include('../connection/conn.php');
error_reporting(0);
$eventId = $_GET['id'];

 $select1 = mysqli_query($conn,"SELECT p.id, p.doctor_name, e.id, e.title, e.start FROM doctor_details AS p INNER JOIN events AS e ON e.doctor_id = p.id WHERE e.id='$eventId' ");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $doctor_name = ucwords($row1['doctor_name']);
  $eventdate = date("M-d Y H:i:a", strtotime($row1['start']));
  $reason = ucfirst($row1['title']);
  $doctorId = $row1['doctor_id'];
  }
$prescrip = mysqli_query($conn,"SELECT d.type, p.drug_name, p.duration, p.repeat_same, p.time_of_the_day, p.to_be_taken, p.patient_id, p.appointment_on FROM prescrip AS p INNER JOIN drug_type AS d ON p.drug_type=d.id WHERE p.event_id='$eventId'");
  $i=0;
  while ($row = mysqli_fetch_assoc($prescrip)) {
    $fetch[$i]['type'] = $row['type'];
    $fetch[$i]['drug_name'] = $row['drug_name'];
    $fetch[$i]['duration'] = $row['duration'];
    $fetch[$i]['repeat_same'] = $row['repeat_same'];
    $fetch[$i]['time_of_the_day'] = $row['time_of_the_day'];
    $fetch[$i]['to_be_taken'] = $row['to_be_taken'];
    $fetch[$i]['appointment_on'] = $row['appointment_on'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $lab = mysqli_query($conn,"SELECT lds.lab_name, ltn.test_name, lds.mobile, lds.email FROM labdetails ld INNER JOIN lab_details lds ON ld.lab_name=lds.id INNER JOIN lab_test_name ltn ON ld.test_name=ltn.id WHERE ld.event_id = '$eventId' ");

  $i=0;
  while ($row = mysqli_fetch_assoc($lab)) {
    $Lab[$i]['lab_name'] = $row['lab_name'];
    $Lab[$i]['test_name'] = $row['test_name'];
    $Lab[$i]['date'] = $row['date'];
    $Lab[$i]['email'] = $row['email'];
    $Lab[$i]['mobile'] = $row['mobile'];
    $i++;
  }

  $allergy = mysqli_query($conn,"SELECT a.*, atype.allergy_type AS altype, alist.allergy_name AS alname FROM allergies a INNER JOIN allergy_types atype ON atype.id=a.allergy_type INNER JOIN allergy_list alist ON a.allergy_name=alist.id WHERE a.event_id = '$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($allergy)) {
    $Allergy[$i]['altype'] = $row['altype'];
    $Allergy[$i]['alname'] = $row['alname'];
    $Allergy[$i]['date'] = $row['date'];
    $Allergy[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $vital = mysqli_query($conn,"SELECT * FROM vitals WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($vital)) {
    $vitals[$i]['height'] = $row['height'];
    $vitals[$i]['weight'] = $row['weight'];
    $vitals[$i]['bp'] = $row['bp'];
    $vitals[$i]['sugar'] = $row['sugar'];
    $vitals[$i]['blood'] = $row['blood'];
    $vitals[$i]['bmi'] = $row['bmi'];
    $i++;
  }

  $file = mysqli_query($conn,"SELECT * FROM fileUpload WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($file)) {
    $files[$i]['file_name'] = $row['file_name'];
    $files[$i]['uploaded_file'] = $row['uploaded_file'];
    $i++;
  }

  $referDoc = mysqli_query($conn,"SELECT d.doctor_name, d.mobile, d.email FROM doctor_details AS d INNER JOIN refer_table AS rt ON d.id=rt.refer_to WHERE event_id ='$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($referDoc)) {
    $Refer[$i]['doctor_name'] = $row['doctor_name'];
    $Refer[$i]['mobile'] = $row['mobile'];
    $Refer[$i]['email'] = $row['email'];
    $i++;
  }

  
  $complaint = mysqli_query($conn,"SELECT * FROM complaints WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($complaint)) {
    $complaints[$i]['complaint'] = $row['complaint'];
    $i++;
  }

  $observe = mysqli_query($conn,"SELECT * FROM observations WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($observe)) {
    $observes[$i]['obervation'] = $row['obervation'];
    $i++;
  }

  $diagnose = mysqli_query($conn,"SELECT * FROM diagnosis WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($diagnose)) {
    $diagnosis[$i]['diagnosis'] = $row['diagnosis'];
    $i++;
  }

  $appointment = mysqli_query($conn,"SELECT * FROM prescriptions WHERE event_id='$eventId' ");
  while ($row = mysqli_fetch_assoc($appointment)) {
    $appoint_date = $row['appoint_date'];
    $nxt_appoint = $row['nxt_appoint'];
    $desc = $row['more_prescrip'];
  }

$currentDate = date('d-m-Y');
        $fromDate = $from_date;
        $medicalReason = $med_desc;
        $currentTime = date('h:i:s a');
        
        $signature = "../uploads/".$dlogo;

        // <font size='2'></font>
        $name = $patient_name;

        $file_data = $file_data ."
    <table width='100%'>
      <tr>
        <td width='100%' style='text-align: center'><font color='blue' size='5'><b><u>PRESCRIPTIONS</u></font></td>
      </tr>
      <tr>
        <td width='100%'><font color='black' size='4'><br><br>
        FROM : $doctor_name
        <br>
        DATE : $eventdate
        </font></td>
      </tr>
  </table>
  <br>

<table width='100%'>
  <tr>
    <td width='90%' style='text-align: center'><font size='4'><u>Appointment Reason :</u><p style='text-align: justify'>$reason
    </p></font></td>
  </tr>
  <tr>
    <td width='20%' colspan = '3'><br><font color='blue' size='4'>Drug List :</font><br></b></td>
  </tr>
</table>
<table width='100%'>
  <tr>
    <th width='10%'>Drug Type</th>
    <th width='40%'>Drug Name</th>
    <th width='5%'>Duration in Days</th>
    <th width='15%'>Repeat</th>
    <th width='20%'>Timings</th>
    <th width='10%'>Take</th>
  </tr>";
    for($i=0; $i<count($fetch); $i++) {
      $dtype = $fetch[$i]['type'];
      $dname = $fetch[$i]['drug_name'];
      $duration = $fetch[$i]['duration'];
      $repeat = ucfirst($fetch[$i]['repeat_same']);
      $time = ucwords($fetch[$i]['time_of_the_day']);
      $take = ucfirst($fetch[$i]['to_be_taken']);
  $file_data = $file_data .
  "<tr>
        <td width='10%'>$dtype</td>
        <td width='40%'>$dname</td>
        <td width='5%'>$duration</td>
        <td width='15%'>$repeat</td>
        <td width='20%'>$time</td>
        <td width='10%'>$take</td>
      </tr>";
   }
   $file_data = $file_data ."
</table>
<table width='100%'>
  <tr>
    <td width='100%'><br><font color='blue' size='4'>Recommanded Labs For Tests :</font><br></b></td>
  </tr>
</table>
<table width='100%'>
  <tr>
    <th width='30%'>Lab Name</th>
    <th width='40%'>Test Name</th>
    <th width='15%'>Contact Number</th>
    <th width='15%'>Email Id</th>
  </tr>";
    for($i=0; $i<count($Lab); $i++) {
      $lname = ucwords($Lab[$i]['lab_name']);
      $ltest = ucwords($Lab[$i]['test_name']);
      $lmobile = $Lab[$i]['mobile'];
      $lemail = strtolower($Lab[$i]['email']);
  $file_data = $file_data .
  "<tr>
        <td width='30%'>$lname</td>
        <td width='40%'>$ltest</td>
        <td width='15%'>$lmobile</td>
        <td width='15%'>$lemail</td>
      </tr>";
   }
   $file_data = $file_data ."
</table>
<table width='100%'>
  <tr>
    <td width='100%'><br><font color='blue' size='4'>Allergies :</font><br></b></td>
  </tr>
</table>
<table width='60%'>
  <tr>
    <th width='30%'>Allergy Type</th>
    <th width='30%'>Allergy Name</th>
  </tr>";
    for($i=0; $i<count($Allergy); $i++) {
      $atype = $Allergy[$i]['altype'];
      $aname = $Allergy[$i]['alname'];
  $file_data = $file_data .
  "<tr>
        <td width='30%'>$atype</td>
        <td width='30%'>$aname</td>
      </tr>";
   }
   $file_data = $file_data ."
</table>
<table width='100%'>
  <tr>
    <td width='100%'><br><font color='blue' size='4'>Complaints :</font><br></b></td>
  </tr>
</table>
<table width='60%'>";
    for($i=0; $i<count($complaints); $i++) {
      $cmp = $complaints[$i]['complaint'];
  $file_data = $file_data .
  "<tr>
        <td width='30%'>$cmp</td>
      </tr>";
   }
   $file_data = $file_data ."
</table>
<table width='100%'>
  <tr>
    <td width='100%'><br><font color='blue' size='4'>Observations :</font><br></b></td>
  </tr>
</table>
<table width='60%'>";
    for($i=0; $i<count($observes); $i++) {
      $obs = $observes[$i]['obervation'];
  $file_data = $file_data .
  "<tr>
        <td width='30%'>$obs</td>
      </tr>";
   }
   $file_data = $file_data ."
</table>
<table width='100%'>
  <tr>
    <td width='100%'><br><font color='blue' size='4'>Diagnosis :</font><br></b></td>
  </tr>
</table>
<table width='60%'>";
    for($i=0; $i<count($diagnosis); $i++) {
      $dia = $diagnosis[$i]['diagnosis'];
  $file_data = $file_data .
  "<tr>
        <td width='30%'>$dia</td>
      </tr>";
   }
   $file_data = $file_data ."
</table>";

include("../library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->WriteHTML($file_data);
$mpdf->Output('prescriptions_'.$eventdate.'.pdf', 'D');
exit;