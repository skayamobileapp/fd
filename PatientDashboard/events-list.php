<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');

$pid = $_SESSION['patient_details']['id'];
$cid = $_SESSION['patient_details']['city'];

$select = "SELECT e.*, c.city, c.state FROM events_list e inner join cities c on c.id = e.city where e.event_fromdatetime > CURRENT_DATE order by e.event_fromdatetime ASC";

$resultcity = mysqli_query($conn,$select);

  $i=0;
  $events = array();

   while ($row=mysqli_fetch_assoc($resultcity))
  {
     
     $row['event_fromdatetime']   = date("d-m-Y", strtotime($row['event_fromdatetime']));
     $row['event_todatetime']   = date("d-m-Y", strtotime($row['event_todatetime']));
     
     $events[$i]['id'] = $row['id'];
     $events[$i]['event_name'] = $row['event_name'];
     $events[$i]['interest_no'] = $row['interest_no'];
     $events[$i]['event_desc'] = $row['event_desc'];
     $events[$i]['event_fromdatetime'] = $row['event_fromdatetime'] . " To <br> ". $row['event_todatetime'];
     $events[$i]['location'] = $row['location'] . ", " . $row['city'];
     $events[$i]['filepath'] = $row['filepath'];
     $events[$i]['fd_colaboration'] = $row['fd_colaboration'];
     $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('main-sidebar.php'); ?>
      <div class="col-md-7 col-lg-8 col-xl-9">
			
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Events List </h3>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
					                              <th>Event NAME</th>
					                              <th>Event Date</th>
					                              <th>Address</th>
					                              <th>FD Collaborated</th>
					                              <th>ACTIONS</th>
												</tr>
											</thead>
											<tbody>
												<?php
            
            for ($i=0; $i <count($events) ; $i++) {
              ?>
              <tr>
                <td><?php echo strtoupper($events[$i]['event_name']); ?></td>
                <!-- <td><?php echo $events[$i]['event_desc']; ?></td> -->

                <td><?php echo $events[$i]['event_fromdatetime']; ?></td>
                <td><?php echo $events[$i]['location']; ?></td>
                <td><?php if($events[$i]['fd_colaboration'] == 1){ echo "yes";}else{ echo "No";} ?></td>
                <td>
                  <?php 
                  $sql = "Select * from event_customer where id_patient='$pid' and id_eventlist='".$events[$i]['id']."' ";
                            $select = mysqli_query($conn,$sql);
                            $row = mysqli_fetch_array($select, MYSQLI_NUM);
                            if ($row[0] <1 )
                              { if($events[$i]['fd_colaboration'] == 1) { ?>
                  <a href="#" data-toggle="modal" data-target="#myModal1" class="btn btn-primary" onclick="fillEVent(<?php echo $events[$i]['id']; ?>)">Interested</a>
                <?php }
                else{ ?>
                  <a href="#" data-toggle="modal" data-target="#myModal2" class="btn btn-primary" onclick="fillEVent(<?php echo $events[$i]['id']; ?>)">Interested</a> <?php
                }
              }
                else {
                  echo "Interested";
                } ?>
                </td>

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
      <input type="hidden" name="event_id" id="event_id" />
				</div>			
			</div>
    </div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
<div id="popup">
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-md" style="width: 50%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Contact Details</h4>

          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                      <label>Contact Name</label>
                      <input type="text" name="contact_name1" class="form-control" id="contact_name1">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                      <label>Contact Number</label>
                      <input type="text" name="contact_number1" class="form-control" id="contact_number1" maxlength="10">
                  </div>
                </div>
              </div>
               <div class="row">
                <div class="col-sm-8">
                  <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="contact_email1" class="form-control" id="contact_email1">
                  </div>
                </div>
              </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-lg" id="addMe1" name="addMe1" >Submit</button>
      </div>
    </div>
  </div>
</div>
</div>

<div id="popup">
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-md" style="width: 70%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Contact Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label >Contact Name</label>
                      <input type="text" name="contact_name" class="form-control" id="contact_name">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Contact Number</label>
                      <input type="text" name="contact_number" class="form-control" id="contact_number" maxlength="10">
                  </div>
                </div>
              </div>
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Email</label>
                      <input type="text" name="contact_email" class="form-control" id="contact_email">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Number of interested People</label>
                      <input type="text" name="no_of_interest" class="form-control" id="no_of_interest">
                  </div>
                </div>
              </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-lg" id="addMe2" name="addMe2" >Submit</button>
      </div>
    </div>
  </div>
</div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>

		<script>
  $(document).ready(function() {
    
    $("#no_of_interest").on('blur', function(){
        var interest_no = $("#no_of_interest").val();
        if (interest_no > 3) {
          alert("Maximum Only Three Number Allowed");
          exit;
        }
      });
});

    function fillEVent(id) {
      $("#event_id").val(id);
    }
</script>

      <script type="text/javascript">
    $("#addMe1").on('click',function(){
    var event_id = $("#event_id").val();
    var contact_name = $("#contact_name1").val();
    var contact_number = $("#contact_number1").val();
    var contact_email = $("#contact_email1").val();
    var interest_no = 0;
    var pid = '<?php echo $pid; ?>';
      $.ajax({
        // type: 'POST',
        url: 'add_customer_for_event.php',
        data:{
          'contact_name': contact_name,
          'contact_number': contact_number,
          'contact_email': contact_email,
          'interest_no': interest_no,
          'event_id': event_id,
          'pid': pid,
        },
        success: function(result){
           alert("Thanks for showing interest");
           location.reload();
      }
        });
      });
    </script>
    <script type="text/javascript">
    $("#addMe2").on('click',function(){
    var event_id = $("#event_id").val();
    var contact_name = $("#contact_name").val();
    var contact_number = $("#contact_number").val();
    var contact_email = $("#contact_email").val();
    var interest_no = $("#no_of_interest").val();
    var pid = '<?php echo $pid; ?>';
      $.ajax({
        // type: 'POST',
        url: 'add_customer_for_event.php',
        data:{
          'contact_name': contact_name,
          'contact_number': contact_number,
          'contact_email': contact_email,
          'interest_no': interest_no,
          'event_id': event_id,
          'pid': pid,
        },
        success: function(result){
           alert("Thanks for showing interest");
           location.reload();
      }
        });
      });
    </script>
		
    </body>
</html>