<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$pid = $_SESSION['patient_details']['id'];

$fileDetails=[];

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from patient_files where id='$id' ";
    $result   = $conn->query($sql);
    $fileDetails = $result->fetch_assoc();
   }

if($_POST)
{
      $file_type = $_POST['file_type'];
      $created_date = date('Y-m-d',strtotime($_POST['created_date']));
      $notes = $_POST['notes'];

      $Profile = $_FILES['photo']['name'];
      $img_tmp =$_FILES['photo']['tmp_name'];
      move_uploaded_file($img_tmp,"../uploads/".$Profile);

      $sql = "INSERT INTO patient_files (file_type, created_date, notes, file, id_patient)VALUES('$file_type', '$created_date', '$notes', '$Profile','$pid')";

    $insert = mysqli_query($conn,$sql);
    if ($insert)
      {
        echo "<script>alert('File Added successfully');</script>";
        echo "<script>parent.location='files_list.php'</script>";
        exit;
      }

  if($_GET['id'])
    {
      $id = $_GET['id'];
      $file_type = $_POST['file_type'];
      $created_date = date('Y-m-d',strtotime($_POST['created_date']));
      $notes = $_POST['notes'];

      $Profile = $_FILES['photo']['name'];
      if ($Profile==" ") {
        $Profile = $fileDetails['photo'];
      }
      else
      {
        $Profile = $_FILES['photo']['name'];
        $img_tmp =$_FILES['photo']['tmp_name'];
        move_uploaded_file($img_tmp,"../uploads/".$Profile);
      }

      $sql = "UPDATE patient_files SET file_type='$file_type', created_date='$created_date', notes='$notes', file='$Profile' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('File Updated successfully');</script>";
        echo "<script>parent.location='files_list.php'</script>";exit;
      }
    }
}

if($fileDetails['created_date']=='') {
$fileDetails['created_date'] = date('Y-m-d');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
   

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
    
  <style type="text/css">
    .error{
      text-transform: uppercase;
      position: relative;
    }
  </style>



<body>     
  <form action="" method="POST" id="form" enctype="multipart/form-data">
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('sidebar.php'); ?>
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <ol class="breadcrumb">
                  <li><a href="clinic_list.php">Files List</a></li>
                  <li class="active"><?php if (!empty($staff['id'])) { echo "Edit";} else { echo "Add"; } ?> Files</li>
                </ol>              
               <h3 class="clearfix"><?php if($fileDetails['id']){echo "Edit"; } else{ echo "Add"; }?> Files</h3>       
                <div class="card">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <select name='file_type' id='file_type' class='form-control' >
                             <option value='Medicine' <?php if($fileDetails['file_type']=='Medicine'){ echo "selected=selected";}?> >Medicine</option>

                             <option value='Prescription' <?php if($fileDetails['file_type']=='Prescription'){ echo "selected=selected";}?> >Prescription</option>
                             <option value='Lab Report' <?php if($fileDetails['file_type']=='Lab Report'){ echo "selected=selected";}?> >Lab Report</option>
                             <option value='Scanning File' <?php if($fileDetails['file_type']=='Scanning File'){ echo "selected=selected";}?> >Scanning File</option>
                             <option value='Others' <?php if($fileDetails['file_type']=='Others'){ echo "selected=selected";}?> >Others</option>
                          </select>
                          
                          <label class="fg-label">File Type<span class="error">*</span></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="date" class="form-control" name="created_date" id="created_date" maxlength="20" autocomplete="off" value="<?php echo $fileDetails['created_date']; ?>">
                          <label class="fg-label">Date <span class="error"></span></label>
                        </div>
                      </div>                            
                    </div>
                  </div>
                 
                  <div class="row">            
                    <div class="col-sm-6">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="text" class="form-control" name="notes" autocomplete="off" value="<?php echo $fileDetails['notes']; ?>">
                          <label class="fg-label">Notes<span class="error">*</span></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <p class="label-text">Upload File</p>
                          <input type="file" class="form-control" name="photo">
                        </div>
                      </div>                            
                    </div>
                  </div>
                  

                    <div class="bttn-group">
                        <a href="files_list.php" class="btn btn-link">Cancel</a>
                        <button class="btn btn-primary btn-lg" name="save" id="save">Save</button>
                    </div>                                         
                </div>            
            </div>
        </section>
      </div>
    </div>    
    </form>
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>  
    <script src="../js/main.js"></script>

    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              file_type : "required",
              created_date : "required",
              notes : "required"
            },
            messages : {

                file_type : "<span> enter file type</span>",
                created_date : "<span> Select created date</span>",
                 notes : "<span> enter notes</span>"
            }
        });
    });
</script>
        
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
</body>

</html>