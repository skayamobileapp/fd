<?php

$CurrentDate = date('Y-m-d');
// $dateOfBirth = $_SESSION['patient_details']['date_of_birth'];

$pid = $_SESSION['patient_details']['id'];
$sql = "SELECT p.*, c.city as cityName, c.state as stateName FROM patient_details p INNER JOIN cities c ON c.id=p.city WHERE p.id='$pid'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$dateOfBirth = $row['date_of_birth'];
$photo = $row['photo'];
$patient_name = $row['patient_name'];
$cityName = $row['cityName'];
$stateName = $row['stateName'];
$age = $CurrentDate - date("Y-m-d", strtotime($dateOfBirth));

$sql = "SELECT * FROM vitals WHERE patient_id='$pid' ORDER BY id DESC LIMIT 1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$weight = $row['weight'];
$height = $row['height'];
$bp = $row['bp'];
$blood = $row['blood'];
$pulse = $row['pulse'];
$bmi = $row['bmi'];

///get allergies
$pid = $_SESSION['patient_details']['id'];
$sql = "Select id from events where patient_id ='$pid' order by id  desc limit 0,1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$eid = $row['id'];

 $getAllergies = "Select * from event_allergies where event_id='$eid' ";
$result = $conn->query($getAllergies);
$i=0;
while($row = mysqli_fetch_assoc($result))
{
  $allergyList[$i]['allergy_name'] = $row['allergy_name'];
  $i++;
}

//get prescriptions
$sql = "Select event_id from prescrip where patient_id ='$pid' order by event_id desc limit 0,1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$eid = $row['event_id'];
$pid = $_SESSION['patient_details']['id'];

$getPrescrip = "Select * from prescrip where patient_id='$pid' and event_id='$eid' ";
$result = $conn->query($getPrescrip);
$i=0;
while($row = mysqli_fetch_assoc($result))
{
  $medList[$i]['drug_name'] = $row['drug_name'];
  $medList[$i]['duration'] = $row['duration'];
  $medList[$i]['repeat_same'] = $row['repeat_same'];
  $medList[$i]['time_of_the_day'] = $row['time_of_the_day'];
  $medList[$i]['to_be_taken'] = $row['to_be_taken'];

  $i++;
}

$dashboard = '';
$apptList = '';
$events = '';
$files = '';
$profile = '';

$url = basename($_SERVER['PHP_SELF']);
if($url =='index.php') {
  $dashboard = "class='active'";
}
if($url =='appointment-list.php') {
  $apptList = "class='active'";
}
if($url =='events-list.php') {
  $events = "class='active'";
}
if($url =='my-files.php') {
  $files = "class='active'";
}
if($url =='patient-profile.php') {
  $profile = "class='active'";
}

$count1=0;
  $notesql= "SELECT * FROM messages where patient_id='$pid' AND sent_by='doctor' AND patient_flag='0'";
    $result=mysqli_query($conn, $notesql);
    $count1=mysqli_num_rows($result);
?>
<!-- Profile Sidebar -->
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
							<div class="profile-sidebar">
								<div class="widget-profile pro-widget-content">
									<div class="profile-info-widget">
										<a href="#" class="booking-doc-img">
											<img src="../uploads/<?php echo $photo; ?>" alt="">
										</a>
										<div class="profile-det-info">
											<h3><?php echo $patient_name; ?></h3>
											<div class="patient-details">
												<h5><i class="fas fa-birthday-cake"></i> <?php echo date("d M Y", strtotime($dateOfBirth)); ?>, <?php echo $age; ?> years</h5>
												<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> <?php echo $cityName; ?>, <?php echo $stateName; ?></h5>
											</div>
										</div>
									</div>
								</div>
                                              
								<div class="dashboard-widget">
									<nav class="dashboard-menu">
										<ul>
											<li <?php echo $dashboard; ?>>
												<a href="index.php">
													<i class="fas fa-columns"></i>
													<span>Dashboard</span>
												</a>
											</li>
											<li <?php echo $apptList; ?>>
												<a href="appointment-list.php">
													<i class="fas fa-hourglass-end"></i>
													<span>Reschedule Appointments</span>
												</a>
											</li>
											<li <?php echo $events; ?>>
												<a href="events-list.php">
													<i class="fas fa-calendar-alt"></i>
													<span>Events List</span>
												</a>
											</li>
											<li <?php echo $files; ?>>
												<a href="my-files.php">
													<i class="fas fa-file-alt"></i>
													<span>My Files</span>
												</a>
											</li>
											<!-- <li>
												<a href="favourites.html">
													<i class="fas fa-bookmark"></i>
													<span>Favourites</span>
												</a>
											</li> -->
											<!-- <li>
												<a href="favourites.html">
													<i class="fas fa-bookmark"></i>
													<span>Favourites</span>
												</a>
											</li> -->
											<li>
												<a href="chat-doctor.php">
													<i class="fas fa-comments"></i>
													<span>Message</span>
													<?php if($count1>0){ echo "<small class='unread-msg'>$count1</small>"; } else { }?>
												</a>
											</li>
											<li <?php echo $profile; ?>>
												<a href="patient-profile.php">
													<i class="fas fa-user-cog"></i>
													<span>Profile Settings</span>
												</a>
											</li>
											<!-- <li >
												<a href="change-password.html">
													<i class="fas fa-lock"></i>
													<span>Change Password</span>
												</a>
											</li> -->
											<li >
												<a href="../patient_login.php">
													<i class="fas fa-sign-out-alt"></i>
													<span>Logout</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>

							</div>
							<!-- Last Booking 
							<div class="card">
								<div class="card-header">
									<h4 class="card-title">Vitals Information</h4>
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">
										<div class="media align-items-center">
											<div class="media-body">
												<span><img src="../assets/img/weight.svg" alt="" width="30px" title="weight">Weight : <?php if($weight){echo $weight;} else{ echo " -- ";} ?> kg</span>
											</div>
											<div class="media-body">
						                        <span><img src="../assets/img/height.svg" alt="" width="30px" title="height">
												Height : <?php if($height){echo $height;} else{ echo " -- ";} ?> cm</span>
											</div>
										</div>
									</li>
									<li class="list-group-item">
										<div class="media align-items-center">
											<div class="media-body">
						                        <span><img src="../assets/img/bp.svg" alt="" width="32px" title="BP">
												BP : <?php if($bp){echo $bp;} else{ echo " -- ";} ?> mmHg</span>
											</div>
										</div>
									</li>
									<li class="list-group-item">
										<div class="media align-items-center">
											<div class="media-body">
						                        <span><img src="../assets/img/bmi.png" alt="" width="32px" title="BMI">
												BMI : <?php if($bmi){echo $bmi;} else{ echo " -- ";} ?> </span>
											</div>
										</div>
									</li>
									<li class="list-group-item">
										<div class="media align-items-center">
											<div class="media-body">
						                        <span><img src="../assets/img/blood.svg" alt="" width="32px" title="Blood Group">
												Blood Group : <?php if($blood){echo $blood;} else{ echo " -- ";} ?> </span>
											</div>
										</div>
									</li>
									<li class="list-group-item">
										<div class="media align-items-center">
											<div class="media-body">
						                        <span><img src="../assets/img/heart.svg" alt="" width="34px" title="Pulse">
												Pulse : <?php if($pulse){echo $pulse;} else{ echo " -- ";} ?> </span>
											</div>
										</div>
									</li>
								</ul>
							</div>-->
							<!-- /Last Booking -->
						</div>
						<!-- / Profile Sidebar -->