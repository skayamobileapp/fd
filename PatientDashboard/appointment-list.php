<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);

$pid = $_SESSION['patient_details']['id'];

$date = date("Y-m-d");
$sql = "Select d.*, e.id as eid, e.start, e.title from doctor_details as d INNER JOIN events as e on d.id=e.doctor_id
 where date(e.start)>=CURRENT_DATE and e.status='0' and e.patient_id='$pid' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $doctor[$i]['id'] = $row['id'];
  $doctor[$i]['doctor_name'] = $row['doctor_name'];
  $doctor[$i]['mobile'] = $row['mobile'];
  $doctor[$i]['email'] = $row['email'];
  $doctor[$i]['start'] = $row['start'];
  $doctor[$i]['title'] = $row['title'];
  $doctor[$i]['eid'] = $row['eid'];
  $i++;
}

$sql   = "select * from doctor_details";
$result = $conn->query($sql);
$doctorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($doctorList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('main-sidebar.php'); ?>
			<div class="col-md-7 col-lg-8 col-xl-9">

			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Appointment List </h3>
								
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
						                            <th>Doctor Name</th>
						                            <th>Appt Purpose</th>
						                            <th>Appt Date</th>
						                            <th>Contact No</th>
						                            <th>Email Id</th>
						                            <th>Reschedule</th>
												</tr>
											</thead>
											<tbody>
												<?php
            
            for ($i=0; $i <count($doctor) ; $i++) { 
              ?>
              <tr>
                <td><?php echo ucwords($doctor[$i]['doctor_name']); ?></td>
                <td><?php echo ucfirst($doctor[$i]['title']); ?></td>
                <td><?php echo date("d M Y", strtotime($doctor[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($doctor[$i]['start'])); ?></span></td>
                <td><?php echo $doctor[$i]['mobile'] ; ?></td>
                <td><?php echo strtolower($doctor[$i]['email']); ?></td>
                <td><a class='btn btn-primary' title='Edit' href='reschedule-appointment.php?id=<?php echo $doctor[$i]['id']; ?>&eid=<?php echo $doctor[$i]['eid']; ?>'>Reschedule</a></td>

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>