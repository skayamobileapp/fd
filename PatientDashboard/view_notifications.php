<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);
$pid = $_SESSION['patient_details']['id'];

$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  while ($row = $result->fetch_assoc()) {
    $idnotify = $row['id'];
    $sql = "Insert into notification_read (id_notify,read_by,id_user) values('$idnotify','Patient','$pid ') ";
mysqli_query($conn, $sql);
}

$notesql= "SELECT * FROM notifications WHERE patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  $readList = array();
  while ($row = $result->fetch_assoc()) {
    array_push($readList, $row);
  }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
  #notification-count{
  position: absolute;
  left: 220px;
  top: 80px;
  font-size: 1.0em;   
  color: #ffffff;
  font-weight:bold;
  border-radius: 4px;
}
</style>

<body>
  <?php include('navbar.php'); ?>
    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('sidebar.php'); ?>
        
        <section class="col-sm-8 col-lg-9">
            <div class="main-container"> 
            <form action="" method="POST" id="myform">
               <h3 class="clearfix">Notifications</h3>       
                <div class="card">
                    <?php for ($i=0; $i <count($readList); $i++) { ?>
                    <div class="row">
                      <div class="col-sm-12">
                        <div>
                          <table>
                            <tr>
                            <td><?php echo $n = $i+1; ?></td>
                            <td class='alert text-primary'><?php echo $readList[$i]['message']; ?></td>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                    <?php
                        }
                        ?>
                </div>
            </form>    
                      
                </div>
        </section>
      </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/main.js"></script>

           <script src="../select2/js/select2.js" ></script>
          <script src="../select2/js/select2-init.js" ></script>
  
        <script type="text/javascript">

        function get_prev(id){
          var pid = $(id).attr("id");
          var did = '<?php echo $did; ?>';
          console.log(did);
          console.log(pid);
          $("#patient_id").val(pid);

          $.ajax({
            url: 'get_questions.php',
            data:{
                  'did': did,
                  'pid': pid,
        },
            success: function(result){
            $("#previous").html(result);
          }
        });
        }
      </script>
  </body>
</html>