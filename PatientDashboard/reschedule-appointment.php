<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$DoctorId = $_GET['id'];
$EventId = $_GET['eid'];

$pid = $_SESSION['patient_details']['id'];

// $doctor_data = [];
$sql = "select d.*, s.name from doctor_details as d INNER JOIN sub_specialties as s on d.sub_specialty=s.id where d.id ='$DoctorId'";
$result = $conn->query($sql);
$doctor_data = $result->fetch_assoc();

$events=[];
if (isset($_GET['eid'])) {

    $EventId       = $_GET['eid'];
    $sql      = "select * from events where id='$EventId' ";
    $result   = $conn->query($sql);
    $events = $result->fetch_assoc();
   }

// $id = $_SESSION['patient_details']['id'];
// $name = $_SESSION['patient_details']['patient_name'];
// $mobile = $_SESSION['patient_details']['mobile_number'];
// $id = $_SESSION['patient_details']['id'];


$select = mysqli_query($conn,"SELECT * FROM patient_details WHERE id = '$pid'");
if ($row = mysqli_num_rows($select)) 
{
    while ($row = mysqli_fetch_array($select))
    {
        //from database fields
        $fname = $row['first_name'];
        $mname = $row['middle_name'];
        $lname = $row['last_name'];
        $fullname = $fname." ".$mname." ".$lname;
        $phone = $row['patient_phone_number'];
        $mobile_number = $row['mobile_number']; 
        $DateOfBirth = $row['date_of_birth'];
        $email = $row['email'];
    }
}
if ($_POST) {

  $id = $_GET['eid'];
  $title = $_POST['title'];
  $doctorName = $_GET['id'];;
                $slotid = $_POST['eventid'];
                
                mysqli_query($conn,"UPDATE timeslots SET slot='y' WHERE id='$events[id_time_slots]'");

                mysqli_query($conn,"UPDATE events SET rescheduled_by=2, status=2 WHERE id='$id' ");

                $slot_query= "SELECT * FROM timeslots WHERE id='$slotid' AND doctor_id='$doctorName' ORDER BY date";
                $slot_result = $conn->query($slot_query);
                $row = $slot_result->fetch_assoc();
                $date = $row['date'];
                $time = $row['time'];
                $slot_id = $row['id'];
                $id_clinic = $row['id_clinic'];
                $datetime  = $date." ".$time;
                $etime = strtotime($row['time']);
                // $startTime = date("H:i", strtotime('-30 minutes', $etime));
                $endTime = date("H:i", strtotime('+30 minutes', $etime));
                $enddatetime = $date." ".$endTime;
                $patientId = $_SESSION['patient_details']['id'];
                $name = $_SESSION['patient_details']['patient_name'];
                $mobile = $_SESSION['patient_details']['mobile_number'];

                $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, id_time_slots, id_clinic, patient_name, mobile, height, weight, bp, sugar, bmi, status, pulse) VALUES('$title', '$datetime', '$enddatetime', '$doctorName', '$patientId', '$slot_id', '$id_clinic', '$name', '$mobile','NA','NA','NA','NA', 'NA', '0','NA')";

                $result = $conn->query($sql);
                if ($result) {
                  mysqli_query($conn,"UPDATE timeslots SET slot='n' WHERE id='$slotid' ");

                  echo "<script>alert('Appointment Rescheduled to '+ '".date("d M Y h:i:a", strtotime($datetime))."')</script>";
                 echo "<script>parent.location='appointment-list.php'</script>";
                }
              }

               $slots = mysqli_query($conn, "SELECT t.*, c.clinic_name FROM timeslots t INNER JOIN clinic_registration c ON t.id_clinic=c.id WHERE t.doctor_id='$events[doctor_id]' AND t.slot='y' AND t.id_clinic=".$events['id_clinic']." AND CONCAT(t.date,' ', t.time)>=CURRENT_TIMESTAMP");

        $i = 0;
          $slotList = array();
          while ($row = mysqli_fetch_assoc($slots)) {
            $slotList[$i]['id'] = $row['id'];
            $slotList[$i]['date'] = $row['date'];
            $slotList[$i]['time'] = $row['time'];
            $slotList[$i]['slot'] = $row['slot'];
            $slotList[$i]['clinic_name'] = $row['clinic_name'];
            $i++;
          }

$sql   = "select id, doctor_name from doctor_details";
$result = $conn->query($sql);
$doctorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($doctorList, $row);
  }

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>First Doctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../assets/img/favicon.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Fancybox CSS -->
		<link rel="stylesheet" href="../assets/plugins/fancybox/jquery.fancybox.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			.error{
				color: red;
			}
		</style>
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<?php include('main-sidebar.php'); ?>

					<!-- Doctor Widget -->
					<div class="card">

								<h3 style="padding: 20px;">Reschedule Appointment</h3>
						<div class="card-body">
							<div class="doctor-widget">
								<div class="doc-info-left">
									<div class="doctor-img">
										<img src="../uploads/<?php echo $doctor_data['photo']; ?>" class="img-fluid" alt="User Image">
									</div>
									<div class="doc-info-cont">
										<h4 class="doc-name"><?php echo ucwords($doctor_data['doctor_name']); ?></h4>
										<p class="doc-speciality"><?php echo $doctor_data['qualification']; ?> in <?php if($doctor_data['qualification']=='UG'){
											echo $doctor_data['qualification_ug'];
										} else { echo $doctor_data['qualification_pg'];} ?></p>
										<p class="doc-department"><?php echo $doctor_data['name']; ?></p>
										<div class="rating">
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star"></i>
											<span class="d-inline-block average-rating">(35)</span>
										</div>
										<div class="clinic-details">
											<p class="doc-location"><i class="fas fa-map-marker-alt"></i> <?php echo $doctor_data['clinic_address'] ?> - <a href="javascript:void(0);">Get Directions</a></p>
											
										</div>
										<div class="clinic-services">
											<!-- <span>Dental Fillings</span> -->
										</div>
									</div>
								</div>
								<div class="doc-info-right">
									<div class="clini-infos">
										<ul>
											<li><i class="far fa-white"></i> </li>
											<li><i class="fa fa-phone"></i> <?php echo $doctor_data['mobile'] ?></li>
											<li><i class="fas fa-envelope"></i> <?php echo $doctor_data['email']; ?></li>
											<!-- <li><i class="far fa-money-bill-alt"></i> $100 per hour </li> -->
										</ul>
									</div>
									
									<div class="clinic-booking">
										<a class="apt-btn" href="#slotscheck">Check Slots &#709;</a>
									</div>
								</div>
							</div>
					<!-- /Doctor Widget -->
				
<hr/>
          <form id="target" action="" method="POST">
            <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <label>Appointment Reason<span class="error"> *</span></label>
                      <input type="text" name="title" class="form-control" id="title" placeholder="Purpose of visit" required value="<?php echo $events['title']; ?>">
                  </div>
                </div>
            </div>
         
          <h3 id="slotscheck"><?php if($slotList){ echo "Available slots : "; } else{ echo "No Slots Available";} ?></h3>
              <div class="row">
                <div class='table-responsive theme-table v-align-top'>
                <table class="table">
            <?php for($i=0; $i<count($slotList); $i++){ ?>
                  <tr>
                    <td style="text-align: center;">
                      <?php echo date('d-M-Y',strtotime($slotList[$i]['date'])); ?>
                         <?php echo ' '.$slotList[$i]['time']; ?>
                    </td>
                    <td style="text-align: center;">
                         <?php echo $slotList[$i]['clinic_name']; ?>
                    </td>
                    <td style="text-align: center;">
                      <a class="btn btn-light" href="appointment-list.php">Cancel</a>
                      <button type="button" class="btn btn-primary btn-lg"  onclick="selectedid(<?php echo $slotList[$i]['id']; ?>)" id='book'>Reschedule</button>
                  </tr>
              <?php
                  }
              ?>
                </table>
              </div>
              </div>
                 <input type='hidden' name='eventid' id="eventid" value='' />

            </form>
					
						</div>
					</div>
				</div>
			</div>		
			<!-- /Page Content -->   
		</div>
		<!-- /Main Wrapper -->
	
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Fancybox JS -->
		<script src="../assets/plugins/fancybox/jquery.fancybox.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		 <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">

function selectedid(id) {
  $("#eventid").val(id);
  $("#target").validate({
    rules : {
        purpose : "required"
      },
    messages : {
        purpose : "<span> ENTER APPOINTMENT REASON</span>"
      }
  });
  var conf = confirm('ARE YOU SURE FOR THIS APPOINTMENT');
   if(conf==true){
    $("#target").submit();
  }
}
</script>
		
	</body>
</html>