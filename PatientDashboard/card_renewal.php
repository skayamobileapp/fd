<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);

$pid = $_SESSION['patient_details']['id'];

//card History

//card renewal
if ($_POST) {

$renewSql = "SELECT * FROM card_history WHERE patient_id ='$pid' AND expiry_date<=CURRENT_DATE and status='1' ORDER BY id DESC LIMIT 1";
$cardRenew = mysqli_query($conn, $renewSql);
$row = $cardRenew->fetch_assoc();
$cardType = $row['card_type'];
$renewedDateFrom = $row['expiry_date'];
$etime = strtotime($row['expiry_date']);
$renewedDateTo = date("Y-m-d", strtotime('+1 year', $etime));

  mysqli_query($conn,"Update card_history set status=0 where patient_id='$pid'");

  mysqli_query($conn,"INSERT INTO card_history(patient_id, card_type, renewal_date, expiry_date,status) VALUES('$pid','$cardType','$renewedDateFrom','$renewedDateTo','0')");
}

$renewSql = "SELECT * FROM card_history WHERE patient_id ='$pid' AND expiry_date<=CURRENT_DATE and status='1' ORDER BY id DESC LIMIT 1";
$cardRenew = mysqli_query($conn, $renewSql);

$sql = "SELECT * FROM card_history WHERE patient_id ='$pid' and status='0' ";
$result = mysqli_query($conn, $sql);
$i=0;

while ($row=mysqli_fetch_assoc($result)) {
  $cardHistory[$i]['id'] = $row['id'];
  $cardHistory[$i]['card_type'] = $row['card_type'];
  $cardHistory[$i]['renewal_date'] = $row['renewal_date'];
  $cardHistory[$i]['expiry_date'] = $row['expiry_date'];
  $i++;

}
$count=0;
$pid = $_SESSION['patient_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
    
</head>

<body>     
    <nav class="navbar navbar-default dashboard-navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">First Doctor</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right main-nav">
            <li><a href="index.php" class="dashboard">Dashboard</a></li>
            <li><a href="card_renewal.php" class="precaution active">Card Renewal</a></li>
            <li><a href="reports.php" class="medical-report">Medical Reports</a></li>
             <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true">Find Providers <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="find_lab.php">Diagnostic Labs</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="find_pharma.php">Pharmacy Stores</a></li>
                </ul>                
            </li>
            <li><a href="find_doctor.php" class="find-doctor">Find Doctor</a></li>
            <li><a href="view_notifications.php" class="notifications-link"><sup id="notification-count"><?php if($count>0) { echo $count; } ?></sup> </a></li>
                    <li><a href="../index.php">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid main-wrapper">
      <div class="row">
        <?php include ('sidebar.php'); ?>
        <section class="col-sm-8 col-lg-9">
            <div class="main-container"> 
            <form action="" method="POST" id="myform">
               <h3 class="clearfix">Renewal</h3>       
                <div class="card">
                    <div class="row">
                      <div class='table-responsive theme-table v-align-top'>
                        <table class="table">
                          <thead>
                            <tr>
                            <th>Card</th>
                            <th>Expired Date</th>
                            <th>Renew Date</th>
                            <th>Renew Card</th>
                            </tr>
                          </thead>
                          <tbody>
            <?php 

                $row = $cardRenew->fetch_assoc();

                     if($row!=NULL) {
                    $etime = strtotime($row['expiry_date']);
                    $reDate = date("Y-m-d", strtotime('+1 year', $etime));

                    ?>
                            <tr>
                              <td><?php echo $row['card_type']; ?></td>
                              <td><?php echo date('d-M-Y',strtotime($row['expiry_date'])); ?></td>
                              <td><?php echo date('d-M-Y',strtotime($reDate)); ?></td>
                              <td><button type="submit" class="btn btn-primary btn-lg" name="renew">Renew</button></td>
                            </tr>
                <?php
              }
              ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
            </form>
             <h3 class="clearfix">Renewed History</h3>         
              <div class="card">
                  <div class="row">
                    <div class='table-responsive theme-table v-align-top'>
                      <table class="table">
                        <thead>
                          <tr>
                          <th>SL. NO</th>
                          <th>Card</th>
                          <th>Renewal Date</th>
                          <th>Expiry Date</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                      for($i=0; $i<count($cardHistory); $i++) {
                      ?>
                          <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $cardHistory[$i]['card_type']; ?></td>
                            <td><?php echo date('d-M-Y',strtotime($cardHistory[$i]['renewal_date'])); ?></td>
                            <td><?php echo date('d-M-Y',strtotime($cardHistory[$i]['expiry_date'])); ?></td>
                          </tr>
                          <?php
                        }
                        ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div>
            </div>
        </section>
      </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/main.js"></script>
  </body>
</html>