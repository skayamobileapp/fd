<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);
$pid = $_SESSION['patient_details']['id'];

$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  while ($row = $result->fetch_assoc()) {
    $idnotify = $row['id'];
    $sql = "Insert into notification_read (id_notify,read_by,id_user) values('$idnotify','Patient','$pid ') ";
mysqli_query($conn, $sql);
}

$notesql= "SELECT * FROM notifications WHERE patient_flag=1 order by datetime desc";
  $result=mysqli_query($conn, $notesql);
  $readList = array();
  while ($row = $result->fetch_assoc()) {
    array_push($readList, $row);
  }

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('main-sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
              
                    <form action="" method="POST" id="myform">
                    	<h3 class="clearfix">Notifications</h3>
                    	<div class="card">
	                    <?php for ($i=0; $i <count($readList); $i++) { ?>
		                    <div class="row">
			                      	<div class="col-sm-1"></div>
		                      		<div class="col-sm-11">

		                          <table>
		                            <tr>
		                            <td><?php echo $n = $i+1; ?>)</td>
		                            <td class='alert text-info'><?php echo $readList[$i]['message']; ?></td>
		                            <td> - <?php echo date("d M Y h:i A", strtotime($readList[$i]['datetime'])); ?></td>
		                            </tr>
		                          </table>
		                    </div>
                    </div>
                    <?php
                        }
                        ?>
                </div>
	            </form>
                </div>
            </div>
        </div>
    </div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Dropzone JS -->
		<script src="../assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>