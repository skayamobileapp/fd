<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
// $select ="";
if(isset($_POST['search']))
{

  $testname = $_POST['testname'];
  $city = $_POST['city'];
    
  $select = "SELECT  a.lab_name, b.test_name, a.home_collection, b.price, a.mobile, a.email, a.profile_pic, a.address FROM lab_test_name b INNER JOIN lab_details a ON b.id_lab=a.id WHERE b.id = '$testname'";
  $result = mysqli_query($conn, $select);
  $i = 0;
  $labDetailsArray = array();
  while ($row = mysqli_fetch_assoc($result)) {

      $results['home_collection'] = $row['home_collection'];
      $results['id'] = $row['id'];
      $results['lab_name'] = $row['lab_name'];
      $results['test_name'] = $row['test_name'];
      $results['profile_pic'] = $row['profile_pic'];
      $results['address'] = $row['address'];
      $results['mobile'] = $row['mobile'];
      $results['email'] = $row['email'];
      $results['price'] = $row['price'];
      $results['testtype'] = 1;
      
     array_push($labDetailsArray,$results);
    $i++;
  }


  // $select = "SELECT  a.*, b.* from  lab_details as a,lab_test_group as b 
  // where a.id=b.id_lab  and b.group_name like '%$testname%'";
  // $result = mysqli_query($conn, $select);
  // $i = 0;
  // while ($row = mysqli_fetch_assoc($result)) {
  //  $results['id'] = $row['id'];
  //  $results['Home_collection'] = $row['Home_collection'];
  //     $results['lab_name'] = $row['lab_name'];
  //     $results['test_name'] = $row['group_name'];
  //     $results['mobile'] = $row['mobile'];
  //     $results['email'] = $row['email'];
  //     $results['price'] = $row['price'];
  //     $results['testtype'] = 2;
      
  //    array_push($labDetailsArray,$results);
  // }


}

$sql   = "select id, test_name from lab_test_name order by test_name";
$result = $conn->query($sql);
$tests = array();
while ($row = $result->fetch_assoc()) {
    array_push($tests, $row);
  }

  $sql = "SELECT id, city from cities order by city";
$result = mysqli_query($conn, $sql);
  $i = 0;
  $cityList = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($cityList, $row);
  }

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Fancybox CSS -->
		<link rel="stylesheet" href="../assets/plugins/fancybox/jquery.fancybox.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('main-sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0">Search Diagnostic Lab</h4>
                  
								</div>
								<div class="card-body">
						<form action="" method="POST" id="form">
                    <div class="row">
	                  <div class="col-sm-6">
	                    <div class="form-group">
	                          <label class="">Select City<span class="error"></span></label>
	                        <select name="city" id="city" class="form-control selitemIcon">
	                                <option value="">SELECT CITY</option>
	                                <?php
	                                for ($i=0; $i<count($cityList); $i++) {  ?>
	                                <option value="<?php echo $cityList[$i]['id']; ?>" <?php if($city==$cityList[$i]['id']) echo "selected";?> ><?php echo $cityList[$i]['city']; ?></option>
	                                <?php
	                                }
	                                ?>
	                            </select>
	                    </div>
	                  </div>
	                  <div class="col-sm-6">
	                    <div class="form-group">
	                        <label>Select Test</label><span class="error"></span>
	                        <select name="testname" id="testname" class="form-control selitemIcon">
	                                <option value="">SELECT TEST</option>
	                                <?php
	                                for($i=0; $i<count($tests); $i++){ ?>
	                                  <option value="<?php echo $tests[$i]['id']; ?>" <?php if($testname == $tests[$i]['id']){ echo "selected=selected"; } ?>><?php echo $tests[$i]['test_name']?></option>
	                                <?php
	                                }

	                                ?>
	                            </select>
	                    </div>
	                  </div>
	              </div>
	            <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group float-right">
                      <label></label><div>
                        <input type="submit" class="btn btn-primary btn-lg" title="Search" name="search" value="Search">
                        </div>
                    </div>
                  </div>
                </div>
              </form>
									
								</div>
							</div>

					<?php for($i=0; $i<count($labDetailsArray); $i++){
                        $roleid = $labDetailsArray[$i]['id'];?>
							<!-- Doctor Widget -->
							<div class="card">
								<div class="card-body">
									<div class="doctor-widget">
										<div class="doc-info-left">
											<div class="doctor-img">
												<a href="#">
													<img src="../uploads/<?php echo $labDetailsArray[$i]['profile_pic']; ?>" class="img-fluid" alt="">
												</a>
											</div>
											<div class="doc-info-cont">
												<h4 class="doc-name"><a href="#"><?php echo ucwords($labDetailsArray[$i]['lab_name']); ?></a></h4>
												<p class="doc-speciality"><?php echo ucfirst($labDetailsArray[$i]['address']); ?></p>
												<h5><font class="doc-department"><?php echo ucwords($labDetailsArray[$i]['test_name']); ?></font>
												 -  
													<span><i class="fa fa-rupee-sign"></i> <?php echo number_format($labDetailsArray[$i]['price']); ?> /- </span> </h5>
												<div class="rating">
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star"></i>
													<span class="d-inline-block average-rating">(17)</span>
												</div>
												<div class="clinic-details">
													<p class="doc-location"><i class="fas fa-map-marker-alt"></i> India</p>
												</div>
											</div>
										</div>
										<div class="doc-info-right">
											<div class="clinic-booking">
												<i class="fas fa-mobile"></i> :
												<span> <?php echo $labDetailsArray[$i]['mobile']; ?> </span>
												<br>
												<i class="fa fa-envelope"></i> :
												<span> <?php echo $labDetailsArray[$i]['email']; ?> </span>
<!-- 												<a class="view-pro-btn" href="doctor-profile.html">View Profile</a> --> <br><br>
												<a class="apt-btn" href="javascript:alert('Test Booked Succesfully');">Book Test</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
							<!-- /Doctor Widget -->

						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Datetimepicker JS -->
		<script src="../assets/js/moment.min.js"></script>
		<script src="../assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Fancybox JS -->
		<script src="../assets/plugins/fancybox/jquery.fancybox.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../select2/js/select2.js" ></script>
	    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>