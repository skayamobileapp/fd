<?php
include('../connection/conn.php');
include('session_check.php');
// include('profile_completion.php');

$pid = $_SESSION['patient_details']['id'];

$date = date("Y-m-d H:i:s");
$sql = "Select d.*, e.id as eid, e.title, e.start, e.amount, s.name as subSpecialty from doctor_details as d INNER JOIN events as e on d.id=e.doctor_id INNER JOIN sub_specialties as s ON d.sub_specialty=s.id
 where e.start>='$date' and e.status='0' and e.patient_id='$pid' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $doctor[$i]['id'] = $row['id'];
  $doctor[$i]['doctor_name'] = $row['doctor_name'];
  $doctor[$i]['mobile'] = $row['mobile'];
  $doctor[$i]['email'] = $row['email'];
  $doctor[$i]['photo'] = $row['photo'];
  $doctor[$i]['subSpecialty'] = $row['subSpecialty'];
  $doctor[$i]['amount'] = $row['amount'];

  $doctor[$i]['title'] = $row['title'];
  $doctor[$i]['start'] = $row['start'];
  $doctor[$i]['eid'] = $row['eid'];
  $i++;
}

$sql = "Select d.*, e.id as eid, e.title, e.start, e.amount, s.name as subSpecialty from doctor_details as d INNER JOIN events as e on d.id=e.doctor_id INNER JOIN sub_specialties as s ON d.sub_specialty=s.id
 where e.patient_id='$pid' and e.status='1'";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $success[$i]['id'] = $row['id'];
  $success[$i]['doctor_name'] = $row['doctor_name'];
  $success[$i]['photo'] = $row['photo'];
  $success[$i]['mobile'] = $row['mobile'];
    $success[$i]['amount'] = $row['amount'];
  $success[$i]['subSpecialty'] = $row['subSpecialty'];

  $success[$i]['start'] = $row['start'];
  $success[$i]['email'] = $row['email'];
  $success[$i]['title'] = $row['title'];
  $success[$i]['eid'] = $row['eid'];
  $i++;
}

$count=0;
$pid = $_SESSION['patient_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						
						<?php include('main-sidebar.php'); ?>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-body pt-0">
								
									<!-- Tab Menu -->
									<nav class="user-tabs mb-4">
										<ul class="nav nav-tabs nav-tabs-bottom nav-justified">
											<li class="nav-item">
												<a class="nav-link active" href="#coming_appointments" data-toggle="tab">Upcoming Appointments</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" href="#previous_appointments" data-toggle="tab">Previous Appointments</a>
											</li>
											<li class="nav-item">
												<a class="nav-link inactive" href="#" onclick="download(<?php echo $pid; ?>)" data-toggle="tab" id="medRecords" title="Download Medical Record"><span class="med-records"><i class="fas fa-download"></i>  Medical Records</span></a>
											</li>
										</ul>
									</nav>
									<!-- /Tab Menu -->
									
									<!-- Tab Content -->
									<div class="tab-content pt-0">
										
										<!--Upcoming Appointment Tab -->
										<div id="coming_appointments" class="tab-pane fade show active">
											<div class="card card-table mb-0">
												<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-center mb-0">
									<thead>
										<tr>
											<th>SL. NO</th>
											<th>Doctor Name</th>
											<th>Appt Date</th>
											<th>Purpose</th>
											<th>Contact NO.</th>
											<th>Email ID</th>
										</tr>
									</thead>
									<tbody>
										<?php
			                        for($i=0; $i<count($doctor); $i++) {
			                            $pid = $doctor[$i]['id'];
			                            $n = $i+1; ?>
										<tr>
											<td><?php echo $i+1; ?></td>
											<td>
												<h2 class="table-avatar">
													<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
														<img class="avatar-img rounded-circle" src="../uploads/<?php echo $doctor[$i]['photo']; ?>" alt="User Image">
													</a>
													<a href="doctor-profile.html"><?php echo ucfirst($doctor[$i]['doctor_name']); ?> <span><?php echo ucfirst($doctor[$i]['subSpecialty']); ?></span></a>
												</h2>
											</td>
											<td> <?php echo date("d M Y", strtotime($doctor[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($doctor[$i]['start'])); ?></span></td>
											<td><?php echo $doctor[$i]['title']; ?></td>
											<td><?php echo $doctor[$i]['mobile']; ?></td>
											<td><?php echo $doctor[$i]['email']; ?></td>
											
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- /Upcoming Appointment Tab -->

				<!--Previous Appointment Tab -->
										<div id="previous_appointments" class="tab-pane fade">
											<div class="card card-table mb-0">
												<div class="card-body">
							<div class="table-responsive">
								<table class="table table-hover table-center mb-0">
									<thead>
										<tr>
											<th>SL. NO</th>
											<th>Doctor Consulted</th>
											<th>Appt Date</th>
											<th>Purpose</th>
											<th>Contact NO.</th>
											<th>Amount</th>
											<th>Prescriptions</th>
											<th>Book Next Appointment</th>
										</tr>
									</thead>
									<tbody>
										<?php
			                        for($i=0; $i<count($success); $i++) {
			                            $pid = $success[$i]['id'];
			                            $n = $i+1; ?>
										<tr>
											<td><?php echo $i+1; ?></td>
											<td>
												<h2 class="table-avatar">
													<a href="doctor-profile.html" class="avatar avatar-sm mr-2">
														<img class="avatar-img rounded-circle" src="../uploads/<?php echo $success[$i]['photo']; ?>" alt="User Image">
													</a>
													<a href="doctor-profile.html"><?php echo ucfirst($success[$i]['doctor_name']); ?> <span><?php echo ucfirst($success[$i]['subSpecialty']); ?></span></a>
												</h2>
											</td>
											<td> <?php echo date("d M Y", strtotime($success[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($success[$i]['start'])); ?></span></td>
											<td><?php echo $success[$i]['title']; ?></td>
											<td><?php echo $success[$i]['mobile']; ?></td>
											<td><i class="fa fa-rupee-sign"></i> <?php echo $success[$i]['amount']; ?> /-</td>
											<td><a href="view-prescriptions.php?id=<?php echo $success[$i]['eid']; ?>" class="btn btn-sm bg-info-light"><i class="far fa-eye"></i>View</a>
												<a href="print_prescription.php?id=<?php echo $success[$i]['eid']; ?>" class="btn btn-sm bg-primary-light"><i class="fas fa-download"></i>Download</a></td>
											<td><a href="book-appointment.php?id=<?php echo $success[$i]['id']; ?>" class="btn btn-primary">Book</a></td>
											
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- /Previous Appointment Tab -->
					
						
									</div>
									<!-- Tab Content -->
									
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
		<script type="text/javascript">
			$("#medRecords").on('click',function(){
		    var pid = '<?php echo $pid; ?>';
		    if(pid != 0) {
    	     window.location.href="print_history.php?id="+pid;
	    }
	});
		</script>
		
	</body>
</html>