<?php

$CurrentDate = date('Y-m-d');
// $dateOfBirth = $_SESSION['patient_details']['date_of_birth'];

$pid = $_SESSION['patient_details']['id'];
$sql = "SELECT p.*, c.city as cityName, c.state as stateName FROM patient_details p INNER JOIN cities c ON c.id=p.city WHERE id='$pid'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$dateOfBirth = $row['date_of_birth'];
$photo = $row['photo'];
$patient_name = $row['patient_name'];
$cityName = $row['cityName'];
$stateName = $row['stateName'];
$age = $CurrentDate - $dateOfBirth;

$sql = "SELECT * FROM vitals WHERE patient_id='$pid' ORDER BY id DESC LIMIT 1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$weight = $row['weight'];
$height = $row['height'];
$bp = $row['bp'];
$blood = $row['blood'];
$pulse = $row['pulse'];
$bmi = $row['bmi'];

///get allergies
$pid = $_SESSION['patient_details']['id'];
$sql = "Select id from events where patient_id ='$pid' order by id  desc limit 0,1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();
$eid = $row['id'];

 $getAllergies = "Select * from event_allergies where event_id='$eid' ";
$result = $conn->query($getAllergies);
$i=0;
while($row = mysqli_fetch_assoc($result))
{
  $allergyList[$i]['allergy_name'] = $row['allergy_name'];
  $i++;
}

//get prescriptions
$sql = "Select event_id from prescrip where patient_id ='$pid' order by event_id desc limit 0,1";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

$eid = $row['event_id'];
$pid = $_SESSION['patient_details']['id'];

$getPrescrip = "Select * from prescrip where patient_id='$pid' and event_id='$eid' ";
$result = $conn->query($getPrescrip);
$i=0;
while($row = mysqli_fetch_assoc($result))
{
  $medList[$i]['drug_name'] = $row['drug_name'];
  $medList[$i]['duration'] = $row['duration'];
  $medList[$i]['repeat_same'] = $row['repeat_same'];
  $medList[$i]['time_of_the_day'] = $row['time_of_the_day'];
  $medList[$i]['to_be_taken'] = $row['to_be_taken'];

  $i++;
}

?>
<aside class="col-sm-4 col-lg-3 sidebar">
            <div class="profile-block">
                <div class="profile-img-container">
                    <img src="<?php echo '../uploads/'.$photo; ?>" alt="" style="width: 100px; height: 105px;"/>
                </div>                
                <h3><?php echo $patient_name; ?></h3>
                <p><a href="patient_profile.php?id=<?php echo $_SESSION['patient_details']['id']; ?>" style="color: white;font-size:20px;">Update Profile&#9998;</a></p>
                <p><?php echo ucfirst($_SESSION['patient_details']['gender']); ?>, <?php echo $age; ?> Years</p>
                <p><?php echo $_SESSION['patient_details']['email']; ?></p>
            </div>
            
            <hr class="divider" />
            
            <div class="patient-report">
                <div class="row">
                    <div class="col-xs-6">
                        <img src="../img/weight.svg" alt="">
                        <p>Weight <span> <?php if($weight){echo $weight;} else{ echo " -- ";} ?> kg</span></p>
                    </div>
                    <div class="col-xs-6">
                        <img src="../img/height.svg" alt="">
                        <p>Height <span><?php if($height){echo $height;} else{ echo " -- ";} ?> cm</span></p>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <img src="../img/weight.svg" alt="">
                        <p>BMI <span><?php if($bmi){echo $bmi;} else{ echo " -- ";} ?> kg/m2</span></p>
                    </div>
                    <div class="col-xs-6">
                        <img src="../img/bp.svg" alt="">
                        <p>BP <span><?php if($bp){echo $bp;} else{ echo " -- ";} ?> mmHg</span></p>
                    </div>                    
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <img src="../img/pulse.svg" alt="">
                        <p>Blood Group <span><?php if($blood){echo $blood;} else{ echo " -- ";} ?></span></p>
                    </div>
                     <div class="col-xs-6">
                        <img src="../img/pulse.svg" alt="">
                        <p>Pulse <span><?php if($pulse){echo $pulse;} else{ echo " -- ";} ?> bpm</span></p>
                    </div>
                    <!-- <div class="col-xs-6">
                        <img src="../img/temperature.svg" alt="">
                        <p>temperature <span> -- °C</span></p>
                    </div>    -->                 
                </div>                                
            </div>
            
            
            <hr class="divider" />
            
                <ul class="sidebar-menu clearfix">
                <li ><a href="messages.php" class="lab">Chat with Doctor</a></li>
                <li ><a href="reschedule_appointment.php" class="reschedule">Reschedule Appointments</a></li>
                <li ><a href="events_list.php" class="events">Events</a></li>
                <li ><a href="files_list.php" class="lab">Files</a></li>
            </ul>
        </aside>