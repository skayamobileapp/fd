<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$pid = $_SESSION['patient_details']['id'];

$date = date("Y-m-d");
$sql = "Select d.*, e.id as eid, e.title from doctor_details as d INNER JOIN events as e on d.id=e.doctor_id
 where date(e.start)>=CURRENT_DATE and e.status='0' and e.patient_id='$pid' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $doctor[$i]['id'] = $row['id'];
  $doctor[$i]['doctor_name'] = $row['doctor_name'];
  $doctor[$i]['mobile'] = $row['mobile'];
  $doctor[$i]['email'] = $row['email'];
  $doctor[$i]['title'] = $row['title'];
  $doctor[$i]['eid'] = $row['eid'];
  $i++;
}

$sql = "Select d.*, e.id as eid, e.title, e.start from doctor_details as d INNER JOIN events as e on d.id=e.doctor_id
 where e.patient_id='$pid' and e.status='1'";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $success[$i]['id'] = $row['id'];
  $success[$i]['doctor_name'] = $row['doctor_name'];
  // $success[$i]['photo'] = '../uploads/'.$row['photo'];
  $success[$i]['mobile'] = $row['mobile'];
  $success[$i]['start'] = $row['start'];
  $success[$i]['email'] = $row['email'];
  $success[$i]['title'] = $row['title'];
  $success[$i]['eid'] = $row['eid'];
  $i++;
}
$count=0;
$pid = $_SESSION['patient_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Patient' and id_user='$pid') and  patient_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
</head>

<body>     
    <?php include('navbar.php');?>

    <div class="container-fluid main-wrapper">
      <div class="row">
        <?php include ('sidebar.php'); ?>
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
                <h3 class="clearfix">Reports List From Doctors</h3>               
                <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Doctor Consered</th>
                            <th>Appointment Reason</th>
                            <th>Appointment Date</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th colspan="2">Prescriptions</th>
                            <!-- <th>Bill Amount</th> -->
                        </tr>
                        </thead>
                        <tbody>
                       <?php
                        for($i=0; $i<count($success); $i++) {
                            $n = $i+1;
                            echo "<tr>
                                    <td>$n</td>
                                     <td>".strtoupper($success[$i]['doctor_name'])."</td>
                                     <td>".ucfirst($success[$i]['title'])."</td>
                                     <td>".date('d-M-Y',strtotime($success[$i]['start']))."</td>
                                     <td>".$success[$i]['mobile']."</td>
                                     <td>".strtoupper($success[$i]['email'])."</td>
                                     <td><a href='medical_reports.php?id=".$success[$i]['eid']."' class='btn btn-primary'>View</a></td>
                                     <td><a href='prescriptions_pdf.php?id=".$success[$i]['eid']."' class='btn btn-primary'>Download</a></td>
                                 </tr>";
                            }
                            
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
           
        </section>
      </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
  </body>
</html>