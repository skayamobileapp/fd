<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
date_default_timezone_set("Asia/Kolkata");

$date = DateTime::createFromFormat('U.u', microtime(TRUE));
$date->setTimeZone(new DateTimeZone('Asia/Kolkata'));
$date = $date->format('dmY_His_u');


$patients=[];
if ($id = $_SESSION['patient_details']['id']) {

    $sql      = "select * from patient_details where id='$id' ";
    $result   = $conn->query($sql);
    $patients = $result->fetch_assoc();
   }

if($_POST)
{

    if($_SESSION['patient_details']['id'])
    {
      $id = $_SESSION['patient_details']['id'];
      $fname = ucfirst($_POST['fname']);
      $mname = ucfirst($_POST['mname']);
      $lname = ucfirst($_POST['lname']);
      
      $patientName = $fname." ".$mname." ".$lname;
      $patientPhoneNumber = $_POST['patient_phone_number'];
      $patientMobileNumber = $_POST['patient_mobile_number'];
      $email = $_POST['email'];
      $address1 = $_POST['address1'];
      $dateOfBirth = $_POST['date_of_birth'];
      // $card = $_POST['card'];
      $state = $_POST['state'];
      $city = $_POST['city'];
      $pincode = $_POST['pincode'];
      $socialSecurityIdentity = $_POST['social_security_identity'];
      $gender = $_POST['gender'];
      $maritalStatus = $_POST['martial_status'];
      $patientEmployer = $_POST['patient_employer'];
      $employmentStatus = $_POST['employment_status'];
      $emergencyContact = $_POST['emergency_contact'];
      $relationshipToPatient = $_POST['relationship_to_patient'];
      $address2 = $_POST['address2'];
      $cardtype = $_POST['card'];

      $Profile = $_FILES['ProfilePhoto']['name'];

    if($Profile=="")
    {
         $Profile = $patients['photo'];
    }
    else
    {
        $Profile = $_FILES['ProfilePhoto']['name'];
        $img_tmp =$_FILES['ProfilePhoto']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($Profile);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $Profile = 'PatPic_'. $date . '.'. $extension;

        $file_path = $upload_path . 'PatPic_' . $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $sql = "UPDATE patient_details SET patient_name ='$patientName', first_name ='$fname', last_name = '$lname', patient_phone_number = '$patientPhoneNumber', mobile_number = '$patientMobileNumber', email = '$email', address1 = '$address1', date_of_birth ='$dateOfBirth', social_security_or_identity_num = '$socialSecurityIdentity', gender = '$gender', martial_status = '$maritalStatus', state='$state', city='$city', pincode='$pincode', patient_employer ='$patientEmployer', employment_status = '$employmentStatus', emergency_contact = '$emergencyContact', relationship_to_patient = '$relationshipToPatient', address2 = '$address2', photo='$Profile', card_type='$cardtype' WHERE id = '$id' ";

      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Profile data updated successfully')</script>";
        echo "<script>parent.location='index.php'</script>";
        // $message= "<div class='alert alert-success'>Please Login Again to Reflect the Changes <a href='logout.php'> <u>continue</u></a> <button type='button' class='btn btn-default pull-right' data-dismiss='alert'>X</button></div>";
      }
    }
}

$sql = "SELECT * FROM martial_status";
$result = $conn->query($sql);
$martialList = array();
while ($row = $result->fetch_assoc()) {
    array_push($martialList, $row);
  }

$sql = "SELECT * FROM employment_status";
$result = $conn->query($sql);
$empstatusList = array();
while ($row = $result->fetch_assoc()) {
    array_push($empstatusList, $row);
  }

  $sql   = "SELECT id, state FROM states";
$result = $conn->query($sql);
$stateList = array();
while ($row = $result->fetch_assoc()) {
    array_push($stateList, $row);
}


?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<?php include('main-sidebar.php'); ?>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-body">
									
									<!-- Profile Settings Form -->
									<form action="" method="POST" id="myform" enctype="multipart/form-data">
										<div class="row form-row">
											<div class="col-12 col-md-12">
												<div class="form-group">
													<div class="change-avatar">
														<div class="profile-img">
															<img src="../uploads/<?php echo $patients['photo']; ?>" >
														</div>

														<div class="upload-img">
															<div class="change-photo-btn">
																<span><i class="fa fa-upload"></i> Upload Photo</span>
																<input type="file" name="ProfilePhoto" class="upload">
															</div>
															<small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB </small>
														</div>
													</div>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Full Name <span class="text-danger"></span></label>
													<input type="text" class="form-control" name="patient_name"  value="<?php echo $patients['patient_name']; ?>" readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>First Name <span class="text-danger">*</span></label>
													<input type="text" class="form-control" name="fname"  value="<?php echo $patients['first_name']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Middle Name <span class="text-danger"></span></label>
													<input type="text" class="form-control" name="mname"  value="<?php echo $patients['middle_name']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Last Name <span class="text-danger">*</span></label>
													<input type="text" class="form-control" name="lname"  value="<?php echo $patients['last_name']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Patient's Home Phone Number <span class="text-danger">*</span></label>
													<input type="text" class="form-control" name="patient_phone_number" maxlength="10" value="<?php echo $patients['patient_phone_number']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Email ID <span class="text-danger">*</span></label>
													<input type="email" class="form-control" name="email"  value="<?php echo $patients['email']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Mobile <span class="text-danger">*</span></label>
													<input type="text" class="form-control" name="patient_mobile_number" maxlength="10" value="<?php echo $patients['mobile_number']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Date of Birth <span class="text-danger">*</span></label>
													<div class="cal-icon">
														<input type="text" class="form-control datetimepicker" name="date_of_birth"  value="<?php echo date("d/m/Y", strtotime($patients['date_of_birth'])); ?>" id="date_of_birth" autocomplete="off">
													</div>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Gender <span class="text-danger">*</span></label>
													<select name="gender" class="form-control selitemIcon">
														<option value="">SELECT</option>
														<option value="Male" <?php if($patients['gender']=='Male'){ echo "selected";} ?>>MALE</option>
														<option value="Female" <?php if($patients['gender']=='Female'){ echo "selected";} ?>>FEMALE</option>
														<option value="Others" <?php if($patients['gender']=='Others'){ echo "selected";} ?>>OTHERS</option>
													</select>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Martial Status <span class="text-danger">*</span></label>
													<select name="martial_status" id="martial_status" class="form-control selitemIcon">
						                            <option value="">SELECT</option>
						                              <?php
						                              for($i=0; $i<count($martialList); $i++){ ?>
						                                <option value="<?php echo $martialList[$i]['martialStatus']; ?>" 
						                              <?php
						                                if ($martialList[$i]['martialStatus']==$patients['martial_status'])
						                                {
						                                  echo "selected=selected";
						                                }?> ><?php echo $martialList[$i]['martialStatus']?></option>
						                              <?php
						                                }

						                              ?>
						                          </select>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Blood Group <span class="text-danger">*</span></label>
													<select class="form-control selitemIcon">
														<option value="">SELECT</option>
														<option value="a-" <?php if($patients['blood_group']=='a-'){ echo "selected"; }?>>A-</option>
														<option value="a+" <?php if($patients['blood_group']=='a+'){ echo "selected"; }?>>A+</option>
														<option value="b-" <?php if($patients['blood_group']=='b-'){ echo "selected"; }?>>B-</option>
														<option value="b+" <?php if($patients['blood_group']=='b+'){ echo "selected"; }?>>B+</option>
														<option value="ab-" <?php if($patients['blood_group']=='ab-'){ echo "selected"; }?>>AB-</option>
														<option value="ab+" <?php if($patients['blood_group']=='ab+'){ echo "selected"; }?>>AB+</option>
														<option value="o-" <?php if($patients['blood_group']=='o-'){ echo "selected"; }?>>O-</option>
														<option value="o+" <?php if($patients['blood_group']=='o+'){ echo "selected"; }?>>O+</option>
													</select>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Social Security Number/ Identity <span class="text-danger">*</span></label>
													<input type="text" class="form-control" name="social_security_identity"  value="<?php echo $patients['social_security_or_identity_num']; ?>" maxlength="16">
												</div>
											</div>
											<div class="col-12">
												<div class="form-group">
												<label>Address <span class="text-danger">*</span></label>
													<input type="text" class="form-control" name="address1"  value="<?php echo $patients['address1']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Patient's Employer <span class="text-danger"></span></label>
													<input type="text" class="form-control" name="patient_employer"  value="<?php echo $patients['patient_employer']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Employment Status <span class="text-danger"></span></label>
													<select name="employment_status" id="employment_status" class="form-control selitemIcon">
				                                  <option value="">SELECT</option>
				                                <?php
				                                  for($i=0; $i<count($empstatusList); $i++){ ?>
				                                    <option value="<?php echo $empstatusList[$i]['name']; ?>"
				                                      <?php
				                                      if ($empstatusList[$i]['name']==$patients['employment_status'])
				                                      {
				                                        echo "selected=selected";
				                                      }?> ><?php echo $empstatusList[$i]['name']?></option>
				                                  <?php
				                                  }

				                                  ?>
				                                  ?>
				                                </select>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Emergency Contact <span class="text-danger"></span></label>
													<input type="text" class="form-control" name="emergency_contact" maxlength="10" value="<?php echo $patients['emergency_contact']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Relationship to Patient <span class="text-danger"></span></label>
													<input type="text" class="form-control" name="relationship_to_patient"  value="<?php echo $patients['relationship_to_patient']; ?>">
												</div>
											</div>
											<div class="col-12">
												<div class="form-group">
													<label>Address 2 <span class="text-danger"></span></label>
													<input type="text" class="form-control" name="address2"  value="<?php echo $patients['address2']; ?>">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>State <span class="text-danger">*</span></label>
													<select name="state" class="form-control selitemIcon" id="state" onchange="getCities()">
			                                      <option value=''>SELECT STATE</option>
			                                        <?php
			                                        for ($i=0; $i<count($stateList); $i++) {  ?>
			                                        <option value="<?php echo $stateList[$i]['id']; ?>" <?php if( $stateList[$i]['id'] == $patients['state'])  { echo "selected=selected"; }
			                                        ?>><?php echo $stateList[$i]['state']; ?></option>
			                                        <?php
			                                        }
			                                        ?>
			                                    </select>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>City <span class="text-danger">*</span></label>
													<select class="form-control selitemIcon" name="city" id="citylist">
			                                      <option value="">SELECT CITY</option>
                                       
			                                    </select>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Postal Pincode <span class="text-danger">*</span></label>
													<input name="pincode" id="pincode" type="number" maxlength="6" value="<?php echo $patients['pincode']; ?>" class="form-control">
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Country</label>
													<input type="text" class="form-control" value="India" readonly>
												</div>
											</div>
											<div class="col-12 col-md-6">
												<div class="form-group">
													<label>Card Type <span class="text-danger">*</span></label>
													<select class="form-control selitemIcon" name="card" id="card" required>
						                          <option value="basic" <?php if($patients['card_type']=='basic'){ echo "selected"; }?>>Basic</option>
						                          <option value="gold" <?php if($patients['card_type']=='gold'){ echo "selected"; }?>>Gold</option>
						                          <option value="silver" <?php if($patients['card_type']=='silver'){ echo "selected"; }?>>Silver</option>
						                          <option value="platinum" <?php if($patients['card_type']=='platinum'){ echo "selected"; }?>>Platinum</option>
						                        </select>
												</div>
											</div>
										</div>
										<div class="submit-section">
											<button type="submit" class="btn btn-primary submit-btn float-right">Save Changes</button>
										</div>
									</form>
									<!-- /Profile Settings Form -->
									
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>		
			<!-- /Page Content -->
	   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Datetimepicker JS -->
		<script src="../assets/js/moment.min.js"></script>
		<script src="../assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
    getCities();
});

        function getCities(){
          var id = $("#state").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#citylist").html(result);
              var idcityselected = '<?php  echo $patients['city'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#citylist").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

        // function getPincode(){
        //   var id = $("#citylist").val();
        //   console.log(id);

        //   $.ajax({url: "getpincode.php?id="+id, success: function(result){
        //     $("#pincode").html(result);

        //      var pincodeselected = '<?php  echo $patients['pincode'];?>';
        //       if(pincodeselected!='' && pincodeselected!= null) {
        //          $("#pincode").val(pincodeselected);
        //       }
        //   }
        // });
          
        // }
    </script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">
   $(document).ready(function()
    {

         $("#myform").validate({
             rules : {

              fname : {
                required : true,
                accept : true
              },
              mname : {
                accept : true
              },
              lname : {
                required : true,
                accept : true
              },
              martial_status : "required",
              email : "required",
              address1 : "required",
              date_of_birth : "required",
              state : "required",
              city : "required",
              pincode : "required",
              relationship_to_patient : {
                accept : true
              },
              emergency_contact : {
                    number : true,
                    minlength : 10,
                    maxlength : 10
                },
              patient_mobile_number : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10
                },
                patient_phone_number : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10
                },
                social_security_identity : {
                    required : true
                }
            },
            messages : {

                fname : {
                  required : "<span> enter first name</span>",
                  accept : "<span> enter letters only</span>"
                },
                mname : {
                  accept : "<span> enter letters only</span>"
                },
                lname : {
                  required : "<span> enter last name</span>",
                  accept : "<span> enter letters only</span>"
                },
                 email : "<span> enter email Id</span>",
                 martial_status : "<span> Select martial status</span>",
                address1 : "<span> enter address</span>",
                date_of_birth : "<span> select date of birth</span>",
                state : "<span> select state</span>",
                city : "<span> select city</span>",
                pincode : "<span> select pincode</span>",
                relationship_to_patient : {
                  accept : "<span> enter letters only</span>"
                },
                emergency_contact : {
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit number</span>"
                },
                 patient_mobile_number : {
                    required : "<span> enter mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                patient_phone_number : {
                    required : "<span> enter phone number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                social_security_identity : {
                    required : "<span> enter social security identity number</span>"
                }
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>