<?php
include('connection/conn.php');
session_start();
include('encryption.php');
include('decryption.php');

  $doctorId = $_GET['id'];
  $from = $_GET['from'];
if($_POST)
{
    $doctorId = $_GET['id'];
    // $email = $_POST['email'];
    // $password = $_POST['password'];
    $mobile = $_POST['mobile'];
    $otp = rand(000000,999999);

    $sql = "UPDATE patient_details SET otp='$otp' WHERE mobile_number='$mobile' AND status=1";
    mysqli_query($conn, $sql);

    $sql1 = "SELECT * FROM patient_details WHERE mobile_number='$mobile' AND status=1";
      $result1 = mysqli_query($conn, $sql1);
      $row = mysqli_num_rows($result1);

      if ($row < 1) {
        echo "<script>alert('Your Mobile Number not Registered')</script>";
        echo "<script>parent.location='patient_login.php'</script>";
      }

    // Sms otp verification code
$mobileno = $mobile;
$message = urlencode("Your OTP verification code is ".$otp." ");

$ch = curl_init();

$url = "http://simplysmsit.com/submitsms.jsp?user=HEALTH&key=a9f3bd5447XX&mobile=$mobileno&message=$message&senderid=FSTDOC&accusage=1";

// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

// grab URL and pass it to the browser
curl_exec($ch);

// close cURL resource, and free up system resources
curl_close($ch);


// need to be Encrypted 
$number = $mobileno;
$docnum = encryption($number);
  $requestfrom = $_GET['from'];
  $requestfor = $_GET['with'];

        echo "<script>parent.location = 'patient-otp-checker.php?id=$docnum&reqfrom=$requestfrom&reqfor=$requestfor'</script>";

      // $sql = "SELECT * FROM patient_details WHERE mobile_number='$mobile' AND password='$password' AND status=1";
      // $result  = $conn->query($sql);
      // $patientList = array();
      // while ($row = $result->fetch_assoc())
      // {
      //   $_SESSION['patient_details'] = $row;
      //   array_push($patientList, $row);
      // }

    // if(empty($patientList))
    // {
    //   echo "<script>alert('Please enter the valid username and password');</script>";
    //   echo "<script>parent.location='patient_login.php'</script>";
    //   exit;
    // }
    // else
    //   if (empty($doctorId)) {
    //   echo "<script>parent.location='PatientDashboard/index.php'</script>";
    //   exit;
    //   }
    //   else{
    //     echo "<script>parent.location='PatientDashboard/bookappointment.php?id=$doctorId'</script>";
    // }
}
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body class="account-page">

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<?php include('navbar.php'); ?>
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					
					<div class="row">
						<div class="col-md-8 offset-md-2">
							
							<!-- Login Tab Content -->
							<div class="account-content">
								<div class="row align-items-center justify-content-center">
									<div class="col-md-7 col-lg-6 login-left">
										<img src="assets/img/login-banner.png" class="img-fluid" alt="Doccure Login">	
									</div>
									<div class="col-md-12 col-lg-6 login-right">
										<div class="login-header">
											<h3>Login <span>Patient</span></h3>
										</div>
									    <form action="" method="POST" id="loginform">
											<div class="form-group">
								                <label>Mobile Number</label>
								                <input name="mobile" type="text" class="form-control" autocomplete="off" id="mobile" maxlength="10" placeholder="Enter Mobile Number">
								            </div> 
                   
											<div class="text-right">
												<!-- <a href="#" data-toggle="modal" data-target="#myModal">Forgot password?</a> -->
											</div>
											<button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Login</button>
											<div class="login-or">
												<span class="or-line"></span>
												<span class="span-or">or</span>
											</div>
											<div class="row form-row social-login">
												<div class="col-6">
													<a href="#" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f mr-1"></i> Login</a>
												</div>
												<div class="col-6">
													<a href="#" class="btn btn-google btn-block"><i class="fab fa-google mr-1"></i> Login</a>
												</div>
											</div>
											<div class="text-center dont-have">Don’t have an account? <a href="patient_register.php">Register</a></div>
										</form>
									</div>
								</div>
							</div>
							<!-- /Login Tab Content -->
								
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->

			<div class="container" id="popup">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header"><br>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Set New Password Now</h4>
        </div>
        <div class="modal-body">
          <div class="form-group fg-float">
            <div class="fg-line">
              <input type="email" name="emailid" class="form-control" id="emailid" required style="width: 100%; " autocomplete="off">
              <label class="fg-label">Enter Your E-MailId / Mobile Number</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" data-dismiss="modal" id="setNow" onclick="setPassword()">Submit</button>
        </div>
      </div>
    </div>
  </div>
</div>

		<?php include('footer.php'); ?>
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		
	</body>
</html>