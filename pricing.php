<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="assets/js/script.js"></script>
  
  </head>
<script>
  $(document).ready(function(){
    $("#myModal").modal('show');
  });
</script>

  <body>
    <div id="myModal" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div class="" style="width:100%;">
                                 <div class="">
                                    <a href="doctor_register.php"><img src="img/FD BROCHURE - 1112020.jpg" width="570px" height="510px" /></a>
                                 </div>
                              </div>
            </div>
        </div>
    </div>
</div>


    <!-- Main Wrapper -->
    <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>
      <!-- Page Content -->
      <div class="content">
        <div class="container">

          
          <div class="comp-sec-wrapper">
          
            
            <!-- Tabs -->
            <section class="comp-section">
              <div class="comp-header">
                                <h2 class="comp-title text-center">Products Pricing</h2>
                                <!-- <div class="line"></div> -->
                            </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                  
                    <div class="card-body">
                      <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                        <li class="nav-item"><a class="nav-link active" href="#doctorsTabs" data-toggle="tab">Clinical Practise Management</a></li>
                        <li class="nav-item"><a class="nav-link" href="#patientsTabs" data-toggle="tab">Patient Health Management</a></li>
                        <li class="nav-item"><a class="nav-link" href="#pharmaTab" data-toggle="tab">Pharmacy Management</a></li>
                        <li class="nav-item"><a class="nav-link" href="#labTab" data-toggle="tab">Laboratory Management</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane show active" id="doctorsTabs">
                          Coming soon !!
                        </div>
                        <div class="tab-pane" id="patientsTabs">
                          Coming soon !!
                        </div>
                        <div class="tab-pane" id="pharmaTab">
                          Coming soon !!
                        </div>
                        <div class="tab-pane" id="labTab">
                          Coming soon !!
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!-- /Tabs -->

          </div>  

        </div>

      </div>    
      <!-- /Page Content -->

   <?php include('footer.php'); ?>
       
    </div>

    <!-- /Main Wrapper -->

    
    <script type="text/javascript">
      $(window).load(function(){
    // ..
    $('#myModal7').modal('show')

    // ..
});
    </script>
    
  </body>
</html>