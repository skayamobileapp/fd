<?php 
session_start();
$pid = $_SESSION['patient_details']['id'];
?>
	<!-- Header -->
			<header class="header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<nav class="navbar navbar-expand-lg header-nav">
					<!-- <div class="navbar-header">
						<a href="index.php" class="navbar-brand logo">
							<img src="logo.png" class="img-fluid" alt="Logo">
						</a>
					</div> -->
					<div class="main-menu-wrapper">
						<!-- <div class="menu-header">
							<a href="index.php" class="menu-logo">
								<img src="logo.png" class="img-fluid" alt="Logo">
							</a>
							<a id="menu_close" class="menu-close" href="javascript:void(0);">
								<i class="fas fa-times"></i>
							</a>
						</div> -->
						<ul class="main-nav">
							<li>
								<a href="index.php">
							<img src="logo.png" alt="Logo" width="135" height="50">
								</a>
							</li>
							<li>
								<a href="index.php">Home</a>
							</li>
							<li class="has-submenu">
								<a href="#">Company <i class="fas fa-chevron-down"></i></a>
								<ul class="submenu">
									<li><a href="about-us.php">About Us</a></li>
									<li><a href="why-firstdoctor.php">Why Firstdoctor</a></li>
									<!--<li><a href="#">Pricing</a></li>-->
									<li><a href="#">Team</a></li>
									<li><a href="career.php">Career</a></li>
								</ul>
							</li>	
							<li class="has-submenu">
								<a href="#">Solution <i class="fas fa-chevron-down"></i></a>
								<ul class="submenu">
									<!-- <li class="has-submenu">
										<a href="#">Doctors</a>
										<ul class="submenu">
											<li><a href="map-grid.html">Map Grid</a></li>
											<li><a href="map-list.html">Map List</a></li>
										</ul>
									</li> -->
									<li><a href="patient-provider.php">Patient</a></li>
									<li><a href="doctor-provider.php">Doctor</a></li>
								</ul>
							</li>	
								<li>
								<a href="pricing.php">Pricing</a>
							</li>
							<li>
								<a href="health-events.php">Health Events</a>
							</li>
							<li class="has-submenu">
								<a href="#">Search Providers <i class="fas fa-chevron-down"></i></a>
								<ul class="submenu">
									<li><a href="search.php">Doctor</a></li>
									<li><a href="search.php">Diagnostic</a></li>
									<li><a href="search.php">Pharmacy</a></li>
								</ul>
							</li>
						</ul>
					</div>		 
					<ul class="nav header-navbar-rht">
						 <li class="nav-item contact-item">
							<div class="header-contact-img">
								<i class="far fa-hospital fa-xs"></i>							
							</div>
							<div class="header-contact-detail">
								<p class="contact-header">Contact</p>
								<!--<p class="contact-info-header"><a href="tel: +918390749520"> +91 83 90 74 95 20</p>-->
								<p class="contact-info-header"><a href="tel: +919423239177"> +91 94 23 23 91 77</p>
							</div>
						</li>

						<?php
						if($pid>0){ ?>

						<li class="nav-item dropdown has-arrow logged-item">
							<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
								<span class="user-img">
									<img class="rounded-circle" src="uploads/<?php echo $_SESSION['patient_details']['photo']; ?>" width="31" alt="">
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="user-header">
									<div class="avatar avatar-sm">
										<img src="uploads/<?php echo $_SESSION['patient_details']['photo']; ?>" alt="" class="avatar-img rounded-circle">
									</div>
									<div class="user-text">
										<h6><?php echo $_SESSION['patient_details']['patient_name']; ?></h6>
										<p class="text-muted mb-0">Patient</p>
									</div>
								</div>
								<a class="dropdown-item" href="PatientDashboard/index.php">Dashboard</a>
								<a class="dropdown-item" href="PatientDashboard/patient-profile.php">Profile Settings</a>
								<a class="dropdown-item" href="patient_logout.php">Logout</a>
							</div>
						</li>
					<?php }
					else { ?>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle header-login" href="#" data-toggle="dropdown">Providers Login/Signup <i class="fas fa-chevron-down"></i> </a>
							<div class="dropdown-menu">
								<a href="doctor_login.php" class="dropdown-item">Doctors</a>
								<a href="patient_login.php" class="dropdown-item">Patient</a>
								<!-- <a href="#" class="dropdown-item">Pharmacy</a> -->
								<!-- <a href="#" class="dropdown-item">Company</a> -->
							</div>
						</li>
					<?php } ?>
						<!-- /User Menu -->
					</ul>
				</nav>
			</header>
			<!-- /Header -->
			