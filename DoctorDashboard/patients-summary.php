<?php
include('../connection/conn.php');
include('session_check.php');
$did = $_SESSION['doctor_details']['id'];
$date = date("Y-m-d");

// echo "Select p.id, p.patient_name, p.email, p.mobile_number, e.id as eid, e.title, max(e.start) as start from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)<='$date' and e.status='1' and e.doctor_id='$did' group by p.id order by eid desc";
$select = mysqli_query($conn,"Select p.id, p.patient_name, p.email, p.mobile_number, max(e.id) as eid, e.title, max(e.start) as start from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)<='$date' and e.status='1' and e.doctor_id='$did' group by p.id order by p.patient_name asc");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['patient_name'] = $row['patient_name'];
  $view[$i]['email'] = $row['email'];
  $view[$i]['mobile_number'] = $row['mobile_number'];
  $view[$i]['start'] = $row['start'];
  $view[$i]['title'] = $row['title'];
  $view[$i]['eid'] = $row['eid'];
  $view[$i]['photo'] = $row['photo'];
  $i++;
}

$sql ="SELECT * FROM med_certificate";
$result = mysqli_query($conn, $sql);
  $i = 0;
  $certList = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($certList, $row);
  }



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Patient Summary </h3>

								
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
					                               <th>Patient Name</th>
					                               <th>Contact Number</th>
													<th>Appt Date</th>
					                               <th>Prescriptions</th>
					                               <th>Medical Certificate</th>
					                               <th>History</th>
												</tr>
											</thead>
											<tbody>
												<?php

               for ($i=0; $i <count($view) ; $i++) {
                $n =$i+1;
                ?>
                <tr>
                  <td><?php echo strtoupper($view[$i]['patient_name']) ; ?></td>
                  <td><?php echo $view[$i]['mobile_number']; ?></td>
                  <td><?php echo date("d M Y", strtotime($view[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($view[$i]['start'])); ?></span></td>
                 <td><?php echo "<a  class='btn btn-primary' href='view-prescriptions.php?id=". $view[$i]['eid']."'> View </a>";?></td>
                 <td><?php echo "<a  class='btn btn-primary 'href='medical-certificate.php?id=". $view[$i]['id']."'>Generate</a>";?></td>
                  <td><?php echo "<a  class='btn btn-primary 'href='view-history.php?id=". $view[$i]['id']."'>History</a>";?></td>

               </tr>
               <?php

             }
             ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>