<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$eventid = $_GET['id'];
$did = $_SESSION['doctor_details']['id'];

// $select = mysqli_query($conn,"SELECT ec.*, el.id as eid, el.event_name, el.event_fromdatetime, el.event_todatetime, el.location, c.city, c.state From event_customer ec inner join events_list el on el.id = ec.id_eventlist inner join cities c on c.id = el.city  where ec.id_eventlist = '$eventid' and el.id_doctor = '1' order by ec.name ASC");
// $i=0;
// $eventCustomer = [];
// while ($row = mysqli_fetch_array($select))
//     {
//         array_push($eventCustomer,$row);
//     }
$select = "SELECT ec.*, el.id as eid, el.event_name, el.event_fromdatetime, el.event_todatetime, el.location, c.city, c.state From event_customer ec inner join events_list el on el.id = ec.id_eventlist inner join cities c on c.id = el.city  where ec.id_eventlist = '$eventid' and el.id_doctor = '$did' order by ec.name ASC";
  $resultcity = mysqli_query($conn,$select);

$i=0;
$events = array();

   while ($row=mysqli_fetch_assoc($resultcity))
  {
     if($row['interest_no'] == 1)
     {
       $row['name'] = $row['name'];
     }
     else
     {
       $row['interest_no'] = $row['interest_no'] - 1;
       $row['name'] = $row['name'] . "   & +".$row['interest_no'];       
     }
     $row['event_fromdatetime']   = date("d-m-Y", strtotime($row['event_fromdatetime']));
     $row['event_todatetime']   = date("d-m-Y", strtotime($row['event_todatetime']));
     $row['created_date']   = date("d-m-Y", strtotime($row['created_date']));
     $events[$i]['location'] = $row['location'] . ", " . $row['city'];
     $events[$i] = $row;
     $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
 
    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
       <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Customer List<a href="events.php" class="btn btn-primary pull-right btn-lg">Back</a></h3>               
                <div class="table-responsive theme-table v-align-top">
                    <table class="table" id="example">
                       <thead>
                           <tr>
                              <th>Name</th>
                              <th>Phone</th>
                              <th>Email</th>
                              <th>Registered Date</th>
                           </tr>
                       </thead>
                        <tbody>
                           <?php
            
            for ($j=0; $j <count($events) ; $j++) { 
              ?>
              <tr>
                <td><?php echo ucfirst($events[$j]['name']); ?></td>
                <td><?php echo $events[$j]['phone']; ?></td>
                <td><?php echo $events[$j]['email']; ?></td>
                <td><?php echo $events[$j]['created_date'] ; ?></td>
               

              </tr>
              <?php

            }
            ?>                                                                                                           
                        </tbody>
                    </table>                   
                </div>               
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="../js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function() {
    $('#example').dataTable( {
    language: {
        searchPlaceholder: "Search Event by Name, Location"
    }
});
});
</script> 
</body>

</html>