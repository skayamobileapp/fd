<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$eventid = $_GET['id'];
$did = $_SESSION['doctor_details']['id'];

// $select = mysqli_query($conn,"SELECT ec.*, el.id as eid, el.event_name, el.event_fromdatetime, el.event_todatetime, el.location, c.city, c.state From event_customer ec inner join events_list el on el.id = ec.id_eventlist inner join cities c on c.id = el.city  where ec.id_eventlist = '$eventid' and el.id_doctor = '1' order by ec.name ASC");
// $i=0;
// $eventCustomer = [];
// while ($row = mysqli_fetch_array($select))
//     {
//         array_push($eventCustomer,$row);
//     }
$select = "SELECT ec.*, el.id as eid, el.event_name, el.event_fromdatetime, el.event_todatetime, el.location, c.city, c.state From event_customer ec inner join events_list el on el.id = ec.id_eventlist inner join cities c on c.id = el.city  where ec.id_eventlist = '$eventid' and el.id_doctor = '$did' order by ec.name ASC";
  $resultcity = mysqli_query($conn,$select);

$i=0;
$events = array();

   while ($row=mysqli_fetch_assoc($resultcity))
  {
     if($row['interest_no'] == 1)
     {
       $row['name'] = $row['name'];
     }
     else
     {
       $row['interest_no'] = $row['interest_no'] - 1;
       $row['name'] = $row['name'] . "   & +".$row['interest_no'];       
     }
     $row['event_fromdatetime']   = date("d-m-Y", strtotime($row['event_fromdatetime']));
     $row['event_todatetime']   = date("d-m-Y", strtotime($row['event_todatetime']));
     $row['created_date']   = date("d-m-Y", strtotime($row['created_date']));
     $events[$i]['location'] = $row['location'] . ", " . $row['city'];
     $events[$i] = $row;
     $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
		<div class="col-md-7 col-lg-8 col-xl-9">
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Customer List <a href="events-list.php" class="btn btn-primary float-right btn-lg">Back</a></h3><br>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
                          <th>SL. NO</th>
                          <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email Id</th>
                            <th>Registered Date</th>
												</tr>
											</thead>
											<tbody>
												<?php
            
            for ($j=0; $j <count($events) ; $j++) { 
              ?>
              <tr>
                <td><?php echo $j+1; ?></td>
                <td><?php echo ucfirst($events[$j]['name']); ?></td>
                <td><?php echo $events[$j]['phone']; ?></td>
                <td><?php echo $events[$j]['email']; ?></td>
                <td><?php echo $events[$j]['created_date'] ; ?></td>
               

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
	
    </body>
</html>