<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$sessionid = session_id();
$doctorId = $_SESSION['doctor_details']['id'];

$EventId = $_GET['id'];

$select1 = mysqli_query($conn,"SELECT p.id as pid, p.patient_name, p.mobile_number, e.id, e.title, e.start FROM patient_details AS p INNER JOIN events AS e ON e.patient_id = p.id WHERE e.id='$EventId' ");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $patient_name = $row1['patient_name'];
  $patient_id = $row1['pid'];
  $mobile_number = $row1['mobile_number'];
  $title = ++$row1['title'];
  }

  $select2 = mysqli_query($conn,"SELECT * FROM payment_table WHERE patient_id='$patient_id' ORDER BY id DESC LIMIT 1");
while ($row2 = mysqli_fetch_assoc($select2)) {
  $bal_amount = $row2['balance_amount'];
  }
if($bal_amount == ''){
	$bal_amount = '0';
}

if($_POST)
{
  $prescrip = mysqli_query($conn,"SELECT * FROM temp_prescrip WHERE session_id='$sessionid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($prescrip)) {
    $fetch[$i]['drug_type'] = $row['drug_type'];
    $fetch[$i]['drug_name'] = $row['drug_name'];
        $fetch[$i]['drugname'] = $row['drugname'];

    $fetch[$i]['duration'] = $row['duration'];
    $fetch[$i]['repeat_same'] = $row['repeat_same'];
    $fetch[$i]['time_of_the_day'] = $row['time_of_the_day'];
    $fetch[$i]['to_be_taken'] = $row['to_be_taken'];
    $fetch[$i]['date'] = $row['date'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $drug_type = $fetch[$i]['drug_type'];
    $drug_name = $fetch[$i]['drug_name'];
    $drugname = $fetch[$i]['drugname'];
    $duration = $fetch[$i]['duration'];
    $repeat_same = $fetch[$i]['repeat_same'];
    $time_of_the_day = $fetch[$i]['time_of_the_day'];
    $to_be_taken = $fetch[$i]['to_be_taken'];
    $date = $fetch[$i]['date'];
    $patient_id = $fetch[$i]['patient_id'];
    // $eventId = $fetch[$i]['event_id'];

  $insertPrescrip = "INSERT INTO prescrip(drug_type, drug_name, duration, repeat_same, time_of_the_day, to_be_taken, patient_id, appointment_on, event_id, drugname) VALUES ('$drug_type', '$drug_name', '$duration', '$repeat_same', '$time_of_the_day', '$to_be_taken', '$patient_id', '$date', '$EventId','$drugname')";

    $result = mysqli_query($conn,$insertPrescrip);
    }

    $lab = mysqli_query($conn,"SELECT * FROM temp_labdetails WHERE session_id='$sessionid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($lab)) {
    $fetch[$i]['lab_name'] = $row['lab_name'];
    $fetch[$i]['test_name'] = $row['test_name'];
    $fetch[$i]['date'] = $row['date'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $labName = $fetch[$i]['lab_name'];
    $testName = $fetch[$i]['test_name'];
    $date = $fetch[$i]['date'];
    $patient_id = $fetch[$i]['patient_id'];

   $insertLab = "INSERT INTO labdetails(lab_name, test_name, patient_id, appointment_on, event_id) VALUES ('$labName', '$testName', '$patient_id', '$date', '$EventId')";
    $result = mysqli_query($conn,$insertLab);
    }

    $allergy = mysqli_query($conn,"SELECT * FROM temp_allergies WHERE session_id='$sessionid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($allergy)) {
    $fetch[$i]['allergy_type'] = $row['allergy_type'];
    $fetch[$i]['allergy_name'] = $row['allergy_name'];
    $fetch[$i]['date'] = $row['date'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $AllergyType = $fetch[$i]['allergy_type'];
    $AllergyName = $fetch[$i]['allergy_name'];

    $date = $fetch[$i]['date'];
    $patient_id = $fetch[$i]['patient_id'];

    $insertallergy = "INSERT INTO event_allergies(allergy_type, allergy_name,event_id) VALUES ('$AllergyType', '$AllergyName', '$EventId')";
    $result = mysqli_query($conn,$insertallergy);
    }

$sessionid = session_id();

    $files_query = mysqli_query($conn,"SELECT * FROM temp_fileUpload WHERE session_id='$sessionid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($files_query)) {
    $fetch_file[$i]['file_name'] = $row['file_name'];
    $fetch_file[$i]['uploaded_file'] = $row['uploaded_file'];
    $fetch_file[$i]['event_id'] = $row['event_id'];
    $i++;
  }

  for($i=0; $i<count($fetch_file); $i++){
    $filename = $fetch_file[$i]['file_name'];
    $file = $fetch_file[$i]['uploaded_file'];
    $eventId = $fetch_file[$i]['event_id'];
    $date = date('Y-m-d');

   $insertfile = "INSERT INTO fileUpload(file_name, uploaded_file, patient_id, appointment_on, event_id) VALUES ('$filename', '$file', '$patient_id', '$date', '$eventId')";
    $result = mysqli_query($conn,$insertfile);
    }

    $refer_query = mysqli_query($conn,"SELECT * FROM temp_refer WHERE session_id='$sessionid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($refer_query)) {
    $fetch_refer[$i]['doctor_id'] = $row['doctor_id'];
    $fetch_refer[$i]['refer_to'] = $row['refer_to'];
    $fetch_refer[$i]['refer_note'] = $row['refer_note'];
    $fetch_refer[$i]['patient_id'] = $row['patient_id'];
    $fetch_refer[$i]['event_id'] = $row['event_id'];
    $fetch_refer[$i]['doctor_name'] = $row['doctor_name'];
    $fetch_refer[$i]['doctor_mobile'] = $row['doctor_mobile'];
    $fetch_refer[$i]['doctor_address'] = $row['doctor_address'];
    $fetch_refer[$i]['is_other'] = $row['is_other'];
    $i++;
  }

  for($i=0; $i<count($fetch_refer); $i++){
    $did = $fetch_refer[$i]['doctor_id'];
    $refered = $fetch_refer[$i]['refer_to'];
    $referNote = $fetch_refer[$i]['refer_note'];
    $pid = $fetch_refer[$i]['patient_id'];
    $eventId = $fetch_refer[$i]['event_id'];
    $doctorName = $fetch_refer[$i]['doctor_name'];
    $doctorMobile = $fetch_refer[$i]['doctor_mobile'];
    $doctorAddress = $fetch_refer[$i]['doctor_address'];
    $isOther = $fetch_refer[$i]['is_other'];
    $date = date('Y-m-d');

   $refer = "INSERT INTO refer_table(doctor_id, refer_to, refer_note, patient_id, event_id, doctor_name, doctor_mobile, doctor_address, is_other) VALUES('$did', '$refered', '$referNote', '$pid', '$eventId', '$doctorName', '$doctorMobile', '$doctorAddress', '$isOther')";
    $result = mysqli_query($conn,$refer);
    }

    $event_query = mysqli_query($conn,"SELECT * FROM vitals WHERE event_id='$EventId' ORDER BY id DESC LIMIT 1 ");
  $i=0;
  while ($row = mysqli_fetch_assoc($event_query)) {
    $fetch_event[$i]['height'] = $row['height'];
    $fetch_event[$i]['weight'] = $row['weight'];
    $fetch_event[$i]['bp'] = $row['bp'];
    $fetch_event[$i]['pulse'] = $row['pulse'];
    $fetch_event[$i]['sugar'] = $row['sugar'];
    $fetch_event[$i]['bmi'] = $row['bmi'];
    $i++;
  }

  for($i=0; $i<count($fetch_event); $i++){
    $height = $fetch_event[$i]['height'];
    $weight = $fetch_event[$i]['weight'];
    $pulse = $fetch_event[$i]['pulse'];
    $bp = $fetch_event[$i]['bp'];
    $sugar = $fetch_event[$i]['sugar'];
    $bmi = $fetch_event[$i]['bmi'];
    // $date = date('Y-m-d');

   $eventupdate = "UPDATE events SET height='$height', weight='$weight', bp='$bp', sugar='$sugar', bmi='$bmi', pulse='$pulse' WHERE id='$EventId' AND doctor_id='$doctorId' "; 
    $result = mysqli_query($conn,$eventupdate);
    }

  $amount = $_POST['amount'];
  if ($amount == '') {
    echo "<script>alert('ENTER AMOUNT')</script>";
    echo "<script>parent.location='add-prescriptions.php?id=$EventId'</script>";
    return false;
  }
  $eventupdateamount = "UPDATE events SET amount='$amount' WHERE id='$EventId'"; 
    $result = mysqli_query($conn,$eventupdateamount);


  $appoint_date = $_POST['appoint_date'];
  $next_appointment = $_POST['nxt_appoint_date'];
  $more_prescrip = $_POST['desc'];
  $EventId = $_GET['id'];
  $doctorId = $_SESSION['doctor_details']['id'];
  $complaint = $_POST['complaints'];
  $observe = $_POST['observations'];
  $diagnosis = $_POST['diagnosis'];
  
  $sign = $_FILES['sign']['name'];
  $tmpsign = $_FILES['sign']['tmp_name'];
  move_uploaded_file($tmpsign, "../uploads/".$sign);
  $doctorId = $_SESSION['doctor_details']['id'];


  mysqli_query($conn, "INSERT INTO complaints(complaint, event_id) VALUES ('$complaint','$EventId')");
  
  mysqli_query($conn, "INSERT INTO observations(obervation, event_id) VALUES ('$observe','$EventId')");

  mysqli_query($conn, "INSERT INTO diagnosis(diagnosis,event_id) VALUES ('$diagnosis','$EventId')");

  mysqli_query($conn, "INSERT INTO doc_signatures(signature, event_id, doctor_id) VALUES ('$sign','$EventId', '$doctorId')");

    $insert = mysqli_query($conn,"INSERT INTO prescriptions(appoint_date, nxt_appoint, doctor_id, more_prescrip, event_id)VALUES('$appoint_date', '$next_appointment', '$doctorId', '$more_prescrip', '$EventId')");
    

    if ($insert)
    {
      // $sql = mysqli_query($conn, "SELECT * FROM Vitals SET status='1' WHERE id='$EventId'");

      $sql = mysqli_query($conn, "UPDATE events SET status='1' WHERE id='$EventId'");
        echo "<script>alert('Prescriptions added successfully');</script>";
        echo "<script>parent.location='index.php'</script>";
    }
    else
    {
        echo "error".mysqli_error($conn);
    }
}

$sql = "SELECT * FROM doctor_details WHERE id !='$doctorId'";
$doctorList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($doctorList, $row);
}

$sql = "SELECT * FROM lab_details";
$labList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($labList, $row);
}

$sql = "SELECT * FROM drug_type ORDER BY id ASC";
$drugTypeList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($drugTypeList, $row);
}

$sql = "SELECT * FROM allergy_types";
$allergyTypes = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($allergyTypes, $row);
}

      mysqli_query($conn, "DELETE FROM temp_prescrip WHERE session_id='$sessionid'");
      mysqli_query($conn, "DELETE FROM temp_labdetails WHERE session_id='$sessionid'");
      mysqli_query($conn, "DELETE FROM temp_allergies WHERE session_id='$sessionid'");
      mysqli_query($conn, "DELETE FROM temp_fileUpload WHERE session_id='$sessionid'");
      mysqli_query($conn, "DELETE FROM temp_refer WHERE session_id='$sessionid'");

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- Header -->
			<?php include('main-navbar.php'); ?>
			<!-- /Header -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
		<form action="" method="post" id="form" enctype="multipart/form-data">
			<div class="row">
			<div class="col-sm-4">
	          <div class="form-group">
	              <label>Appointment Date</label>
	              <input type="text" class="form-control" name="appoint_date" id="appoint_date" value="<?php echo date("Y-m-d h:m:s");?>" readonly>
	          </div>
	        </div>
			</div>
		<div class="row">
			<div class="col-md-12">
				<h4 class="mb-4">Add Prescriptions</h4>
				<div class="appointment-tab">
				
					<!-- Prescriptions Tab -->
					<ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
						<li class="nav-item">
							<a class="nav-link active" href="#prescrip" data-toggle="tab">Prescriptions</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#lab" data-toggle="tab">Lab</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#allergy" data-toggle="tab">Allergies</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#vital" data-toggle="tab">Vitals</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#files" data-toggle="tab">Files</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#refer" data-toggle="tab">Refer doctor</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#complaints" data-toggle="tab">Complaints</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#observations" data-toggle="tab">Observations</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#diagnosis" data-toggle="tab">Diagnosis</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#amount" data-toggle="tab">Amount</a>
						</li>
					</ul>
					<!-- /Prescriptions Tab -->
					
					<div class="tab-content">
					
					<!-- Add Prescriptions Tab -->
						<div class="tab-pane show active" id="prescrip">
                <p style="padding: 20px;"><a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary">Add Prescriptions</a></p>
			          <div id="viewPrescrip"></div>
							
						</div>
				
						<!-- /Add Prescriptions Tab -->

						<!-- Lab Tab -->
						<div class="tab-pane" id="lab">
			                <p style="padding: 20px;"><a href="#" data-toggle="modal" data-target="#myModal2" class="btn btn-primary">Add Lab Details</a></p>
					          <div id="viewLab"></div>
						</div>
						<!-- /Lab Tab -->
				   
						<!-- Allergies Tab -->
						<div class="tab-pane " id="allergy">
			                <p style="padding: 20px;"><a href="#" data-toggle="modal" data-target="#myModal3" class="btn btn-primary">Add Allergies</a></p>
				          <div id="viewAllergy"></div>
						</div>
						<!-- /Allergies Tab -->

						<!-- Vitals Tab -->
						<div class="tab-pane " id="vital">
			                <p style="padding: 20px;"><a href="#" data-toggle="modal" data-target="#myModal5" class="btn btn-primary">Add Vitals</a></p>
					        <div id="viewVitals"></div>
						</div>
						<!-- /Vitals Tab -->

						<!-- Files Tab -->
						<div class="tab-pane " id="files">
			                <p style="padding: 20px;"><a href="#" data-toggle="modal" data-target="#myModal4" class="btn btn-primary">Add File</a></p>
					        <div id="addfiles"></div>
						</div>
						<!-- /Files Tab -->

						<!-- Refer doctor Tab -->
						<div class="tab-pane " id="refer">
			                <p style="padding: 20px;"><a href="#" data-toggle="modal" data-target="#myModal6" class="btn btn-primary">Refer doctor</a></p>
					        <div id="viewDoctor"></div>
						</div>
						<!-- /Refer doctor Tab -->

						<!-- Complaints Tab -->
						<div class="tab-pane " id="complaints">
			                <div class="row">
					            <div class="col-sm-10">
					              <div class="form-group">
					                  <label style="padding: 10px">Complaints</label>
					                  <textarea class="form-control ckeditor" name="complaints"></textarea>
					              </div>
					            </div>
					        </div>
						</div>
						<!-- /Complaints Tab -->

						<!-- Observations Tab -->
						<div class="tab-pane " id="observations">
			                <div class="row">
					            <div class="col-sm-10">
					              <div class="form-group">
					                  <label style="padding: 10px">Observations</label>
					                  <textarea class="form-control ckeditor" name="observations"></textarea>
					              </div>
					            </div>
					        </div>
						</div>
						<!-- /Observations Tab -->

						<!-- Diagnosis Tab -->
						<div class="tab-pane " id="diagnosis">
			                <div class="row">
					            <div class="col-sm-10">
					              <div class="form-group">
					                  <label style="padding: 10px">Diagnosis</label>
					                  <textarea class="form-control ckeditor" name="diagnosis"></textarea>
					              </div>
					            </div>
					        </div>
						</div>
						<!-- /Diagnosis Tab -->

						<!-- Amount Tab -->
						<div class="tab-pane" id="amount">
				          <div class="row">
				            <div class="col-sm-4">
				              <div class="form-group">
				                  <label style="padding: 10px;">Amount</label>
				                  <input type='number' name='amount' id='amount' class="form-control" name="amount" />
				                </div>
				              </div>
				            </div>
				          </div>
						<!-- /Amount Tab -->
						
					</div>
					<div class="bttn-group float-right">
            <a href="index.php" class="btn btn-light">Cancel</a>
            <button class="btn btn-primary btn-lg" name="save" id="save">Save</button>
          </div>

					
				</div>
			</div>
		</div>
	</form>

						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->

                <div class="modal fade" id="myModal" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Detailed Prescription</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-5">
                                <div class="form-group">
                                    <label class="">Drug Type</label>
                                    <select class="form-control selitemIcon" name="drug" id="drugtype" style="width: 300px;" onchange="getDosage()">
                                          <?php 
                                          for ($i=0; $i<count($drugTypeList); $i++)
                                          {
                                            $id = $drugTypeList[$i]['id'];
                                            $dtype = $drugTypeList[$i]['type']; ?>
                                            <option value="<?php echo $id; ?>"><?php echo $dtype; ?></option>
                                            <?php
                                          } ?>
                                      </select>
                                </div>
                              </div>
                              <div class="col-sm-7" >
                                <div class="form-group">
                                    <label class="">Drug Name and Dosage</label>
                                    <select class="form-control selitemIcon" name="drugDose" id="drugDose" style="width: 300px;" onchange="showtextBox()">
                                          <option value="">SELECT DRUG AND DOSAGE</option>
                                      </select>
                                </div>
                              </div>
                            </div>
                            <div class="row" id="drugnameDiv" style="display: none;">
                              <div class="col-sm-9">
                                <div class="form-group">
                                    <label>Drug Name</label>
                                    <input type="text" class="form-control" name="drugname" id="drugname" autocomplete="off" placeholder="Drug Name">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Duration in days</label>
                                    <input type="number" class="form-control" name="duration" id="duration" autocomplete="off">
                                </div>
                              </div>
                              <div class="col-sm-8">
                              <label>Repeat</label>
                                <div class="form-group">
                                    <input type="radio" name="repeat_same" id="inlineRadio1" value="everyday"><span class="check-radio"></span> Everyday

                                    <input type="radio" name="repeat_same" id="inlineRadio2" value="alternate_days"><span class="check-radio"></span> Alternate days

                                  <!-- <label class="radio-inline">
                                    <input type="radio" name="repeat_same" id="inlineRadio3" value="specific_days"><span class="check-radio"></span> Specific days
                                  </label> -->
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-sm-5">
                                <label>Time of the day</label>
                                  <div class="form-group">
                                    <label class="checkbox-inline">
                                    <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox1" value="morning" checked="checked">
                                    <span class="check-radio"></span> Morning
                                  </label>
                                  <label class="checkbox-inline">
                                    <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox2" value="noon">
                                    <span class="check-radio"></span> Noon
                                  </label>
                                  <label class="checkbox-inline">
                                    <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox3" value="evening">
                                    <span class="check-radio"></span> Evening
                                  </label>
                                  <label class="checkbox-inline">
                                    <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox4" value="night">
                                    <span class="check-radio"></span> Night
                                  </label>
                                </div>
                              </div>
                              <div class="col-sm-4">
                                <label>To be Taken</label>
                                <div class="form-group">
                                  <label class="radio-inline">
                                  <input type="radio" name="to_be_taken" id="inlineRadio4" value="after_food">
                                  <span class="check-radio"></span> After Food
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" name="to_be_taken" id="inlineRadio5" value="before_food">
                                  <span class="check-radio"></span> Before Food
                                </label>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary btn-lg" id="addPrescrip" name="addPrescrip"> Add </button>
                    </div>
                  </div>
                </div>
            </div>

                <div class="modal fade" id="myModal2" role="dialog">
                  <div class="modal-dialog modal-md" style="width: 70%;">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Lab Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="">Lab Name</label>
                                    <select class="form-control selitemIcon" name="lab_name" id="lab_name" style="width: 300px;" onchange="getTests()">
                                      <option value="">SELECT LAB NAME</option>
                                      <?php 
                                      for ($i=0; $i<count($labList); $i++)
                                      {
                                        $id = $labList[$i]['id'];
                                        $name = $labList[$i]['lab_name']; ?>
                                        <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                        <?php
                                      } ?>
                                    </select>
                                </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="">Lab Test Name</label>
                                    <select class="form-control selitemIcon" name="test_name" id="test_name" style="width: 300px;">
                                        <option value="">SELECT TEST NAME</option>
                                    </select>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary btn-lg" id="addLab" name="addLab">Add</button>
                    </div>
                  </div>
                </div>
              </div>


                <div class="modal fade" id="myModal3" role="dialog">
                  <div class="modal-dialog modal-md" style="width: 70%;">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Allergies</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="">Allergy Type</label>
                                    <select class="form-control selitemIcon" name="allergy_type" id="allergytype" style="width: 300px;" onchange="getAllergies()">
                                      <option value="">SELECT ALLERGY TYPE</option>
                                      <?php 
                                      for ($i=0; $i<count($allergyTypes); $i++)
                                      {
                                        $id = $allergyTypes[$i]['id'];
                                        $atype = $allergyTypes[$i]['allergy_type']; ?>
                                        <option value="<?php echo $id; ?>"><?php echo $atype; ?></option>
                                        <?php
                                      } ?>
                                      </select>
                                </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="">Allergy Name</label>
                                    <select class="form-control selitemIcon" name="allergy" id="allergyname" style="width: 300px;">
                                          <option value="">SELECT ALLERGY NAME</option>
                                      </select>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary btn-lg" id="addAllergy" name="addAllergy">Add</button>
                    </div>
                  </div>
                </div>
              </div>


                <div class="modal fade" id="myModal5" role="dialog">
                  <div class="modal-dialog modal-md" style="width: 70%;">
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Enter Vitals</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
						  <span id="total" style="color: #a94442; font-family: 'SourceSansPro-Bold';"></span>
                        <div class="form-group fg-float">
                          <div class="fg-line">
                            <div class="row">
                              <div class="col-sm-3">
                                <div class="form-group">
                                    <label >Height (cm)</label>
                                    <input type="number" name="height" id="height" class="form-control" autocomplete="off">
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group">
                                    <label >Weight (kg)</label>
                                    <input type="number" name="weight" id="weight" class="form-control" autocomplete="off">
                                </div>
                              </div>
			                <div class="col-sm-2">
                                <div class="form-group">
                                    <label >Pulse</label>
                                    <input type="number" name="pulse" id="pulse" class="form-control" autocomplete="off">
                                </div>
                              </div>
                              <div class="col-sm-2">
                                <div class="form-group">
                                    <label>BP</label>
                                    <input type="number" name="bp" id="bp" class="form-control" autocomplete="off">
                                </div>
                              </div>
                              <div class="col-sm-2">
                                <div class="form-group">
                                    <label>Sugar</label>
                                    <input type="number" name="sugar" id="sugar" class="form-control" autocomplete="off">
                                </div>
                              </div>
                              <!-- <div class="col-sm-2">
                                <div class="form-group fg-float">
                                  <div class="fg-line fg-toggled">
                                    <select name="blood" id="blood" class="form-control">
                                      <option value="">SELECT</option>
                                      <option value="A+">A+</option>
                                      <option value="A-">A-</option>
                                      <option value="B+">B+</option>
                                      <option value="B-">B-</option>
                                      <option value="O+">O+</option>
                                      <option value="O-">O-</option>
                                      <option value="AB+">AB+</option>
                                      <option value="AB-">AB-</option>
                                    </select>
                                    <label class="fg-label">Blood Group</label>
                                  </div>
                                </div>
                              </div> -->
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal" id="addVitals" name="addVitals">Add</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="myModal4" role="dialog">
                  <div class="modal-dialog modal-md" style="width: 70%;">
                     <!-- Modal content -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Upload File</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label >File Name</label>
                                    <input type="text" name="file_name" id="file_name" class="form-control" autocomplete="off">
                                </div>
                              </div>
                              <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="">File Upload</label>
                                    <input type="file" class="form-control" name="file" id="file" required>
                                </div>
                              </div>
                            </div>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary btn-lg" id="addFile" name="addFile">Add</button>
                    </div>
                  </div>
                </div>
              </div>

                <div class="modal fade" id="myModal6" role="dialog">
                  <div class="modal-dialog modal-md">
                     <!-- Modal content -->
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Refer a Doctor</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                          <div class="fg-line">
                            <label >Refer Doctor From</label>
                            <div class="row">
                              <div class="col-sm-8">
                                  <label class="radio-inline radio-box-style">
                                  <input type="radio" name="choice" id="choice1" value="1" onchange="getfirst(this)" checked="checked"><span class="check-radio" ></span> FirstDoctor
                                  </label>
                                 <label class="radio-inline radio-box-style">
                                  <input type="radio" name="choice" id="choice2" value="2" onchange="getnew(this)"><span class="check-radio"></span> Add New Doctor
                                </label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-12" id="itemdiv">
                                <div class="form-group">
                                    <label class="">Select Doctor </label>
                                    <select class="form-control selitemIcon" name="doctor" id="doctor" style="width: 550px;">
                                      <option value="">SELECT DOCTOR</option>
                                      <?php 
                                      for ($i=0; $i<count($doctorList); $i++)
                                      {
                                        $id = $doctorList[$i]['id'];
                                        $name = $doctorList[$i]['doctor_name']; ?>
                                        <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                        <?php
                                      } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Refer Note</label>
                                    <input type="text" name="refer_note" id="refer_note" class="form-control" autocomplete="off">
                                </div>
                              </div>
                              <div class="col-sm-12" id="div2">
                                <div class="form-group">
                                    <label >Doctor Name</label>
                                    <input type="text" name="doctor_name" id="doctor_name" class="form-control" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone_number" id="phone_number" class="form-control" autocomplete="off" maxlength="10">
                                </div>
                                <div class="form-group">
                                    <label>Doctor Address</label>
                                    <input type="text" name="doctor_address" id="doctor_address" class="form-control" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label >Refer Note</label>
                                    <input type="text" name="refer_note" id="refer_note" class="form-control" autocomplete="off">
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary btn-lg" id="referto" name="referto" data-dismiss="modal">Refer</button>
                    </div>
                  </div>
                </div>
              </div>

		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>

		  <script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script type="text/javascript">

    function showtextBox() {
      $("#drugnameDiv").hide();

      var iddrug = $("#drugDose").val();
      if(iddrug =='Others') {
        $("#drugnameDiv").show();
      } else {
                $("#drugnameDiv").hide();

      }
    }
    $("#addPrescrip").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var dtype = $("#drugtype").val();
    if(dtype == 0) {
      alert("SELECT DRUG TYPE");
      return false;
    }

    var dose = $("#drugDose").val();
    if(dose == 0) {
      alert("SELECT DRUG NAME AND DOSAGE");
      return false;
    }
    var drugname = $("#drugname").val();
    var dose = $("#drugDose").val();
    if(dose== 'Others' && drugname=='') {
      alert("ENTER DRUG NAME");
      return false;
    }

    var dur = $("#duration").val();
    if(dur == '') {
      alert("ENTER DURATION");
      return false;
    }

    var repeat = $("input[name='repeat_same']:checked").val();
    var taken = $("input[name='to_be_taken']:checked").val();
    var time = [];
    $.each($("input[type='checkbox']:checked"), function(){
      time.push($(this).val());
    }); 
      var timing = time.join(",");
      $.ajax({
        // type: 'POST',
        url: 'addPrescription.php',
        data:{
          'drugType': dtype,
          'drugname':drugname,
          'drugDose': dose,
          'duration': dur,
          'taken': taken,
          'repeat': repeat,
          'time': timing,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
          $("#viewPrescrip").html(result);
             // $("#drugtype").val('0');
$("#drugtype").empty();//To reset cities
$("#drugtype").append('<?php for($i=0; $i<count($drugTypeList); $i++) { ?>' +
  '<option value="' +
  '<?php echo $drugTypeList[$i]['id']; ?>' +
  '">' +
  '<?php echo $drugTypeList[$i]['type']?>' +
  '</option>' +
  '<?php } ?>');
$("#drugDose").empty();
$("#drugDose").append('<option value="">SELECT DRUG NAME AND DOSAGE</option>');

        $("#duration").val(' ');
        $("#drugname").val(' ');
      $("#drugnameDiv").hide();
        // $("input[name='repeat_same']").val('');
        // $("input[name='to_be_taken']").val('');
      }
        });
      });
    </script>
    <script type="text/javascript">
    $("#addLab").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var labName = $("#lab_name").val();
    if(labName == '') {
      alert("SELECT LAB NAME");
      return false;
    }
    var testName = $("#test_name").val();
      $.ajax({
        // type: 'POST',
        url: 'add_labdetails.php',
        data:{
          'labName': labName,
          'testName': testName,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewLab").html(result);
        // $('#myModal2').close();
        $("#lab_name").empty();
        $("#lab_name").append('<option value="">SELECT LAB NAME</option>' 
          + '<?php for($i=0; $i<count($labList); $i++) { ?>' 
          + '<option value="' 
          + '<?php echo $labList[$i]['id']; ?>' +
            '">' 
          + '<?php echo $labList[$i]['lab_name']?>' +
            '</option>' +
            '<?php } ?>');
        $("#test_name").empty();
        $("#test_name").append('<option value="">SELECT TEST NAME</option>');
        // $("#lab_name").val('');
        // $("#test_name").val('');
      }
        });
      });
    </script>

    <script type="text/javascript">
    $("#addAllergy").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var allergy_type = $("#allergytype").val();
    if(allergy_type == '') {
      alert("SELECT ALLERGY TYPE");
      return false;
    }
    var allergy_name = $("#allergyname").val();
      $.ajax({
        // type: 'POST',
        url: 'add_allergies.php',
        data:{
          'allergy_type': allergy_type,
          'allergy_name': allergy_name,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewAllergy").html(result);
        // $("#myModal3").close();
        $("#allergytype").empty();
        $("#allergytype").append('<option value="">SELECT ALLERGY TYPE</option>' 
          + '<?php for($i=0; $i<count($allergyTypes); $i++) { ?>' 
          + '<option value="' 
          + '<?php echo $allergyTypes[$i]['id']; ?>' +
            '">' 
          + '<?php echo $allergyTypes[$i]['allergy_type']?>' +
            '</option>' +
            '<?php } ?>');
        $("#allergyname").empty();
        $("#allergyname").append('<option value="">SELECT ALLERGY NAME</option>');
        // $("#allergytype").val('');
        // $("#allergyname").val('');
      }
        });
      });
    </script>

<script type="text/javascript">
        $("#div2").hide();

      function getfirst(c1) {
        var cval = c1.value;
      if(cval =='1') {
        $("#itemdiv").show();
        $("#div2").hide();
      }
      else {
        $("#itemdiv").hide();
      }
    }
    function getnew(c2) {
        var cval = c2.value;
      if(cval =='2') {
        $("#div2").show();
        $("#itemdiv").hide();
      }
      else {
        $("#div2").hide();
        $("#itemdiv").show();
      }
    }
</script>
<script type="text/javascript">
    $("#weight").on('blur',function(){

        //BMI calculation
        var height = $("#height").val();
        var weight = $("#weight").val();
        var height = height/100;
        var height = height * height;
        var BMI = weight/height;
        var BMI = Math.round(BMI, 2);
        
        if (BMI < 15) {result = 'Very severely underweight'}
          else
        if (15 <= BMI && BMI<16) {result = 'Severely underweight'}
          else
        if (16 <= BMI && BMI<18.5) {result = 'Underweight'}
          else
        if (18.5 <= BMI && BMI<25) {result = 'Normal (healthy weight)'}
          else
        if (25 <= BMI && BMI<30) {result = 'Overweight'}
          else
        if (30 <= BMI && BMI<35) {result = 'Obese Class I (Moderately obese)'}
          else
        if (35 <= BMI && BMI<40) {result = 'Obese Class II (Severely obese)'}
          else
        if (BMI >=40) {result = 'Obese Class III (Very severely obese)'}
      
          $("#total").html("BMI : "+ BMI+" "+result);
          });
    </script>
  <script type="text/javascript">
    $("#referto").on('click',function(){
      var ch = $('input[name=choice]:checked').val();
    if (ch == '1') {
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var doctor = '<?php echo $_SESSION[doctor_details][id]; ?>';
    var referto = $("#doctor").val();
    if (referto == '') {
      alert('SELECT DOCTOR NAME');
      return false;
    }
    var refernote = $("#refer_note").val();
    // if (refernote == '') {
    //   alert('ENTER REFER NOTE');
    //   return false;
    // }
    var ch1 = $("#choice1").val();
    console.log(ch1);
      $.ajax({
        // type: 'POST',
        url: 'refer_doctor.php',
        data:{
          'referto': referto,
          'refernote': refernote,
          'doctor': doctor,
          'ch1': ch1,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewDoctor").html(result);
        // $("#myModal3").close();
        $("#doctor").val('');
      }
        });
      }

  if (ch == '2') {

    var ch2 = $("#choice2").val();
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var doctor = '<?php echo $_SESSION[doctor_details][id]; ?>';
    var dname = $("#doctor_name").val();
    if (dname == '') {
      alert('ENTER DOCTOR NAME');
      return false;
    }
    var phoneno = $("#phone_number").val();
    if (phoneno == '') {
      alert('ENTER DOCTOR PHONE NUMBER');
      return false;
    }
    var dadd = $("#doctor_address").val();
    if (dadd == '') {
      alert('ENTER DOCTOR ADDRESS');
      return false;
    }
    var refernote = $("#refer_note").val();
    // if (refernote == '') {
    //   alert('ENTER REFER NOTE');
    //   return false;
    // }
    console.log(ch2);
      $.ajax({
        // type: 'POST',
        url: 'refer_doctor.php',
        data:{
          'dname': dname,
          'refernote': refernote,
          'doctor': doctor,
          'ch2': ch2,
          'phoneno': phoneno,
          'dadd': dadd,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewDoctor").html(result);
        // $("#myModal3").close();
        $("#doctor_name").val('');
        $("#phone_number").val('');
        $("#doctor_address").val('');
        $("#refer_note").val('');
      }
        });
    }
});
</script>

    <script type="text/javascript">
      $(document).ready(function (e) {
          $('#addFile').on('click', function () {
              var eid = '<?php echo $EventId; ?>';
              var sid = '<?php echo $sessionid; ?>';
              var fileName = $("#file_name").val();
              if(fileName == ''){
                alert('ENTER FILE NAME');
                return false;
              }
              var file_data = $('#file').prop('files')[0];
              if(file_data == undefined){
                alert('UPLOAD FILE');
                return false;
              }
              var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('filename', fileName);
              form_data.append('sid', sid);
              form_data.append('eid', eid);

              $.ajax({
                  url: 'file_upload.php', // point to server-side PHP script 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#addfiles').html(response); // display success response from the PHP script
                      $("#file_name").val('');
                      $('#file').val('');
                  },
              });
          });
      });
    </script>

    <!-- <script type="text/javascript">
    $("#addFile").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var fileName = $("#file_name").val();
    var fileUploaded = $("input[type='file']").val();
      $.ajax({
        // type: 'POST',
        url: 'file_upload.php',
        data:{
          'fileName': fileName,
          'file': fileUploaded,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
         }
        });
    });

    </script> -->

    <script type="text/javascript">
    $("#addVitals").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var height = $("#height").val();
    if(height == '') {
        alert("ENTER PATIENT HEIGHT");
        return false;
        }
    var weight = $("#weight").val();
    if(weight == '') {
        alert("ENTER PATIENT WEIGHT");
        return false;
    }
    var pulse = $("#pulse").val();
    if(pulse == '') {
        alert("ENTER PATIENT PULSE");
        return false;
    }
    var bp = $("#bp").val();
    if(bp == '') {
        alert("ENTER PATIENT BP LEVEL");
        return false;
    }
    var sugar = $("#sugar").val();
    if(sugar == '') {
        alert("ENTER PATIENT SUGAR LEVEL");
        return false;
    }
    // var blood = $("#blood").val();
      $.ajax({
        // type: 'POST',
        url: 'add_Vitals.php',
        data:{
          'height': height,
          'weight': weight,
          'pulse': pulse,
          'bp': bp,
          'sugar': sugar,
          // 'blood': blood,
          'eid': eid,
        },
        success: function(result){
        $("#viewVitals").html(result);
        $("#myModal5").close(); }
        });
    });

    </script>


 <script type="text/javascript">

        function getDosage(){
          var id = $("#drugtype").val();
          console.log(id);

          $.ajax({url: "getDosageList.php?id="+id, success: function(result){
            $("#drugDose").html(result);
          }
        });
        }

        function getAllergies(){
          var id = $("#allergytype").val();
          console.log(id);

          $.ajax({url: "getallergies.php?id="+id, success: function(result){
            $("#allergyname").html(result);
          }
        });
        }


        function getTests(){
          var id = $("#lab_name").val();
          console.log(id);

          $.ajax({url: "get_lab_tests.php?id="+id, success: function(result){
            $("#test_name").html(result);
          }
        });
        }

    </script>
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>