<?php 
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];


$select = mysqli_query($conn,"SELECT * FROM patient_details  WHERE id = '$id'");
if ($row = mysqli_num_rows($select)) 
{
    while ($row = mysqli_fetch_array($select))
    {
        //from database fields
        $fname = $row['first_name'];
        $mname = $row['middle_name'];
        $lname = $row['last_name'];
        $phone = $row['patient_phone_number'];
        $mobile_number = $row['mobile_number']; 
        $DateOfBirth = $row['date_of_birth'];
        $email = $row['email'];
        $Password = $row['password'];
        $Gender = $row['gender'];
        $MaritalStatus = $row['martial_status'];
        $PatientEmployer = $row['patient_employer']; 
        $Pincode = $row['pincode'];
        $EmploymentStatus = $row['employment_status'];
        $photo = $row['photo'];
        $State = $row['state'];
        $City = $row['city'];
        $EmergencyContact = $row['emergency_contact'];
        $Recieveotp = $row['receive_otp'];
        $Address1 = $row['address1'];  
    }
}
$sql   = "select * from states";
$result = $conn->query($sql);
$stateList = array();
while ($row = $result->fetch_assoc()) {
    array_push($stateList, $row);
}

$sql   = "select * from cities";
$result = $conn->query($sql);
$cityList = array();
while ($row = $result->fetch_assoc()) {
    array_push($cityList, $row);
}

$select1 = mysqli_query($conn,"SELECT * FROM patient_details");  
$i=0;
    while ($row1 = mysqli_fetch_array($select1))
    {
       $view[$i]['id'] = $row1['id'];
       $view[$i]['martial_status'] = $row1['martial_status'];
       $view[$i]['employment_status'] = $row1['employment_status'];

        $i++;
    } 

if (isset($_POST['save'])) 
{
    //from names given in html
    
    $fname = $_POST['fname'];
  $midname = $_POST['mname'];
  $lname = $_POST['lname'];
  $patientName = $fname." ".$lname;
  $patientPhoneNumber = $_POST['patient_phone_number'];
  $patientMobileNumber = $_POST['patient_mobile_number'];
  $email = $_POST['email'];
  $address1 = $_POST['address1'];
  $dateOfBirth = $_POST['date_of_birth'];
  $gender = $_POST['gender'];
  $maritalStatus = $_POST['martial_status'];
  $pincode = $_POST['pincode'];
  $state = $_POST['state'];
  $city = $_POST['city'];
  $patientEmployer = $_POST['patient_employer'];
  $employmentStatus = $_POST['employment_status'];
  $emergencyContact = $_POST['emergency_contact'];
  $password = $_POST['password'];

  $recieveotp = $_POST['recieveotp'];

  $Profile = $_FILES['photo']['name'];
  if ($Profile == "") {
      $Profile = $photo;
  }
  else
  {
    $Profile = $_FILES['photo']['name'];
    $img_tmp =$_FILES['photo']['tmp_name'];
    move_uploaded_file($img_tmp,"../uploads/".$Profile);
  }


    $query = "UPDATE  patient_details SET patient_name ='$patientName', patient_phone_number = '$patientPhoneNumber', mobile_number = '$patientMobileNumber', email = '$email', address1 = '$address1', date_of_birth ='$dateOfBirth', first_name = '$fname', middle_name = '$mname', gender = '$gender', martial_status = '$maritalStatus', patient_employer ='$patientEmployer', employment_status = '$employmentStatus', emergency_contact = '$emergencyContact', password = '$password', receive_otp = '$recieveotp', state ='$state', city ='$city', photo='$Profile', pincode='$pincode' WHERE id = '$id'";
    
    $update = mysqli_query($conn,$query);

    if ($update) 
    {
        echo "<script>alert('Patient details updated successfully');</script>";
        echo "<script>parent.location='view_patient_details.php'</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>  
    <script src="../js/main.js"></script>
</head>
    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>

 <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              patient_name : "required",
              email : "required",
              address1 : "required",
              date_of_birth : "required",
                martial_status : "required",
                patient_employer : "required",
                relationship_to_patient : "required",
                address2 : "required",

                  patient_phone_number : {
                    required : true,
                    number : true,
                    minlength : 11,
                    maxlength : 11

                },
                emergency_contact : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
                patient_mobile_number : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
         
            },
            messages : {

                patient_name : "<span>Please enter patient name</span>",
                 email : "<span>Please enter email</span>",
                address1 : "<span>Please enter address1</span>",
                date_of_birth : "<span>Please enter date of birth</span>",
                martial_status : "<span>Please martial_status</span>",
                patient_employer : "<span>Please enter patient_employer </span>",
                relationship_to_patient : "<span>Please enter relationship to patient</span>",
                 address2 : "<span>Please enter address2</span>",
                
                 patient_phone_number : {
                    required : "<span>Please enter the phone number</span>",
                    number : "<span>Please enter number only</span>",
                    minlength : "<span>Please enter 11 digit number</span>",
                    maxlength : "<span>Please enter max 11 digit  number</span>"
                },
                emergency_contact : {
                    required : "<span>Please enter the emergency number</span>",
                    number : "<span>Please enter number only</span>",
                    minlength : "<span>Please enter 10 digit number</span>",
                    maxlength : "<span>Please enter max 10 digit  number</span>"
                },
                 patient_mobile_number : {
                    required : "<span>Please enter the mobile number</span>",
                    number : "<span>Please enter number only</span>",
                    minlength : "<span>Please enter 10 digit number</span>",
                    maxlength : "<span>Please enter max 10 digit  number</span>"
                }
            }
        })
    })
</script>

<body> 
<form action="" method="post" id="form" enctype="multipart/form-data">    
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
        <aside class="col-sm-4 col-lg-3 sidebar">
            <?php include('menu.php'); ?>
            
            <ul class="sidebar-menu clearfix">
                <li><a href="index.php" class="dashboard">Dashboard</a></li>
                <li ><a href="today_appointments.php" class="patient">Today Appointments</a></li>
                <li class="active"><a href="view_patient_details.php" class="patient">Patients List</a></li>
                <li><a href="view_patient_summary.php" class="patient">Patient Summary</a></li>
                <li><a href="patient_bill_summary.php" class="patient">Patient Bill Summary</a></li>
                <li><a href="view_doctor_availability.php" class="doctor">Doctor's Clinic Schedule</a></li>
                <li><a href="appointmentReschedule.php" class="patient">Reschedule Appointments</a></li>
                <li><a href="messages.php" class="lab">Messages from patient</a></li>
                <li><a href="events.php" class="healthcard">Events</a></li>
                
                <?php include('list.php') ?>
            </ul>       
        </aside>
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
                <ol class="breadcrumb">
                  <li><h4>Patient Registration</h4></li>
                </ol>              
               <h3 class="clearfix">Edit Patient</h3>               
                    <div class="card">
                        <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line">
                              <input type="text" class="form-control" name="fname"  maxlength="20" id="fname" value="<?php echo $fname; ?>">
                              <label class="fg-label">First Name<span class="error">*</span></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line">
                              <input type="text" class="form-control" name="mname" id="mname" maxlength="20" value="<?php echo $mname; ?>">
                              <label class="fg-label">Middle Name <span class="error">*</span></label>
                            </div>
                          </div>                            
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line">
                              <input type="text" class="form-control" name="lname"  maxlength="20" id="lname" value="<?php echo $lname; ?>">
                              <label class="fg-label">Last Name<span class="error">*</span></label>
                            </div>
                          </div>
                        </div>
                      </div>
                <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="email" class="form-control" name="email" maxlength="30" value="<?php echo $email; ?>">
                          <label class="fg-label">E-Mail Id<span class="error">*</span></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="password" class="form-control" name="password" required="required" maxlength="10" value="<?php echo $Password; ?>">
                          <label class="fg-label left">Password<span class="error">*</span></label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="patient_phone_number" maxlength="11" value="<?php echo $phone; ?>">
                                <label class="fg-label">Patient's Home Phone Number<span class="error">*</span></label>
                            </div>
                        </div>                            
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="text" class="form-control" name="patient_mobile_number" maxlength="10" value="<?php echo $phone; ?>">
                          <label class="fg-label">Mobile Number<span class="error">*</span></label>
                        </div>
                      </div>                            
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <input type="date" class="form-control" name="date_of_birth" value="<?php echo $DateOfBirth; ?>">
                          <label class="fg-label">Date of Birth<span class="error">*</span></label>
                        </div>
                      </div>                         
                    </div>                        
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="text" class="form-control" name="address1" maxlength="150" value="<?php echo $Address1; ?>">
                          <label class="fg-label">Address 1<span class="error">*</span></label>
                        </div>
                    </div>                         
                  </div>                                           
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <select name="state" class="form-control" id="state" onchange="getCities()">
                          <option value=''>Select</option>
                          <?php
                          for ($i=0; $i<count($stateList); $i++) {  ?>
                            <option value="<?php echo $stateList[$i]['id']; ?>" <?php if ($State == $stateList[$i]['id']) { echo "selected=selected";}?> >
                                <?php echo $stateList[$i]['state']; ?></option>
                            <?php
                          }
                          ?>
                        </select>
                        <label class="fg-label">State<span class="error">*</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line fg-toggled">
                              <select class="form-control" name="city" id="citylist">
                                <?php
                          for ($i=0; $i<count($cityList); $i++) {  ?>
                            <option value="<?php echo $cityList[$i]['id']; ?>" <?php if ($City == $cityList[$i]['id']) { echo "selected=selected";}?> >
                                <?php echo $cityList[$i]['city']; ?></option>
                            <?php
                          }
                          ?>

                              </select>
                              <label class="fg-label">City<span class="error">*</span></label>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line">
                              <input type="text" class="form-control" name="pincode" maxlength="6" value="<?php echo $Pincode; ?>">
                              <label class="fg-label">Pin Code<span class="error">*</span></label>
                            </div>
                          </div>
                        </div>
                      </div>   
                      <div class="row">
                        <div class="col-sm-4"> 
                          <div class="form-group fg-float">
                            <div class="fg-line">
                              <select name="martial_status" id="martial_status" class="form-control"  value="<?php echo $MaritalStatus; ?>">
                                    <?php for ($i=0; $i <count($view); $i++) { 

                                        $id   =  $view[$i]['id'];
                                        $martial_status =  $view[$i]['martial_status'];
                                        ?>
                                        <option value="<?php echo $martial_status; ?>"<?php if( $MaritalStatus == $martial_status)  { echo "selected=selected"; }
                                        ?>
                                        > <?php echo $martial_status; ?> </option>
                                    <?php } ?>
                                </select>
                              <label class="fg-label">Martial Status<span class="error">*</span></label>
                            </div>
                          </div>
                        </div> 
                      <div class="col-sm-4">
                       <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="text" class="form-control" name="patient_employer" maxlength="20" value="<?php echo $PatientEmployer;?>">
                          <label class="fg-label">Patient’s Employer<span class="error">*</span></label>
                        </div>
                      </div>                         
                    </div>
                    <div class="col-sm-4"> 
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <label class="fg-label">Employment Status</label>
                          <select name="employment_status" id="employment_status" class="form-control">
                            <?php for ($i=0; $i <count($view); $i++) { 

                                $id   =  $view[$i]['id'];
                                $employment_status =  $view[$i]['employment_status'];
                                ?>
                                <option value="<?php echo $employment_status; ?>"<?php if( $EmploymentStatus == $employment_status)  { echo "selected=selected"; }
                                ?>
                                > <?php echo $employment_status; ?> </option>
                            <?php } ?>
                          </select>
                        </div>
                      </div> 
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                     <div class="form-group fg-float">
                      <div class="fg-line">
                        <input type="text" class="form-control" name="emergency_contact" maxlength="10" value="<?php echo $EmergencyContact; ?>">
                        <label class="fg-label">Emergency Contact<span class="error">*</span></label>
                      </div>
                    </div>                         
                  </div>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <label>
                            <!--  <input type="radio">
                             <span class="check-radio"></span> -->
                             Send OTP to <span class="error">*</span></label><br>
                             <label class="radio-inline">
                              <input type="radio" name="recieveotp" id="recieveotp" value="1" <?php if( $Recieveotp ==1) { echo "checked";} ?>><span class="check-radio" ></span> Mobile Number
                            </label>
                            <label class="radio-inline">
                             <input type="radio" name="recieveotp" id="recieveotp" value="2" <?php if( $Recieveotp ==2) { echo "checked";} ?>><span class="check-radio"></span> Cell Phone Number
                           </label>
                           <label class="radio-inline">
                             <input type="radio" name="recieveotp" id="recieveotp" value="3" <?php if( $Recieveotp ==3) { echo "checked";} ?>><span class="check-radio"></span> Alternate Number
                           </label>
                         </div>
                       </div>
                     </div>
                    <div class="row">
                     <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <label class="fg">Upload Profile Photo<span class="error">*</span></label>
                          <input type="file" class="form-control" name="photo">
                        </div>
                      </div>                            
                    </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                           <label>
                            <!--  <input type="radio">
                             <span class="check-radio"></span> -->
                             Gender
                           </label>
                           <label class="radio-inline">
                             <input type="radio" name="gender" id="gender"   value="male" <?php if( $Gender =='male') { echo "checked";} ?>><span class="check-radio"></span> M
                           </label>
                           <label class="radio-inline">
                             <input type="radio" name="gender" id="gender"   value="female" <?php if( $Gender =='female') { echo "checked";} ?>><span class="check-radio"></span> F
                           </label>
                           <label class="radio-inline">
                             <input type="radio" name="gender" id="gender"   value="other" <?php if( $Gender =='other') { echo "checked";} ?>><span class="check-radio"></span> Other
                           </label>                                
                       </div>
                   </div>
               </div>

                    <div class="bttn-group">
                        <a href="view_patient_details.php" class="btn btn-link">Cancel</a>
                        <button class="btn btn-primary btn-lg" name="save" id="save">Save</button>
                    </div>                                         
                </div>           
            </div>
        </section>
      </div>
    </div>    
    </form>  
    <script type="text/javascript">

        function getCities(){
          var id = $("#state").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#citylist").html(result);
          }
        })
        }

    </script> 
</body>

</html>