<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);
$date = date("Y-m-d");
$did = $_SESSION['doctor_details']['id'];
$sql = "Select p.*, e.id as eid, e.title, e.start from patient_details as p INNER JOIN events as e on p.id=e.patient_id
 where date(e.start)='$date' and e.status='0' and e.doctor_id='$did' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $patient[$i]['id'] = $row['id'];
  $patient[$i]['patient_name'] = $row['patient_name'];
  $patient[$i]['photo'] = '../uploads/'.$row['photo'];
  $patient[$i]['mobile_number'] = $row['mobile_number'];
  $patient[$i]['email'] = $row['email'];
  $patient[$i]['title'] = $row['title'];
  $patient[$i]['start'] = $row['start'];
  $patient[$i]['eid'] = $row['eid'];
  $i++;
}
$sql = "Select p.*, e.id as eid, e.title, e.start from patient_details as p INNER JOIN events as e on p.id=e.patient_id
 where date(e.start)='$date' and e.status='1' and e.doctor_id='$did' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $success[$i]['id'] = $row['id'];
  $success[$i]['patient_name'] = $row['patient_name'];
  $success[$i]['photo'] = '../uploads/'.$row['photo'];
  $success[$i]['mobile_number'] = $row['mobile_number'];
  $success[$i]['email'] = $row['email'];
  $success[$i]['title'] = $row['title'];
  $success[$i]['start'] = $row['start'];
  $success[$i]['eid'] = $row['eid'];
  $i++;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
         
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Today's Appointments </h3>               
                <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Patient Name</th>
                            <th>Appointment Reason</th>
                            <th>Appointment Date</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>Add Prescriptions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for($i=0; $i<count($patient); $i++) {
                            $pid = $patient[$i]['id'];
                            $pname = $patient[$i]['patient_name'];
                            $photo = $patient[$i]['photo'];
                            $n = $i+1;
                            echo "<tr>
                                    <td>$n</td>
                                     <td>".strtoupper($patient[$i]['patient_name'])."</td>
                                     <td>".ucfirst($patient[$i]['title'])."</td>
                                     <td>".date("dM Y H:i:a",strtotime($patient[$i]['start']))."</td>
                                     <td><a href='tel:".$patient[$i]['mobile_number']."'>".$patient[$i]['mobile_number']."</a></td>
                                     <td><a href='mailto:".$patient[$i]['email']."'>".strtoupper($patient[$i]['email'])."</a></td>
                                     <td><a href='prescriptions.php?id=".$patient[$i]['eid']."' class='btn btn-primary'>Add Prescriptions</a></td>
                                 </tr>";
                            }
                           
                        ?>
                        </tbody>
                    </table>
                </div>
                <h3 class="clearfix">Today's Completed Appointments</h3>               
                <div class="table-responsive theme-table v-align-top">
                    <table class="table" >
                        <thead>
                        <tr><th>SL. NO</th>
                            <th>Patient Name</th>
                            <th>Appointment Reason</th>
                            <th>Appointment Date</th>
                            <th>Mobile</th>
                            <!-- <th>Email</th> -->
                            <th>Prescriptions</th>
                            <th>Pay Bill</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        for($i=0; $i<count($success); $i++) {
                            $pid = $success[$i]['id'];
                            $pname = $success[$i]['patient_name'];
                            $photo = $success[$i]['photo'];
                            $n = $i+1;
                            echo "<tr>
                                    <td>$n</td>
                                     <td>".strtoupper($success[$i]['patient_name'])."</td>
                                     <td>".ucfirst($success[$i]['title'])."</td>
                                     <td>".date("dM Y H:i:a",strtotime($success[$i]['start']))."</td>
                                     <td><a href='tel:".$success[$i]['mobile_number']."'>".$success[$i]['mobile_number']."</a></td>
                                     <td><a href='view_prescriptions.php?id=".$success[$i]['eid']."' class='btn btn-primary'>View Prescriptions</a></td>
                                     <td><a href='bill_payment.php?id=".$success[$i]['id']."' class='btn btn-primary'>Payment</a></td>
                                 </tr>";
                            }
                            
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
           
        </section>
      </div>
    </div> 
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>      
</body>

</html>