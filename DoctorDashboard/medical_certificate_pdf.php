<?php
include('../connection/conn.php');
include('session_check.php');

date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('d-m-Y');
$did = $_SESSION['doctor_details']['id'];

$mid = $_GET['id'];
$pid = $_GET['pid'];


$sql="SELECT e.id as eid, e.title, e.start, c.clinic_name from events as e INNER JOIN clinic_registration as c ON c.id=e.id_clinic where e.status='1' and e.doctor_id='$did' and e.patient_id='$pid' order by eid desc limit 1";

$result = mysqli_query($conn,$sql);
while ($row=mysqli_fetch_assoc($result)) {
  $event_id = $row['eid'];
  $title = $row['title'];
  $start = $row['start'];
  $clinic_name = $row['clinic_name'];
}

 $sql="SELECT m.from_date, m.to_date, m.description, d.doctor_name, d.address, d.pincode, d.logo, c.city, s.state, p.patient_name FROM med_certificate m INNER JOIN doctor_details d ON m.id_doctor=d.id INNER JOIN patient_details p ON m.id_patient=p.id INNER JOIN cities c ON d.city=c.id INNER JOIN states s ON d.state=s.id WHERE m.id='$mid' ";

$result = mysqli_query($conn,$sql);
while ($row=mysqli_fetch_assoc($result)) {
  $med_desc = $row['description'];
  $from_date = date("d-m-Y", strtotime($row['from_date']));
  $to_date = date("d-m-Y", strtotime($row['to_date']));
  $doctor_name = $row['doctor_name'];
  $dlogo = $row['logo'];
  $patient_name = strtoupper($row['patient_name']);
  $address = $row['address'];
  $pincode = $row['pincode'];
  $city = $row['city'];
  $state = $row['state'];
  $med_desc = $row['description'];
}

$currentDate = date('d-m-Y');
        $fromDate = $from_date;
        $medicalReason = $med_desc;
        $currentTime = date('h:i:s a');
        
        $logo = '<img src="'.$dlogo.'"/>';
        // <font size='2'></font>
        // $name = $patient_name;

        $file_data = $file_data ."
    <table width='100%'>
      <tr>
      <th style='text-align: right' width='40%'>DATE :&nbsp; $date</th>
      </tr>
  </table>
  <table width='100%'>
  <tr>
  <td width='100%' style='text-align: center'><font color='blue' size='5'><b><u>MEDICAL CERTIFICATE</u></font></td>
  </tr>
  </table>
  <table>
  <tr>
    <td style='text-align:left;'><br><br></td>
    <td style='text-align:right;'></td>
  <td style='text-align:right;'></td>
  </tr>
  
   <tr>
    <th style='text-align: center' width='50%'></th>
    <th style='text-align: right' width='40%'></th>
  <td style='text-align: center' width='10%'></td>
  </tr>

   <tr>
    <th style='text-align: center' width='50%'></th>
  <td style='text-align: center' width='10%'></td>
  </tr>
  </table>
  
  <table width='80%' align='center'>
  <tr>
    <td style='text-align:left;'><br><br></td>
  </tr>
   <tr>
     <th  style='text-align: center' width='100%' colspan='3'>To Whom It May Concern</th>
   </tr>
</table>
  <table width='100%' >
      <tr>
        <td style='text-align: right' width='20%' >".$logo."</td>
      </tr>
    </table>
  <br><br>

  <table width='80%' align='center'>
  <tr>
    <td style='text-align:left;'><br><br></td>
  </tr>
   
   <tr>
    <td>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  THIS IS TO CERTIFY that <u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    $patient_name    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </u> &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;
&nbsp; &nbsp; Was Examined and Treated at the  &nbsp;&nbsp; <u> &nbsp;&nbsp; $clinic_name &nbsp;&nbsp; </u> &nbsp;&nbsp; On  &nbsp;&nbsp; <u> &nbsp;&nbsp;  $from_date  &nbsp;&nbsp; </u> &nbsp;&nbsp;   To  &nbsp;&nbsp; <u> &nbsp;&nbsp;  $to_date  &nbsp;&nbsp; </u> &nbsp;&nbsp;  with the Following Diagnosis: 
      
    </td>
   </tr>
   <tr>
    <td style='text-align:left;'><br>$medicalReason<br><hr></td>
  </tr>
   <tr>
    <td style='text-align:left;'><br><br></td>
  </tr>
  <tr>
    <td style='text-align:left;'><br><br></td>
  </tr>
   </table>
   
   <table width='100%'>

  <tr>

  <th style='text-align: center' width='50%'></th>
    <th style='text-align: right' width='40%'><u> &nbsp;&nbsp;$doctor_name&nbsp;&nbsp; </u></th>
  <th style='text-align: center' width='10%'></th>
  </tr>

   <tr>
   <th style='text-align: center' width='50%'></th>
    <th style='text-align: right' width='40%'>(Attending Physician)</th>
  <th style='text-align: center' width='10%'></th>
  </tr>
</table>";

$logo = $_SERVER['DOCUMENT_ROOT']."/uploads/logo.png";

$currentDate = date('d_M_Y_H_i_s');

include("../library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetHeader("<table>
                  <tr>
            <td><img src='$logo' height='32px;'>
                   </td>
                  </tr>
                  </table>");
$mpdf->WriteHTML($file_data);
$filename = $patient_name. "_" .$currentDate.".pdf";
$mpdf->Output($filename, 'D');
exit;




// $logo = $_SERVER['DOCUMENT_ROOT']."/uploads/logo.png";


// $mpdf=new mPDF();
// $mpdf->SetHeader("<table>
//                   <tr>
//             <td><img src='$logo' height='32px;'>
//                    </td>
//                   </tr>
//                   </table>");

// $filename = $patient_name. "_" .$currentDate;

// $doc_root = $_SERVER['DOCUMENT_ROOT'] ."/download/". $filename.".pdf";

?>
<?php 
echo "<script>parent.location='patients-summary.php'</script>";
 ?>