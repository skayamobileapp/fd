<?php 
include('../connection/conn.php');
include('session_check.php');
date_default_timezone_set("Asia/Kolkata");

// $tdate = date('Y-m-d');
$fdate = date('Y-m-d');

  $did = $_SESSION['doctor_details']['id'];


$sql = "SELECT a.id, a.patient_name, a.mobile_number, a.email, a.address1 
        FROM patient_details as a INNER JOIN events as b ON b.patient_id=a.id WHERE b.doctor_id='$did' and date(b.start)>='$fdate' AND b.status =0";
$select = mysqli_query($conn,$sql);
$result = $conn->query($sql);
$patientList = array();
while ($row=$result->fetch_assoc())
{
    array_push($patientList, $row);
}


if($_POST)
{
  $did = $_SESSION['doctor_details']['id'];

  $fdate = trim($_POST['fdate']);
$startDateArray = explode('/',$fdate);
$mysqlStartDate = $startDateArray[2]."-".$startDateArray[1]."-".$startDateArray[0];
$fdate = $mysqlStartDate;

  $tdate = trim($_POST['tdate']);
$startDateArray = explode('/',$tdate);
$mysqlStartDate = $startDateArray[2]."-".$startDateArray[1]."-".$startDateArray[0];
$tdate = $mysqlStartDate;

  $pid = $_POST['name'];

  $sql="SELECT * FROM events WHERE date(start) >='$fdate' AND date(end) <='$tdate' AND doctor_id='$did' AND patient_id='$pid' AND status ='0'";
  $query = mysqli_query($conn,$sql);

}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0">Reshedule Appointments</h4>
                  
								</div>
								<div class="card-body">
								<form action="" method="POST" id="form">
									<div class="row">
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                          <label class="">Patient Name<span class="error"></span></label>
                        <select class="form-control selitemIcon" name="name" id="name">
                          <option value="">SELECT</option>
                          <?php
                          for ($i=0; $i <count($patientList); $i++) { ?>
                            <option value="<?php echo $patientList[$i]['id']; ?>" <?php if ($_POST['name'] == $patientList[$i]['id']){ echo "selected";} ?>
                            >
                              <?php echo strtoupper($patientList[$i]['patient_name']); ?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>From Date:</label><span class="error"></span>
                        <div class="cal-icon">
                          <input type="text" name="fdate" class="form-control datetimepicker" placeholder="Select Date" id="fdate" autocomplete="off" value="<?php echo $_POST['fdate']; ?>">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>To Date:</label><span class="error"></span>
                        <div class="cal-icon">
                          <input type="text" name="tdate" class="form-control datetimepicker" placeholder="Select Date" id="tdate" autocomplete="off" value="<?php echo $_POST['tdate']; ?>">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                        <input type="submit" name="getdata" class="btn btn-primary btn-lg float-right" id="getdata" value="GET DATA">
                        </div>
                  </div>
                </div>
              </form>

              <?php
                    if (isset($query))
                        {
                            $viewdata =[];
                            $i=0;
                            while($row=mysqli_fetch_assoc($query))
                            {
                                $viewdata[$i]['id']=$row['id'];
                                $viewdata[$i]['title']=$row['title'];
                                $viewdata[$i]['start']=$row['start'];
                                $viewdata[$i]['end']=$row['end'];
                                $viewdata[$i]['patient_id']=$row['patient_id'];
                                $viewdata[$i]['patient_name']=$row['patient_name'];
                                $i++;
                            } ?>
                            <h4>Appointment List To Reschedule</h4><br>
                            <div class="table-responsive theme-table v-align-top">
                              <table class="table">
                                <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Patient Name</th>
                                    <th>Appt Reason</th>
                                    <th>Appt Date</th>
                                    <th>Reschedule</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i=0; $i<count($viewdata); $i++) { ?>
                                    <tr><td><?php echo $i+1; ?></td>                          
                                        <td><?php echo strtoupper($viewdata[$i]['patient_name']); ?></td>
                                        <td><?php echo ucfirst($viewdata[$i]['title']);?></td>
                                        <td><?php echo date("d M Y", strtotime($viewdata[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($viewdata[$i]['start'])); ?></span></td>
                                        <!-- <td><?php echo "<a class='btn btn-primary' href='update_appointment.php?id=". $viewdata[$i]['id']."'>Reschedule Now</a>"; ?></td> -->
                                        <td><?php echo "<a class='btn btn-primary' href='reschedule-appointment.php?id=".$viewdata[$i]['id']."'>Reschedule Now</a>"; ?></td>

                                    </tr>
                                    <?php
                                }
                                ?>
                        </tbody>
                    </table>
                  </div>
                                  <?php
                                }
                        ?>
									
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>

		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
		
	</body>
</html>