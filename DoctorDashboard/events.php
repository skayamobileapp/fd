<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');

$did = $_SESSION['doctor_details']['id'];

$select = "SELECT e.*, c.city, c.state FROM events_list e inner join cities c on c.id = e.city where e.id_doctor='$did' and e.event_fromdatetime > CURRENT_DATE order by e.event_fromdatetime ASC";

$resultcity = mysqli_query($conn,$select);

  $i=0;
  $events = array();

   while ($row=mysqli_fetch_assoc($resultcity))
  {
     $event_id = $row['id'];

     $row['interest_no'] = "0";
     
     $j = 0;

    $sqlGetNo = "SELECT * from event_customer where id_eventlist = '$event_id' ";

     $resultNo          = $conn->query($sqlGetNo);

    while ($row_interest = $resultNo->fetch_assoc())
    {
      //echo json_encode("gfgf");exit;
      if($row_interest['interest_no'] == null)
      {
        $row_interest['interest_no'] = 0; 
      }

      $j = $j + $row_interest['interest_no'];
    }
     
    $n = number_format($j, 0, '', ',');
     $row['interest_no'] = $n;
     
     
     $row['event_fromdatetime']   = date("d-m-Y", strtotime($row['event_fromdatetime']));
     $row['event_todatetime']   = date("d-m-Y", strtotime($row['event_todatetime']));
     
     $events[$i]['id'] = $row['id'];
     $events[$i]['event_name'] = $row['event_name'];
     $events[$i]['interest_no'] = $row['interest_no'];
     $events[$i]['event_desc'] = $row['event_desc'];
     $events[$i]['event_fromdatetime'] = $row['event_fromdatetime'] . " To <br> ". $row['event_todatetime'];
     $events[$i]['location'] = $row['location'] . ", " . $row['city'];
     $events[$i]['filepath'] = $row['filepath'];
     $events[$i]['fd_colaboration'] = $row['fd_colaboration'];
     $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

  <link href="../css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <style>

      .dataTables_filter input { width: 200px }
    </style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
       <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Events List <a href="addedit_events.php" class="btn btn-primary pull-right btn-lg">+ Add Event</a></h3>               
                <div class="table-responsive theme-table v-align-top">
                    <table class="table" id="example">
                       <thead>
                           <tr>
                              <th>Event NAME</th>
                              <th>Event Date</th>
                              <th>Address</th>
                              <th>People Interested</th>
                              <!-- <th>View Enquiry</th> -->
                              <th>FD Collaboration</th>
                              <th>ACTIONS</th>
                           </tr>
                       </thead>
                        <tbody>
                           <?php
            
            for ($i=0; $i <count($events) ; $i++) {
              ?>
              <tr>
                <td><?php echo strtoupper($events[$i]['event_name']); ?></td>
                <!-- <td><?php echo $events[$i]['event_desc']; ?></td> -->
                
                <td><?php echo $events[$i]['event_fromdatetime']; ?></td>
                <td><?php echo $events[$i]['location'] ; ?></td>
                <td><a href="event_customers.php?id=<?php echo $events[$i]['id'];?> "><?php echo $events[$i]['interest_no']; ?> + INTERESTED</a></td>
                <!-- <td><a href="event_customers.php?id=<?php echo $events[$i]['id'];?> ">View</a></td> -->
                <td><?php if($events[$i]['fd_colaboration'] == 1){ echo "Yes"; } else{ echo "No"; } ?></td>
                <td>
                	<?php echo "<a class='action-link edit' href='addedit_events.php?id=". $events[$i]['id']."'></a>"; ?>
                    <?php echo "<a class='action-link delete' onclick = 'javascript: delete_id($(this));return false;' href='delete_event_list.php?id=". $events[$i]['id']."'></a>"; ?>
                </td>

              </tr>
              <?php

            }
            ?>                                                                                                           
                        </tbody>
                    </table>                   
                </div>               
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="../js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function() {
    $('#example').dataTable( {
    language: {
        searchPlaceholder: "Search Event by Name, Location"
    }
});
});
</script>
</body>

</html>