<?php
include('../connection/conn.php');
include('session_check.php');

  $did = $_SESSION['doctor_details']['id'];

$FromDate = $_GET['fd'];
$ToDate = $_GET['td'];
$ItemNo = $_GET['item'];

if (empty($ItemNo)) {
    $sql="SELECT e.*, i.item_name FROM expense_item AS e INNER JOIN items AS i ON e.id_item=i.id WHERE date(e.date) >='$FromDate' AND date(e.date)<='$ToDate' AND e.id_item = '$ItemNo' AND e.id_doctor='$did'";
    $query = mysqli_query($conn,$sql);
   $events = array();
   while ($row1 = mysqli_fetch_assoc($query)) {
    array_push($events,$row1);
    }
  }
  else
  {
    $sql="SELECT e.*, i.item_name FROM expense_item AS e INNER JOIN items AS i ON e.id_item=i.id WHERE date(e.date) >='$FromDate' AND date(e.date)<='$ToDate' AND e.id_item = '$ItemNo' AND e.id_doctor='$did'";
    $query = mysqli_query($conn,$sql);
   $events = array();
   while ($row1 = mysqli_fetch_assoc($query)) {
    array_push($events,$row1);
    }
  }

 $file_data.="
          <table width='100%' style='text-align:center;font-size: 16pt;color:#1e88e5;padding-top:20px;' >
   <tr>
    <th>Purchase of Consumables Report</th>
  </tr>
  
   
   </table>";

  $file_data.="<table width='100%'>
   <tr>
   <td colspan='5'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='5'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> </td>
   </tr>
   </table>
   <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>SL. NO </th>
   <th> Consumable Name</th>
   <th>Date </th>
   <th> Amount</th>
   <th class='text-right'> Total Amount </th>

   </tr>";
    $totalAmount = 0;
   for($e=0;$e<count($events);$e++) {
    $n = $e+1;
  $amount = $events[$e]['amount'];
  $date = date("d-m-Y", strtotime($events[$e]['date']));
  $item_name = $events[$e]['item_name'];
  $totalAmount = $totalAmount + $events[$e]['amount'];
  $file_data.="<tr>
   <td>$n </td>
   <td>$item_name</td>
   <td>$date </td>
   <td>$amount</td>
   <td class='text-right'>₹ $amount.00</td>
   </tr>";
 }
    $file_data.="<tr><td colspan='4'>Total Amount<td class='text-right'>₹ $totalAmount.00
    </td>
    </tr>
    </table>";

 // echo $file_data;exit;
$logo = '/var/www/html/firstdoctor/uploads/logo.jpg';


include("../library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetHeader("<table>
                  <tr>
            <td><img src='$logo' height='32px;'>
                   </td>
                  </tr>
                  </table>");

$mpdf->SetFooter('<div>Powered by firstDoctor</div>');
// echo $file_data;exit;

$mpdf->SetFooter('<div>Powered by firstDoctor</div>');
$mpdf->WriteHTML($file_data);

$mpdf->Output('ExpenseItems_reports.pdf', 'D');
exit;

?>