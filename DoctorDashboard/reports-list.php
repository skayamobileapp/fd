<?php 
include('../connection/conn.php');
include('session_check.php');
 $did = $_SESSION['doctor_details']['id'];
 // print_r($idadmin).exit();
if($_POST)
{
  $did = $_SESSION['doctor_details']['id'];
  $fdate = trim($_POST['fdate']);
$startDateArray = explode('/',$fdate);
$mysqlStartDate = $startDateArray[2]."-".$startDateArray[1]."-".$startDateArray[0];
$fdate = date("Y-m-d", strtotime($mysqlStartDate));

  $tdate = trim($_POST['tdate']);
$startDateArray = explode('/',$tdate);
$mysqlStartDate = $startDateArray[2]."-".$startDateArray[1]."-".$startDateArray[0];
$tdate = date("Y-m-d", strtotime($mysqlStartDate));

  $item = $_POST['item'];

  $choice = $_POST['choice'];

if ($choice == 1) {

  if (empty($item)) {
    $sql="SELECT e.*, i.item_name FROM expense_item AS e INNER JOIN items AS i ON e.id_item=i.id WHERE e.date >='$fdate' AND e.date<='$tdate' OR e.id_item = '$item' AND e.id_doctor='$did'";
    $query = mysqli_query($conn,$sql);
  }
  else
  {
    $sql="SELECT e.*, i.item_name FROM expense_item AS e INNER JOIN items AS i ON e.id_item=i.id WHERE e.date >='$fdate' AND e.date<='$tdate' AND e.id_item = '$item' AND e.id_doctor='$did'";
    $query = mysqli_query($conn,$sql);
  }
}
else
if ($choice == 2) {
  $sql="SELECT p.*, s.name FROM petty_cash as p inner join staff as s on s.id=p.id_staff WHERE date >='$fdate' AND date<='$tdate' AND id_doctor='$did'";
    $query_pettyCash = mysqli_query($conn,$sql);
}

}

$sql   = "select id, item_name from items where id_doctor='$did'";
$result = $conn->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
}
 
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0">Reports</h4>
                  
								</div>
								<div class="card-body">
                <form action="" method="POST" id="myform">
                  <div class="row">
                  <div class="col-sm-6">
                      <label class="radio-inline radio-box-style">
                      <input type="radio" name="choice" id="choice1" value="1" onchange="getdetails(this)" <?php if($_POST['choice']=='1'){ echo 'checked=checked'; }?>><span class="check-radio"></span> Purchase For Items
                      </label>
                     <label class="radio-inline radio-box-style">
                      <input type="radio" name="choice" id="choice2" value="2" onchange="getdetails(this)" <?php if($_POST['choice']=='2'){ echo 'checked=checked'; }?>><span class="check-radio"></span> Petty Cash
                           </label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group">
                        <label>From Date:</label><span class="error"></span>
                        <div class="cal-icon">
                          <input type="text" name="fdate" class="form-control datetimepicker" placeholder="Select Date" id="fdate" autocomplete="off" value="<?php echo $_POST['fdate']; ?>">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <label>To Date:</label><span class="error"></span>
                        <div class="cal-icon">
                          <input type="text" name="tdate" class="form-control datetimepicker" placeholder="Select Date" id="tdate" autocomplete="off" value="<?php echo $_POST['tdate']; ?>">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-4" id="itemdiv">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                          <label class="">SELECT ITEM NAME<span class="error"></span></label>
                          <select name="item" class="form-control selitemIcon" id="item">
                            <option value=''>Select</option>
                              <?php
                                for ($i=0; $i<count($itemList); $i++) {  ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if( $itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['item_name']; ?></option>
                                <?php
                                  }
                                ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  
                  
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label></label><div>
                        <input type="submit" name="getdata" class="btn btn-primary btn-lg float-right" id="getdata" value="GET DATA">
                        </div>
                    </div>
                  </div>
                </div>
              </form>
              <?php
                    if (isset($query))
                        {
                            $viewdata =[];
                            $i=0;
                            while($row=mysqli_fetch_assoc($query))
                            {
                                $n =$i+1;
                                $viewdata[$i]['item_name']=$row['item_name'];
                                $viewdata[$i]['date']=$row['date'];
                                $viewdata[$i]['amount']=$row['amount'];
                                $i++;
                            } ?>
                            <h4 style="padding: 16px">Report List
                            <a href="print_expense_reports.php?fd=<?php echo $fdate;?>&td=<?php echo $tdate;?>&item=<?php echo $item;?>" class="btn btn-primary float-right btn-lg" style='margin-left:14px;'>Download Report</a></h4><br/>
                            <div class="table-responsive theme-table v-align-top">
                              <table class="table">
                                <thead>
                                <tr>
                                    <th>SL. NO</th>
                                    <th>Consumable Name</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th class="text-right">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $totalAmount = 0;
                                for ($i=0; $i<count($viewdata); $i++) { 
                                  $n=$i+1;
                                  $totalAmount = $totalAmount + $viewdata[$i]['amount']?>
                                    <tr>                                    
                                        <td><?php echo $n;?></td>
                                        <td><?php echo strtoupper($viewdata[$i]['item_name']);?></td>
                                        <td><?php echo date('d-m-Y',strtotime($viewdata[$i]['date']));?></td>
                                        <td><?php echo strtoupper($viewdata[$i]['amount']); ?></td>
                                     <td class="text-right">₹ <?php echo $viewdata[$i]['amount'];?>.00</td>
                                    </tr>

                                    <?php
                                }
                                $sql="SELECT sum(amount) as total FROM expense_item WHERE date >='$fdate' AND date<='$tdate' ";
                                  $result = mysqli_query($conn,$sql);
                                  while ($rows=mysqli_fetch_array($result)) { ?>
                                  <tr>
                                    <td colspan="5" class="text-right">Total : ₹ <?php echo $totalAmount;?>.00
                                   
                                  </td>
                                </tr>
                        </tbody>
                    </table>
                  </div>
                                  <?php
                                }
                              }

                    if (isset($query_pettyCash))
                        {
                            $viewdata =[];
                            $i=0;
                            while($row=mysqli_fetch_assoc($query_pettyCash))
                            {
                                $n =$i+1;
                                $viewdata[$i]['name']=$row['name'];
                                $viewdata[$i]['description']=$row['description'];
                                $viewdata[$i]['date']=$row['date'];
                                $viewdata[$i]['amount']=$row['amount'];
                                $i++;
                            } ?>
                            <h4>Report List
                            <a href="print_petty_reports.php?fd=<?php echo $fdate;?>&td=<?php echo $tdate;?>" class="btn btn-primary pull-right btn-lg" style='margin-left:14px;'>Download Report</a></h4><br/>
                            <div class="table-responsive theme-table v-align-top">
                              <table class="table">
                                <thead>
                                <tr>
                                    <th>SL. NO</th>
                                    <th>Staff Name</th>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th class="text-right">Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $totalAmount = 0;
                                for ($i=0; $i<count($viewdata); $i++) { 
                                  $n=$i+1;
                                  $totalAmount = $totalAmount + $viewdata[$i]['amount']?>
                                    <tr>                                    
                                        <td><?php echo $n;?></td>
                                        <td><?php echo $viewdata[$i]['name']; ?></td>
                                        <td><?php echo $viewdata[$i]['description']; ?></td>
                                        <td><?php echo date('d-m-Y',strtotime($viewdata[$i]['date']));?></td>
                                        <td><?php echo strtoupper($viewdata[$i]['amount']); ?></td>
                                     <td class="text-right">₹ <?php echo $viewdata[$i]['amount'];?>.00</td>
                                    </tr>

                                    <?php
                                }
                                $sql="SELECT sum(amount) as total FROM petty_cash WHERE date >='$fdate' AND date<='$tdate' ";
                                  $result = mysqli_query($conn,$sql);
                                  while ($rows=mysqli_fetch_array($result)) { ?>
                                  <tr>
                                    <td colspan="5" class="text-right">Total : ₹ <?php echo $totalAmount;?>.00
                                   
                                  </td>
                                </tr>
                        </tbody>
                    </table>
                  </div>
                                  <?php
                                }
                              }
                        ?>
									
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>

		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

    <script type="text/javascript">
      function getdetails(c1) {
        var cval = c1.value;
      if(cval =='1') {
        $("#itemdiv").show();
      }
      else {
        $("#itemdiv").hide();
      }
    }
    </script>
		
	</body>
</html>