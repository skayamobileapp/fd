<?php
include('profile_completion.php');

$patientListUrl = '';
$calendar = '';
$patientSummary = '';
$patientBillSummary = '';
$patientindex = '';
$doctorAvaialbilty = '';
$staffDetailsUrl = '';
$messagesUrl = '';
$eventsUrl = '';
$recheduleAppointmentUrl = '';
$items = '';
$ExpenseItems = '';
$reports = '';
$clinic = '';
$outStandingBalance = '';
$outStandingBalance = '';

$url = basename($_SERVER['PHP_SELF']);
if($url =='view_patient_details.php') {
  $patientListUrl = "class='active'";
}
if($url =='view_patient_summary.php') {
  $patientSummary = "class='active'";
}
if($url =='patient_bill_summary.php') {
  $patientBillSummary = "class='active'";
}
if($url =='clinic_list.php') {
  $clinic = "class='active'";
}
if($url =='index.php') {
  $patientindex = "class='active'";
}
if($url =='view_doctor_availability.php') {
  $doctorAvaialbilty = "class='active'";
}

if($url =='staff_details.php') {
  $staffDetailsUrl = "class='active'";
}

if($url =='messages.php') {
  $messagesUrl = "class='active'";
}
if($url =='events.php') {
  $eventsUrl = "class='active'";
}

if($url =='appointmentReschedule.php') {
    $recheduleAppointmentUrl = "class='active'";
}

if($url =='item_details.php') {
  $items = "class='active'";
}

if($url =='expense_items.php') {
  $ExpenseItems = "class='active'";
}

if($url =='petty_cash.php') {
  $PettyCash = "class='active'";
}

if($url =='reports.php') {
  $reports = "class='active'";
}
if($url =='doctor_outstanding_balance.php') {
  $outStandingBalance = "class='active'";
}
if($url =='calendar.php') {
  $calendar = "class='active'";
}

?>
<aside class="col-sm-4 col-lg-3 sidebar">
    <div class="profile-block">
        <div class="profile-img-container">
            <?php if(empty($_SESSION['doctor_details']['photo'])){?>
                <img src="https://ui-avatars.com/api/?background=02afee&color=fff&name=<?php echo $_SESSION['doctor_details']['doctor_name']; ?>">
                <?php } else { ?>
                    <img src="<?php echo '../uploads/'.$_SESSION['doctor_details']['photo']; ?>" alt="">
                <?php } ?>
        </div>
        <h3>Dr. <?php echo ucfirst($_SESSION['doctor_details']['first_name'])." ".ucfirst($_SESSION['doctor_details']['last_name']); ?></h3>
        <p><a href="update_doctor_details.php?id=<?php echo $_SESSION['doctor_details']['id']; ?>">Update Profile <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></p>
        <p><?php echo strtolower($_SESSION['doctor_details']['email']); ?></p>
    </div>   
        <h4>Profile Completion:<?php echo $percentage; ?>%</h4>
                
    <hr class="divider" />
    <ul class="sidebar-menu clearfix">
        <li <?php echo $patientindex;?>><a href="index.php" class="dashboard">Dashboard</a></li>
        <li <?php echo $calendar;?>><a href="calendar.php" class="calendar">Calendar</a></li>
        <li <?php echo $patientListUrl;?>><a href="view_patient_details.php" class="patient">Patients List</a></li>
        <li <?php echo $clinic;?>><a href="clinic_list.php" class="hospital">Clinic List</a></li>
        <li <?php echo $patientSummary;?>><a href="view_patient_summary.php" class="healthcard">Patient Summary</a></li>
        <li <?php echo $patientBillSummary;?>><a href="patient_bill_summary.php" class="bill">Patient Bill Summary</a></li>
        <li <?php echo $doctorAvaialbilty;?>><a href="view_doctor_availability.php" class="clinic-schedule">Doctor's Clinic Schedule</a></li>
        <li <?php echo $recheduleAppointmentUrl;?>><a href="appointmentReschedule.php" class="reschedule">Reschedule Appointments</a></li>
        <li <?php echo $messagesUrl;?>><a href="messages.php" class="chat">Messages from patient</a></li>
        <li <?php echo $eventsUrl;?>><a href="events.php" class="events">Events</a></li>
        <li <?php echo $staffDetailsUrl;?>><a href="staff_details.php" class="staff">Staff Members</a></li>
        <li <?php echo $items;?>><a href="item_details.php" class="staff">Consumables List</a></li>
        <li <?php echo $ExpenseItems;?>><a href="expense_items.php" class="staff">Purchase of Consumables</a></li>
        <li <?php echo $PettyCash;?>><a href="petty_cash.php" class="bill">Petty Cash List</a></li>
        <li <?php echo $reports;?>><a href="reports.php" class="report">Reports</a></li>
        <li <?php echo $outStandingBalance;?>><a href="doctor_outstanding_balance.php" class="bill">Out Standing Balance</a></li>
    </ul>
</aside>