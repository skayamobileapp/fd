<?php
include('../connection/conn.php');
include('session_check.php');
$patientId = $_GET['id'];

error_reporting(0);

$select = mysqli_query($conn,"SELECT * FROM prescriptions WHERE id='$patientId'");
    while ($row = mysqli_fetch_array($select))
    {
        //from database fields
        $patient_name = $row['patient_name'];
        $appoint_date = $row['appoint_date'];
        $appoint_time = $row['appoint_time'];
        $allergies = $row['allergies'];
        $lab_name = $row['lab_name'];
        $lab_test = $row['lab_test'];
        $drug_type = $row['drug_type'];
        $drug_dosage = $row['drug_dosage'];
        $medicine = $row['medicine'];
        $nxt_appoint = $row['nxt_appoint'];

        $report_file1 = $row['report_file'];
        $Dosage = $row['drug_dosage'];
        $Duration = $row['duration'];
        $RepeatSame = $row['repeat_same'];
        $ToBeTaken = $row['to_be_taken']; 
        $TimeOfTheDay = $row['time_of_the_day'];
      
        $TimeOfTheDay1 = explode(",",$TimeOfTheDay);
        $patient_id = $row['patient_id'];
        $morePrescription = $row['more_prescrip'];
    }

    


if(isset($_POST['save']))
{
  $patient_name = $_POST['patientName'];
  $appoint_date = $_POST['appoint_date'];
  $appoint_time = $_POST['appoint_time'];
  $allergies = $_POST['allergies'];
  $lab_name = implode(',', $_POST['lab']);
  $lab_test = implode(',', $_POST['test']);
  $drug_type = $_POST['drugtype'];
  $drug_dose = $_POST['drugDose'];
  $drug_medicine = implode(',', $_POST['drug']);
  $next_appointment = $_POST['nxt_appoint_date'];

  $report_file = $_FILES['file']['name'];
  if ($report_file == "") {
    $report_file = $report_file1;
  }
  else{
    $tmp_file = $_FILES['file']['tmp_name'];
    move_uploaded_file($tmp_file, '../uploads/'.$report_file);
  }

  $duration = $_POST['duration'];
  $repeatSame = $_POST['repeat_same'];
  // $patient_name = $_POST['patient'];
  $timeOfTheDay = implode(',', $_POST['time_of_the_day']);
  $toBeTaken = $_POST['to_be_taken'];
  $more_prescrip = $_POST['desc'];

  // $drugName = $_POST['drug_name'];
  // $dosage = $_POST['dosage'];
  // $duration = $_POST['duration'];
  // $repeatSame = $_POST['repeat_same'];
  // $toBeTaken = $_POST['to_be_taken'];
  //  $timeOfTheDay = implode(',', $_POST['time_of_the_day']);
 

    
    
     $query="UPDATE prescriptions SET appoint_date='$appoint_date', appoint_time='$appoint_time', allergies='$allergies', lab_name='$lab_name', lab_test='$lab_test', drug_type='$drug_type', drug_dosage='$drug_dose', medicine='$drug_medicine', nxt_appoint='$next_appointment', report_file='$report_file', duration='$duration', repeat_same='$repeatSame', time_of_the_day='$timeOfTheDay', to_be_taken='$toBeTaken', more_prescrip='$more_prescrip' WHERE id= '$patientId'";
     $insert =  mysqli_query($conn,$query);

    
    if ($insert)
    {
        echo "<script>alert('prescriptions updated successfully');</script>";
        echo "<script>parent.location='view_patient_summary.php'</script>";
    }
    else
    {
        echo "error".mysqli_error($conn);
    }
}

$sql = "SELECT * FROM patient_details";
$patientList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($patientList, $row);
}

$sql = "SELECT * FROM lab";
$labList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($labList, $row);
}
//print_r($labList).exit();


//print_r($drugList).exit();

$sql = "SELECT * FROM drug_type";
$drugTypeList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($drugTypeList, $row);
}

$sql = "SELECT * FROM drug";
$drugDataList = array();
$result = $conn->query($sql);
while ($row = $result->fetch_assoc()) {
  array_push($drugDataList, $row);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>  
    <script src="../js/main.js"></script>

  <script type="text/javascript" src="../library/ckeditor/ckeditor.js"></script>

</head>
    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>


<body>     
  <form action="" method="post" id="form" enctype="multipart/form_data">
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
        <aside class="col-sm-4 col-lg-3 sidebar">
          <?php include('menu.php'); ?>
            
           <ul class="sidebar-menu clearfix">
                <li><a href="index.php" class="dashboard">Dashboard</a></li>
                <!-- <li><a href="AdminHospitalDetails.php" class="hospital">Hospitals</a></li> -->
                <li ><a href="view_patient_details.php" class="patient">Patients</a></li>
                <li class="active"><a href="view_patient_summary.php" class="patient">Patient Summary</a></li>
                <li ><a href="view_doctor_availability.php" class="patient">Doctor's Clinic Schedule</a></li>
                <li><a href="appointmentReschedule.php" class="patient">Reschedule Appointment</a></li>
                <li><a href="messages.php" class="patient">Messages from patient</a></li>
                <li><a href="events.php" class="healthcard">Events</a></li>
                
                <?php include('list.php') ?>               
            </ul>       
        </aside>
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
                <ol class="breadcrumb">
                  <li><h4>Prescriptions</h4></li>
                </ol>              
               <h3 class="clearfix">Edit Prescriptions</h3>   
               <div class="card">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <select class="form-control" name="patientName" id="parent">
                            <option value="">Select</option>
                            <?php for ($i=0; $i <count($patientList); $i++) { 

                                        $id   =  $patientList[$i]['id'];
                                        $name = $patientList[$i]['patient_name']; ?>
                                        ?>
                                        <option value="<?php echo $name; ?>"<?php if( $name == $patient_name)  { echo "selected=selected"; }
                                        ?>
                                        > <?php echo $name; ?> </option>
                                    <?php } ?>
                          </select>
                          <label class="fg-label">Patient Name</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <input type="date" class="form-control" name="appoint_date" id="appoint_date" value="<?php echo $appoint_date;?>">
                          <label class="fg-label">Appointment Date</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <input type="text" class="form-control" name="appoint_time" id="appoint_time" value="<?php echo $appoint_time;?>">
                          <label class="fg-label">Appointment Time</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <textarea class="form-control" name="allergies" id="allergies" maxlength="250"> <?php echo $allergies; ?></textarea>
                          <label class="fg-label">Allergies</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <label class="fg-label">Lab Test</label>
                        </div>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <div class="table-responsive theme-table">
                            <table class="table" id="dynamic_field">
                              <tr>
                                <td><select class="form-control name_list" name="lab[]" style="width: 300px;">
                                  <option value="">Select</option>
                                  <?php

                                   for ($i=0; $i <count($labList); $i++) { 

                                        $id   =  $labList[$i]['id'];
                                        $name =  $labList[$i]['lab_name'];
                                        ?>
                                        <option value="<?php echo $name; ?>"<?php if($lab_name == $name)  { echo "selected=selected"; }
                                        ?>
                                        > <?php echo $name; ?> </option>
                                    <?php } ?>
                                </select></td>
                                <td><input type="text" name="test[]" placeholder="Enter Test Name" class="form-control name_list" value="<?php echo $lab_test; ?>" /></td>
                                <td><button type="button" name="add" id="add" class="btn btn-primary">Add More</button></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <label class="fg-label">Drug Type</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <label class="fg-label">Drug Name And Dosage</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <div class="table-responsive theme-table">
                            <table class="table" id="dynamic_field">
                              <tr>
                                <td><select class="form-control name_list" name="drugtype" style="width: 300px;">
                                  <option value="">Select</option>
                                  <?php for ($i=0; $i <count($drugTypeList); $i++) { 

                                        $id   =  $drugTypeList[$i]['id'];
                                        $type =  $drugTypeList[$i]['type'];
                                        ?>
                                        <option value="<?php echo $type; ?>"<?php if( $type == $drug_type)  { echo "selected=selected"; }
                                        ?>
                                        > <?php echo $type; ?> </option>
                                    <?php } ?>
                                </select></td>
                                <td><select class="form-control name_list" name="drugDose" style="width: 300px;">
                                  <option value="">Select</option>
                                  <?php for ($i=0; $i <count($drugDataList); $i++) { 

                                        $id   =  $drugDataList[$i]['id'];
                                        $type =  $drugDataList[$i]['id_drugtype'];
                                        $dosage =  $drugDataList[$i]['dosage'];
                                        ?>
                                        <option value="<?php echo $dosage; ?>"<?php if( $dosage == $Dosage)  { echo "selected=selected"; }
                                        ?>
                                        > <?php echo $dosage; ?> </option>
                                    <?php } ?>
                                </select></td>
                                <!-- <td><button type="button" name="add" id="add" class="btn btn-primary">Add More</button></td> -->
                              </tr>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <label class="fg-label"> Medicines</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                           <div class="table-responsive theme-table"> 
                            <table class="table table" id="dynamic_field2" width="100%">
                              <tr>
                                <td><select class="form-control name_list" name="drug[]" style="width:330px;">
                                  <option value="">Select</option>
                                  <?php for ($i=0; $i <count($drugDataList); $i++) { 

                                        $id   =  $drugList[$i]['MEDID'];
                                        $name =  $drugList[$i]['MED_NAME'];
                                        ?>
                                        <option value="<?php echo $name; ?>"<?php if( $name == $medicine)  { echo "selected=selected"; }
                                        ?>
                                        > <?php echo $name; ?> </option>
                                    <?php } ?>
                                </select></td>
                                <td><button type="button" name="addrug" id="addrug" class="btn btn-primary">Add More</button></td>
                              </tr>
                            </table>
                           </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-5">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <input type="date" class="form-control" name="nxt_appoint_date" value="<?php echo $nxt_appoint; ?>" >
                          <label class="fg-label"> Next Appointment Date</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-1">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <label class="fg-label">File Upload</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-5">
                      <div class="form-group fg-float">
                        <div class="fg-line fg-toggled">
                          <input type="file" class="form-control" name="file" id="file"  value="<?php echo $report_file1; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                    <!-- <div class="col-sm-4">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="text" class="form-control" name="dosage">
                          <label class="fg-label">Dosage</label>
                        </div>
                      </div>
                    </div> -->

                  <div class="row">
                    <div class="col-sm-5">
                      <div class="form-group fg-float">
                        <div class="fg-line">
                          <input type="text" class="form-control" name="duration" value="<?php echo $Duration; ?>">
                          <label class="fg-label">Duration</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-7">
                    <label class="fg-label">Repeat</label>
                      <div class="form-group">
                        <label class="radio-inline">
                          <input type="radio" name="repeat_same" id="inlineRadio1" value="everyday" <?php if( $RepeatSame =='everyday') { echo "checked";} ?>><span class="check-radio"></span> Everyday
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="repeat_same" id="inlineRadio2" value="alternate_days" <?php if( $RepeatSame =='alternate_days') { echo "checked";} ?>><span class="check-radio"></span> Alternate days
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="repeat_same" id="inlineRadio3" value="specific_days" <?php if( $RepeatSame =='specific_days') { echo "checked";} ?>><span class="check-radio"></span> Specific days
                        </label>
                      </div>
                    </div>
                  <div class="row">
                    <div class="col-sm-5">
                    <label class="fg-label">Time of the day</label>
                      <div class="form-group">
                        <label class="checkbox-inline">
                        <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox1" value="morning" <?php
                                                     if (in_array("morning", $TimeOfTheDay1)) {

                                                        echo "checked";
                                                     }
                                                     ?> >
                        <span class="check-radio"></span> Morning
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox2" value="noon" <?php
                                                     if (in_array("noon", $TimeOfTheDay1)) {

                                                        echo "checked";
                                                     }
                                                     ?>>
                        <span class="check-radio"></span> Noon
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox3" value="evening" <?php
                                                     if (in_array("evening", $TimeOfTheDay1)) {

                                                        echo "checked";
                                                     }
                                                     ?>>
                        <span class="check-radio"></span> Evening
                      </label>
                      <label class="checkbox-inline">
                        <input type="checkbox" name="time_of_the_day[]" id="inlineCheckbox4" value="night" <?php
                                                     if (in_array("night", $TimeOfTheDay1)) {

                                                        echo "checked";
                                                     }
                                                     ?>>
                        <span class="check-radio"></span> Night
                      </label>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <label class="fg-label">To be Taken</label>
                    <div class="form-group">
                      <label class="radio-inline">
                      <input type="radio" name="to_be_taken" id="inlineRadio4" value="after_food" <?php if( $ToBeTaken =='after_food') { echo "checked";} ?>>
                      <span class="check-radio"></span> After Food
                    </label>
                    <label class="radio-inline">
                      <input type="radio" name="to_be_taken" id="inlineRadio5" value="before_food" <?php if( $ToBeTaken =='before_food') { echo "checked";} ?>>
                      <span class="check-radio"></span> Before Food
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-10">
                  <div class="form-group fg-float">
                    <div class="fg-line">
                      <label>Suggestions to Patients</label>
                      <textarea class="form-control ckeditor" name="desc"><?php echo $morePrescription; ?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                
              <div class="bttn-group">
                <a href="view_patient_summary.php" class="btn btn-link">Cancel</a>
                <button class="btn btn-primary btn-lg" name="save" id="save">Save</button>
              </div>
            </div>        
            </div>
        </section>
      </div>
    </div>    
    </form>      
</body>
<script>
$(document).ready(function(){
  var i=1;
  $('#add').click(function(){
    i++;
    $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="lab[]" class="form-control name_list" placeholder="Enter Lab Name"/></td><td><input type="text" name="test[]" placeholder="Enter Lab Test Name" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
  });
  
  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id"); 
    $('#row'+button_id+'').remove();
  });
  
});
</script>
<script>
$(document).ready(function(){
  var i=1;
  $('#addrug').click(function(){
    i++;
    $('#dynamic_field2').append('<tr id="row'+i+'"><td><input type="text" name="drug[]" class="form-control name_list" style="width:330px;" placeholder="Enter Medicine Name"/></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
  });
  
  $(document).on('click', '.btn_remove', function(){
    var button_id = $(this).attr("id"); 
    $('#row'+button_id+'').remove();
  });
});
</script>

</html>