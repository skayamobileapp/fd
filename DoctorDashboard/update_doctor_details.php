<?php 
include('../connection/conn.php');
include('session_check.php');
include('profile_completion.php');
error_reporting(0);
date_default_timezone_set("Asia/Kolkata");

$date = DateTime::createFromFormat('U.u', microtime(TRUE));
$date->setTimeZone(new DateTimeZone('Asia/Kolkata'));
$date = $date->format('dmY_His_u');

$id = $_GET['id'];

if($id = $_GET['id']){
	$docList = [];
$sql = "SELECT *  FROM doctor_details WHERE id = '$id' ";
$result = $conn->query($sql);
$docList = $result->fetch_assoc();
}
else{
	$did = $_SESSION['doctor_details']['id'];

$docList = [];
$sql = "SELECT *  FROM doctor_details WHERE id = '$did' ";
$result = $conn->query($sql);
$docList = $result->fetch_assoc();
}

if($percentage>=75){
        mysqli_query($conn, "UPDATE doctor_details SET profile_status='1' WHERE id='$id'");
      }

if ($_POST) 
{
    //from names given in html
$id = $_GET['id'];
    //$doctorName = $_POST['doctor_name'];
  $firstName = $_POST['first_name'];
  $midName = $_POST['middle_name'];
  $lastName = $_POST['last_name'];
  $fullName = "Dr. ".ucfirst($firstName)." ".ucfirst($midName)." ".ucfirst($lastName);
  $emailAddress = $_POST['email_address'];
  $password = $_POST['password'];
  $gender = $_POST['gender'];
  $stream = $_POST['stream'];
  $speciality = $_POST['speciality'];
  $sub_speciality = $_POST['sub_speciality'];
  // $telphone = $_POST['Telcheck'];
  // $teldata = implode(",", $telphone);
  $doctorMobileNum = $_POST['mobile'];
  // $cellPhone = $_POST['cell_number'];
  $pharmacy = $_POST['pharmacy'];
  $postalAddress = $_POST['postal_address'];
  $pincode = $_POST['pincode'];
  $state = $_POST['state'];
  $city = $_POST['city'];
  $practiceContact = $_POST['practice_contact'];
  $clinic_address = $_POST['clinic_address'];
  $experiance = $_POST['years_of_practice'];
  $qualification = $_POST['qualification'];
  $ug_qualification = $_POST['ug_qualification'];
  $pg_qualification = $_POST['pg_qualification'];
  $ug_file = $_FILES['ug_qualification_file']['name'];

  if($ug_file=="")
    {
         $ug_file = $docList['ug_qualification_file'];
    }
    else
    {
        $ug_file = $_FILES['ug_qualification_file']['name'];
        $img_tmp =$_FILES['ug_qualification_file']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($ug_file);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $ug_file = 'UG_'. $date . '.'. $extension;

        $file_path = $upload_path . 'UG_' . $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

  $pg_file = $_FILES['pg_qualification_file']['name'];
if($pg_file=="")
    {
         $pg_file = $docList['pg_qualification_file'];
    }
    else
    {
        $pg_file = $_FILES['pg_qualification_file']['name'];
        $img_tmp =$_FILES['pg_qualification_file']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($pg_file);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $pg_file = 'PG_'. $date . '.'. $extension;

        $file_path = $upload_path . 'PG_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);

    }
    

  $institution = $_POST['institution'];
  $fellowShip = $_POST['fellow_ship'];
  $imaNo = $_POST['ima_no'];
  $licence1 = $_POST['licence1'];

  $licence_file1 = $_FILES['licence_file1']['name'];
  if($licence_file1=="")
    {
         $licence_file1 = $docList['license_file'];
    }
    else
    {
        $licence_file1 = $_FILES['licence_file1']['name'];
        $img_tmp =$_FILES['licence_file1']['tmp_name'];
        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($licence_file1);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $licence_file1 = 'L1_'. $date . '.'. $extension;

        $file_path = $upload_path . 'L1_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }


  $license2 = $_POST['license2'];


  $licence_file2 = $_FILES['licence_file2']['name'];
  if($licence_file2=="")
    {
         $licence_file2 = $docList['license_file2'];
    }
    else
    {
        $licence_file2 = $_FILES['licence_file2']['name'];
        $img_tmp =$_FILES['licence_file2']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($licence_file2);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $licence_file2 = 'L2_'. $date . '.'. $extension;

        $file_path = $upload_path . 'L2_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

  $timeslot = $_POST['timeslot'];

  $Profile = $_FILES['ProfilePhoto']['name'];

    if($Profile=="")
    {
         $Profile = $docList['photo'];
    }
    else
    {
        $Profile = $_FILES['ProfilePhoto']['name'];
        $img_tmp =$_FILES['ProfilePhoto']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($Profile);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $Profile = 'DocPic_'. $date . '.'. $extension;

        $file_path = $upload_path . 'DocPic_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $logo = $_FILES['logoPhoto']['name'];

    if($logo=="")
    {
         $logo = $docList['logo'];
    }
    else
    {
        $logo = $_FILES['logoPhoto']['name'];
        $img_tmp =$_FILES['logoPhoto']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($logo);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $logo = 'DocLogo_'. $date . '.'. $extension;

        $file_path = $upload_path . 'DocLogo_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $sign = $_FILES['sign']['name'];

    if($sign=="")
    {
         $sign = $docList['signature'];
    }
    else
    {
        $sign = $_FILES['sign']['name'];
        $img_tmp =$_FILES['sign']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($sign);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $sign = 'DocSign_'. $date . '.'. $extension;

        $file_path = $upload_path . 'DocSign_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $query = "UPDATE  doctor_details SET doctor_name ='$fullName', first_name = '$firstName', middle_name = '$midName', last_name = '$lastName', email = '$emailAddress', gender ='$gender', stream='$stream', specialty = '$speciality', sub_specialty = '$sub_speciality', mobile = '$doctorMobileNum', address = '$postalAddress', pincode = '$pincode', state = '$state', city ='$city', practice_contact = '$practiceContact', clinic_address = '$clinic_address', experiance = '$experiance', qualification = '$qualification', qualification_ug='$ug_qualification', qualification_ug_file='$ug_file', qualification_pg='$pg_qualification', qualification_pg_file='$pg_file', institute = '$institution', fellowship='$fellowShip', ima_no='$imaNo', pharmacy='$pharmacy', licence = '$licence1', license2='$license2', license_file='$licence_file1', license_file2='$licence_file2', photo = '$Profile', logo='$logo', timeslot='$timeslot', signature='$sign' WHERE id = '$id'";

    $update = mysqli_query($conn,$query);

    if ($update)
    {
      if($percentage>=75){
        mysqli_query($conn, "UPDATE doctor_details SET profile_status='1' WHERE id='$id'");
      }
        echo "<script>alert('update your profile successfully');</script>";
    		echo "<script>parent.location='index.php'</script>";

    }
}

$sql   = "SELECT id, name FROM stream";
$result = $conn->query($sql);
$streamList = array();
while ($row = $result->fetch_assoc()) {
    array_push($streamList, $row);
}

$sql   = "SELECT id, specialty FROM specialties";
$result = $conn->query($sql);
$specialtyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($specialtyList, $row);
}

$sql   = "SELECT id, state FROM states";
$result = $conn->query($sql);
$stateList = array();
while ($row = $result->fetch_assoc()) {
    array_push($stateList, $row);
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
      <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<style type="text/css">
    .error{
      text-transform: uppercase;
      position: relative;
    }
  </style>

<body> 
     <nav class="navbar navbar-default dashboard-navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">First Doctor</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right main-nav">
			  <!-- <li><a>Profile Completion: <?php echo $percentage; ?>%</a></li> -->
              <li><a href="#" data-toggle="modal" data-target="#myModal">Reset Password</a></li>
              <li><a href="logout.php">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php if($percentage>=75 && $doctor_status == '1') { include('menu.php'); } else { ?>
        <aside class="col-sm-4 col-lg-3 sidebar">
        <div class="profile-block">
          <div class="profile-img-container">
              <img src="<?php echo '../uploads/'.$_SESSION['doctor_details']['photo']; ?>" alt="" style="width: 100px; height: 105px;">
          </div>
          <h3>Dr. <?php echo ucfirst($_SESSION['doctor_details']['first_name'])." ".ucfirst($_SESSION['doctor_details']['last_name']); ?></h3>
                
        </div>
            
        <hr class="divider" />
        <ul class="sidebar-menu clearfix">
          
        </ul>      
        </aside>


        <?php  } ?>

        <section class="col-sm-8 col-lg-9">          
          <div class="main-container">
                <ol class="breadcrumb">
                  <li><a href="index.php">Dashboard</a></li>
                  <li class="active">Edit Profile</li>
                </ol>              
               <h3 class="clearfix">Edit Profile</h3>
                <div class="row">
                  <div class="col-sm-8">
                    <?php echo $message; ?>
                    <div id="chageresult"></div>
                  </div>
                </div>
              <form action="" method="post" id="form" enctype="multipart/form-data"> 
                <div class="card">
                    <div class="row">
                        <div class="col-sm-4">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo $docList['first_name']; ?>">
                                <label class="fg-label">First Name <span class="error">*</span></label>
                            </div>
                        </div>                            
                        </div>
                        <div class="col-sm-4">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="middle_name" id="middle_name" value="<?php echo $docList['middle_name']; ?>">
                                <label class="fg-label">Middle Name <span class="error"></span></label>
                            </div>
                        </div>                            
                        </div>
                        <div class="col-sm-4">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" class="form-control" name="last_name" value="<?php echo $docList['last_name']; ?>">
                                <label class="fg-label">Last Name <span class="error">*</span></label>
                            </div>
                        </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="email" class="form-control" name="email_address" value="<?php echo $docList['email']; ?>">
                                <label class="fg-label">Email address <span class="error">*</span></label>
                            </div>
                        </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>
                             Gender</label>
                           <label class="radio-inline radio-box-style">
                            <input type="radio" name="gender" id="gender" value="Male" <?php if( $docList['gender'] =='Male') { echo "checked"; } ?> ><span class="check-radio"></span> M
                            </label>
                           <label class="radio-inline radio-box-style">
                             <input type="radio" name="gender" id="gender" value="Female" <?php if( $docList['gender'] =='Female') { echo "checked";} ?>><span class="check-radio"></span> F
                           </label>
                          </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="mobile" maxlength="10" value="<?php echo $docList['mobile']; ?>">
                                    <label class="fg-label">Doctor's Mobile Number <span class="error">*</span></label>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line fg-toggled">
                                    <label class="">STREAM <span class="error">*</span></label>
                                    <select class="form-control selitemIcon" name="stream" id="stream" onchange="getspecialty()">
                                        <option value="">Select</option>
                                        <?php
                                        for ($i=0; $i <count($streamList); $i++) {
                                            ?>
                                            <option value="<?php echo $streamList[$i]['id']; ?>" <?php if($streamList[$i]['id'] == $docList['stream'])  { echo "selected=selected"; }
                                        ?>><?php echo $streamList[$i]['name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line fg-toggled">
                                    <label class="">SPECIALITY <span class="error">*</span></label>
                                    <select class="form-control selitemIcon" name="speciality" id="speciality" onchange="get_subspecialty()">
                                        
                                    </select>
                                </div>
                            </div>                            
                        </div>
                         <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line fg-toggled">
                                    <label class=""> SUB SPECIALITY <span class="error">*</span></label>
                                    <select class="form-control selitemIcon" name="sub_speciality" id="sub_speciality">
                                        
                                    </select>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <label class="fg-label">Alternate Mobile Number<span class="error" ></span></label>
                                    <input type="text" class="form-control" name="alter_phone" maxlength="10" id="alter_phone" value="<?php echo $docList['alter_number']; ?>">
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <textarea class="form-control" name="postal_address" maxlength="250"><?php echo $docList['address']; ?></textarea>
                                    <label class="fg-label"><b>Postal Address</b><span class="error"> *</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line fg-toggled">
                                    <label class="">STATE<span class="error">*</span></label>
                                    <select name="state" class="form-control selitemIcon" id="state" onchange="getCities()">
                                        <option value=''>Select</option>
                                        <?php
                                        for ($i=0; $i<count($stateList); $i++) {  ?>
                                        <option value="<?php echo $stateList[$i]['id']; ?>" <?php if( $stateList[$i]['id'] == $docList['state'])  { echo "selected=selected"; }
                                        ?>><?php echo $stateList[$i]['state']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line fg-toggled">
                                    <label class="">CITY<span class="error">*</span></label>
                                    <select class="form-control selitemIcon" name="city" id="citylist" >
                                    
                                       
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                  <input type="number" name="pincode" id="pincode" maxlength="6" class="form-control" value="<?php echo $docList['pincode']; ?>">
                                  <label class="fg-label">PINCODE<span class="error">*</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="practice_contact" maxlength="10" value="<?php echo $docList['practice_contact']; ?>">
                                    <label class="fg-label">Practice Contact <span class="error"></span></label>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="clinic_address" maxlength="250" value="<?php echo $docList['clinic_address']; ?>">
                                    <label class="fg-label">Clinic / Hospital Address</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="number" class="form-control" name="years_of_practice" maxlength="3" value="<?php echo $docList['experiance']; ?>">
                                    <label class="fg-label">Total Professional Experience <span class="error">*</span></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Highest Educational Qualification<span class="error"> *</span></label>
                           <label class="radio-inline radio-box-style">
                            <input type="radio" name="qualification" id="qualification" value="UG" <?php if( $docList['qualification'] =='UG') { echo "checked"; } ?> onchange="getug(this)"><span class="check-radio"></span> UG
                            </label>
                           <label class="radio-inline radio-box-style">
                             <input type="radio" name="qualification" id="qualification" value="PG" <?php if( $docList['qualification'] =='PG') { echo "checked";} ?> onchange="getpg(this)"><span class="check-radio"></span> PG
                           </label>
                          </div>
                        </div>
                    </div>
                    <div class="row" id="ugdiv" style="display: none;">
                      <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line"><br>
                                    <input type="text" class="form-control" name="ug_qualification" value="<?php echo $docList['qualification_ug']; ?>">
                                    <label class="fg-label">UG Qualification Name<span class="error">*</span></label>
                                </div>
                            </div>                 
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                  <label class="">UG Qualification Certificate<span class="error">*</span></label>
                                    <input type="file" class="form-control" name="ug_qualification_file" >
                                    <div id="span1"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="pgdiv" style="display: none;">
                      <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line"><br>
                                    <input type="text" class="form-control" name="pg_qualification" value="<?php echo $docList['qualification_pg']; ?>">
                                    <label class="fg-label">PG Qualification Name<span class="error">*</span></label>
                                </div>
                            </div>                 
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                  <label class="">PG Qualification Certificate<span class="error">*</span></label>
                                    <input type="file" class="form-control" name="pg_qualification_file" >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="institution" value="<?php echo $docList['institute']; ?>">
                                    <label class="fg-label">University / Institute<span class="error">*</span></label>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="fellow_ship" value="<?php echo $docList['fellowship']; ?>">
                                    <label class="fg-label">Fellowship Number</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <input type="text" class="form-control" name="ima_no" value="<?php echo $docList['ima_no']; ?>">
                                    <label class="fg-label">Indian Medical Assoc. Number<span class="error">*</span></label>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line"><br>
                                    <input type="text" class="form-control" name="licence1" value="<?php echo $docList['licence']; ?>">
                                    <label class="fg-label">Licence 1<span class="error"></span></label>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <label>Licence File 1<span class="error"></span></label>
                                    <input type="file" class="form-control" name="licence_file1">
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line"><br>
                                    <input type="text" class="form-control" name="license2" value="<?php echo $docList['license2']; ?>">
                                    <label class="fg-label">Licence 2<span class="error"></span></label>
                                </div>
                            </div>                            
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line">
                                    <label>Licence File 2<span class="error"></span></label>
                                    <input type="file" class="form-control" name="licence_file2" >
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line"><br>
                                    <label class=""> Checkup Slot<span class="error">*</span></label>
                                    <select class="form-control selitemIcon" name="timeslot" id="timeslot">
                                        <option value="">Select</option>
                                        <?php
                                        for ($i=0; $i <60; $i++) { ?>
                                            <option value="<?php echo $i; ?>" <?php if($i == $docList['timeslot'])  { echo "selected=selected"; }
                                        ?>><?php echo $i; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                                <div class="fg-line"><br>
                                    <input type="text" class="form-control" name="pharmacy" value="<?php echo $docList['pharmacy']; ?>">
                                    <label class="fg-label">Pharmacy Name<span class="error"></span></label>
                                </div>
                            </div>                            
                        </div>
                        
                    </div>
                   <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group fg-float">
                              <div class="fg-line">
                                <label class="">Upload Profile Photo<span class="error"></span></label>
                                <input type="file" class="form-control" name="ProfilePhoto">
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line">
                                <label class="">Upload Clinic/Hospital Logo<span class="error"></span></label>
                                <input type="file" class="form-control" name="logoPhoto">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group fg-float">
                            <div class="fg-line">
                              <label>Upload Signature<span class="error"></span></label>
                              <input type='file' id='sign' class="form-control" name="sign" />
                            </div>
                          </div>
                      </div>
                    </div>
                    </div>
                    <div class="bttn-group">
                        <a href="index.php" class="btn btn-link">Cancel</a>
                        <button class="btn btn-primary btn-lg" name="submit" id="submit">Save</button>
                    </div>
                  </form>                                        
                </div>
            </section>
            <div class="login-links">
              <div class="container" id="popup">
                <div class="modal fade" id="myModal" role="dialog">
                  <div class="modal-dialog modal-sm" style="width: 30%;">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Reset Password</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group fg-float">
                          <div class="fg-line">
                            <div class="row">
                              <div class="col-sm-10">
                                <div class="form-group fg-float">
                                  <div class="fg-line">
                                    <input type="password" name="new_password" id="new_password" class="form-control" autocomplete="off" required>
                                    <label class="fg-label"> Enter New password</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-10">
                                <div class="form-group fg-float">
                                  <div class="fg-line">
                                    <input type="password" class="form-control" name="cnf_password" id="cnf_password" autocomplete="off" required>
                                    <label class="fg-label">Confirm Password</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-lg" id="change" name="change" data-dismiss="modal">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>    

     <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>  
    <script src="../js/main.js"></script>

    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>

 <script type="text/javascript">
    $(document).ready(function()
    {
         $("#form").validate({
     
             rules : {

              first_name : {
                required : true,
                accept : true
              },
              middle_name : {
                accept : true
              },
              last_name : {
                required : true,
                accept : true
              },
              email_address : "required",
              gender : "required",
              stream : "required",
              alter_phone : {
                number: true
            },
              speciality : "required",
              pharmacy : {
                accept : true
              },
              state : "required",
              city : "required",
              pincode : {
                    required : true,
                    number : true,
                    minlength : 6,
                    maxlength : 6
                },
              sub_speciality : "required",
              postal_address : "required",
              
              years_of_practice : "required",
              qualification : "required",
              ug_qualification : {
                required : true,
                accept : true
              },
              pg_qualification : {
                required : true,
                accept : true
              },
              ug_qualification_file : "required",
              pg_qualification_file : "required",

              timeslot : "required",
                
                  mobile : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
                telephone_details : {
                    required : true,
                    number : true,
                    minlength : 11,
                    maxlength : 11

                },
                cell_phone : {
                   number : true,
                    minlength : 10,
                    maxlength : 10

                }
         
            },
            messages : {

                first_name : {
                  required : "<span> enter first name</span>",
                  accept : "<span> enter letters only</span>",
                },
                middle_name : {
                  accept : "<span> enter letters only</span>"
                },
                last_name : {
                  required : "<span> enter last name</span>",
                  accept : "<span> enter letters only</span>",
                },
                speciality : "<span> Select speciality</span>",
                postal_address : "<span> enter postal address</span>",
                email_address : "<span> enter email address </span>",
                years_of_practice : "<span> enter years of practice</span>",
                gender : "<span> Select Gender</span>",
                sub_speciality : "<span> Select sub speciality </span>",
                qualification : "<span> Select Qualification</span>",
                ug_qualification : {
                  required : "<span> enter qualification name</span>",
                    accept : "<span> enter letters only</span>"
                },
                pg_qualification : {
                  required : "<span> enter qualification name</span>",
                    accept : "<span> enter letters only</span>"
                },
                ug_qualification_file : "<span> Upload ug qualification Certificate</span>",
                pg_qualification_file : "<span> Upload Pg qualification Certificate</span>",

                stream : "<span> Select stream</span>",
                pharmacy : {
                    accept : "<span> enter letters only</span>",
                },
                alter_phone : {
                    number : "<span> enter number only</span>",
                },
                timeslot : "<span> Select timeslot</span>",
                state : "<span> Select state</span>",
                city : "<span> Select city</span>",

                 pincode : {
                    required : "<span> enter the pincode</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 6 digit pincode</span>",
                    maxlength : "<span> enter max 6 digit number</span>"
                },
                mobile : {
                    required : "<span> enter the mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                telephone_details : {
                    required : "<span> enter the telephone number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 11 digit number</span>",
                    maxlength : "<span> enter max 11 digit  number</span>"
                },
                 practice_contact : {
                    required : "<span> enter the practice contact number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                alter_phone : {
                    required : "<span> enter the alternative number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                cell_phone : {
                    required : "<span> enter the cell contact number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });
</script>
<script type="text/javascript">
// ug fields

      function getug(c1) {
        var cval = c1.value;
      if(cval =='UG') {
        $("#ugdiv").show();
        $("#pgdiv").hide();
      }
    }

// pg fields

    function getpg(c2) {
        var cval = c2.value;
      if(cval =='PG') {
        $("#ugdiv").show();
        $("#pgdiv").show();
      }
      else
      {
        $("#pgdiv").hide();
      }
    }

    var cval = '<?php echo $docList['qualification']; ?>';
    if(cval =='PG') {
        $("#ugdiv").show();
        $("#pgdiv").show();
      }else
      if(cval =='UG') {
        $("#ugdiv").show();
      }
      else{
        $("#ugdiv").hide();
      }
  
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

     <script type="text/javascript">
$( document ).ready(function() {
    getCities();
    getspecialty();
});

        function getCities(){
          var id = $("#state").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#citylist").html(result);
              var idcityselected = '<?php  echo $docList['city'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#citylist").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

        function getspecialty(){
          var id = $("#stream").val();

          $.ajax({url: "getSpeciality.php?id="+id, success: function(result){
            $("#speciality").html(result);

            var idspecialityselected = '<?php  echo $docList['specialty'];?>';
              if(idspecialityselected!='' && idspecialityselected!= null) {
                 $("#speciality").val(idspecialityselected);
                 get_subspecialty();
              }

          }
        });
        }

        function get_subspecialty(){
          var id = $("#speciality").val();
          console.log(id);

          $.ajax({url: "get_sub_speciality.php?id="+id, success: function(result){
            $("#sub_speciality").html(result);
            var idsub_specialityselected = '<?php  echo $docList['sub_speciality'];?>';
              if(idsub_specialityselected!='' && idsub_specialityselected!= null) {
                 $("#sub_speciality").val(idsub_specialityselected);
              }
          }
        });
        }

        // function getPincode(){
        //   var id = $("#citylist").val();
        //   console.log(id);

        //   $.ajax({url: "getpincode.php?id="+id, success: function(result){
        //     $("#pincode").html(result);

        //      var pincodeselected = '<?php  echo $docList['pincode'];?>';
        //       if(pincodeselected!='' && pincodeselected!= null) {
        //          $("#pincode").val(pincodeselected);
        //       }
        //   }
        // });
          
        // }
    </script>

    <script type="text/javascript">
    $("#change").on('click',function(){
    var did = '<?php echo $did; ?>';
    var newp = $("#new_password").val();
    var cnfp = $("#cnf_password").val();
      $.ajax({
        // type: 'POST',
        url: 'change_password.php',
        data:{
          'did': did,
          'newp': newp,
          'cnfp': cnfp
        },
        success: function(result){
        $("#chageresult").html(result);
        $("#new_password").val('');
        $("#cnf_password").val('');
        // $('#myModal').close();
      }
        });
      });
    </script>

    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>

</body>

</html>