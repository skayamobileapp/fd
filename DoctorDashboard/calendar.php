<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);
$date = date("Y-m-d");
$sql = "Select v.*, e.patient_id from vitals as v INNER JOIN events as e on v.event_id=e.id  where v.patient_id='e.patient_id' and e.status='1'";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $patient[$i]['id'] = $row['id'];
  $patient[$i]['height'] = $row['height'];
  $patient[$i]['weight'] = $row['weight'];
  $patient[$i]['pulse'] = $row['pulse'];
  $patient[$i]['bp'] = $row['bp'];
  $patient[$i]['sugar'] = $row['sugar'];
  $patient[$i]['bmi'] = $row['bmi'];
  $i++;
}

$sql   = "select * from doctor_details";
$result = $conn->query($sql);
$doctorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($doctorList, $row);
}

$sql   = "select * from patient_details";
$result = $conn->query($sql);
$patientList = array();
while ($row = $result->fetch_assoc()) {
    array_push($patientList, $row);
}

$sql   = "select * from lab_details";
$result = $conn->query($sql);
$labList = array();
while ($row = $result->fetch_assoc()) {
    array_push($labList, $row);
}

$sql   = "select * from pharmacy_registration";
$result = $conn->query($sql);
$drugList = array();
while ($row = $result->fetch_assoc()) {
    array_push($drugList, $row);
}

require_once('bdd.php');
$did = $_SESSION['doctor_details']['id'];

// $sql = "SELECT e.*, p.photo FROM events as e INNER JOIN patient_details as p ON p.id=e.patient_id";
$sql = "SELECT * FROM events WHERE doctor_id='$did'";

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Doccure</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../assets/img/favicon.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Full Calander CSS -->
        <link rel="stylesheet" href="../assets/plugins/fullcalendar/fullcalendar.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- Header -->
			<?php include('main-navbar.php'); ?>
			<!-- /Header -->
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						
						<!-- Calendar -->
						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-body">
									<div id="calendar"></div>
								</div>
							</div>
						</div>
						<!-- /Calendar -->
						
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
		
		<!-- Add Event Modal -->
		<div id="ModalAdd" class="modal custom-modal fade" role="dialog">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Add Event</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label>Event Name <span class="text-danger">*</span></label>
								<input class="form-control" type="text">
							</div>
							<div class="form-group">
								<label>Event Date <span class="text-danger">*</span></label>
								<div class="cal-icon">
									<input class="form-control datetimepicker" type="text">
								</div>
							</div>
							<div class="submit-section">
								<button class="btn btn-primary submit-btn">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Add Event Modal -->
		
		<!-- Add Event Modal -->
		<div class="modal custom-modal fade none-border" id="my_event">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Add Event</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer justify-content-center">
						<button type="button" class="btn btn-success save-event submit-btn">Create event</button>
						<button type="button" class="btn btn-danger delete-event submit-btn" data-dismiss="modal">Delete</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /Add Event Modal -->
		
		<!-- Add Category Modal -->
		<div class="modal custom-modal fade" id="add_new_event">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Add Category</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label>Category Name</label>
								<input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
							</div>
							<div class="form-group">
								<label>Choose Category Color</label>
								<select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
									<option value="success">Success</option>
									<option value="danger">Danger</option>
									<option value="info">Info</option>
									<option value="primary">Primary</option>
									<option value="warning">Warning</option>
									<option value="inverse">Inverse</option>
								</select>
							</div>
							<div class="submit-section text-center">
								<button type="button" class="btn btn-primary save-category submit-btn" data-dismiss="modal">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /Add Category Modal -->

		 <script>

  $(document).ready(function() {
    
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      // defaultDate: '2016-01-12',
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
        
        $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
        $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
        if(start.isBefore(moment())) {
          alert("Appointments are not allowed on this date");
          $('#calendar').fullCalendar('unselect');
          return false;
        }
        $('#ModalAdd').modal('show');
      },

      eventRender: function(event, element) {
        element.bind('click', function() {
         $('#ModalEdit #id').val(event.id);
          $('#ModalEdit #title').val(event.title);
          $('#ModalEdit #color').val(event.color);
          $('#ModalEdit #start').val(event.start);
          $('#ModalEdit #end').val(event.end);
          $('#ModalEdit #patient_name').val(event.patient_name);
          $('#ModalEdit #mobile').val(event.mobile);
          // $('#ModalEdit img').val(event.photo);
          $('#ModalEdit #height').val(event.height);
          $('#ModalEdit #pulse').val(event.pulse);
          $('#ModalEdit #weight').val(event.weight);
          $('#ModalEdit #bp').val(event.bp);
          $('#ModalEdit #sugar').val(event.sugar);
          $('#ModalEdit #bmi').val(event.bmi);
          $('#ModalEdit').modal('show');
        });
      },


      eventDrop: function(event, delta, revertFunc) { // si changement de position

        edit(event);

      },
      eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

        edit(event);

      },
      events: [
      <?php foreach($events as $event): 
      
        $start = explode(" ", $event['start']);
        $end = explode(" ", $event['end']);
        if($start[1] == '00:00:00'){
          $start = $start[0];
        }else{
          $start = $event['start'];
        }
        if($end[1] == '00:00:00'){
          $end = $end[0];
        }else{
          $end = $event['end'];
        }
      ?>
        {
          id: '<?php echo $event[id]; ?>',
          title: '<?php echo $event[title]; ?>',
          start: '<?php echo $start; ?>',
          end: '<?php echo $end; ?>',
          // color: '<?php echo $event[color]; ?>',
          patient_name: '<?php echo $event[patient_name]; ?>',
          mobile: '<?php echo $event[mobile]; ?>',
          photo: '<?php echo $event[photo]; ?>',
          height: '<?php echo $event[height]; ?>',
          pulse: '<?php echo $event[pulse]; ?>',
          weight: '<?php echo $event[weight]; ?>',
          bp: '<?php echo $event[bp]; ?>',
          sugar: '<?php echo $event[sugar]; ?>',
          bmi: '<?php echo $event[bmi]; ?>'
        },
      <?php endforeach; ?>
      ]
    });
    
    function edit(event){
      start = event.start.format('YYYY-MM-DD HH:mm:ss');
      if(event.end){
        end = event.end.format('YYYY-MM-DD HH:mm:ss');
      }else{
        end = start;
      }
      
      id =  event.id;
      
      Event = [];
      Event[0] = id;
      Event[1] = start;
      Event[2] = end;
      
      $.ajax({
       url: 'editEventDate.php',
       type: "POST",
       data: {Event:Event},
       success: function(rep) {
          if(rep == 'OK'){
            alert('Saved');
          }else{
            alert('Could not be saved. try again.'); 
          }
        }
      });
    }
    
  });
  
</script>
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Datetimepicker JS -->
		<script src="../assets/js/moment.min.js"></script>
		<script src="../assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Full Calendar JS -->
        <script src="../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="../assets/plugins/fullcalendar/fullcalendar.min.js"></script>
        <script src="../assets/plugins/fullcalendar/jquery.fullcalendar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
		
	</body>
</html>