<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];

$item=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from items where id='$id' ";
    $result   = $conn->query($sql);
    $item = $result->fetch_assoc();
   }

if($_POST)
{
    if($_GET['id'])
    {
      $id = $_GET['id'];
      $name = $_POST['name'];
      $cat_name = $_POST['cat_name'];
      $company = $_POST['company'];

      $sql = "UPDATE items SET item_name='$name', item_category_id='$cat_name', company='$company' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Consumable details updated successfully');</script>";
        echo "<script>parent.location='consumables-list.php'</script>";
      }
    }
    else
    {
      $doc_id = $_SESSION['doctor_details']['id'];

      $name = $_POST['name'];
      $cat_name = $_POST['cat_name'];
      $company = $_POST['company'];

      $sql1 = "SELECT * FROM items WHERE item_name = '$name' AND item_category_id='$cat_name' AND id_doctor='$doc_id'";
      $result1  = $conn->query($sql1);
      $resnum = mysqli_num_rows($result1);

      if ($resnum < 1) {

      $sql = "INSERT INTO items(item_name, item_category_id, company, id_doctor, status) VALUES('$name', '$cat_name', '$company', '$doc_id', '1')";
        $result  = $conn->query($sql);
        if ($result)
        {
          echo "<script>alert('Consumable added successfully');</script>";
          echo "<script>parent.location='consumables-list.php'</script>";
        }
      }
      else
      {
          echo "<script>alert('Consumable Already exits');</script>";
      }
    }
}
$sql ="SELECT id, category FROM item_categories";
$result = mysqli_query($conn, $sql);
  $i = 0;
  $catList = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($catList, $row);
  }
?>
<!DOCTYPE html> 
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="consumables-list.php">Consumables List</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Add Consumable</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Add Consumable</h4>
									<div class="row form-row">
										<div class="col-sm-6">
                        <div class="form-group">
                              <label class="">SELECT CONSUMABLE CATEGORY<span class="text-danger">*</span></label>
                                <select name="cat_name" class="form-control selitemIcon" id="cat_name">
                                  <option value="">SELECT</option>
                                  <?php for($i=0; $i<count($catList); $i++){?>
                                    <option value="<?php echo $catList[$i]['id']; ?>" <?php 
                                    if ($catList[$i]['id']==$item['item_category_id']) {
                                      echo "selected";
                                    }
                                    ?> ><?php echo $catList[$i]['category']; ?></option>
                                  <?php } ?>
                                  </select>
                        </div>                            
                      </div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Consumable Name <span class="text-danger">*</span></label>
												<input type="text" name="name" class="form-control" id="name" value="<?php echo $item['item_name'];?>" autocomplete="off">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label> Company Name <span class="text-danger"> *</span></label>
												<input type="text" name="company" class="form-control" id="company" value="<?php echo $item['company'];?>" autocomplete="off">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="consumables-list.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Dropzone JS -->
		<script src="../assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              name : {
                required : true,
                accept: true
            },
            company : {
                required : true,
                accept: true
            },
              cat_name : "required"
         
            },
            messages : {

                name : {
               required : "<span> enter Consumable name</span>",
               accept : "<span> enter letters only</span>"
               },
               company : {
               required : "<span> enter company name</span>",
               accept : "<span> enter letters only</span>"
               },
                cat_name : "<span> select Consumable category</span>"
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

		 <script type="text/javascript">
$( document ).ready(function() {

	 var idstate = '<?php  echo $clinicDetails['state_id'];?>';
	 if(idstate!='' && idstate!= null) {
		getCities();
	 }
    // getCities();
    // getspecialty();
});

        function getCities(){
          var id = $("#state_id").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#city_id").html(result);
              var idcityselected = '<?php  echo $clinicDetails['city_id'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#city_id").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

       

        function getPincode(){
          var id = $("#city_id").val();
          console.log(id);

          $.ajax({url: "getpincode.php?id="+id, success: function(result){
            $("#pincode").html(result);

             var pincodeselected = '<?php  echo $clinicDetails['pincode'];?>';
              if(pincodeselected!='' && pincodeselected!= null) {
                 $("#pincode").val(pincodeselected);
              }
          }
        });
          
        }
    </script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>