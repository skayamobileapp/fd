<?php

$sql   = "select id, patient_name, email, mobile_number, photo from patient_details";
$result = $conn->query($sql);
$patientlist = array();
while ($row = $result->fetch_assoc()) {
    array_push($patientlist, $row);
}

$sql   = "select id, lab_name, mobile, email, profile_pic from lab_details";
$result = $conn->query($sql);
$labList = array();
while ($row = $result->fetch_assoc()) {
    array_push($labList, $row);
}

$sql   = "select id, pharma_name, mobile, email, profile_pic from pharmacy_registration";
$result = $conn->query($sql);
$drugList = array();
while ($row = $result->fetch_assoc()) {
    array_push($drugList, $row);
}
$count=0;
$did = $_SESSION['doctor_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Doctor' and id_user='$did') and  doctor_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);

  // $count = 10;
?>


<nav class="navbar navbar-default dashboard-navbar navbar-fixed-top">
      <div class="container-fluid">
	  <div id="navbar" class="navbar-collapse collapse">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">First Doctor</a>
        </div>
          <ul class="nav navbar-nav main-nav stats-nav">
                <li><a href="patient_list.php" class="">Patients<span class="stats-count"><?php echo count($patientlist); ?></span></a></li>
                <li><a href="diagnostic_list.php">Diagnostic Service<span class="stats-count"><?php echo count($labList); ?></span></a></li>
                <li><a href="pharmacy_list.php">Pharmacy<span class="stats-count"><?php echo count($drugList); ?></span></a></li>  
         <!-- </ul>
          <ul class="nav navbar-nav navbar-right main-nav stats-nav"> -->
            <li  style="    color: #fff;
    border: 1px solid #02afee;
    background-color: #02afee;"><a href="customer_list.php" style="    color: white;
    font-weight: bold;
    font-size: 16px; width: 180px; height: 60px;">Customer Support Center<span class="stats-count"></span></a></li>

            <li><a href="view_notifications.php" class="notifications-link"><sup id="notification-count"><?php if($count>0) { echo $count; } ?></sup> </a></li>

                    <li><a href="logout.php">Logout</a></li>
               <!--  </ul>                
            </li> -->
          </ul>
        </div>
      </div>
    </nav>