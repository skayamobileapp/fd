<?php
include('../connection/conn.php');
include('session_check.php');
date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('Y-m-d h:i:s');
$interval_date = date('Y-m-d h:i:s', strtotime($date. '+120 days'));
$eid = $_GET['id'];
$did = $_SESSION['doctor_details']['id'];
$currDate = date("M-Y");

$sql = "Select p.*, e.id as eid, e.title,e.amount from patient_details as p INNER JOIN events as e on p.id=e.patient_id where e.id='$eid'";
$select = mysqli_query($conn,$sql);
while ($row = mysqli_fetch_array($select))
    {
        $amount1 = $row['amount'];
        $pid = $row['id'];
        $fname = $row['first_name'];
        $mname = $row['middle_name'];
        $lname = $row['last_name'];
        $fullname = $fname." ".$mname." ".$lname;
        $phone = $row['patient_phone_number'];
        $mobile_number = $row['mobile_number']; 
        $DateOfBirth = $row['date_of_birth'];
        $email = $row['email'];
    }

$doctor_id = $_SESSION['doctor_details']['id'];
$patient_id = $pid;

$amount = 0;
$old_total = 0;
$interval_for_followup = 0;
$number_of_follow_ups = 0;
$subscription_id = '1';
$follow_up_id = '1';
$follow_up_no = '1';
$last_event_count = 0;
$interval_amount = 0;
$is_follow_up = '0';




    $select_old_balance = "SELECT * FROM `payment_table` where doctor_id = '$doctor_id' and patient_id = '$patient_id' ORDER BY `id`  DESC limit 0,1";
    $result_old_balance = mysqli_query($conn,$select_old_balance);

       while ($row_old_balance = mysqli_fetch_assoc($result_old_balance))
      {
         $patient_old_balance = $row_old_balance['balance_amount'];
      }
 $old_total = $old_total + $patient_old_balance;
  

$select = "SELECT dd.id, ds.amount, ds.id as subscription_id, ds.subscriptoin_plan, ds.interval_for_followup from doctor_details dd inner join doctor_subscription ds on ds.id = dd.id_subscription where dd.id = '$doctor_id'";

  $resultcity = mysqli_query($conn,$select);


   while ($row=mysqli_fetch_assoc($resultcity))
  {
     $subscription_id = $row['subscription_id'];
     $interval_for_followup = $row['interval_for_followup'];
     $subscription = $row['subscriptoin_plan'];
     $amount = $row['amount'];
  }


if($subscription == "Postpaid")
{
  $view['name'] = $old_total;
$view['status'] = 200;
  $view['message'] = $amount;
}

if($subscription == "Prepaid")
{
  $view['name'] = $old_total;
  $view['status'] = 208;
  $view['message'] = "0";
  //$view['message'] = $amount;
}


 $sql = "SELECT count(*) as no FROM `doctor_subscription_interval` where id_subscription = '$subscription_id' and status = '1'";
$result = $conn->query($sql);
while ($row = $result->fetch_assoc())
{
  $number_of_follow_ups = $row['no'];
}
$stop_date = date('Y-m-d', strtotime($currentDate . " -$interval_for_followup day"));

$sql_select_event_count = "SELECT count(*) as event_count FROM `events` where doctor_id = '$doctor_id' and patient_id = '$patient_id' and date(end) > '$stop_date' and status = '1' and payee_status='1' ORDER BY `id` DESC limit 0,1";
    $result_event_count = mysqli_query($conn,$sql_select_event_count);
while ($row_result_event_count = mysqli_fetch_assoc($result_event_count))
{
  $last_event_count = $row_result_event_count['event_count'];
}
//echo json_encode($last_event_count);exit;

if($number_of_follow_ups == $last_event_count)
{
  if($number_of_follow_ups == 0)
  {
    $number_of_follow_ups = '1';
  }
  $sql_interval_amount = "SELECT * FROM `doctor_subscription_interval` where id_subscription = '$subscription_id' and follow_up_no = '$number_of_follow_ups' and status = '1' limit 0,1"; 
}
else if($number_of_follow_ups < $last_event_count)
{
  $sql_interval_amount = "SELECT * FROM `doctor_subscription_interval` where id_subscription = '$subscription_id' and status = '1' order by follow_up_no DESC limit 0,1";
}

else if($number_of_follow_ups > $last_event_count)
{
  if($last_event_count == '0')
  {
    $last_event_count = $last_event_count+1;
  }
  $sql_interval_amount = "SELECT * FROM `doctor_subscription_interval` where id_subscription = '$subscription_id' and follow_up_no = '$last_event_count' limit 0,1";
}


//echo json_encode($last_event_count);exit;
$result_interval_data = $conn->query($sql_interval_amount);
while ($row_interval_data = $result_interval_data->fetch_assoc())
{
  $follow_up_id = $row_interval_data['id'];
  $follow_up_no = $row_interval_data['follow_up_no'];
  $interval_amount = $row_interval_data['amount'];
  $subscription_id = $row_interval_data['id_subscription'];
  $r = $row_interval_data;
  $is_follow_up = '1';
  
  if($interval_amount == '' || $interval_amount == null)
  {
    $interval_amount = $amount;
  }
}

$view['message'] = $interval_amount;
$view['id_follow_up'] = $follow_up_id;
$view['id_subscription'] = $subscription_id;
$view['follow_up_no'] = $follow_up_no;
$view['is_follow_up'] = $is_follow_up;


$billing_amount = $view['message'];
$id_followup = $view['id_follow_up'];
$id_subscription = $view['id_subscription'];
$followup_no = $view['follow_up_no'];
$is_follow = $view['is_follow_up'];
// echo json_encode($view);exit;

  $select = "SELECT payment_date, total_amount, amount, balance_amount FROM payment_table WHERE patient_id='$pid' AND doctor_id='$did' ";
$result = mysqli_query($conn, $select);
$i = 0;
$view1 = array();
while ($row = mysqli_fetch_assoc($result)) {

  $view1[$i]['payment_date'] = $row['payment_date'];
  $view1[$i]['total_amount'] = $row['total_amount'];
  $view1[$i]['amount'] = $row['amount'];
  $view1[$i]['balance_amount'] = $row['balance_amount'];
  $i++;
}

$select2 = mysqli_query($conn,"SELECT * FROM payment_table WHERE patient_id='$pid' AND doctor_id='$did' ORDER BY id DESC LIMIT 1");
while ($row2 = mysqli_fetch_assoc($select2)) {
  $bal_amount = $row2['balance_amount'];
  }
  if ($bal_amount == '') {
    $bal_amount = '0';
  }

  $select1 = mysqli_query($conn,"SELECT s.amount, sum(b.amount) as tot FROM doctor_outstanding_balance as b INNER JOIN doctor_details as d ON d.id=b.id_doctor INNER JOIN doctor_subscription as s ON s.id = d.id_subscription WHERE d.id='$did' AND b.month='$currDate'");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $billamount = $row1['amount'];
  $tot = $row1['tot'];
  $total = $tot+$billamount;
  }

if ($_POST) {

  $did = $_SESSION['doctor_details']['id'];

  $id = $_GET['id'];
  $fullname = $_POST['name'];
  $pdate = $_POST['date'];
  $pamount = $_POST['amount'];
  $tot_amount = $_POST['tot_amount'];
  $balance = $pamount - $tot_amount;
  $pmode = $_POST['paymentMode'];
  $pid = $_POST['pid'];
  $month = date('M-Y');

$billing_amount = $view['message'];
$id_followup = $view['id_follow_up'];
$id_subscription = $view['id_subscription'];
$followup_no = $view['follow_up_no'];
$is_follow = $view['is_follow_up'];

  $sql= "INSERT INTO payment_table(patient_id, patient_name, payment_date, total_amount, amount, payment_mode, doctor_id, event_id, balance_amount, is_follow_up, id_follow_up, follow_up_no, id_subscription) VALUES('$pid', '$fullname', '$pdate', '$tot_amount', '$pamount', '$pmode', '$did', '$id', '$balance', '$is_follow', '$id_followup', '$followup_no', '$id_subscription')";

  $select1 = mysqli_query($conn,"SELECT s.amount, sum(b.amount) as tot FROM doctor_outstanding_balance as b INNER JOIN doctor_details as d ON d.id=b.id_doctor INNER JOIN doctor_subscription as s ON s.id = d.id_subscription WHERE d.id='$did' AND b.month='$currDate'");
while ($row1 = mysqli_fetch_assoc($select1)) {
  // $billamount = $row1['amount'];
  $tot = $row1['tot'];
  $total = $tot+$billing_amount;
  }

  mysqli_query($conn, "INSERT INTO doctor_outstanding_balance(date_time, month, id_doctor, id_patient, id_event, amount, total, status) VALUES('$pdate', '$month','$did', '$pid', '$id', '$billing_amount', '$total', '0')");
  $result = $conn->query($sql);
  if ($result) {
  mysqli_query($conn, "UPDATE events SET payee_status='1', end='$date' WHERE id='$eid' ");

    echo "<script>alert('payment details added successfully')</script>";
    echo "<script>parent.location='index.php'</script> ";
  }
}


?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
		                <form action="" method="POST">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0">Add Billing</h4>
								</div>
								<div class="card-body">
									<div class="row">
					                  <div class="col-sm-6">
					                    <div class="form-group">
					                        <label >Patient Name<span class="error"></span></label>
					                        <input type="text" name="name" class="form-control" id="name" value="<?php echo $fullname;?>" readonly>
					                      </div>
					                    </div>
						                <div class="col-sm-6">
						                    <div class="form-group">
					                        <label>Payment Date<span class="error"></span></label>
					                        <input type="text" name="date" class="form-control" id="date" value="<?php echo date("Y-m-d h:i:s");?>" readonly>
					                    </div>
					                  </div>
					                </div>
					                <div class="row">
					                  <div class="col-sm-6">
					                    <div class="form-group">
					                        <label class="">Mode Of Payment<span class="error"></span></label>
					                        <select name="paymentMode" class="form-control selitemIcon" id="paymentMode" placeholder="Enter Amount" required="required">
					                          <option value="">SELECT PAYMENT MODE</option>
					                          <option value="cash">Cash</option>
					                          <option value="credit">Credit Card</option>
					                          <option value="debit">Debit Card</option>
					                        </select>
					                    </div>
					                  </div>
					                  <div class="col-sm-6">
					                    <div class="form-group">
					                        <label>Amount<span class="error"></span></label>
					                        <input type="number" name="amount" class="form-control" id="amount" placeholder="Enter Amount" autocomplete="off" onkeyup="getamount()" required value="<?php echo $amount1;?>" maxlength="10">
					                    </div>
					                  </div>
					                  <input type="hidden" name="tot_amount" value="<?php echo $amount1-$bal_amount; ?>" id="tot_amount">
					                </div>
					                <div class="row">
          					<div class="col-sm-6">
          						<input type="hidden" name="pid" value="<?php echo $pid; ?>">
          					</div>
                  </div>
                <div class="row">
                  <div class="col-sm-6">
                    <b>Payment Details</b><hr>
                    Appointment Amount(₹) : <block class="pull-right"><?php echo $amount1;?>.00</block> <br>
                    First Doctor Billing Amount(₹) : <block class="pull-right"><?php echo $billing_amount;?>.00 </block>
                    <br>
                    Previous Balance(₹) : <block class="pull-right"><?php echo $bal_amount;?>.00 </block>
                  </div>
                  <div class="col-sm-6">
                    <h4 class="fa fa-eye pull-right"><a  href="#" data-toggle="modal" data-target="#newModal">Previous Payment details</a></h4><br>
                    <hr>
                    Total Payable Amount(₹) : <block class="pull-right"><?php echo $amount1-$bal_amount;?>.00</block> <br>
                    Paid Amount(₹) : <block class="pull-right" id="payamount"><?php echo $amount1;?>.00</block><br>
                    Balance Amount(₹) : <block class="pull-right" id="balamount"><?php echo $amount1-$bal_amount; ?>.00</block>
                  </div>
                </div>
									
									<!-- Signature -->
									<!-- <div class="row">
										<div class="col-md-12 text-right">
											<div class="signature-wrap">
												<div class="signature">
													Click here to sign
												</div>
												<div class="sign-name">
													<p class="mb-0">( Dr. Darren Elder )</p>
													<span class="text-muted">Signature</span>
												</div>
											</div>
										</div>
									</div> -->
									<!-- /Signature -->
									<br>
									<!-- Submit Section -->
									<div class="row">
										<div class="col-md-12">
											<div class="submit-section float-right">
												<a href="index.php" class="btn btn-light">Cancel</a>
												<button type="submit" class="btn btn-primary submit-btn" name="save" id="save">Pay</button>
											</div>
										</div>
									</div>
									<!-- /Submit Section -->
									
								</div>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->

		<div  id="popup">
      <div class="modal fade" id="newModal" role="dialog">
        <div class="modal-dialog modal-md">
           <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;"><br> Previous Payment Details. </h4>
            </div>
            <div class="modal-body">
                <div class="fg-line">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group text-center">
                        <div class="table-responsive theme-table v-align-top">
                          <table class="table" id="example">
                            <tr>
                              <th>Payment Date</th>
                              <th>Total Amount</th>
                              <th>Payed Amount</th>
                              <th>Balance Amount</th>
                            </tr>
                        <?php for($i=0; $i<count($view1); $i++){?>
                            <tr>
                              <th><?php echo $view1[$i]['payment_date']; ?></th>
                              <th><?php echo $view1[$i]['total_amount']; ?></th>
                              <th><?php echo $view1[$i]['amount']; ?></th>
                              <th><?php echo $view1[$i]['balance_amount']; ?></th>
                            </tr>
                          <?php } ?>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <a href='#' class='btn btn-primary btn-lg' id='ok' name='ok' data-dismiss='modal'>Ok</a>
           </div>
          </div>
      </div>
    </div>
  </div>
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
		<script type="text/javascript">

            function getamount() {
              var amount = $("#amount").val()+'.00';
              var tot = $("#tot_amount").val()+'.00';
              var balance = amount - tot;
                $("#payamount").html(amount);
                $("#balamount").html(balance);
            }

              var amount1 = $("#amount").val()+'.00';
              var tot = $("#tot_amount").val()+'.00';
              var balance = amount1 - tot;
                $("#payamount").html(amount1);
                $("#balamount").html(balance);
</script>
		
	</body>
</html>