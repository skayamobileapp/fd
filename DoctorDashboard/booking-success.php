<?php 
include('../connection/conn.php');
include('session_check.php');

  $event_id = $_GET['eid'];
$did = $_GET['did'];
$slotid = $_GET['slot_id'];
$title = $_GET['purpose'];
$patient_id = $_GET['patient_id'];
$patientName = $_GET['patient_name'];
$mobile = $_GET['mobile'];

$sql = "select * from doctor_details where id ='$did'";
$result = $conn->query($sql);
$doctor_data = $result->fetch_assoc();

                
                mysqli_query($conn,"UPDATE timeslots SET slot='r' WHERE id='$sid'");

                mysqli_query($conn,"UPDATE events SET rescheduled_by=1, status=2 WHERE id='$event_id' ");

                $slot_query= "SELECT * FROM timeslots WHERE id='$slotid' AND doctor_id='$did' ORDER BY date";
                $slot_result = $conn->query($slot_query);
                $row = $slot_result->fetch_assoc();
                $date = $row['date'];
                $time = $row['time'];
                $slot_id = $row['id'];
                $id_clinic = $row['id_clinic'];
                $datetime  = $date." ".$time;
                $etime = strtotime($row['time']);
                // $startTime = date("H:i", strtotime('-30 minutes', $etime));
                $endTime = date("H:i", strtotime('+30 minutes', $etime));
                $enddatetime = $date." ".$endTime;
				$did = $_GET['did'];

                $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, id_time_slots, id_clinic, patient_name, mobile, height, weight, bp, sugar, bmi, status, pulse) VALUES('$title', '$datetime', '$enddatetime', '$did', '$patient_id', '$slot_id', '$id_clinic', '$patientName', '$mobile','NA','NA','NA','NA', 'NA', '0','NA')";

                $result = $conn->query($sql);
                if ($result) {
                  mysqli_query($conn,"UPDATE timeslots SET slot='n' WHERE id='$slotid' ");

                  echo "<script>alert('Appointment Rescheduled to '+ '".date("d M Y h:i:a", strtotime($datetime))."')</script>";
                }

                $slot_query= "SELECT * FROM timeslots WHERE id='$slotid' ORDER BY date";
                $slot_result = $conn->query($slot_query);
                $row = $slot_result->fetch_assoc();
                $date = $row['date'];
                $time = $row['time'];
                $slot_id = $row['id'];
                $id_clinic = $row['id_clinic'];
                $datetime  = $date." ".$time;
                $etime = strtotime($row['time']);
                // $startTime = date("H:i", strtotime('-30 minutes', $etime));
                $endTime = date("H:i", strtotime('+30 minutes', $etime));
                $enddatetime = $date." ".$endTime;
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content success-page-cont">
				<div class="container-fluid">
				
					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
						
							<!-- Success Card -->
							<div class="card success-card">
								<div class="card-body">
									<div class="success-cont">
										<i class="fas fa-check"></i>
										<h3>Appointment booked Successfully!</h3>
										<p>Appointment booked with Patient : <strong><?php echo ucfirst($patientName); ?></strong><br> on <strong><?php echo date("d M Y", strtotime($date)); ?> <?php echo date("h:i A", strtotime($time)); ?> to <?php echo date("h:i A", strtotime($endTime)); ?></strong></p>
										<a href="index.php" class="btn btn-primary view-inv-btn">OK</a>
									</div>
								</div>
							</div>
							<!-- /Success Card -->
							
						</div>
					</div>
					
				</div>
			</div>		
			<!-- /Page Content -->
   
			<?php include('footer.php'); ?>
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
		
	</body>
</html>