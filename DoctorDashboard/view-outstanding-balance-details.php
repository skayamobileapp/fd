<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$id = $_GET['id'];
$did = $_SESSION['doctor_details']['id'];

$sql="SELECT d.date_time, d.amount, e.start, p.patient_name FROM doctor_outstanding_balance d INNER JOIN events e ON d.id_event=e.id INNER JOIN patient_details p ON e.patient_id=p.id WHERE d.id_doctor='$did' AND date(d.date_time)='$id'";
  $query = mysqli_query($conn,$sql);
  $viewdata =[];
  $i=0;
  while($row=mysqli_fetch_assoc($query))
  {
      $n =$i+1;
      $viewdata[$i]['date_time']=$row['date_time'];
      $viewdata[$i]['amount']=$row['amount'];
      $viewdata[$i]['patient_name']=$row['patient_name'];
      $viewdata[$i]['start']=$row['start'];
      $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
		<div class="col-md-7 col-lg-8 col-xl-9">
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Outstanding Balance List <a href="doctor-outstanding-balance.php" class="btn btn-primary float-right btn-lg">Back</a></h3><br>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
													<th>SL. NO</th>
								                      <th>Patient Name</th>
								                      <th>Appointment Date</th>
								                      <th>Payment Date</th>
								                      <th>Amount</th>
												</tr>
											</thead>
											<tbody>
												<?php
            
            for ($j=0; $j <count($viewdata) ; $j++) { 
              ?>
              <tr>
                <td><?php echo $j+1; ?></td>
                <td><?php echo ucfirst($viewdata[$j]['patient_name']); ?></td>
                <td><?php echo date('d M Y h:i a',strtotime($viewdata[$j]['start'])); ?></td>
                <td><?php echo date('d M Y H:i:a',strtotime($viewdata[$i]['date_time'])); ?></td>
                <td><?php echo $viewdata[$j]['amount'] ; ?></td>
               

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
	
    </body>
</html>