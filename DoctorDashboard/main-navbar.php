<?php

$sql   = "select id, patient_name, email, mobile_number, photo from patient_details";
$result = $conn->query($sql);
$patientlist = array();
while ($row = $result->fetch_assoc()) {
    array_push($patientlist, $row);
}

$sql   = "select id, lab_name, mobile, email, profile_pic from lab_details";
$result = $conn->query($sql);
$labList = array();
while ($row = $result->fetch_assoc()) {
    array_push($labList, $row);
}

$sql   = "select id, pharma_name, mobile, email, profile_pic from pharmacy_registration";
$result = $conn->query($sql);
$drugList = array();
while ($row = $result->fetch_assoc()) {
    array_push($drugList, $row);
}
$count=0;
$did = $_SESSION['doctor_details']['id'];
$notesql= "SELECT * FROM notifications where id not in ( Select id_notify from notification_read where read_by='Doctor' and id_user='$did') and  doctor_flag=1";
  $result=mysqli_query($conn, $notesql);
  $count=mysqli_num_rows($result);

  // $count = 7;
?>
<!-- Header -->
<style type="text/css">
	#notification-count {
    position: absolute;
    right: 105px;
    background: rgba(251, 188, 5, 1);
    width: 24px;
    text-align: center;
    border-radius: 15px;
    color: #000;
    top: 15px;
    font-weight: normal;
    font-size: 12px;
    line-height: 30px;
}
</style>
			<header class="header">
				<nav class="navbar navbar-expand-lg header-nav">
					<div class="navbar-header">
						<a id="mobile_btn" href="javascript:void(0);">
							<span class="bar-icon">
								<span></span>
								<span></span>
								<span></span>
							</span>
						</a>
						<a href="../index.php" class="navbar-brand logo">
							<img src="../logo.png" class="img-fluid" alt="Logo">
						</a>
					</div>
					<ul class="nav header-navbar-rht">
			            <!-- <li class="nav-item dropdown has-arrow logged-item">
							<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
								<span class="caret">
									Find Providers
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item" href="find-lab.php">Diagnostic Labs</a>
								<a class="dropdown-item" href="find-pharma.php">Pharmacy Stores</a>
							</div>
						</li> -->

						<!-- <li class="nav-item logged-item">
			                <a href="find-doctor.php" class="btn btn-primary">Find Doctor <span class="caret"></span></a>               
			            </li> -->
			            <li><a href="chat-patient.php">Patients</a></li>
			            <li><a href="chat-diagnostic.php">Diagnostic Service</a></li>

			            <li><a href="chat-pharmacy.php">Pharmacy</a></li>
						<li class="nav-item logged-item">
			                <a href="customer-list.php" class="btn btn-primary btn-lg">Customer Support Center <span class="caret"></span></a>               
			            </li>
            <li><a href="view-notifications.php"><i class="fas fa-bell fa-2x"></i><sup id="notification-count"><?php if($count>0) { echo $count; } ?></sup> </a></li>

						<!-- User Menu -->
						<li class="nav-item dropdown has-arrow logged-item">
							<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
								<span class="user-img">
									<img class="rounded-circle" src="../uploads/<?php echo $_SESSION['doctor_details']['photo']; ?>" width="31" alt="">
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="user-header">
									<div class="avatar avatar-sm">
										<img src="../uploads/<?php echo $_SESSION['doctor_details']['photo']; ?>" alt="" class="avatar-img rounded-circle">
									</div>
									<div class="user-text">
										<h6><?php echo $_SESSION['doctor_details']['doctor_name']; ?></h6>
										<p class="text-muted mb-0">Doctor</p>
									</div>
								</div>
								<a class="dropdown-item" href="index.php">Dashboard</a>
								<a class="dropdown-item" href="doctor-profile.php">Profile Settings</a>
								<a class="dropdown-item" href="../doctor_login.php">Logout</a>
							</div>
						</li>
						<!-- /User Menu -->
						
					</ul>
				</nav>
			</header>
			<!-- /Header -->