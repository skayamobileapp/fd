<?php
include('../connection/conn.php');
include('session_check.php');
// include('monthly_payment_check.php');

error_reporting(0);

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');
 //$idadmin = $_SESSION['admin']['id'];
 // print_r($idadmin).exit();
$current_month = date('M-Y');

$did = $_SESSION['doctor_details']['id'];

// $previous_month = date("M", strtotime("-1 month"));
// $previous_year = date("Y", strtotime("-1 month"));
// $history_month = date("M", strtotime("-2 month"));
// $history_year = date("Y", strtotime("-2 month"));

   $discount_amount = '0';
    $discount_percentage = '0';
    $discount_description = '';
    $after_discount_total = '0';
    $id_subscription = '0';

$sel_get_doctor_discount = "SELECT ds.* FROM `doctor_subscription` ds inner join doctor_details dd on ds.id = dd.id_subscription where dd.id = '$did' and ds.status = '1' order by dd.id DESC limit 0,1";
  $result_discount          = $conn->query($sel_get_doctor_discount);
  
//echo json_encode($sel_get_outstanding_total_balance);exit;

  while ($row_discount = $result_discount->fetch_assoc())
  {
    $discount_percentage = $row_discount['final_packege_discount_percent'];
    $id_subscription = $row_discount['id'];
    $discount_description = $row_discount['discount_description'];
  }


$sql_select_month = "SELECT distinct(dob.month) as month FROM `doctor_outstanding_balance` dob  where dob.id_doctor = '$did'";

  $result_month          = $conn->query($sql_select_month);
  
//echo json_encode($sql_select_month);exit;
$y = array();
$i = 0;
$data_array = array();
while ($row_month = $result_month->fetch_assoc())
{
  $y[$i] = $row_month;

  $previous_date = $row_month['month'];
  
  $sel_get_outstanding_total_balance = "SELECT dob.* from `doctor_outstanding_balance` dob where dob.month = '$previous_date' and dob.id_doctor = '$did' and dob.status = '0' order by dob.id DESC limit 0,1";
    $result_total_sum          = $conn->query($sel_get_outstanding_total_balance);

  while ($row_total_sum = $result_total_sum->fetch_assoc())
  {
    //if($i == 1)
  //{
  //echo json_encode($data_array);exit;
  //}
    $data_variable['id'] = $row_total_sum['id'];
    $data_variable['total'] = $row_total_sum['total'];
    $data_variable['month'] = $row_total_sum['month'];
    
    if(isset($data_variable) && ($data_variable != null || $data_variable != ''))
    {
      array_push($data_array, $data_variable);
    }
  }
  $i++;
}
//echo json_encode($data_array);exit;
//echo json_encode($y);exit;
$history_balance_amount = $data_array[0]['total'];
$history_month = $data_array[0]['month'];

$previous_balance_amount = $data_array[1]['total'];
$previous_month = $data_array[1]['month'];

if($previous_month == null)
{
  $previous_month = '';
}
if($history_month == null)
{
  $history_month = '';
}
if($history_paid_amount == null)
{
  $history_paid_amount = '0';
}
if($history_balance_amount == null)
{
  $history_balance_amount = '0';
}
if($previous_paid_amount == null)
{
  $previous_paid_amount = '0';
}
if($previous_balance_amount == null)
{
  $previous_balance_amount = '0';
}

if($discount_description == '' || $discount_description == '0')
{
  $discount_description = 'Discount'; 
}

$minimum_payable = '0';
$total_payable = '0';
$isdiscount = '0';
$minimum_payable_after_discount = '0';

if(count($data_array) == 0) 
{
  $myArray['previous_total'] = '0';
  $myArray['previous_month'] = "";
  $myArray['previous_paid'] = "0";
  $myArray['previous_balance'] = "0";
  $myArray['history_total'] = "0";
  $myArray['history_month'] = '';
  $myArray['history_paid'] = "0";
  $myArray['history_balance'] = '0';

  $myArray['min_payable'] = "0";
  $myArray['total_payable'] = "0";
  $myArray['---'] = '---';
  $myArray['total_after_discount'] = '0';
  $myArray['discount_amount'] = $discount_amount;
  $myArray['discount_percentage'] = $discount_percentage;
  $myArray['minimum_payable_after_discount'] = $minimum_payable_after_discount;
  $myArray['discount_description'] = $discount_description;
  $myArray['isdiscount'] = $isdiscount;
  $myArray['pay_month'] = '';
  // echo json_encode($myArray);exit;
  
}

if($history_month == $current_month)
{
  $myArray['previous_total'] = '0';
  $myArray['previous_month'] = $previous_month;
  $myArray['previous_paid'] = $previous_paid_amount;
  $myArray['previous_balance'] = $previous_balance_amount;
  $myArray['history_total'] = $previous_balance_amount;
  $myArray['history_month'] = '';
  $myArray['history_paid'] = $history_paid_amount;
  $myArray['history_balance'] = '0';

  $myArray['min_payable'] = ceil($minimum_payable);
  $myArray['total_payable'] = $total_payable;
  $myArray['---'] = '---';
  $myArray['total_after_discount'] = $after_discount_total;
  $myArray['discount_amount'] = $discount_amount;
  $myArray['discount_percentage'] = $discount_percentage;
  $myArray['minimum_payable_after_discount'] = $minimum_payable_after_discount;
  $myArray['discount_description'] = $discount_description;
  $myArray['isdiscount'] = $isdiscount;
  $myArray['pay_month'] = $previous_month;

  // echo json_encode($myArray);exit;
  
  
}
//echo json_encode($data_array);exit;
if($previous_month == $current_month)
{
  $previous_month = $data_array[0]['month'];
  $previous_balance_amount = $history_balance_amount;
  $myArray['previous_total'] = $history_balance_amount;
  $history_balance_amount = '0';
  $history_month = '';
  
}
else
{
  $myArray['previous_total'] = $previous_balance_amount;
}

//echo json_encode($previous_balance_amount);exit;

//$total_payable = '888';

$total_payable = $previous_balance_amount + $history_balance_amount;


//$total_payable = '100';
//$discount_percentage = 8;

$total_payable_a = $total_payable; 

if($discount_percentage >= 1)
{
  $isdiscount = '1';
  $after_discount_total = ($total_payable * 0.01 * (100 - $discount_percentage));
  $discount_amount = $total_payable - $after_discount_total;
  $total_payable_a = $after_discount_total;
}
else  
{
  $discount_amount = $discount_amount;
  $after_discount_total = $total_payable;
  
}




$minimum_payable = ($total_payable * 0.01 * 75);
$minimum_payable_after_discount = ($total_payable_a * 0.01 * 75);

$pay_month = '';
if($previous_month != "" && $history_month != "")
{
  $pay_month = $history_month. ", ". $previous_month; 
}
if(($previous_month != "" && $history_month == ""))
{
  $pay_month = $previous_month;
}
if($previous_month == "" && $history_month != "")
{
  $pay_month = $history_month;
}


$myArray['previous_month'] = $previous_month;
$myArray['previous_paid'] = $previous_paid_amount;
$myArray['previous_balance'] = $previous_balance_amount;

$myArray['history_total'] = $history_balance_amount;
$myArray['history_month'] = $history_month;
$myArray['history_paid'] = $history_paid_amount;
$myArray['history_balance'] = $history_balance_amount;

$myArray['min_payable'] = ceil($minimum_payable);
$myArray['total_payable'] = ceil($total_payable);
$myArray['---'] = '---';
$myArray['total_after_discount'] = ceil($after_discount_total);
$myArray['discount_amount'] = ceil($discount_amount);
$myArray['discount_percentage'] = $discount_percentage;
$myArray['minimum_payable_after_discount'] = ceil($minimum_payable_after_discount);
$myArray['discount_description'] = $discount_description;
$myArray['isdiscount'] = $isdiscount;
$myArray['id_subscription'] = $id_subscription;
$myArray['pay_month'] = $pay_month;

// echo json_encode($myArray);exit;

if($total_payable > 0)
  {
    $sql = "SELECT * FROM doctor_outstanding_payment WHERE id_doctor='$did' AND month='$previous_month' OR month='$history_month'";
$result = $conn->query($sql);

$firstdate = date("Y-m-01");
$lastdate = date("Y-m-10");

if($date >= $firstdate && $date <= $lastdate){
  $payment_check = mysqli_num_rows($result);
}

if($date > $lastdate){

  $payment_check = mysqli_num_rows($result);
}

$currentDate = date("Y-m-d", strtotime($current_month));
$previousDate = date("Y-m-d", strtotime($previous_month));
$historyDate = date("Y-m-d", strtotime($history_month));

if($currentDate > $previousDate && $currentDate > $historyDate){

  $payment_check = mysqli_num_rows($result);
}
}
else
{
  $payment_check = 1;
}

// $get_doctor_subscription="SELECT s.id, s.subscriptoin_plan, s.amount, s.package, s.final_packege_discount_percent, s.discount_description FROM doctor_subscription AS s INNER JOIN doctor_details AS d ON s.id=d.id_subscription WHERE d.id='$did'";
// $result_subscription   = $conn->query($get_doctor_subscription);

//   while ($row_subscription = $result_subscription->fetch_assoc())
//   {
//     $subscription_id = $row_subscription['id'];
//     $discount_percent = $row_subscription['final_packege_discount_percent'];
//     $discount_description = $row_subscription['discount_description'];
//   }
//   $discount_no = $discount_percent/100;
//   $totAmount = $previous_balance_amount;
//   $findDiscount = ceil($totAmount * $discount_no);

// $min = 75;
$Tot = $previous_balance_amount + $history_balance_amount;
$finalAmount =ceil($Tot - $discount_amount);
// $minno = ($finalAmount*$min)/100;

if ($_POST)
{
  $previous_total_amount = $finalAmount;
  $previous_paid_amount = $_POST['amount'];
  // $history_amount = $history_paid_amount;

  $did = $_SESSION['doctor_details']['id'];
  $date = date('Y-m-d h:i:s');

  $currDate = date("M-Y");

   $get_history_doctor_payment = "SELECT * FROM `doctor_outstanding_payment` where month = '$history_month' and id_doctor = '$did' order by id DESC limit 0,1";
  $result_history       = $conn->query($get_history_doctor_payment);
  
  $previous_total = '0';
  while ($row_history = $result_history->fetch_assoc())
  {
    $history_total_amount_payment = $row_history['total'];
    $history_paid_amount = $row_history['paid_amount'];
    $history_balance_amount = $row_history['balance_amount'];
  }

  if(($history_balance_amount == $history_amount) && $history_balance_amount > 0)
  {
    $update_doctor_payment = "UPDATE `doctor_outstanding_payment` set balance_amount = '0' , paid_percentage = '100%' , paid_amount = '$history_total_amount_payment', status = '1' where month = '$history_month' and id_doctor = '$did'";

  $result_insert = $conn->query($update_doctor_payment);
  }

$previous_balance_amount = $previous_total_amount - $previous_paid_amount;
$total_excluding_discount = (int)$previous_paid_amount + (int)$discount_amount;

if($previous_paid_amount == $previous_total_amount)
{
  $privious_status = '1';
  $previous_paid_percentage = '100.00%';
}
else
{
  $privious_status = '0';
  $single_percent = $previous_total_amount * 0.01;
  $previous_paid_percentage = $previous_paid_amount/$single_percent;
  $previous_paid_percentage = number_format($previous_paid_percentage, 2, '.', ',').'%';
}

  
  $insert_doctor_payment = "INSERT INTO `doctor_outstanding_payment` (`date_time`, `month`, `id_doctor`, `total`, `paid_amount`, `balance_amount`, `paid_percentage`, `status`,`total_exc_discount`,`discount_percentage`,`id_subscription`) VALUES ('$date', '$previous_month', '$did', '$previous_total_amount', '$previous_paid_amount', '$previous_balance_amount', '$previous_paid_percentage', '$privious_status','$total_excluding_discount','$discount_percentage','$id_subscription')";

  $result_insert = $conn->query($insert_doctor_payment);
  
if($result_insert)
{
  
  $update_doctor_balance = "UPDATE `doctor_outstanding_balance` set status = '1' where month = '$previous_month' and id_doctor = '$did' and total <= $total_excluding_discount";

  $result_insert_update = $conn->query($update_doctor_balance);

    echo "<script>alert('payment details added successfully')</script>";
    echo "<script>parent.location='doctor_outstanding_balance.php'</script> ";
  }
} 


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
	
    <link href="../css/jquery.datetimepicker.css" rel="stylesheet">

    <link href="../css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<style>

  .dataTables_filter input { width: 400px }
</style>
</head>

<body>     
    <?php if($payment_check < 1 && $currentDate > $previousDate && $currentDate > $historyDate){ ?>
    <nav class="navbar navbar-default dashboard-navbar navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../index.php">First Doctor</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right main-nav">
            <!-- <li>
                <a href="#" class="dropdown-toggle settings" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings <span class="caret"></span></a>
                <ul class="dropdown-menu"> -->
                    <li><a href="logout.php">Logout</a></li>
               <!--  </ul>                
            </li> -->
          </ul>
        </div>
      </div>
    </nav>
    <?php }else{
      include('navbar.php');
    }?>

    <div class="container-fluid main-wrapper">
      <div class="row">

          <?php if($payment_check < 1 && $currentDate > $previousDate && $currentDate > $historyDate){ ?>
            <aside class="col-sm-4 col-lg-3 sidebar">
        <div class="profile-block">
          <div class="profile-img-container">
              <img src="<?php echo '../uploads/'.$_SESSION['doctor_details']['photo']; ?>" alt="" style="width: 100px; height: 105px;">
          </div>
          <h3>Dr. <?php echo ucfirst($_SESSION['doctor_details']['first_name'])." ".ucfirst($_SESSION['doctor_details']['last_name']); ?></h3>
        <p><u>Update Profile <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></u></p>
        <p><?php echo strtolower($_SESSION['doctor_details']['email']); ?></p>
    </div>
                
    <hr class="divider" />
        <ul class="sidebar-menu clearfix">
          
        </ul>      
        </aside>
        <?php
          }
          else { include('menu.php'); } ?>

        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Doctor Outstanding Balance Payment</h3>
               <div class="card" style="width: 90%;">
                <form action="" method="POST" id="form">
                  <h5>Note : Min. 75% of the total amount should be paid.* <b>(Amount should be greater than or equal to : <?php echo round($minno); ?>. *)</b></h5>
                <div class="row">
                  <!-- <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <label class="">Mode Of Payment<span class="error"></span></label>
                        <select name="paymentMode" class="form-control selitemIcon" id="paymentMode" placeholder="Enter Amount" required="required">
                          <option value="">SELECT PAYMENT MODE</option>
                          <option value="cash">Cash</option>
                          <option value="credit">Credit Card</option>
                          <option value="debit">Debit Card</option>
                        </select>
                      </div>
                    </div>
                  </div> -->
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="number" name="amount" class="form-control" id="amount" placeholder="Enter Amount" autocomplete="off" onkeyup="getamount()" value="<?php echo $finalAmount;?>" maxlength="10" readonly="readonly">
                        <label class="fg-label" style="color: #2287de;">Paid Amount (₹)<span class="error"> *</span></label>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="tot_amount" value="<?php echo $finalAmount; ?>" id="tot_amount">
                </div>
                  <div class="row">
          					<div class="col-sm-6">
          						<input type="hidden" name="pid" value="<?php echo $pid; ?>">
          					</div>
                  </div>
                <div class="row">
                  <div class="col-sm-12">
                    <b>Payment Details :<b><hr>

                    <b class="fa fa-eye pull-left"><a href="#" data-toggle="modal" data-target="#newModal" onclick="get_history(this);" id="<?php echo $history_balance_amount; ?>"><?php echo $history_month; ?> Balance(₹)</a></b>
                    <b class="pull-right"><?php echo $history_balance_amount; ?>.00</b><br>
 
                    <b class="fa fa-eye pull-left"><a href="#" data-toggle="modal" data-target="#newModal" onclick="get_previous(this);" id="<?php echo $previous_balance_amount; ?>"><?php echo $previous_month; ?> Balance(₹)</a></b>
                    <b class="pull-right"><?php echo $previous_balance_amount; ?>.00</b><br>
                    <hr>
                    Total Amount(₹) : <block class="pull-right"><?php echo $Tot;?>.00</block> <br>
                    Discount Amount(₹) : <block class="pull-right"><?php echo ceil($discount_amount);?>.00</block> <br>
                    Total Payable Amount(₹) : <block class="pull-right"><?php echo $finalAmount;?>.00</block> <br>
                    Paid Amount(₹) : <block class="pull-right" id="payamount"><?php echo $amount;?>.00</block> <br>
                    Balance Amount(₹) : <block class="pull-right" id="balamount"><?php echo $totalAmount-$Amount; ?>.00</block>
                  </div>
                </div>
                <br>
                <div class="bttn-group">
                  <?php if ($payment_check < 1 && $currentDate > $previousDate && $currentDate > $historyDate) { ?>
                  <a href="index.php" class="btn btn-link">Cancel</a>
                <?php }
                else{
                  ?>
                  <a href="doctor_outstanding_balance.php" class="btn btn-link">Cancel</a>
                <?php } if($finalAmount > 0 && $history_month != $current_month){ ?>
                  <button type="button" class="btn btn-primary btn-lg" name="save" id="save" onclick="check()">Pay</button>
					<?php } ?>
                </div>
              </form>
              </div>
            </div>
        </section>
      </div>
    </div>
    <div  id="popup">
      <div class="modal fade" id="newModal" role="dialog">
        <div class="modal-dialog modal-md">
           <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;"><br> Outstanding Balance On <b id="monthid"></b>. </h4>
            </div>
            <div class="modal-body">
                <div class="fg-line">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group text-center">
                        <div class="table-responsive theme-table v-align-top">
                          <table class="table" id="example">
                            <tr>
                                <th>Date</th>
                                <th>Appointments</th>
                                <th>Total Amount</th>
                            </tr>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
              <a href='#' class='btn btn-primary btn-lg' id='ok' name='ok' data-dismiss='modal'>Close</a>
           </div>
          </div>
      </div>
    </div>
  </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
	
    <script src="../js/jquery.datetimepicker.full.js"></script>
	<script>
$('#date').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
// startDate:  '2019-07-05',
 format:'Y-m-d H:m:s',
 // minDate: new Date();
});

</script>
<script src="../js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function() {
    $('#example').dataTable( {
    language: {
        searchPlaceholder: "Search Patient by Name, Email, Mobile"
    }
} );
} );
</script>
<script type="text/javascript">

            function getamount() {
              var amount = $("#amount").val()+'.00';
              var tot = $("#tot_amount").val()+'.00';

          var per = parseInt(amount) *100/parseInt(tot);
                $("#percentage").html(per);

              if (parseInt(amount) > parseInt(tot)) {
                alert('Pay Amount should not be greater than Total Payable Amount(₹)');
                return true;
              }
              else{

              }
              var balance = amount - tot;
                $("#payamount").html(amount);
                $("#balamount").html(balance);
            }

              var amount = $("#amount").val()+'.00';
              var tot = $("#tot_amount").val()+'.00';
              var balance = amount - tot;
                $("#payamount").html(amount);
                $("#balamount").html(balance);


    function check(){
      var amount = $("#amount").val();
      var tot = $("#tot_amount").val();

          var per = parseInt(amount) *100/parseInt(tot);

      if (parseInt(amount) > parseInt(tot)) {
                alert('Pay Amount should not be greater than Total Payable Amount(₹)');
        exit();
      }else
      if (per <75) {
        alert('Amount should be min. 75% of Total Payable Amount(₹)');
        exit();
      }
      else{
                $("#form").submit();
          }
    }
                
</script>
<script type="text/javascript">

        function get_previous(id){
          var balance = $(id).attr("id");
          var did = '<?php echo $did; ?>';
          var month = '<?php echo $previous_month; ?>';
          console.log(did);
          console.log(month);
                $("#monthid").html(month);

          $.ajax({
            url: 'get_previous_balance.php',
            data:{
                  'did': did,
                  'balance': balance,
                  'month': month,
        },
            success: function(result){
            $("#example").html(result);
          }
        });
        }
      </script>
      <script type="text/javascript">

        function get_history(id){
          var balance = $(id).attr("id");
          var did = '<?php echo $did; ?>';
          var month = '<?php echo $history_month; ?>';

          console.log(did);
          console.log(month);
                $("#monthid").html(month);

          $.ajax({
            url: 'get_history_balance.php',
            data:{
                  'did': did,
                  'balance': balance,
                  'month': month,
        },
            success: function(result){
            $("#example").html(result);
          }
        });
        }
      </script>

      <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              amount : "required"
            },
            messages : {

              amount : "<span> ENTER AMOUNT</span>"
            }
        });
    });
</script>
</body>

</html>