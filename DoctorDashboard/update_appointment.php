<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];

$did = $_SESSION['doctor_details']['id'];

$select = mysqli_query($conn,"SELECT * FROM events WHERE id = '$id'");
if ($row = mysqli_num_rows($select)) 
{
    while ($row = mysqli_fetch_array($select))
    {
        //from database fields
        $title = $row['title'];
        $start = $row['start'];
        $patient_id = $row['patient_id'];
        $patient_name = $row['patient_name']; 
        $mobile = $row['mobile'];
        $clinic = $row['id_clinic'];
		$sid = $row['id_time_slots'];
    }
}
if ($_POST) {

  $id = $_GET['id'];
  $title = $_POST['title'];
	$did = $_SESSION['doctor_details']['id'];
	
  $patientName = $_POST['name'];
  $mobile = $_POST['mobile'];
	
	 $id = $_GET['id'];
                $slotid = $_POST['eventid'];
                
                mysqli_query($conn,"UPDATE timeslots SET slot='r' WHERE id='$sid'");

                mysqli_query($conn,"UPDATE events SET rescheduled_by=1, status=2 WHERE id='$id' ");

                $slot_query= "SELECT * FROM timeslots WHERE id='$slotid' AND doctor_id='$did' ORDER BY date";
                $slot_result = $conn->query($slot_query);
                $row = $slot_result->fetch_assoc();
                $date = $row['date'];
                $time = $row['time'];
                $slot_id = $row['id'];
                $id_clinic = $row['id_clinic'];
                $datetime  = $date." ".$time;
                $etime = strtotime($row['time']);
                // $startTime = date("H:i", strtotime('-30 minutes', $etime));
                $endTime = date("H:i", strtotime('+30 minutes', $etime));
                $enddatetime = $date." ".$endTime;
                $doctorId = $_SESSION['doctor_details']['id'];

                $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, id_time_slots, id_clinic, patient_name, mobile, height, weight, bp, sugar, bmi, status, pulse) VALUES('$title', '$datetime', '$enddatetime', '$doctorId', '$patient_id', '$slot_id', '$id_clinic', '$patientName', '$mobile','NA','NA','NA','NA', 'NA', '0','NA')";
                $result = $conn->query($sql);
                if ($result) {
                  mysqli_query($conn,"UPDATE timeslots SET slot='n' WHERE id='$slotid' ");

                  echo "<script>alert('Appointment Rescheduled to '+ '".date("d M Y h:i:a", strtotime($datetime))."')</script>";
                 echo "<script>parent.location='appointmentReschedule.php'</script>";
                }
}

 $slots = mysqli_query($conn, "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND CONCAT(t.date,' ',t.time)>=CURRENT_TIMESTAMP order by t.id ASC");

        $i = 0;
          $slotList = array();
          while ($row = mysqli_fetch_assoc($slots)) {
            $slotList[$i]['id'] = $row['id'];
            $slotList[$i]['date'] = $row['date'];
            $slotList[$i]['time'] = $row['time'];
            $slotList[$i]['clinic_name'] = $row['clinic_name'];
            $slotList[$i]['slot'] = $row['slot'];
            $i++;
          }


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

     <link href="../css/jquery.datetimepicker.css" rel="stylesheet">
    
</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Reschedule Appointment</h3>
               <div class="card" style="width: 70%;">
				   <p style="font-size: 20px;">Current Appointment Date :<i> <?php echo $start; ?></i></p>
                <form action="" method="POST" id="target">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="name" class="form-control" id="name" value="<?php echo $patient_name;?>" readonly>
                        <label class="fg-label">Patient Name<span class="error"></span></label>
                      </div>
                    </div>
                  </div>
                </div>
               <!-- <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="start" class="form-control" id="start" value="<?php echo $start; ?>">
                        <label class="fg-label">Appointment Date<span class="error"></span></label>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="title" class="form-control" id="title" placeholder="Purpose of visit" required value="<?php echo $title; ?>" readonly>
                        <label class="fg-label">Appointment Reason<span class="error"></span></label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="mobile" class="form-control" id="mobile" value="<?php echo $mobile; ?>" readonly>
                        <label class="fg-label">Mobile Number<span class="error"></span></label>
                      </div>
                    </div>
                  </div>
                </div>
					
					<h3><?php if($slotList){ echo "Avalailable slots : "; } else{ echo "No Slots Avalailable";} ?></h3>
              <div class="row">
                <div class='table-responsive theme-table v-align-top'>
                <table class="table">
                  <tr>
                    <th style="text-align: center;">Date</th>
                    <th style="text-align: center;">Clinic</th>
                    <th> </th>
                  </tr>
            <?php for($i=0; $i<count($slotList); $i++){ ?>
                  <tr>
                    <td style="text-align: center;">
                      <?php echo date('d-M-Y',strtotime($slotList[$i]['date'])); ?>
                         <?php echo ' '.$slotList[$i]['time']; ?>
                    </td>
                    <td style="text-align: center;">
                         <?php echo $slotList[$i]['clinic_name']; ?>
                    </td>
                    
                    <td style="text-align: center;">
                      <button type="button" class="btn btn-primary btn-lg"  onclick="selectid(<?php echo $slotList[$i]['id']; ?>)" id='book'>Book</button>
                    </td>
                  </tr>
              <?php
                  }
              ?>
                </table>
              </div>
              </div>
                 <input type='hidden' name='eventid' id="eventid" value='' />
					
                <div class="bttn-group">
                  <a href="appointmentReschedule.php" class="btn btn-link">Cancel</a>
                <!--  <button type="submit" class="btn btn-primary btn-lg" name="save" id="save">Save</button> -->
                </div>
              </form>
              </div>
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>      

    <script src="../js/jquery.datetimepicker.full.js"></script>
    <script type="text/javascript">
      $('#start').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        // startDate:  0,
         format:'Y-m-d H:m:s',
        minDate: 0
         // minDate: new Date();
        });

    </script>
	<script>
function selectid(id) {

  $("#eventid").val(id);
  var conf = confirm('ARE YOU SURE FOR THIS APPOINTMENT');
   if(conf==true){
    $("#target").submit();
  }
}
</script>

</body>

</html>