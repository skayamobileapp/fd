<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];
$doc_id = $_SESSION['doctor_details']['id'];

$item=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from petty_cash where id='$id' ";
    $result   = $conn->query($sql);
    $item = $result->fetch_assoc();
   }

if($_POST)
{
    if($_GET['id'])
    {
      $id = $_GET['id'];
      $amount = $_POST['amount'];
      $desc = $_POST['description'];
      $date = date("Y-m-d", strtotime($_POST['date'])." ".date("H:i:s"));
      $staff = $_POST['staff'];

      $sql = "UPDATE petty_cash SET description='$desc', date='$date', amount='$amount', id_staff='$staff' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Petty Cash details updated successfully');</script>";
        echo "<script>parent.location='petty-cash-list.php'</script>";
      }
    }
    else
    {
      $doc_id = $_SESSION['doctor_details']['id'];
      $amount = $_POST['amount'];
      $desc = $_POST['description'];
      $date = date("Y-m-d", strtotime($_POST['date'])." ".date("H:i:s") );
      $staff = $_POST['staff'];
      
    $sql = "INSERT INTO petty_cash(description, date, id_doctor, amount, id_staff) VALUES('$desc', '$date','$doc_id', '$amount', '$staff')";
      $result  = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Petty Cash added successfully');</script>";
        echo "<script>parent.location='petty-cash-list.php'</script>";
      }
    }
}

$sql   = "select * from staff where created_by='$doc_id'";
$result = $conn->query($sql);
$staffList = array();
while ($row = $result->fetch_assoc()) {
    array_push($staffList, $row);
}
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="petty-cash-list.php">Petty Cash List</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Add Petty Cash</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Add Petty Cash</h4>
									<div class="row form-row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Amount <span class="text-danger">*</span></label>
												<input type="text" name="amount" class="form-control" id="amount" value="<?php echo $item['amount'];?>" autocomplete="off">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label> Date <span class="text-danger"> *</span></label>
                        <div class="cal-icon">
												<input type="text" name="date" class="form-control datetimepicker" id="date" value="<?php if(!empty($item['id'])){ echo date('d-m-Y', strtotime($item['date'])); } else { } ?>" autocomplete="off">
                        </div>
											</div>
										</div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Description <span class="text-danger"> *</span></label>
                        <input type="text" name="description" class="form-control" id="description" value="<?php echo $item['description'];?>" autocomplete="off">
                      </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                              <label class=""> Staff Name<span class="text-danger">*</span></label>
                                <select name="staff" class="form-control selitemIcon" id="staff">
                                  <option value="">Select</option>
                                    <?php
                                    for ($i=0; $i<count($staffList); $i++) {  ?>
                                    <option value="<?php echo $staffList[$i]['id']; ?>" <?php if( $staffList[$i]['id'] == $item['id_staff'])  { echo "selected=selected"; }
                                    ?>><?php echo $staffList[$i]['name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                        </div>                            
                    </div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="petty-cash-list.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>

		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              date : "required",
              description : "required",
              staff : "required",
                
                  amount : {
                    required : true,
                    number : true

                }
         
            },
            messages : {
               
                date : "<span> enter date</span>",
                description : "<span> enter description</span>",
                staff : "<span> select staff</span>",
            
                 amount : {
                    required : "<span> enter amount</span>",
                    number : "<span> enter number only</span>",
                }
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

		 <script type="text/javascript">
$( document ).ready(function() {

	 var idstate = '<?php  echo $clinicDetails['state_id'];?>';
	 if(idstate!='' && idstate!= null) {
		getCities();
	 }
    // getCities();
    // getspecialty();
});

        function getCities(){
          var id = $("#state_id").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#city_id").html(result);
              var idcityselected = '<?php  echo $clinicDetails['city_id'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#city_id").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

       

        function getPincode(){
          var id = $("#city_id").val();
          console.log(id);

          $.ajax({url: "getpincode.php?id="+id, success: function(result){
            $("#pincode").html(result);

             var pincodeselected = '<?php  echo $clinicDetails['pincode'];?>';
              if(pincodeselected!='' && pincodeselected!= null) {
                 $("#pincode").val(pincodeselected);
              }
          }
        });
          
        }
    </script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>