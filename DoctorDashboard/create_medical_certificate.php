<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from patient_details where id='$id' ";
    $result   = $conn->query($sql);
    $patients = $result->fetch_assoc();
   }

if ($_POST) {

  $id = $_GET['id'];
  $did = $_SESSION['doctor_details']['id'];
  
  $fromdate = date("Y-m-d", strtotime($_POST['fdate']));
  $todate = date("Y-m-d", strtotime($_POST['tdate']));
  $descrip = $_POST['desc'];

  $sql= "INSERT INTO med_certificate(id_doctor, id_patient, from_date, to_date, description) VALUES('$did', '$id', '$fromdate', '$todate', '$descrip')";
  $result = $conn->query($sql);
  if ($result) {
    $lastid = mysqli_insert_id($conn);
	  echo "<script>parent.location='medical_certificate_pdf.php?id=$lastid&pid=$id'</script>";
?>

  <script>
    window.open('medical_certificate_pdf.php?id=<?php echo $lastid; ?>&pid=<?php echo $id; ?>');
    </script>
  <?php
  }
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

 <link href="../css/jquery-ui.css" rel="stylesheet">
    <link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">

          <?php include('menu.php'); ?>

        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
              <ol class="breadcrumb">
                  <li><a href="view_patient_summary.php">Patient Summary</a></li>
                  <li class="active"> Medical Certificate</li>
                </ol>
                <div class="patient-summary-head">
                  <h4><font color="black">Medical Certificate for &nbsp</font>&nbsp <?php echo "Patient : ". strtoupper($patients['patient_name']); ?></h4>
                </div>
               <div class="card">
                <form action="" method="POST" id="form">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line">
                        <input type="text" name="fdate" id="fdate" class="form-control" autocomplete="off" value="">
                        <label class="fg-label">From Date<span class="error"> *</span></label>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line">
                        <input type="text" name="tdate" id="tdate" class="form-control" autocomplete="off">
                        <label class="fg-label">To Date<span class="error"> *</span></label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group fg-float">
                      <div class="fg-line">
                        <input type="text" name="desc" class="form-control" id="desc" autocomplete="off">
                        <label class="fg-label">Description<span class="error"> *</span></label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="bttn-group">
                  <a href="view_patient_summary.php" class="btn btn-link">Cancel</a>
                  <button type="submit" class="btn btn-primary btn-lg" name="save" id="save">Save</button>
                </div>
              </form>
              </div>
            </div>
        </section>
      </div>
    </div>
      <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>

   <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>

     <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              fdate : "required",
              tdate : "required",
              desc : "required"
            },
            messages : {

                fdate : "<span> select from date</span>",
                tdate : "<span> select to date</span>",
                desc : "<span> Enter Description</span>"
            }
        });
    });
</script>



  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#fdate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy'
    });
  });
  $( function() {
    $( "#tdate" ).datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy'
    });
  });
  </script>
</body>
</html>
