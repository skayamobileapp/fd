<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

$did = $_SESSION['doctor_details']['id'];

$select = mysqli_query($conn,"SELECT * FROM staff where created_by='$did'");
$i=0;
while ($row = mysqli_fetch_array($select))
    {
        $staff[$i]['id'] = $row['id'];
        $staff[$i]['name'] = $row['name'];
        $staff[$i]['email'] = $row['email'];
        $staff[$i]['mobile'] = $row['mobile'];
        $staff[$i]['address'] = $row['address'];
        $i++;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<!-- Page Wrapper -->
      <div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">

            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">My Staff Members <a href="add-staff.php" class="btn btn-primary float-right btn-lg">+ Add Staff</a></h3>
                <br>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
					                               <th>SL. NO</th>
						                              <th>name</th>
						                              <th>email</th>
						                              <th>mobile</th>
						                              <th>address</th>
						                              <th>change password</th>
						                              <th>ACTIONS</th>
						                           </tr>
											</thead>
											<tbody>
												<?php
            
            for ($i=0; $i <count($staff) ; $i++) {
              ?>
              <tr>
                <td><?php echo $i+1; ?></td>
                <td><?php echo strtoupper($staff[$i]['name']); ?></td>
                <td><?php echo $staff[$i]['email']; ?></td>
                <td><?php echo $staff[$i]['mobile']; ?></td>
                <td><?php echo $staff[$i]['address'] ; ?></td>
                <td><a href="#" data-toggle="modal" data-target="#newModal" onclick="change(this)" id="<?php echo $staff[$i]['id']; ?>">Click here</a></td>
                <td>
                  <?php echo "<a class='btn bg-info-light pencil' href='add-staff.php?id=". $staff[$i]['id']."' title='Edit'><i class='fa fa-edit'></i></a>"; ?>
                    <?php echo "<a class='btn bg-danger-light trash' onclick = 'javascript: delete_id($(this));return false;' href='delete_staff.php?id=". $staff[$i]['id']."' title='Delete'><i class='fa fa-trash-alt'></i></a>"; ?>
                </td>

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
									<input type="hidden" name="staff_id" id="staff_id"/> 
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
    </div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
<div class="login-links">
              <div class="container" id="popup">
                <div class="modal fade" id="newModal" role="dialog">
                  <div class="modal-dialog modal-sm" style="width: 40%;">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Reset Password</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-10">
                                <div class="form-group">
                                    <label > Enter New password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control" autocomplete="off" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-sm-10">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="cnf_password" id="cnf_password" autocomplete="off" required>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                      <div class="modal-footer ">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="change" name="change" data-dismiss="modal">Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		<script type="text/javascript">
      function change(id){
        var sid = $(id).attr('id');
        console.log(sid);
        $("#staff_id").val(sid);
      }
    $("#change").on('click',function(){
      var newp = $("#new_password").val();
      var cnfp = $("#cnf_password").val();
      var sid= $("#staff_id").val();
      console.log(sid);
      console.log(newp);
      console.log(cnfp);
        $.ajax({
          // type: 'POST',
          url: 'change_staff_password.php',
          data:{
            'sid': sid,
            'newp': newp,
            'cnfp': cnfp
          },
          success: function(result){
          $("#change").html(result);
          $("#new_password").val('');
          $("#cnf_password").val('');
          // $('#myModal').close();
        }
      });
    });
    </script>
		
    </body>
</html>