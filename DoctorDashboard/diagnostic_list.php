<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);

$did = $_SESSION['doctor_details']['id'];

$sql   = "select * from lab_details";
$result = $conn->query($sql);
$labList = array();
while ($row = $result->fetch_assoc()) {
    array_push($labList, $row);
}

$count1=0;
// $did = $_SESSION['doctor_details']['id'];
// $notesql= "SELECT * FROM questions where patient_id in ( Select id from patient_details) AND doctor_id='$did' and read_flag='0'";
//   $result=mysqli_query($conn, $notesql);
//   $count1=mysqli_num_rows($result);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <?php include('navbar.php'); ?>
    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
        <section class="col-sm-8 col-lg-9">
            <div class="main-container"> 
            <!-- <form action="" method="" id="myform"> -->
               <h3 class="clearfix">Chat With Diagnostic Lab</h3>       
                <div class="card">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group fg-float">
                          <input type="text" name="search" class="form-control user-search" placeholder="Search" id="search" autocomplete="off">
                          </div>
                          <div class="users-list">                          
                          <div id="result">
                            <?php
                            for($i=0; $i<count($labList); $i++)
                            {
                              ?>

                              <div class="chat-user-profile" onclick="get_prev(this);" id="<?php echo $labList[$i]['id']; ?>" >
                              <span id="notification-count" class="notification-count">
                                <?php if($count1>0) { echo $count1; } ?></span>
                                <div class="user-image">
                                  <?php if(empty($labList[$i]['profile_pic'])){?>
                                  <img src="https://ui-avatars.com/api/?background=02afee&color=fff&name=<?php echo $labList[$i]['lab_name']; ?>">
                                <?php } else { ?>
                                  <img src="../uploads/<?php echo $labList[$i]['profile_pic']; ?>">
                                <?php } ?>
                                </div>
                                <div class="user-name"><?php echo strtoupper($labList[$i]['lab_name']); ?></div>
                                <div><?php echo strtoupper($labList[$i]['mobile']); ?></div>
                              </div>
                              <?php
                            }
                            ?>
                                <input type="hidden" name="lab_id" id="lab_id" value="">
                        </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="message-container">
                          <h3 class="clearfix"></h3>
                          <div class='scrollbar' id='style-5'>
                            <div  id="previous"></div>
                          </div>
                        </div>
                        <div class="message-form">
                        <div class="form-group fg-float">
                          <div class="fg-line">
                            <input type="text" class="form-control" name="answer" id="answer" placeholder="Type a message..." autocomplete="off" required/>
                          </div>
                        </div>
                        <div class="bttn-group">
                          <button class="btn btn-primary btn-lg" name="save" id="save">Send</button>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
            <!-- </form> -->
                </div>
        </section>
      </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/main.js"></script>
  
        <script type="text/javascript">

        function get_prev(id){
          var lid = $(id).attr("id");
          var did = '<?php echo $did; ?>';
          console.log(did);
          console.log(lid);
          $("#lab_id").val(lid);

          $.ajax({
            url: 'get_labchat.php',
            data:{
                  'did': did,
                  'lid': lid,
        },
            success: function(result){
            $("#previous").html(result);
          }
        });
        }
      </script>
      
    <script type="text/javascript">
    $("#save").on('click',function(){
    var aid = $('#answer').val();
    var did = '<?php echo $did; ?>';
    var lid = $("#lab_id").val();
    console.log(did);
    console.log(lid);
    console.log(aid);
      $.ajax({
        // type: 'POST',
        url: 'send_message_lab.php',
        data:{
          'aid': aid,
          'did': did,
          'lid': lid,
        },
        success: function(result){
        $("#previous").html(result);
        $('#answer').val(' ');
      }
        });
      });
    </script>

      <script src="../select2/js/select2.js" ></script>
          <script src="../select2/js/select2-init.js" ></script>
  </body>
</html>