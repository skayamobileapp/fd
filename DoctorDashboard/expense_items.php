<?php
include('../connection/conn.php');
include('session_check.php');
$did = $_SESSION['doctor_details']['id'];
$date = date("Y-m-d");

$select = mysqli_query($conn,"Select e.*, i.item_name from expense_item AS e INNER JOIN items AS i ON i.id=e.id_item Where e.id_doctor='$did' ");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['amount'] = $row['amount'];
  $view[$i]['item_name'] = $row['item_name'];
  $view[$i]['date'] = $row['date'];
  $view[$i]['approval_status'] = $row['approval_status'];
  $i++;
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <link href="../css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
<style>

  .dataTables_filter input { width: 400px }
</style>
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>

<body>     
   <?php include('navbar.php'); ?>
    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        <section class="col-sm-8 col-lg-9">          
          <div class="main-container">
           <h3 class="clearfix"> Purchase of Consumables<a href="add_expense_items.php" class="btn btn-primary pull-right btn-lg">+ Add Purchase of Consumables</a></h3>
           <div class="table-responsive theme-table v-align-top">
            <table class="table" id="example">
             <thead>
               <tr>
                <th>SL. NO</th>
                <th>Consumables Name</th>
                 <th>amount</th>
                 <th>date</th>
                <!-- <th>Approval Status</th> -->
                 <!-- <th>Actions</th> -->
               </tr>
             </thead>
             <tbody>
               <?php

               for ($i=0; $i <count($view) ; $i++) {
                $n =$i+1;
                ?>
                <tr>
                <!-- <img src="../uploads/<?php echo $view[$i][photo]; ?>" width='100px' height="100px;" /> -->
                  <td><?php echo $n; ?></td>
                  <td><?php echo $view[$i]['item_name']; ?></td>
                  <td><?php echo $view[$i]['amount']; ?></td>
                  <td><?php echo date("d M Y", strtotime($view[$i]['date'])); ?></td>

                 <!-- <td><?php if ($view[$i]['approval_status']==1) { echo "Active"; } else{ echo "Inactive"; } ?></td>
                 <td>
                  <?php echo "<a class='action-link edit' href='add_expense_items.php?id=". $view[$i]['id']."'></a>"; ?>

                    <?php echo "<a class='action-link delete' onclick = 'javascript: delete_id($(this));return false;' href='delete_expense.php?id=". $view[$i]['id']."'></a>"; ?> 
                </td>-->

               </tr>
               <?php

             }
             ?>                                                                                                           
           </tbody>
         </table>                   
       </div>               
     </div>
   </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>    


<script src="../js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function() {
    $('#example').dataTable( {
    language: {
        searchPlaceholder: "Search Item by Name, Date"
    }
} );
} );
</script>

</body>

</html>