<?php
include('profile_completion.php');

$patientListUrl = '';
$calendar = '';
$patientSummary = '';
$patientBillSummary = '';
$patientindex = '';
$doctorAvaialbilty = '';
$staffDetailsUrl = '';
$messagesUrl = '';
$eventsUrl = '';
$recheduleAppointmentUrl = '';
$items = '';
$ExpenseItems = '';
$reports = '';
$clinic = '';
$outStandingBalance = '';
$outStandingBalance = '';

$url = basename($_SERVER['PHP_SELF']);
if($url =='my-patients.php') {
  $patientListUrl = "class='active'";
}
if($url =='add-patient.php') {
  $patientListUrl = "class='active'";
}
if($url =='patients-summary.php') {
  $patientSummary = "class='active'";
}
if($url =='patient-bill-summary.php') {
  $patientBillSummary = "class='active'";
}
if($url =='my-clinics.php') {
  $clinic = "class='active'";
}
if($url =='add-clinic.php') {
  $clinic = "class='active'";
}
if($url =='index.php') {
  $patientindex = "class='active'";
}
if($url =='schedule-timings.php') {
  $doctorAvaialbilty = "class='active'";
}
if($url =='add-availability.php') {
  $doctorAvaialbilty = "class='active'";
}

if($url =='my-staff.php') {
  $staffDetailsUrl = "class='active'";
}

if($url =='messages.php') {
  $messagesUrl = "class='active'";
}
if($url =='events-list.php') {
  $eventsUrl = "class='active'";
}

if($url =='appointment-list.php') {
    $recheduleAppointmentUrl = "class='active'";
}

if($url =='consumables-list.php') {
  $items = "class='active'";
}

if($url =='consumables-purchase-list.php') {
  $ExpenseItems = "class='active'";
}

if($url =='petty-cash-list.php') {
  $PettyCash = "class='active'";
}
if($url =='doctor-profile.php') {
  $doctorProfile = "class='active'";
}
if($url =='reports-list.php') {
  $reports = "class='active'";
}
if($url =='doctor-outstanding-balance.php') {
  $outStandingBalance = "class='active'";
}
if($url =='calendar.php') {
  $calendar = "class='active'";
}

?>
<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Profile Sidebar -->
			<div class="profile-sidebar">
				<div class="widget-profile pro-widget-content">
					<div class="profile-info-widget">
						<a href="#" class="booking-doc-img">
							<?php if(empty($_SESSION['doctor_details']['photo'])){?>
								<img src="https://ui-avatars.com/api/?background=02afee&color=fff&name=<?php echo $_SESSION['doctor_details']['doctor_name']; ?>">
				                <?php } else { ?>
				                    <img src="<?php echo '../uploads/'.$_SESSION['doctor_details']['photo']; ?>" alt="Doctor Image">
				                <?php } ?>
							<!-- <img src="../assets/img/doctors/doctor-thumb-02.jpg" > -->
						</a>
						<div class="profile-det-info">
							<h3>Dr. <?php echo ucfirst($_SESSION['doctor_details']['first_name'])." ".ucfirst($_SESSION['doctor_details']['last_name']); ?></h3>
							
							<div class="patient-details">
								<!-- <h5 class="mb-0">BDS, MDS - Oral & Maxillofacial Surgery</h5> -->
								<h5 class="mb-0">Profile Completion:<?php echo $percentage; ?>%</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="dashboard-widget">
					<nav class="dashboard-menu">
						<ul>
							<li <?php echo $patientindex;?>>
								<a href="index.php">
									<i class="fas fa-columns"></i>
									<span>Dashboard</span>
								</a>
							</li>
							<!-- <li <?php echo $calendar;?>>
								<a href="calendar.php">
									<i class="fas fa-calendar-check"></i>
									<span>Appointments Calendar</span>
								</a>
							</li> -->
							<li <?php echo $patientListUrl;?>>
								<a href="my-patients.php">
									<i class="fas fa-user-injured"></i>
									<span>My Patients</span>
								</a>
							</li>
							<li <?php echo $clinic;?>>
								<a href="my-clinics.php">
									<i class="fas fa-hospital"></i>
									<span>My Clinics</span>
								</a>
							</li>
							<li <?php echo $patientSummary;?>>
								<a href="patients-summary.php">
									<i class="fas fa-list-alt"></i>
									<span>Patients Summary</span>
								</a>
							</li>
							<li <?php echo $patientBillSummary;?>>
								<a href="patient-bill-summary.php">
									<i class="far fa-money-bill-alt"></i>
									<span>Patient Bill Summary</span>
								</a>
							</li>
							<li <?php echo $doctorAvaialbilty; ?>>
								<a href="schedule-timings.php">
									<i class="far fa-clock"></i>
									<span>Schedule Timings</span>
								</a>
							</li>
							<li <?php echo $recheduleAppointmentUrl; ?>>
								<a href="appointment-list.php">
									<i class="fas fa-hourglass-start"></i>
									<span>Reschedule Appointments</span>
								</a>
							</li>
							<li <?php echo $eventsUrl; ?>>
								<a href="events-list.php">
									<i class="fas fa-clipboard-list"></i>
									<span>My Events</span>
								</a>
							</li>
							<li <?php echo $staffDetailsUrl; ?>>
								<a href="my-staff.php">
									<i class="fas fa-id-card"></i>
									<span>My Staff Members</span>
								</a>
							</li>
							<li <?php echo $outStandingBalance;?>>
								<a href="doctor-outstanding-balance.php">
									<i class="fas fa-balance-scale-right"></i>
									<span>OutStanding Balance</span>
								</a>
							</li>
							<li <?php echo $items;?>>
								<a href="consumables-list.php">
									<i class="fas fa-toolbox"></i>
									<span>Consumables List</span>
								</a>
							</li>
							<li <?php echo $ExpenseItems;?>>
								<a href="consumables-purchase-list.php">
									<i class="fas fa-shopping-cart"></i>
									<span>Purchase of Consumables</span>
								</a>
							</li>
							<li <?php echo $PettyCash;?>>
								<a href="petty-cash-list.php">
									<i class="fas fa-rupee-sign"></i>
									<span>Petty Cash List</span>
								</a>
							</li>
							<li <?php echo $reports;?>>
								<a href="reports-list.php">
									<i class="fas fa-file-invoice"></i>
									<span>Reports</span>
								</a>
							</li>
							<li <?php echo $doctorProfile; ?>>
								<a href="doctor-profile.php?id=<?php echo $_SESSION['doctor_details']['id'];?>">
									<i class="fas fa-user-cog"></i>
									<span>Profile Settings</span>
								</a>
							</li>
							<!-- <li>
								<a href="doctor-change-password.html">
									<i class="fas fa-lock"></i>
									<span>Change Password</span>
								</a>
							</li> -->
							<li>
								<a href="../doctor_login.php">
									<i class="fas fa-sign-out-alt"></i>
									<span>Logout</span>
								</a>
							</li>
							<!-- <li>
								<a href="invoices.html">
									<i class="fas fa-file-invoice"></i>
									<span>Invoices</span>
								</a>
							</li>
							<li>
								<a href="reviews.html">
									<i class="fas fa-star"></i>
									<span>Reviews</span>
								</a>
							</li>
							<li>
								<a href="chat-doctor.html">
									<i class="fas fa-comments"></i>
									<span>Message</span>
									<small class="unread-msg">23</small>
								</a>
							</li>
							<li>
								<a href="social-media.html">
									<i class="fas fa-share-alt"></i>
									<span>Social Media</span>
								</a>
							</li> -->
						</ul>
					</nav>
				</div>
			</div>
							<!-- /Profile Sidebar -->
							
</div>