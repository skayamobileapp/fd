<?php
include('../connection/conn.php');
include('session_check.php');
date_default_timezone_set("Asia/Kolkata");

error_reporting(0);

$did = $_SESSION['doctor_details']['id'];
// echo $mo = $_POST['mobNum'];
$patients=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from patient_details where id='$id' ";
    $result   = $conn->query($sql);
    $patients = $result->fetch_assoc();
   }

if (isset($_GET['mob'])) {
    $mobNum   = $_GET['mob'];
    $sql      = "select * from patient_details where mobile_number='$mobNum' order by id desc limit 1";
    $result   = $conn->query($sql);
    $patients = $result->fetch_assoc();
}

if($_POST)
{

  if($_GET['id'])
    {
      $id = $_GET['id'];
      $fname = $_POST['fname'];
      $midname = $_POST['mname'];
      $lname = $_POST['lname'];
      $patientName = $fname." ".$lname;
      $patientMobileNumber = $_POST['patient_mobile_number'];
      $email = $_POST['email'];
      $address = $_POST['address'];
      $dateOfBirth = $_POST['date_of_birth'];
      $gender = $_POST['gender'];

      $sql = "UPDATE patient_details SET patient_name='$patientName', first_name='$fname', middle_name='$midname', last_name='$lname', mobile_number='$patientMobileNumber', email='$email', address1='$address', date_of_birth='$dateOfBirth', gender='$gender' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Patient details updated successfully');</script>";
        echo "<script>parent.location='my-patients.php'</script>";
      }
    }
    else
    {

  $fname = $_POST['fname'];
  $midname = $_POST['mname'];
  $lname = $_POST['lname'];
  $patientName = $fname." ".$lname;
  $patientMobileNumber = $_POST['patient_mobile_number'];
  $email = $_POST['email'];
  $address = $_POST['address'];
  $dateOfBirth = $_POST['date_of_birth'];
  $gender = $_POST['gender'];
  $id_clinic = $_POST['id_clinic'];
        $Profile = "default.png";
      
    $check ="SELECT * FROM patient_details WHERE email='$email' or mobile_number = '$patientMobileNumber'";
  $checkresult = mysqli_query($conn,$check);
  $chklp =mysqli_num_rows($checkresult);
  if ($chklp=='') {

    $insert = mysqli_query($conn,"INSERT INTO patient_details (patient_name, first_name, middle_name, last_name, mobile_number, email, address1, date_of_birth, gender, password, photo, status)VALUES('$patientName', '$fname', '$midname', '$lname','$patientMobileNumber','$email', '$address', '$dateOfBirth', '$gender', '123456', '$Profile', '1')");

      $last_id = mysqli_insert_id($conn);
      $did = $_SESSION['doctor_details']['id'];
        $date = date('Y-m-d H:i:s');

      $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, patient_name, id_clinic, mobile, height, weight, bp, sugar, bmi, status) VALUES('Appointment from doctor', '$date', '$date', '$did', '$last_id', '$patientName', '$id_clinic', '$patientMobileNumber','NA','NA','NA','NA', 'NA', '0')";
        $result = $conn->query($sql);
        if ($result) {
          echo "<script>alert('Patient Added successfull');</script>";
          echo "<script>parent.location='index.php'</script>";
        }
    }
    else
    {
      
      $select1 = mysqli_query($conn,"SELECT id, patient_name, mobile_number FROM patient_details WHERE mobile_number = '$patientMobileNumber' ORDER BY id DESC LIMIT 1");
        while ($row1 = mysqli_fetch_assoc($select1)) {
          $patientId = $row1['id'];
          $patientName = $row1['patient_name'];
          $mobileNumber = $row1['mobile_number'];
          }
          $did = $_SESSION['doctor_details']['id'];
          $date = date('Y-m-d H:i:s');

      $sql= "INSERT INTO events(title, start, end, doctor_id, patient_id, patient_name, id_clinic, mobile, height, weight, bp, sugar, bmi, status) VALUES('Appointment from doctor', '$date', '$date', '$did', '$patientId', '$patientName', '$id_clinic', '$mobileNumber','NA','NA','NA','NA', 'NA', '0')";
      $result = $conn->query($sql);
        if ($result) {
          echo "<script>alert('Patient Added successfull');</script>";
          echo "<script>parent.location='index.php'</script>";
        }
    }
  }
}

$sql   = "select id, state from states";
$result = $conn->query($sql);
$stateList = array();
while ($row = $result->fetch_assoc()) {
    array_push($stateList, $row);
}

$sql = "SELECT * FROM clinic_registration where id_doctor='$did' AND flag='1'";
$select = mysqli_query($conn,$sql);

$i = 0;
$clinicName = array();
while ($row = mysqli_fetch_assoc($select)) {
  array_push($clinicName, $row);
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="my-patients.php">My Patients</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Add Patient</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Add Patient</h4>
                  <p>Note: Please enter mobile number to fetch patient data. *</p>
									<div class="row form-row">
										
										<div class="col-md-6">
											<div class="form-group">
												<label> Mobile Number <span class="text-danger"> *</span></label>
												<input type="text" class="form-control" name="patient_mobile_number" id="patient_mobile_number" maxlength="10" autocomplete="off" value="<?php echo $mobNum; ?>" >
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>First Name <span class="text-danger"> *</span></label>
												<input type="text" class="form-control" name="fname"  maxlength="20" id="fname" autocomplete="off" value="<?php echo $patients['first_name']; ?>" <?php if(!empty($patients['first_name'])){ echo "disabled=disabled";} ?>>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Middle Name <span class="text-danger"></span></label>
												<input type="text" class="form-control" name="mname" id="mname" maxlength="20" autocomplete="off" value="<?php echo $patients['middle_name']; ?>" <?php if(!empty($patients['middle_name'])){ echo "disabled=disabled";} ?>>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Last Name <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="lname"  maxlength="20" id="lname" autocomplete="off" value="<?php echo $patients['last_name']; ?>" <?php if(!empty($patients['mobile_number'])){ echo "disabled=disabled";} ?>>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Email Id<span class="text-danger">*</span></label>
												<input type="email" class="form-control" name="email" maxlength="50" autocomplete="off" value="<?php echo $patients['email']; ?>" <?php if(!empty($patients['email'])){ echo "disabled=disabled";} ?>>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Address <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="address" maxlength="150" autocomplete="off" value="<?php echo $patients['address1']; ?>" <?php if(!empty($patients['mobile_number'])){ echo "disabled=disabled";} ?>>
											</div>
										</div>
										<div class="col-md-6">
												<label>Gender <span class="text-danger">*</span></label>
										<div class="form-group mb-0">
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" name="gender" id="price_free" value="Male" <?php if($patients['gender'] =='Male') { echo "checked";} ?> <?php if(!empty($patients['mobile_number'])){ echo "disabled=disabled";} ?> class="custom-control-input" >
												<label class="custom-control-label" for="price_free">Male</label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" name="gender" id="price_custom" value="Female" <?php if($patients['gender'] =='Female') { echo "checked";} ?> <?php if(!empty($patients['mobile_number'])){ echo "disabled=disabled";} ?> class="custom-control-input">
												<label class="custom-control-label" for="price_custom">Female</label>
											</div>
										</div>
										</div>
										<!-- <div class="col-md-6">
											<div class="form-group">
												<label>Gender</label>
												<select class="form-control select">
													<option>Select</option>
													<option>Male</option>
													<option>Female</option>
												</select>
											</div>
										</div> -->
										<div class="col-md-6">
											<div class="form-group mb-0">
												<label>Clinic Name <span class="text-danger">*</span></label>
												<select class="form-control selitemIcon" name="id_clinic" id="id_clinic">
                          <?php for($i=0;$i<count($clinicName);$i++) { ?>
                          <option value="<?php echo $clinicName[$i]['id'];?>"><?php echo $clinicName[$i]['clinic_name'];?></option>
                          <?php } ?> 
                        </select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="my-patients.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Dropzone JS -->
		<script src="../assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

     <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              fname : {
              required : true,
              accept : true
            },
            mname : {
              accept : true
            },
            lname : {
              required : true,
              accept : true
            },
              email : "required",
              gender : "required",
              address : "required",
              id_clinic : "required",
              patient_mobile_number : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10
                },
         
            },
            messages : {

                fname : {
                 required : "<span> enter First name</span>",
                 accept : "<span> enter letters only</span>"
               },
               mname : {
                 accept : "<span> enter letters only</span>"
               },
               lname : {
                 required : "<span> enter last name</span>",
                 accept : "<span> enter letters only</span>"
               },
                 email : "<span> enter email Id</span>",
                 gender : "<span> choose gender</span>",
                address : "<span> enter address</span>",
                id_clinic : "<span>select Clinic Name</span>",
                 patient_mobile_number : {
                    required : "<span> enter mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

     <script type="text/javascript">
  // $(document).ready(function(){  
      // $('#patient_mobile_number').blur(function(){  
      //   var mob = $('#patient_mobile_number').val();

      //           $.ajax({  
      //                url:"get_patient_data.php",  
      //                method:"POST",  
      //                data:{mob:mob},  
      //                success:function(data)  
      //                {  
      //                     $('#mobNum').val(data);
      //                }
      //           });  
      // });  

  $(document).ready(function(){  
  $('#patient_mobile_number').blur(function(){  
    var mob = $('#patient_mobile_number').val();

              parent.location='add-patient.php?mob='+mob;
  });
});
</script>

    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>