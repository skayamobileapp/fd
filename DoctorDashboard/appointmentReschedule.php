<?php 
include('../connection/conn.php');
include('session_check.php');

$tdate = date('Y-m-d');
$fdate = date('Y-m-d');

  $did = $_SESSION['doctor_details']['id'];


  $sql = "SELECT a.id, a.patient_name, a.mobile_number, a.email, a.address1 
        FROM patient_details as a INNER JOIN events as b ON b.patient_id=a.id WHERE b.doctor_id='$did' and date(b.start)>='$fdate' AND b.status =0";
$select = mysqli_query($conn,$sql);
$result = $conn->query($sql);
$patientList = array();
while ($row=$result->fetch_assoc())
{
    array_push($patientList, $row);
}


if($_POST)
{
  $did = $_SESSION['doctor_details']['id'];
  if($_POST['fdate']=='') {
    $fdate = date('Y-m-d');
  } else {
  $fdate = date("Y-m-d", strtotime($_POST['fdate']));
  }

  if($_POST['tdate']=='') {
    $tdate = date('Y-m-d');
  } else {
  $tdate = date("Y-m-d", strtotime($_POST['tdate']));
  }

  $pid = $_POST['name'];

  $sql="SELECT * FROM events WHERE date(start) >='$fdate' AND date(end) <='$tdate' AND doctor_id='$did' AND patient_id='$pid' AND status ='0'";
  $query = mysqli_query($conn,$sql);
$viewdata =[];
      $i=0;
      while($row=mysqli_fetch_assoc($query))
      {
          $viewdata[$i]['id']=$row['id'];
          $viewdata[$i]['title']=$row['title'];
          $viewdata[$i]['start']=$row['start'];
          $viewdata[$i]['end']=$row['end'];
          $viewdata[$i]['patient_id']=$row['patient_id'];
          $viewdata[$i]['patient_name']=$row['patient_name'];
          $i++;
     }

}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../css/jquery-ui.css" rel="stylesheet">
    <link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">

     <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    
</head>

<body>
  <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
              <h3>Reschedule Appointments</h3>
              <div class="card">
                <form action="" method="POST">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                          <label class="">Patient Name<span class="error"></span></label>
                        <select class="form-control selitemIcon" name="name" id="name">
                          <option value="">SELECT</option>
                          <?php
                          for ($i=0; $i <count($patientList) ; $i++) { ?>
                            <option value="<?php echo $patientList[$i]['id']; ?>" <?php if ($_POST['name'] == $patientList[$i]['id']){ echo "selected";} ?>
                            >
                              <?php echo strtoupper($patientList[$i]['patient_name']); ?>
                            </option>
                            <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="fdate" class="form-control" id="fdate" autocomplete="off" value="<?php if($_POST){ echo $fdate; } else{" ";} ?>">
                        <label class="fg-label">From Date:<span class="error"> *</span></label>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="tdate" class="form-control" id="tdate" autocomplete="off" value="<?php if($_POST){ echo $tdate; } else{" ";}?>">
                        <label class="fg-label">To Date:<span class="error"> *</span></label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                        <input type="submit" name="getdata" class="btn btn-primary btn-lg pull-right" id="getdata" value="Submit">
                    </div>
                  </div>
                </div>      
                            <h2>Appointment List</h2>
                            <hr>
                            <div class="table-responsive theme-table v-align-top">
                            <table class="table" >
                            <thead>
                                <tr>
                                    <th>Patient Name</th>
                                    <th>Appointment Reason</th>
                                    <th>Appointment On</th>
                                    <th>Appointment Reschedule</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i=0; $i<count($viewdata); $i++) { ?>
                                    <tr>                                    
                                        <td><?php echo strtoupper($viewdata[$i]['patient_name']); ?></td>
                                        <td><?php echo ucfirst($viewdata[$i]['title']);?></td>
                                        <td><?php echo date("dM Y h:i:a", strtotime($viewdata[$i]['start']));?></td>
                                        <td><?php echo "<a class='btn btn-primary' href='update_appointment.php?id=". $viewdata[$i]['id']."'>Reschedule Now</a>"; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                        </tbody>
                    </table>
                    </div>                                                                           
                      
              </form>
              </div>
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>

    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/jquery.validate.min.js"></script>

     <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              fdate : "required",
              tdate : "required"
            },
            messages : {

                fdate : "<span> select from datee</span>",
                tdate : "<span> select to date</span>"
                 
            }
        });
    });
</script>



  <link rel="stylesheet" href="../css/jquery-ui.css">
  <script src="../js/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#fdate" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  });
  $( function() {
    $( "#tdate" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  });
  </script>

  <script src="../select2/js/select2.js" ></script>
  <script src="../select2/js/select2-init.js" ></script>

<script type="text/javascript">
    document.getElementById('name').value="<?php echo $pid; ?>";
</script>
</body>

</html>