<?php 
include('../connection/conn.php');
include('session_check.php');
include('profile_completion.php');
error_reporting(0);
date_default_timezone_set("Asia/Kolkata");

$date = DateTime::createFromFormat('U.u', microtime(TRUE));
$date->setTimeZone(new DateTimeZone('Asia/Kolkata'));
$date = $date->format('dmY_His_u');

$id = $_GET['id'];



if($id = $_GET['id']){
  $docList = [];
$sql = "SELECT *  FROM doctor_details WHERE id = '$id' ";
$result = $conn->query($sql);
$docList = $result->fetch_assoc();
}
else{
  $did = $_SESSION['doctor_details']['id'];

$docList = [];
$sql = "SELECT *  FROM doctor_details WHERE id = '$did' ";
$result = $conn->query($sql);
$docList = $result->fetch_assoc();
}

if($percentage>=75){
        mysqli_query($conn, "UPDATE doctor_details SET profile_status='1' WHERE id='$id'");
      }

if ($_POST) 
{
    //from names given in html
    $id = $_SESSION['doctor_details']['id'];
    
    //$doctorName = $_POST['doctor_name'];
  $firstName = $_POST['first_name'];
  $midName = $_POST['middle_name'];
  $lastName = $_POST['last_name'];
  $fullName = "Dr. ".ucfirst($firstName)." ".ucfirst($midName)." ".ucfirst($lastName);
  $emailAddress = $_POST['email_address'];
  $password = $_POST['password'];
  $gender = $_POST['gender'];
  $stream = $_POST['stream'];
  $speciality = $_POST['speciality'];
  $sub_speciality = $_POST['sub_speciality'];
  // $telphone = $_POST['Telcheck'];
  // $teldata = implode(",", $telphone);
  $doctorMobileNum = $_POST['mobile'];
  // $cellPhone = $_POST['cell_number'];
  $pharmacy = $_POST['pharmacy'];
  $postalAddress = $_POST['postal_address'];
  $pincode = $_POST['pincode'];
  $state = $_POST['state'];
  $city = $_POST['city'];
  $practiceContact = $_POST['practice_contact'];
  $clinic_address = $_POST['clinic_address'];
  $experiance = $_POST['years_of_practice'];
  $qualification = $_POST['qualification'];
  $ug_qualification = $_POST['ug_qualification'];
  $pg_qualification = $_POST['pg_qualification'];
  $ug_file = $_FILES['ug_qualification_file']['name'];

  if($ug_file=="")
    {
         $ug_file = $docList['ug_qualification_file'];
    }
    else
    {
        $ug_file = $_FILES['ug_qualification_file']['name'];
        $img_tmp =$_FILES['ug_qualification_file']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($ug_file);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $ug_file = 'UG_'. $date . '.'. $extension;

        $file_path = $upload_path . 'UG_' . $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

  $pg_file = $_FILES['pg_qualification_file']['name'];
if($pg_file=="")
    {
         $pg_file = $docList['pg_qualification_file'];
    }
    else
    {
        $pg_file = $_FILES['pg_qualification_file']['name'];
        $img_tmp =$_FILES['pg_qualification_file']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($pg_file);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $pg_file = 'PG_'. $date . '.'. $extension;

        $file_path = $upload_path . 'PG_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);

    }
    

  $institution = $_POST['institution'];
  $fellowShip = $_POST['fellow_ship'];
  $imaNo = $_POST['ima_no'];
  $licence1 = $_POST['licence1'];
  $about_me = $_POST['about_me'];

  $licence_file1 = $_FILES['licence_file1']['name'];
  if($licence_file1=="")
    {
         $licence_file1 = $docList['license_file'];
    }
    else
    {
        $licence_file1 = $_FILES['licence_file1']['name'];
        $img_tmp =$_FILES['licence_file1']['tmp_name'];
        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($licence_file1);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $licence_file1 = 'L1_'. $date . '.'. $extension;

        $file_path = $upload_path . 'L1_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }


  $license2 = $_POST['license2'];


  $licence_file2 = $_FILES['licence_file2']['name'];
  if($licence_file2=="")
    {
         $licence_file2 = $docList['license_file2'];
    }
    else
    {
        $licence_file2 = $_FILES['licence_file2']['name'];
        $img_tmp =$_FILES['licence_file2']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($licence_file2);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $licence_file2 = 'L2_'. $date . '.'. $extension;

        $file_path = $upload_path . 'L2_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

  $timeslot = $_POST['timeslot'];

  $Profile = $_FILES['ProfilePhoto']['name'];

    if($Profile=="")
    {
         $Profile = $docList['photo'];
    }
    else
    {
        $Profile = $_FILES['ProfilePhoto']['name'];
        $img_tmp =$_FILES['ProfilePhoto']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($Profile);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $Profile = 'DocPic_'. $date . '.'. $extension;

        $file_path = $upload_path . 'DocPic_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $logo = $_FILES['logoPhoto']['name'];

    if($logo=="")
    {
         $logo = $docList['logo'];
    }
    else
    {
        $logo = $_FILES['logoPhoto']['name'];
        $img_tmp =$_FILES['logoPhoto']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($logo);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $logo = 'DocLogo_'. $date . '.'. $extension;

        $file_path = $upload_path . 'DocLogo_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $sign = $_FILES['sign']['name'];

    if($sign=="")
    {
         $sign = $docList['signature'];
    }
    else
    {
        $sign = $_FILES['sign']['name'];
        $img_tmp =$_FILES['sign']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($sign);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $sign = 'DocSign_'. $date . '.'. $extension;

        $file_path = $upload_path . 'DocSign_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

    $query = "UPDATE  doctor_details SET doctor_name ='$fullName', first_name = '$firstName', middle_name = '$midName', last_name = '$lastName', email = '$emailAddress', gender ='$gender', stream='$stream', specialty = '$speciality', sub_specialty = '$sub_speciality', mobile = '$doctorMobileNum', address = '$postalAddress', pincode = '$pincode', state = '$state', city ='$city', practice_contact = '$practiceContact', clinic_address = '$clinic_address', experiance = '$experiance', qualification = '$qualification', qualification_ug='$ug_qualification', qualification_ug_file='$ug_file', qualification_pg='$pg_qualification', qualification_pg_file='$pg_file', institute = '$institution', fellowship='$fellowShip', ima_no='$imaNo', pharmacy='$pharmacy', licence = '$licence1', license2='$license2', license_file='$licence_file1', license_file2='$licence_file2', photo = '$Profile', logo='$logo', timeslot='$timeslot', signature='$sign', about_me='$about_me' WHERE id = '$id'";

    $update = mysqli_query($conn,$query);

    if ($update)
    {
      if($percentage>=75){
        mysqli_query($conn, "UPDATE doctor_details SET profile_status='1' WHERE id='$id'");
      }
        echo "<script>alert('update your profile successfully');</script>";
        echo "<script>parent.location='index.php'</script>";

    }
}

$sql   = "SELECT id, name FROM stream";
$result = $conn->query($sql);
$streamList = array();
while ($row = $result->fetch_assoc()) {
    array_push($streamList, $row);
}

$sql   = "SELECT id, specialty FROM specialties";
$result = $conn->query($sql);
$specialtyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($specialtyList, $row);
}

$sql   = "SELECT id, state FROM states";
$result = $conn->query($sql);
$stateList = array();
while ($row = $result->fetch_assoc()) {
    array_push($stateList, $row);
}


?>
<!DOCTYPE html> 
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<style type="text/css">
		#profile{
			left: 1000px;
		}
	</style>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
            
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Basic Information</h4>
									<div class="row form-row">
										<div class="col-md-12">
											<div class="form-group">
												<div class="change-avatar">
													<div class="profile-img">
														<img src="<?php echo '../uploads/'.$docList['photo']; ?>" alt="Doctor Image">
													</div>
													<div class="upload-img">
														<div class="change-photo-btn">
															<span><i class="fa fa-upload"></i> Upload Photo</span>
															<input type="file" class="upload" name="ProfilePhoto">
														</div>
														<small class="form-text text-muted">Allowed JPG, GIF or PNG. Max size of 2MB</small>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Doctor Name <span class="text-danger"></span></label>
												<input type="text" class="form-control" value="<?php echo $docList['doctor_name']; ?>" readonly>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Email <span class="text-danger"></span></label>
												<input type="email" class="form-control" name="email_address" value="<?php echo $docList['email']; ?>" readonly>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>First Name <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="first_name" value="<?php echo $docList['first_name']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Middle Name <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="middle_name" id="middle_name" value="<?php echo $docList['middle_name']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Last Name <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="last_name" value="<?php echo $docList['last_name']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Phone Number <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="mobile" maxlength="10" value="<?php echo $docList['mobile']; ?>">
											</div>
										</div>
										<div class="col-md-6">
												<label>Gender <span class="text-danger">*</span></label>
                        <select class="form-control" name="gender" id="gender">
                          <option>Select</option>
                          <option value="Male" <?php if($docList['gender']== 'Male'){ echo "selected=selected"; } ?>>MALE</option>
                          <option value="Female" <?php if($docList['gender']== 'Female'){ echo "selected=selected"; } ?>>FEMALE</option>
                        </select>
										</div>
										<!-- <div class="col-md-6">
											<div class="form-group">
												<label>Gender</label>
												<select class="form-control select">
													<option>Select</option>
													<option>Male</option>
													<option>Female</option>
												</select>
											</div>
										</div> -->
										<div class="col-md-6">
											<div class="form-group mb-0">
												<label>Date of Birth <span class="text-danger">*</span></label>
                        <div class="cal-icon">
												<input type="text" class="form-control datetimepicker" name="date_of_birth" id="date_of_birth" value="<?php echo date("d/m/Y", strtotime($docList['date_of_birth'])); ?>">
                        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->

              <!-- About Me -->
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">About Me</h4>
                  <div class="form-group mb-0">
                    <label>Biography</label>
                    <textarea class="form-control" rows="5" name="about_me" id="about_me"><?php echo $docList['about_me']; ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /About Me -->
							
							<!-- Clinic Info 
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Clinic Info</h4>
									<div class="row form-row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Clinic Name</label>
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Clinic Address</label>
												<input type="text" class="form-control">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label>Clinic Images</label>
												<form action="#" class="dropzone"></form>
											</div>
											<div class="upload-wrap">
												<div class="upload-images">
													<img src="../assets/img/features/feature-01.jpg" alt="Upload Image">
													<a href="javascript:void(0);" class="btn btn-icon btn-danger btn-sm"><i class="far fa-trash-alt"></i></a>
												</div>
												<div class="upload-images">
													<img src="../assets/img/features/feature-02.jpg" alt="Upload Image">
													<a href="javascript:void(0);" class="btn btn-icon btn-danger btn-sm"><i class="far fa-trash-alt"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>-->
							<!-- /Clinic Info -->

							<!-- Contact Details -->
							<div class="card contact-card">
								<div class="card-body">
									<h4 class="card-title">Contact Details</h4>
									<div class="row form-row">
										<div class="col-md-6">
											<div class="form-group">
												<label>Address Line 1 <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="postal_address" maxlength="250" value="<?php echo $docList['address']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">State <span class="text-danger">*</span></label>
												<select name="state" class="form-control selitemIcon" id="state" onchange="getCities()">
		                                        <option value=''>Select</option>
		                                        <?php
		                                        for ($i=0; $i<count($stateList); $i++) {  ?>
		                                        <option value="<?php echo $stateList[$i]['id']; ?>" <?php if( $stateList[$i]['id'] == $docList['state'])  { echo "selected=selected"; }
		                                        ?>><?php echo $stateList[$i]['state']; ?></option>
		                                        <?php
		                                        }
		                                        ?>
		                                    </select>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">City <span class="text-danger">*</span></label>
												<select class="form-control selitemIcon" name="city" id="citylist" >
			                                    </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Country</label>
												<input type="text" class="form-control" value="India" readonly>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Postal Code <span class="text-danger">*</span></label>
												<input type="text" name="pincode" id="pincode" maxlength="6" class="form-control" value="<?php echo $docList['pincode']; ?>">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Contact Details -->
							
							<!-- Services and Specialization -->
							<div class="card services-card">
								<div class="card-body">
									<h4 class="card-title">Services and Specialization</h4>
									<div class="row form-row">
									<!-- <div class="col-md-12">
										<div class="form-group">
											<label>Services</label>
											<input type="text" data-role="tagsinput" class="input-tags form-control" placeholder="Enter Services" name="services" value="Tooth cleaning " id="services">
											<small class="form-text text-muted">Note : Type & Press enter to add new services</small>
										</div>
									</div> -->
									<div class="col-md-6">
										<div class="form-group">
											<label class="control-label">Stream <span class="text-danger">*</span></label>
											<select class="form-control selitemIcon" name="stream" id="stream" onchange="getspecialty()">
		                                        <option value="">Select</option>
		                                        <?php
		                                        for ($i=0; $i <count($streamList); $i++) {
		                                            ?>
		                                            <option value="<?php echo $streamList[$i]['id']; ?>" <?php if($streamList[$i]['id'] == $docList['stream'])  { echo "selected=selected"; }
		                                        ?>><?php echo $streamList[$i]['name']; ?></option>
		                                            <?php
		                                        }
		                                        ?>
		                                    </select>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Speciality <span class="text-danger">*</span></label>
												<select class="form-control selitemIcon" name="speciality" id="speciality" onchange="get_subspecialty()">
                                        
			                                    </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Sub Speciality <span class="text-danger">*</span></label>
												<select class="form-control selitemIcon" name="sub_speciality" id="sub_speciality">
                                        
			                                    </select>
											</div>
										</div>
									</div>
								</div>              
							</div>
							<!-- /Services and Specialization -->

							<!-- Education -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Education</h4>
									<div class="education-info">
										<div class="row form-row education-cont">
											<div class="col-12 col-md-10 col-lg-11">
												<div class="row form-row">
													<div class="col-sm-7">
													<label>Highest Educational Qualification<span class="text-danger">*</span></label>
													<div class="form-group mb-0">
													<div class="custom-control custom-radio custom-control-inline">
														<input type="radio" name="qualification" id="price_free" value="UG" <?php if( $docList['qualification'] =='UG') { echo "checked"; } ?> onchange="getug(this)" class="custom-control-input" >
														<label class="custom-control-label" for="price_free">Under Graduation(UG)</label>
														<br/><br/>
													</div>
													<div class="custom-control custom-radio custom-control-inline">
														<input type="radio" name="qualification" id="price_custom" value="PG" <?php if( $docList['qualification'] =='PG') { echo "checked";} ?> onchange="getpg(this)" class="custom-control-input">
														<label class="custom-control-label" for="price_custom">Post Graduation(PG)</label>
													</div>
												</div>
												</div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>University / Institute Name<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="institution" value="<?php echo $docList['institute']; ?>">
                            </div>                 
                        </div>
				                    <div class="row" id="ugdiv" style="display: none;">
												<div class="col-sm-6">
						                            <div class="form-group">
				                                    <label>UG Qualification Name<span class="text-danger">*</span></label>
				                                    <input type="text" class="form-control" name="ug_qualification" value="<?php echo $docList['qualification_ug']; ?>">
						                            </div>                 
						                        </div>
						                        <div class="col-sm-6">
						                            <div class="form-group">
					                                  <label>UG Qualification Certificate<span class="text-danger">*</span></label>
					                                    <input type="file" class="form-control" name="ug_qualification_file" >
				                                    <div id="span1"></div>
					                                </div>
					                            </div>
					                        </div>
					                        <div class="row" id="pgdiv" style="display: none;">
												<div class="col-sm-6">
						                            <div class="form-group">
				                                    <label>PG Qualification Name<span class="text-danger">*</span></label>
				                                    <input type="text" class="form-control" name="pg_qualification" value="<?php echo $docList['qualification_pg']; ?>">
						                            </div>  
						                        </div>
						                        <div class="col-sm-6">
						                            <div class="form-group">
					                                  <label>PG Qualification Certificate<span class="text-danger">*</span></label>
					                                    <input type="file" class="form-control" name="pg_qualification_file" >
				                                    <div id="span1"></div>
					                                </div>
					                            </div>
					                        </div>
					                        </div>
						                    </div>
												</div>
											</div>
										</div>
									</div>
							<!-- /Education -->
						
							<!-- Experience -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Experience and Proof of Doctor Certifications.</h4>
									<div class="experience-info">
										<div class="row form-row experience-cont">
											<div class="col-12 col-md-10 col-lg-11">
												<div class="row form-row">
													<div class="col-sm-6">
														<div class="form-group">
															<label>Total Professional Experience(In Years) <span class="text-danger">*</span></label>
															<input type="number" class="form-control" name="years_of_practice" maxlength="3" value="<?php echo $docList['experiance']; ?>">
														</div> 
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label>Fellowship Number <span class="text-danger">*</span></label>
															<input type="text" class="form-control" name="fellow_ship" value="<?php echo $docList['fellowship']; ?>">
														</div> 
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label>Indian Medical Assoc. Number <span class="text-danger">*</span></label>
															<input type="text" class="form-control" name="ima_no" value="<?php echo $docList['ima_no']; ?>">
														</div> 
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label>Licence Number 1<span class="text-danger">*</span></label>
															<input type="text" class="form-control" name="licence1" value="<?php echo $docList['licence']; ?>">
														</div> 
													</div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Licence File 1<span class="text-danger">*</span></label>
                              <input type="file" class="form-control" name="licence_file1">
                            </div> 
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Licence Number 2<span class="text-danger"></span></label>
                              <input type="text" class="form-control" name="license2" value="<?php echo $docList['license2']; ?>">
                            </div> 
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Licence File 2<span class="text-danger"></span></label>
                              <input type="file" class="form-control" name="licence_file2">
                            </div> 
                          </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Experience -->

              <!-- Clinic Info -->
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Clinic And Pharmacy Info</h4>
                  <div class="row form-row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Clinic / Hospital Address</label>
                        <input type="text" class="form-control" name="clinic_address" maxlength="250" value="<?php echo $docList['clinic_address']; ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Practice Contact Number</label>
                        <input type="text" class="form-control" name="practice_contact" maxlength="10" value="<?php echo $docList['practice_contact']; ?>">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Checkup Time Slot</label>
                        <select class="form-control selitemIcon" name="timeslot" id="timeslot">
                            <option value="">Select</option>
                            <?php
                            for ($i=0; $i <60; $i++) { ?>
                                <option value="<?php echo $i; ?>" <?php if($i == $docList['timeslot'])  { echo "selected=selected"; }
                            ?>><?php echo $i; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Clinic / Hospital Logo</label>
                        <input type="file" class="form-control" name="logoPhoto">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Pharmacy Name</label>
                        <input type="text" class="form-control" name="pharmacy" value="<?php echo $docList['pharmacy']; ?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Clinic Info -->

              <!-- Clinic Info -->
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Professional Signature</h4>
                  <div class="row form-row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Upload signature</label>
                        <input type="file" id='sign' class="form-control" name="sign" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /Clinic Info -->
							
							<div class="submit-section submit-btn-bottom">
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>

    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

    <script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

 <script type="text/javascript">
    $(document).ready(function()
    {
         $("#form").validate({
     
             rules : {

              first_name : {
                required : true,
                accept : true
              },
              middle_name : {
                accept : true
              },
              last_name : {
                required : true,
                accept : true
              },
              email_address : "required",
              gender : "required",
              stream : "required",
              alter_phone : {
                number: true
            },
              speciality : "required",
              pharmacy : {
                accept : true
              },
              state : "required",
              city : "required",
              pincode : {
                    required : true,
                    number : true,
                    minlength : 6,
                    maxlength : 6
                },
              sub_speciality : "required",
              postal_address : "required",
              
              years_of_practice : "required",
              qualification : "required",
              ug_qualification : {
                required : true,
                accept : true
              },
              pg_qualification : {
                required : true,
                accept : true
              },
              // ug_qualification_file : "required",
              // pg_qualification_file : "required",

              timeslot : "required",
                
                  mobile : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
                telephone_details : {
                    required : true,
                    number : true,
                    minlength : 11,
                    maxlength : 11

                },
                cell_phone : {
                   number : true,
                    minlength : 10,
                    maxlength : 10

                }
         
            },
            messages : {

                first_name : {
                  required : "<span> enter first name</span>",
                  accept : "<span> enter letters only</span>",
                },
                middle_name : {
                  accept : "<span> enter letters only</span>"
                },
                last_name : {
                  required : "<span> enter last name</span>",
                  accept : "<span> enter letters only</span>",
                },
                speciality : "<span> Select speciality</span>",
                postal_address : "<span> enter postal address</span>",
                email_address : "<span> enter email address </span>",
                years_of_practice : "<span> enter years of practice</span>",
                gender : "<span> Select Gender</span>",
                sub_speciality : "<span> Select sub speciality </span>",
                qualification : "<span> Select Qualification</span>",
                ug_qualification : {
                  required : "<span> enter qualification name</span>",
                    accept : "<span> enter letters only</span>"
                },
                pg_qualification : {
                  required : "<span> enter qualification name</span>",
                    accept : "<span> enter letters only</span>"
                },
                // ug_qualification_file : "<span> Upload ug qualification Certificate</span>",
                // pg_qualification_file : "<span> Upload Pg qualification Certificate</span>",

                stream : "<span> Select stream</span>",
                pharmacy : {
                    accept : "<span> enter letters only</span>",
                },
                alter_phone : {
                    number : "<span> enter number only</span>",
                },
                timeslot : "<span> Select timeslot</span>",
                state : "<span> Select state</span>",
                city : "<span> Select city</span>",

                 pincode : {
                    required : "<span> enter the pincode</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 6 digit pincode</span>",
                    maxlength : "<span> enter max 6 digit number</span>"
                },
                mobile : {
                    required : "<span> enter the mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                telephone_details : {
                    required : "<span> enter the telephone number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 11 digit number</span>",
                    maxlength : "<span> enter max 11 digit  number</span>"
                },
                 practice_contact : {
                    required : "<span> enter the practice contact number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                alter_phone : {
                    required : "<span> enter the alternative number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                cell_phone : {
                    required : "<span> enter the cell contact number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });
</script>

		<script type="text/javascript">
// ug fields

      function getug(c1) {
        var cval = c1.value;
      if(cval =='UG') {
        $("#ugdiv").show();
        $("#pgdiv").hide();
      }
    }

// pg fields

    function getpg(c2) {
        var cval = c2.value;
      if(cval =='PG') {
        $("#ugdiv").show();
        $("#pgdiv").show();
      }
      else
      {
        $("#pgdiv").hide();
      }
    }

    var cval = '<?php echo $docList['qualification']; ?>';
    if(cval =='PG') {
        $("#ugdiv").show();
        $("#pgdiv").show();
      }else
      if(cval =='UG') {
        $("#ugdiv").show();
      }
      else{
        $("#ugdiv").hide();
      }
  
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

     <script type="text/javascript">
$( document ).ready(function() {
    getCities();
    getspecialty();
});

        function getCities(){
          var id = $("#state").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#citylist").html(result);
              var idcityselected = '<?php  echo $docList['city'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#citylist").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

        function getspecialty(){
          var id = $("#stream").val();

          $.ajax({url: "getSpeciality.php?id="+id, success: function(result){
            $("#speciality").html(result);

            var idspecialityselected = '<?php  echo $docList['specialty'];?>';
              if(idspecialityselected!='' && idspecialityselected!= null) {
                 $("#speciality").val(idspecialityselected);
                 get_subspecialty();
              }

          }
        });
        }

        function get_subspecialty(){
          var id = $("#speciality").val();
          console.log(id);

          $.ajax({url: "get_sub_speciality.php?id="+id, success: function(result){
            $("#sub_speciality").html(result);
            var idsub_specialityselected = '<?php  echo $docList['sub_speciality'];?>';
              if(idsub_specialityselected!='' && idsub_specialityselected!= null) {
                 $("#sub_speciality").val(idsub_specialityselected);
              }
          }
        });
        }

        // function getPincode(){
        //   var id = $("#citylist").val();
        //   console.log(id);

        //   $.ajax({url: "getpincode.php?id="+id, success: function(result){
        //     $("#pincode").html(result);

        //      var pincodeselected = '<?php  echo $docList['pincode'];?>';
        //       if(pincodeselected!='' && pincodeselected!= null) {
        //          $("#pincode").val(pincodeselected);
        //       }
        //   }
        // });
          
        // }
    </script>
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>