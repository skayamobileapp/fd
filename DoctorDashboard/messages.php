<?php
include('../connection/conn.php');
include('session_check.php');

$did = $_SESSION['doctor_details']['id'];

$sql   = "SELECT a.id, a.patient_name, a.mobile_number, a.email, a.address1 
        FROM patient_details as a,
        events as b 
        where a.id=b.patient_id 
        and b.doctor_id='$did' group by a.id";
          $result = $conn->query($sql);
          $patientList = array();
          while ($row = $result->fetch_assoc()) {
          array_push($patientList, $row);
        }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>

<body>
  <?php include('navbar.php'); ?>
    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
        <section class="col-sm-8 col-lg-9">
            <div class="main-container"> 
            <!-- <form action="" method="" id="myform"> -->
               <h3 class="clearfix">Chat With Patient</h3>       
                <div class="card">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group fg-float">
                          <input type="text" name="search" class="form-control user-search" placeholder="Search" id="search" onkeyup="getpatient()" autocomplete="off">
                          </div>
                          <div class="users-list">                          
                          <div id="result">
                            <?php
                            for($i=0; $i<count($patientList); $i++)
                            {
                              $count1=0;
                              $notesql= "SELECT * FROM messages where doctor_id='$did' AND sent_by='patient' AND doctor_flag='0' AND patient_id='".$patientList[$i]['id']."' ";
                                $result=mysqli_query($conn, $notesql);
                                $count1=mysqli_num_rows($result);
                              ?>

                              <div class="chat-user-profile" onclick="get_prev(this);" id="<?php echo $patientList[$i]['id']; ?>" >
                              <span id="notification-count" class="notification-count">
                                <?php if($count1>0) { echo $count1; } ?></span>
                                <div class="user-image">
                                  <?php if(empty($patientList[$i]['photo'])){?>
                                  <img src="https://ui-avatars.com/api/?background=02afee&color=fff&name=<?php echo $patientList[$i]['patient_name']; ?>">
                                <?php } else { ?>
                                  <img src="../uploads/<?php echo $patientList[$i]['photo']; ?>">
                                <?php } ?>
                                </div>
                                <div class="user-name"><?php echo strtoupper($patientList[$i]['patient_name']); ?></div>
                                <div><?php echo strtoupper($patientList[$i]['mobile_number']); ?></div>
                              </div>
                              <?php
                            }
                            ?>
                                <input type="hidden" name="patient_id" id="patient_id" value="">
                        </div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="message-container">
                          <h3 class="clearfix"></h3>
                          <div class='scrollbar' id='style-5'>
                            <div  id="previous"></div>
                          </div>
                        </div>
                        <div class="message-form">
                        <div class="form-group fg-float">
                          <div class="fg-line">
                            <input type="text" class="form-control" name="answer" id="answer" placeholder="Type a message..." required/>
                          </div>
                        </div>
                        <div class="bttn-group">
                          <button class="btn btn-primary btn-lg" name="save" id="save">Send</button>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
            <!-- </form> -->
                </div>
        </section>
      </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="../js/jquery-1.11.1.min.js"></script>
            <script src="../js/bootstrap.min.js"></script>
            <script src="../js/main.js"></script>
  
        <script type="text/javascript">

        function get_prev(id){
          var pid = $(id).attr("id");
          var did = '<?php echo $did; ?>';
          console.log(did);
          console.log(pid);
          $("#patient_id").val(pid);

          $.ajax({
            url: 'get_questions.php',
            data:{
                  'did': did,
                  'pid': pid,
        },
            success: function(result){
            $("#previous").html(result);
          }
        });
        }
      </script>
      <script type="text/javascript">
    
        function getpatient(){
          var find = $('input').val();
          console.log(find);

          $.ajax({
            url: 'get_patients.php',
            data:{
                  'find': find,
        },
            success: function(result){
            $("#result").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript">
    $("#save").on('click',function(){
    var aid = $('#answer').val();
    var did = '<?php echo $did; ?>';
    var pid = $("#patient_id").val();
    console.log(did);
    console.log(pid);
    console.log(aid);
      $.ajax({
        // type: 'POST',
        url: 'send_message.php',
        data:{
          'aid': aid,
          'did': did,
          'pid': pid,
        },
        success: function(result){
        $("#previous").html(result);
        $('#answer').val(' ');
      }
        });
      });
    </script>

      <script src="../select2/js/select2.js" ></script>
          <script src="../select2/js/select2-init.js" ></script>
  </body>
</html>