<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);
date_default_timezone_set("Asia/Kolkata");
$did = $_SESSION['doctor_details']['id'];

$id = $_GET['id'];

$select = mysqli_query($conn,"SELECT e.id, e.title, e.start, e.patient_id, e.id_clinic, e.id_time_slots, p.patient_name, p.mobile_number, p.email, p.address1, p.photo FROM events as e inner join patient_details as p on e.patient_id=p.id WHERE e.id = '$id'");
if ($row = mysqli_num_rows($select)) 
{
    while ($row = mysqli_fetch_array($select))
    {
        //from database fields
        $title = $row['title'];
        $start = $row['start'];
        $patient_id = $row['patient_id'];
        $patient_name = $row['patient_name']; 
        $mobile = $row['mobile_number'];
        $email = $row['email'];
        $address = $row['address1'];
        $photo = $row['photo'];
        $clinic = $row['id_clinic'];
		$sid = $row['id_time_slots'];
    }
}

$patient_data = [];
$sql = "SELECT d.*, s.specialty, c.city as cityname, c.state as statename FROM doctor_details d INNER JOIN specialties s ON d.specialty=s.id INNER JOIN cities c ON d.city=c.id WHERE d.id='$DoctorId'";
$result = $conn->query($sql);
$doctor_data = $result->fetch_assoc();

$date2 = date("Y-m-d", strtotime('+1 day'));
$date3 = date("Y-m-d", strtotime('+2 day'));
$date4 = date("Y-m-d", strtotime('+3 day'));
$date5 = date("Y-m-d", strtotime('+4 day'));
$date6 = date("Y-m-d", strtotime('+5 day'));
$date7 = date("Y-m-d", strtotime('+6 day'));

$day1 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND CONCAT(t.date,' ',t.time)>=CURRENT_TIMESTAMP AND t.date<'$date2' order by t.time ASC";
$i = 0;
$slots1 = mysqli_query($conn, $day1);
          $slotList1 = array();
          while ($row = mysqli_fetch_assoc($slots1)) {
            $slotList1[$i]['id'] = $row['id'];
            $slotList1[$i]['date'] = $row['date'];
            $slotList1[$i]['time'] = date("h:i A", strtotime($row['time']));
            $slotList1[$i]['clinic_name'] = $row['clinic_name'];
            $slotList1[$i]['slot'] = $row['slot'];
            $i++;
          }
          if (empty($slotList1)) {
          	$slotList1[$i]['clinic_name'] = "No Slot";
          	$slotList1[$i]['time'] = "--:--";
          }

$day2 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND t.date='$date2' order by t.time ASC";
$j = 0;
$slots2 = mysqli_query($conn, $day2);
          $slotList2 = array();
          while ($row = mysqli_fetch_assoc($slots2)) {
            $slotList2[$j]['id'] = $row['id'];
            $slotList2[$j]['date'] = $row['date'];
            $slotList2[$j]['time'] = date("h:i A", strtotime($row['time']));
            $slotList2[$j]['clinic_name'] = $row['clinic_name'];
            $slotList2[$j]['slot'] = $row['slot'];
            $j++;
          }
          if (empty($slotList2)) {
          	$slotList2[$j]['clinic_name'] = "No Slot";
          	$slotList2[$j]['time'] = "--:--";
          }

$day3 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND t.date='$date3' order by t.time ASC";
$k = 0;
$slots3 = mysqli_query($conn, $day3);
          $slotList3 = array();
          while ($row = mysqli_fetch_assoc($slots3)) {
            $slotList3[$k]['id'] = $row['id'];
            $slotList3[$k]['date'] = $row['date'];
            $slotList3[$k]['time'] = date("h:i A", strtotime($row['time']));
            $slotList3[$k]['clinic_name'] = $row['clinic_name'];
            $slotList3[$k]['slot'] = $row['slot'];
            $k++;
          }
          if (empty($slotList3)) {
          	$slotList3[$k]['clinic_name'] = "No Slot";
          	$slotList3[$k]['time'] = "--:--";
          }

$day4 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND t.date='$date4' order by t.time ASC";
$l = 0;
$slots4 = mysqli_query($conn, $day4);
          $slotList4 = array();
          while ($row = mysqli_fetch_assoc($slots4)) {
            $slotList4[$l]['id'] = $row['id'];
            $slotList4[$l]['date'] = $row['date'];
            $slotList4[$l]['time'] = date("h:i A", strtotime($row['time']));
            $slotList4[$l]['clinic_name'] = $row['clinic_name'];
            $slotList4[$l]['slot'] = $row['slot'];

            $l++;
          }
          if (empty($slotList4)) {
          	$slotList4[$l]['clinic_name'] = "No Slot";
          	$slotList4[$l]['time'] = "--:--";
          }

$day5 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND t.date='$date5' order by t.time ASC";
$m = 0;
$slots5 = mysqli_query($conn, $day5);
          $slotList5 = array();
          while ($row = mysqli_fetch_assoc($slots5)) {
            $slotList5[$m]['id'] = $row['id'];
            $slotList5[$m]['date'] = $row['date'];
            $slotList5[$m]['time'] = date("h:i A", strtotime($row['time']));
            $slotList5[$m]['clinic_name'] = $row['clinic_name'];
            $slotList5[$m]['slot'] = $row['slot'];
            $m++;
          }
          if (empty($slotList5)) {
          	$slotList5[$m]['clinic_name'] = "No Slot";
          	$slotList5[$m]['time'] = "--:--";
          }

$day6 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND t.date='$date6' order by t.time ASC";
$n = 0;
$slots6 = mysqli_query($conn, $day6);
          $slotList6 = array();
          while ($row = mysqli_fetch_assoc($slots6)) {
            $slotList6[$n]['id'] = $row['id'];
            $slotList6[$n]['date'] = $row['date'];
            $slotList6[$n]['time'] = date("h:i A", strtotime($row['time']));
            $slotList6[$n]['clinic_name'] = $row['clinic_name'];
            $slotList6[$n]['slot'] = $row['slot'];
            $n++;
          }
          if (empty($slotList6)) {
          	$slotList6[$n]['clinic_name'] = "No Slot";
          	$slotList6[$n]['time'] = "--:--";
          }

$day7 = "SELECT t.*, c.clinic_name FROM timeslots as t INNER JOIN clinic_registration as c ON c.id=t.id_clinic WHERE t.doctor_id='$did' AND t.slot='y' AND c.flag='1' AND t.date='$date7' order by t.time ASC";
$p = 0;
$slots7 = mysqli_query($conn, $day7);
          $slotList7 = array();
          while ($row = mysqli_fetch_assoc($slots7)) {
            $slotList7[$p]['id'] = $row['id'];
            $slotList7[$p]['date'] = $row['date'];
            $slotList7[$p]['time'] = date("h:i A", strtotime($row['time']));
            $slotList7[$p]['clinic_name'] = $row['clinic_name'];
            $slotList7[$p]['slot'] = $row['slot'];
            $p++;
          }
          if (empty($slotList7)) {
          	$slotList7[$p]['clinic_name'] = "No Slot";
          	$slotList7[$p]['time'] = "--:--";
          }

$day = date("D");
if ($day=='Mon'){
	$DayName = 'Monday';
}
else
if ($day=='Tue'){
	$DayName = 'Tuesday';
}
else
if ($day=='Wed'){
	$DayName = 'Wednesday';
}
else
if ($day=='Thu'){
	$DayName = 'Thursday';
}
else
if ($day=='Fri'){
	$DayName = 'Friday';
}
else
if ($day=='Sat'){
	$DayName = 'Saturday';
}
else
{
	$DayName = 'Sunday';
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Daterangepikcer CSS -->
		<link rel="stylesheet" href="../assets/plugins/daterangepicker/daterangepicker.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">

		<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
				
					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">

							<div class="card">
								<div class="card-body">
									<div class="booking-doc-info">
										<a href="#" class="booking-doc-img">
											<img src="../uploads/<?php echo $photo; ?>" alt="User Image">
										</a>
										<div class="booking-info col-md-5">
											<h3><a href="#"><?php echo ucfirst($patient_name); ?></a></h3>
											<p class="text-muted mb-0">
												<i class="fas fa-phone-alt"></i> <?php echo $mobile; ?><br>
												<i class="fas fa-envelope"></i> 
												<?php echo $email; ?><br>
											<i class="fas fa-map-marker-alt"></i> <?php echo $address; ?></p>
										</div>
										<div class="booking-info">
											<h4>Appt Purpose : <?php echo $title; ?></h4>
											<h4>DateTime : <?php echo date("d M Y h:i A", strtotime($start)); ?></h4>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12 col-sm-4 col-md-6">
									<h4 class="mb-1"><?php echo date("d F Y"); ?></h4>
									<p class="text-muted"><?php echo $DayName; ?></p>
								</div>
                            </div>
							<!-- Schedule Widget -->
							<div class="card booking-schedule schedule-widget">
							
								<!-- Schedule Header -->
								<div class="schedule-header">
									<div class="row">
										<div class="col-md-12">
										
											<!-- Day Slot -->
											<div class="day-slot">
												<ul>
													
													<li>
														<span><?php echo date('D');?></span>
														<span class="slot-date"><?php echo date('d M ');?> <small class="slot-year"><?php echo date('Y');?></small></span>
													</li>
													<li>
														<span><?php echo date('D', strtotime('+1 day'));?></span>
														<span class="slot-date"><?php echo date('d M ', strtotime('+1 day'));?> <small class="slot-year"><?php echo date('Y', strtotime('+1 day'));?></small></span>
													</li>
													<li>
														<span><?php echo date('D', strtotime('+2 day'));?></span>
														<span class="slot-date"><?php echo date('d M ', strtotime('+2 day'));?><small class="slot-year"><?php echo date('Y', strtotime('+2 day'));?></small></span>
													</li>
													<li>
														<span><?php echo date('D', strtotime('+3 day'));?></span>
														<span class="slot-date"><?php echo date('d M ', strtotime('+3 day'));?><small class="slot-year"><?php echo date('Y', strtotime('+3 day'));?></small></span>
													</li>
													<li>
														<span><?php echo date('D', strtotime('+4 day'));?></span>
														<span class="slot-date"><?php echo date('d M ', strtotime('+4 day'));?><small class="slot-year"><?php echo date('Y', strtotime('+4 day'));?></small></span>
													</li>
													<li>
														<span><?php echo date('D', strtotime('+5 day'));?></span>
														<span class="slot-date"><?php echo date('d M ', strtotime('+5 day'));?><small class="slot-year"><?php echo date('Y', strtotime('+5 day'));?></small></span>
													</li>
													<li>
														<span><?php echo date('D', strtotime('+6 day'));?></span>
														<span class="slot-date"><?php echo date('d M ', strtotime('+6 day'));?><small class="slot-year"><?php echo date('Y', strtotime('+6 day'));?></small></span>
													</li>
												</ul>
											</div>
											<!-- /Day Slot -->
											
										</div>
									</div>
								</div>
								<!-- /Schedule Header -->
								
								<!-- Schedule Content -->
								<div class="schedule-cont">
									<div class="row">
										<div class="col-md-12">
										
											<!-- Time Slot -->
											<div class="time-slot">
												<ul class="clearfix">
													<li>
												<?php for($i=0; $i<count($slotList1); $i++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList1[$i]['id']; ?>)" id='book' <?php if($slotList1[$i]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList1[$i]['time']; ?></span><br> 
															<span><?php echo $slotList1[$i]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
													<li>
														<?php for($j=0; $j<count($slotList2); $j++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList2[$j]['id']; ?>)" id='book' <?php if($slotList2[$j]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList2[$j]['time']; ?></span><br> 
															<span><?php echo $slotList2[$j]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
													<li>
														<?php for($k=0; $k<count($slotList3); $k++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList3[$k]['id']; ?>)" id='book' <?php if($slotList3[$k]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList3[$k]['time']; ?></span><br> 
															<span><?php echo $slotList3[$k]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
													<li>
														<?php for($l=0; $l<count($slotList4); $l++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList4[$l]['id']; ?>)" id='book' <?php if($slotList4[$l]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList4[$l]['time']; ?></span><br> 
															<span><?php echo $slotList4[$l]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
													<li>
														<?php for($m=0; $m<count($slotList5); $m++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList5[$m]['id']; ?>)" id='book' <?php if($slotList5[$m]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList5[$m]['time']; ?></span><br> 
															<span><?php echo $slotList5[$m]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
													<li>
														<?php for($n=0; $n<count($slotList6); $n++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList6[$n]['id']; ?>)" id='book' <?php if($slotList6[$n]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList6[$n]['time']; ?></span><br> 
															<span><?php echo $slotList6[$n]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
													<li>
														<?php for($p=0; $p<count($slotList7); $p++){
													?>
														<a class="timing" href="#" onclick="selectedid(<?php echo $slotList7[$p]['id']; ?>)" id='book' <?php if($slotList7[$p]['id']==''){ } else { echo "data-toggle='modal' data-target='#myModal7'"; }?>>
															<span><?php echo $slotList7[$p]['time']; ?></span><br> 
															<span><?php echo $slotList7[$p]['clinic_name']; ?></span>
														</a>
													<?php } ?>
													</li>
												</ul>
											</div>
											<!-- /Time Slot -->
											
										</div>
									</div>
								</div>
								<!-- /Schedule Content -->
								
							</div>
							<!-- /Schedule Widget -->
							
							<!-- Submit Section -->
							<!-- <div class="submit-section proceed-btn text-right">
								<a href="checkout.html" class="btn btn-primary submit-btn">Proceed to Pay</a>
							</div> -->
							<!-- /Submit Section -->
							
						</div>
					</div>
				</div>

			</div>

			<!-- /Page Content -->
		<?php include('footer.php'); ?>

		</div>
		<p><a href="#" data-toggle="modal" data-target="#myModal7" data-backdrop="static" data-keyboard="false"></a></p>
	<div  id="popup">
      <div class="modal fade" id="myModal7" role="dialog">
        <div class="modal-dialog modal-md">
           <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;"> </h4>
            </div>
            <div class="modal-body">
                <div class="fg-line">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group text-center">
                        <input type="hidden" name="slot_id" id="slot_id">
                        <div class="col-sm-6">
                        	<div class="form-group">
                        		<label>Enter Appointment Purpose</label>
                        		<input type="text" name="purpose" id="purpose" class="form-control" placeholder="Appointment Purpose">
                        	</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
            <a href="#" class="btn btn-primary btn-lg" id="accept" name="accept">Ok</a>
           </div>
          </div>
      </div>
    </div>
  </div>

		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Daterangepikcer JS -->
		<script src="../assets/js/moment.min.js"></script>
		<script src="../assets/plugins/daterangepicker/daterangepicker.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script type="text/javascript">

		function selectedid(id) {
		  // var conf = confirm('ARE YOU SURE FOR THIS APPOINTMENT');
		  //  if(conf==true){
		  	$("#slot_id").val(id);
		  	
		    // $("#target").submit();
		  // }
		}
		</script>

    <script type="text/javascript">
		$("#accept").on('click',function(){
			var eid = '<?php echo $id; ?>';
			var did = '<?php echo $did; ?>';
			var patient_name = '<?php echo $patient_name; ?>';
			var patient_id = '<?php echo $patient_id; ?>';
			var mobile = '<?php echo $mobile; ?>';
			var pre_slot = '<?php echo $sid; ?>';
		    var slot_id = $("#slot_id").val();
		    var purpose = $("#purpose").val();
		    if(purpose == ''){
		    	alert('Enter Appointment Purpose');
		    	return false;
		    }
    	     window.location.href="booking-success.php?did="+did+"&slot_id="+slot_id+"&purpose="+purpose+"&patient_name="+patient_name+"&mobile="+mobile+"&sid="+pre_slot+"&eid="+eid+"&patient_id="+patient_id;
	});
		</script>

<!-- 
    <script type="text/javascript">
    $("#accept").on('click',function(){
    var did = '<?php echo $DoctorId; ?>';
    var slot_id = $("#slot_id").val();
    var purpose = $("#purpose").val();
    if(purpose == ''){
    	alert('Enter Appointment Purpose');
    	return false;
    }
      $.ajax({
        // type: 'POST',
        url: 'booking_appointment.php',
        data:{
          'did': did,
          'slot_id': slot_id,
          'purpose': purpose,
        },
        success: function(result){
         }
        });
    });

    </script> -->
		
	</body>
</html>