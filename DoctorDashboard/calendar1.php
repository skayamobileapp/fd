<?php
include('../connection/conn.php');
include('session_check.php');

error_reporting(0);
$date = date("Y-m-d");
$sql = "Select v.*, e.patient_id from vitals as v INNER JOIN events as e on v.event_id=e.id  where v.patient_id='e.patient_id' and e.status='1'";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $patient[$i]['id'] = $row['id'];
  $patient[$i]['height'] = $row['height'];
  $patient[$i]['weight'] = $row['weight'];
  $patient[$i]['pulse'] = $row['pulse'];
  $patient[$i]['bp'] = $row['bp'];
  $patient[$i]['sugar'] = $row['sugar'];
  $patient[$i]['bmi'] = $row['bmi'];
  $i++;
}

$sql   = "select * from doctor_details";
$result = $conn->query($sql);
$doctorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($doctorList, $row);
}

$sql   = "select * from patient_details";
$result = $conn->query($sql);
$patientList = array();
while ($row = $result->fetch_assoc()) {
    array_push($patientList, $row);
}

$sql   = "select * from lab_details";
$result = $conn->query($sql);
$labList = array();
while ($row = $result->fetch_assoc()) {
    array_push($labList, $row);
}

$sql   = "select * from pharmacy_registration";
$result = $conn->query($sql);
$drugList = array();
while ($row = $result->fetch_assoc()) {
    array_push($drugList, $row);
}

require_once('bdd.php');
$did = $_SESSION['doctor_details']['id'];

// $sql = "SELECT e.*, p.photo FROM events as e INNER JOIN patient_details as p ON p.id=e.patient_id";
$sql = "SELECT * FROM events WHERE doctor_id='$did'";

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
   
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

     <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>  
    <script src="../js/main.js"></script>

  <!-- FullCalendar -->
  <link href='../library/calendar/css/fullcalendar.css' rel='stylesheet' />


    <!-- Custom CSS -->
    <style>
    /*body {
        padding-top: 70px;
    }*/
  #calendar {
    max-width: 800px;
    height: 300px;
  }
  u{
    size: 2%;
    color: blue;
  }
  .fc-event {
    background-color:#2287de;
    border-color:#2287de;
  }
  .fc-unthemed .fc-today {
    background: #f0f8ff;
  }
  .fc-state-default.fc-state-default {
    background: #f2f2f2;
    box-shadow: none;
    border-color: #fff;
    text-transform: capitalize;
  }
  .fc-state-default.fc-state-default.fc-state-active {
    background-color:#2287de;
    color:#fff;
  } 
  
  .fc-content-skeleton table > tbody > tr:nth-of-type(2n) > td > .fc-event {
    background-color:rgb(255, 139, 0);
    border-color:rgb(255, 139, 0);        
  }
  .fc-content-skeleton table > tbody > tr:nth-of-type(3n) > td > .fc-event {
    background-color:rgb(82, 67, 170);
    border-color:rgb(82, 67, 170);        
  }
  .fc-content-skeleton table > tbody > tr:nth-of-type(4n) > td > .fc-event {
    background-color:rgb(0, 102, 68);
    border-color:rgb(0, 102, 68);        
  }
  .fc-content-skeleton table > tbody > tr:nth-of-type(5n) > td > .fc-event {
    background-color:rgb(89, 255, 0);
    border-color:rgb(36, 240, 5);        
  }  
    </style>

</head>
  <!-- jQuery Version 1.11.1 -->
    <script src="../library/calendar/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../library/calendar/js/bootstrap.min.js"></script>
  
  <!-- FullCalendar -->
  <script src='../library/calendar/js/moment.min.js'></script>
  <script src='../library/calendar/js/fullcalendar.min.js'></script>

<body>
  <?php include('navbar.php') ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
            <?php include('menu.php'); ?>

        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
            
            <div class="row">
              <br>
              <hr>
                <div id="calendar">
            </div>
      
        </div>
    
    <!-- Modal -->
    <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
      <div class="modal-content">
      <form class="form-horizontal" method="POST" action="editEventTitle.php">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">View Appointment</h4>
        </div>
        <div class="modal-body">
          <div class="form-group fg-float">
            <div class="fg-line">
              <div class="row">
                <div class="col-sm-1">
                </div>
                <!-- <div class="col-sm-3">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <label for="photo"></label>
                      <img src="../uploads/" alt=""/ width="320px" height="250px" id="photo">
                    </div>
                  </div>
                </div> -->
                <div class="col-sm-2">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="patient_name" class="form-control" id="patient_name" readonly>
                      <label for="patient_name" class="fg-label">Name </label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-2">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="mobile" class="form-control" id="mobile" readonly>
                      <label for="mobile" class="fg-label">Mobile </label>
                    </div>
                  </div>
                </div>
				  <div class="col-sm-1">
                </div>
                <div class="col-sm-4">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="bmi" class="form-control" id="bmi" readonly>
                      <label for="bmi" class="fg-label">BMI</label>
                    </div>
                  </div>
              </div>
			</div>
              <div class="row">
                <div class="col-sm-1">
                </div>
                <div class="col-sm-4">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="title" class="form-control" id="title" readonly>
                      <label for="title" class="fg-label">Appointment for</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="start" class="form-control" id="start" readonly>
                        <label for="start" class="fg-label">Appointment date</label>
                      </div>
                    </div>
                  </div>
              </div>
            <div class="row">
              <div class="col-sm-1">
                </div>
                <div class="col-sm-1">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="height" class="form-control" id="height" readonly>
                      <label for="height" class="fg-label">Height in CM</label>
                  </div>
                </div>
              </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-1">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="weight" class="form-control" id="weight" readonly>
                      <label for="weight" class="fg-label">Weight in KG</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-1">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="bp" class="form-control" id="bp" readonly>
                      <label for="bp" class="fg-label">BP</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-1">
                </div>
                <div class="col-sm-1">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="sugar" class="form-control" id="sugar" readonly>
                      <label for="sugar" class="fg-label">Sugar</label>
                    </div>
                  </div>
                </div>
              <div class="col-sm-1">
                </div>
                <div class="col-sm-1">
                  <div class="form-group fg-float">
                    <div class="fg-line fg-toggled">
                      <input type="text" name="pulse" class="form-control" id="pulse" readonly>
                      <label for="pulse" class="fg-label">Pulse</label>
                  </div>
                </div>
              </div>
                </div>
            </div>
          </div>
               <!--  <div class="row">
                  <div class="col-sm-5">
                    <label for="height" class="fg-label" id="height">Height</label>
                    <label for="title" class="fg-label"></label>
                    <label for="title" class="fg-label"></label>
                    <label for="title" class="fg-label"></label>
                </div>
              </div> -->
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!-- <button type="submit" class="btn btn-primary">Save changes</button> -->
            </div>
        </form>
      </div>
    </div>
  </div>

    <script>

  $(document).ready(function() {
    
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      // defaultDate: '2016-01-12',
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
        
        $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
        $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
        if(start.isBefore(moment())) {
          alert("Appointments are not allowed on this date");
          $('#calendar').fullCalendar('unselect');
          return false;
        }
        $('#ModalAdd').modal('show');
      },

      eventRender: function(event, element) {
        element.bind('click', function() {
         $('#ModalEdit #id').val(event.id);
          $('#ModalEdit #title').val(event.title);
          $('#ModalEdit #color').val(event.color);
          $('#ModalEdit #start').val(event.start);
          $('#ModalEdit #end').val(event.end);
          $('#ModalEdit #patient_name').val(event.patient_name);
          $('#ModalEdit #mobile').val(event.mobile);
          // $('#ModalEdit img').val(event.photo);
          $('#ModalEdit #height').val(event.height);
          $('#ModalEdit #pulse').val(event.pulse);
          $('#ModalEdit #weight').val(event.weight);
          $('#ModalEdit #bp').val(event.bp);
          $('#ModalEdit #sugar').val(event.sugar);
          $('#ModalEdit #bmi').val(event.bmi);
          $('#ModalEdit').modal('show');
        });
      },


      eventDrop: function(event, delta, revertFunc) { // si changement de position

        edit(event);

      },
      eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

        edit(event);

      },
      events: [
      <?php foreach($events as $event): 
      
        $start = explode(" ", $event['start']);
        $end = explode(" ", $event['end']);
        if($start[1] == '00:00:00'){
          $start = $start[0];
        }else{
          $start = $event['start'];
        }
        if($end[1] == '00:00:00'){
          $end = $end[0];
        }else{
          $end = $event['end'];
        }
      ?>
        {
          id: '<?php echo $event[id]; ?>',
          title: '<?php echo $event[title]; ?>',
          start: '<?php echo $start; ?>',
          end: '<?php echo $end; ?>',
          // color: '<?php echo $event[color]; ?>',
          patient_name: '<?php echo $event[patient_name]; ?>',
          mobile: '<?php echo $event[mobile]; ?>',
          photo: '<?php echo $event[photo]; ?>',
          height: '<?php echo $event[height]; ?>',
          pulse: '<?php echo $event[pulse]; ?>',
          weight: '<?php echo $event[weight]; ?>',
          bp: '<?php echo $event[bp]; ?>',
          sugar: '<?php echo $event[sugar]; ?>',
          bmi: '<?php echo $event[bmi]; ?>'
        },
      <?php endforeach; ?>
      ]
    });
    
    function edit(event){
      start = event.start.format('YYYY-MM-DD HH:mm:ss');
      if(event.end){
        end = event.end.format('YYYY-MM-DD HH:mm:ss');
      }else{
        end = start;
      }
      
      id =  event.id;
      
      Event = [];
      Event[0] = id;
      Event[1] = start;
      Event[2] = end;
      
      $.ajax({
       url: 'editEventDate.php',
       type: "POST",
       data: {Event:Event},
       success: function(rep) {
          if(rep == 'OK'){
            alert('Saved');
          }else{
            alert('Could not be saved. try again.'); 
          }
        }
      });
    }
    
  });
  
</script>
           
          </div>
                
        </section>
      </div>
    </div>
            
</body>

</html>
