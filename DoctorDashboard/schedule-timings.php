<?php 
include('../connection/conn.php');
include('session_check.php');
 //$idadmin = $_SESSION['admin']['id'];
 $did= $_SESSION['doctor_details']['id'];
 // print_r($idadmin).exit();
$sql   = "select a.*,b.clinic_name from doctor_avalability as a, clinic_registration as b  where a.id_clinic=b.id and a.doctor = '$did'";

$result = $conn->query($sql);
$avalabilityList = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($avalabilityList, $row);
}
 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Doctor Schedule 
								<a href="add-availability.php" class="btn btn-primary btn-lg float-right">+ Add Doctor Schedule</a></h3>
								<br>
								
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
					                               <th>DAY</th>
					                               <th>Clinic Name</th>
					                               <th>Available From Time</th>
					                               <!-- <th>Tuesday To Time</th> -->
					                               <th> Available To Time</th>
					                               <th>Actions</th>

					                           </tr>
											</thead>
											<tbody>
												<?php foreach ($avalabilityList as $avalability) {
                        $avalabilityid = $avalability['id'];?>
                                   
                            <tr>
                                <td><?php echo strtoupper($avalability['day']); ?></td>
                                <td><?php echo strtoupper($avalability['clinic_name']); ?></td>
                                
                                <td><?php echo $avalability['from_time']; ?></td>
                                <td><?php echo $avalability['to_time']; ?></td>

                                <td><a href="add-availability.php?id=<?php echo $avalabilityid;?>" class="btn bg-info-light pencil" title="Edit"><i class='fa fa-edit'></i></a>
                                <a href="javascript:deleterole(<?php echo $avalabilityid;?>)" class="btn bg-danger-light trash" title="Delete"> <i class='far fa-trash-alt'></i></a></td>
                            </tr>
                        <?php }?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		<script type="text/javascript">

    function deleterole(avalabilityid) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true) {
            parent.location="delete_doctor_availability.php?id="+avalabilityid;
        }
    }
</script>
		
    </body>
</html>