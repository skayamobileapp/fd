<?php 
include('../connection/conn.php');
include('session_check.php');
 //$idadmin = $_SESSION['admin']['id'];
 // print_r($idadmin).exit();
date_default_timezone_set("Asia/Kolkata");

if($_POST)
{
  $did = $_SESSION['doctor_details']['id'];
  $fdate = date("Y-m-d", strtotime($_POST['fdate']));

  $endDate = trim($_POST['tdate']);
$startDateArray = explode('/',$endDate);
$mysqlStartDate = $startDateArray[2]."-".$startDateArray[1]."-".$startDateArray[0];
$endDate = $mysqlStartDate;

  $tdate = date("Y-m-d", strtotime($_POST['tdate']));

  $sql="SELECT * FROM payment_table WHERE date(payment_date) >='$fdate' AND date(payment_date) <='$endDate' AND doctor_id='$did'";
  $query = mysqli_query($conn,$sql);

}

 
?>
<!DOCTYPE html> 
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0">Patient Bill Summary</h4>
                  
								</div>
								<div class="card-body">
								<form action="" method="POST" id="form">
									<div class="row">
                  <div class="col-sm-5">
                    <div class="form-group">
                        <label>From Date:</label><span class="error"></span>
                        <div class="cal-icon">
                          <input type="text" name="fdate" class="form-control datetimepicker" placeholder="Select Date" id="fdate" autocomplete="off" value="<?php echo $_POST['fdate']; ?>">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-5">
                    <div class="form-group">
                      <label>To Date:</label><span class="error"></span>
                        <div class="cal-icon">
                          <input type="text" name="tdate" class="form-control datetimepicker" placeholder="Select Date" id="tdate" autocomplete="off" value="<?php echo $_POST['tdate']; ?>">
                        </div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      <label></label><div>
                        <input type="submit" name="getdata" class="btn btn-primary btn-lg" id="getdata" value="GET DATA">
                        </div>
                    </div>
                  </div>
                </div>
              </form>

              <?php
                    if (isset($query))
                        {
                            $viewdata =[];
                            $i=0;
                            while($row=mysqli_fetch_assoc($query))
                            {
                                $n =$i+1;
                                $viewdata[$i]['patient_name']=$row['patient_name'];
                                $viewdata[$i]['payment_date']=$row['payment_date'];
                                $viewdata[$i]['amount']=$row['amount'];
                                $viewdata[$i]['payment_mode']=$row['payment_mode'];
                                $i++;
                            } ?>
                            <h4>Patient Bill Summary List</h4>
                            <div class="table-responsive theme-table v-align-top">
                              <table class="table">
                                <thead>
                                <tr>
                                    <th>SL. NO</th>
                                    <th>Patient Name</th>
                                    <th>Payment Date</th>
                                    <th>Payment Mode</th>
                                    <th class="text-right">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $totalAmount = 0;
                                for ($i=0; $i<count($viewdata); $i++) { 
                                  $n=$i+1;
                                  $totalAmount = $totalAmount + $viewdata[$i]['amount']?>
                                    <tr>                                    
                                        <td><?php echo $n;?></td>
                                        <td><?php echo strtoupper($viewdata[$i]['patient_name']);?></td>
                                        <td><?php echo date('d M Y',strtotime($viewdata[$i]['payment_date']));?></td>
                                        <td><?php echo strtoupper($viewdata[$i]['payment_mode']); ?></td>
                                     <td class="text-right">₹ <?php echo $viewdata[$i]['amount'];?>.00</td>
                                    </tr>

                                    <?php
                                }
                                $sql="SELECT sum(amount) as total FROM payment_table WHERE payment_date >='$fdate' AND payment_date<='$tdate' ";
                                  $result = mysqli_query($conn,$sql);
                                  while ($rows=mysqli_fetch_array($result)) { ?>
                                  <tr>
                                    <td colspan="5" class="text-right">Total : ₹ <?php echo $totalAmount;?>.00
                                   
                                  </td>
                                </tr>
                        </tbody>
                    </table>
                  </div>
                                  <?php
                                }
                              }
                        ?>
									
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>

		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>
    <script>
  $( function() {
    $( "#fdate" ).datetimepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy'
    });
  });
  $( function() {
    $( "#tdate" ).datetimepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'dd-mm-yy'
    });
  });
  </script>
		
	</body>
</html>