<?php
include('../connection/conn.php');
include('session_check.php');
include('profile_completion.php');
include('monthly_payment_check.php');

date_default_timezone_set("Asia/Kolkata");

error_reporting(0);
$did = $_SESSION['doctor_details']['id'];

// if (isset($_POST['checkin'])) {
//   $indate = date("Y-m-d h:i:s");
//   $checkin = mysqli_query($conn,"INSERT INTO check_in(id_doctor,checked_in)VALUES('$did','$indate')");
//   if ($checkin) {
//     echo "<script>alert('Your Check-In Received Successfully')</script>";
//   }
// }

// if (isset($_POST['checkout'])) {
//   $outdate = date("Y-m-d h:i:s");
//   $checkout = mysqli_query($conn,"INSERT INTO check_out(id_doctor,checked_out)VALUES('$did','$outdate')");
//   if ($checkout) {
//     echo "<script>alert('Your Check-Out Received Successfully')</script>";
//   }
// }

$date = date("Y-m-d");
$sql = "Select p.*, e.id as eid, e.title, e.start from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)<='$date' and e.status='1' and e.doctor_id='$did' order by e.start ASC ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $totapp[$i]['id'] = $row['id'];
  $i++;
}

$sql = "Select distinct(p.id) from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)<='$date' and e.doctor_id='$did' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $totpat[$i]['id'] = $row['id'];
  $i++;
}

$date = date("Y-m-d");
$sql = "Select p.*, e.id as eid, e.title, e.start from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)='$date' and e.status='0' and e.doctor_id='$did' order by e.start ASC ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $patient[$i]['id'] = $row['id'];
  $patient[$i]['patient_name'] = $row['patient_name'];
  $patient[$i]['photo'] = '../uploads/'.$row['photo'];
  $patient[$i]['mobile_number'] = $row['mobile_number'];
  $patient[$i]['email'] = $row['email'];
  $patient[$i]['start'] = $row['start'];
  $patient[$i]['title'] = $row['title'];
  $patient[$i]['eid'] = $row['eid'];
  $i++;
}
$sql = "Select p.*, e.id as eid, e.title, e.start, e.amount from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)='$date' and e.status='1' and e.doctor_id='$did' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $success[$i]['id'] = $row['id'];
  $success[$i]['amount'] = $row['amount'];
  $success[$i]['patient_name'] = $row['patient_name'];
  $success[$i]['photo'] = '../uploads/'.$row['photo'];
  $success[$i]['mobile_number'] = $row['mobile_number'];
  $success[$i]['email'] = $row['email'];
  $success[$i]['title'] = $row['title'];
  $success[$i]['start'] = $row['start'];
  $success[$i]['eid'] = $row['eid'];
  $i++;
}

$sql = "Select p.*, e.id as eid, e.title, e.start, e.amount from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)>'$date' and e.status='0' and e.doctor_id='$did' ";
$result = mysqli_query($conn,$sql);
$i=0;
while ($row=mysqli_fetch_assoc($result)) {
  $coming[$i]['id'] = $row['id'];
  $coming[$i]['amount'] = $row['amount'];
  $coming[$i]['patient_name'] = $row['patient_name'];
  $coming[$i]['photo'] = '../uploads/'.$row['photo'];
  $coming[$i]['mobile_number'] = $row['mobile_number'];
  $coming[$i]['email'] = $row['email'];
  $coming[$i]['title'] = $row['title'];
  $coming[$i]['start'] = $row['start'];
  $coming[$i]['eid'] = $row['eid'];
  $i++;
}
?>
<!DOCTYPE html> 
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- Header -->
			<?php include('main-navbar.php'); ?>
			<!-- /Header -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						
						<div class="col-md-7 col-lg-8 col-xl-9">

							<div class="row">
								<div class="col-md-12">
									<div class="card dash-card">
										<div class="card-body">
											<div class="row">
												<div class="col-md-12 col-lg-4">
													<div class="dash-widget dct-border-rht">
														<div class="circle-bar circle-bar1">
															<div class="circle-graph1" data-percent="75">
																<img src="../assets/img/icon-01.png" class="img-fluid" alt="patient">
															</div>
														</div>
														<div class="dash-widget-info">
															<h6>Total Patient</h6>
															<h3><?php echo count($totpat);?></h3>
															<p class="text-muted">Till Today</p>
														</div>
													</div>
												</div>
												
												<div class="col-md-12 col-lg-4">
													<div class="dash-widget dct-border-rht">
														<div class="circle-bar circle-bar2">
															<div class="circle-graph2" data-percent="65">
																<img src="../assets/img/icon-02.png" class="img-fluid" alt="Patient">
															</div>
														</div>
														<div class="dash-widget-info">
															<h6>Today Patient</h6>
															<h3><?php echo count($patient);?></h3>
															<p class="text-muted"><?php echo date("d, M Y"); ?></p>
														</div>
													</div>
												</div>
												
												<div class="col-md-12 col-lg-4">
													<div class="dash-widget">
														<div class="circle-bar circle-bar3">
															<div class="circle-graph3" data-percent="50">
																<img src="../assets/img/icon-03.png" class="img-fluid" alt="Patient">
															</div>
														</div>
														<div class="dash-widget-info">
															<h6>Appoinments</h6>
															<h3><?php echo count($totapp);?></h3>
															<p class="text-muted">Till Today</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
		
		<div class="row">
			<div class="col-md-12">
				<h4 class="mb-4">Patient Appoinment</h4>
				<div class="appointment-tab">
				
					<!-- Appointment Tab -->
					<ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
						<li class="nav-item">
							<a class="nav-link" href="#today-appointments" data-toggle="tab">Today</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#completed-appointments" data-toggle="tab">Completed</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="#upcoming-appointments" data-toggle="tab">Upcoming</a>
						</li>
					</ul>
					<!-- /Appointment Tab -->
					
					<div class="tab-content">
					
					<!-- Today Appointment Tab -->
						<div class="tab-pane" id="today-appointments">
							<div class="card card-table mb-0">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>Sl. No</th>
													<th>Patient Name</th>
													<th>Appt Date</th>
													<th>Purpose</th>
													<th>Email</th>
													<th>Contact No.</th>
													<th>Add Prescriptions</th>
												</tr>
											</thead>
											<tbody>
												<?php
				                        for($i=0; $i<count($patient); $i++) {?>
												<tr>
													<td><?php echo $i+1; ?></td>
													<td>
														<h2 class="table-avatar">
															<!-- <a href="#" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="<?php echo $patient[$i]['photo']; ?>" alt=""></a> -->
															<?php echo $patient[$i]['patient_name']; ?>
														</h2>
													</td>
													<td><?php echo date("d M Y", strtotime($patient[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($patient[$i]['start'])); ?></span></td>
													<td><?php echo $patient[$i]['title']; ?></td>
													<td><?php echo $patient[$i]['email']; ?></td>
													<td><?php echo $patient[$i]['mobile_number']; ?></td>
													<td>
															<a href="add-prescriptions.php?id=<?php echo $patient[$i]['eid']; ?>" class="btn btn-primary btn-lg"> Add
															</a>
													</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>		
									</div>	
								</div>	
							</div>
						</div>
						<!-- /Today Appointment Tab -->

						<!-- Completed Appointment Tab -->
						<div class="tab-pane" id="completed-appointments">
							<div class="card card-table mb-0">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>SL. NO</th>
						                            <th>Patient Name</th>
						                            <th>Appt Date</th>
						                            <th>Purpose</th>
						                            <th>Contact No.</th>
						                            <th>Prescriptions</th>
						                            <th>Charge (Rs)</th>
						                            <th>Bill</th>
												</tr>
											</thead>
											<tbody>
												<?php
                         $total = 0;
                        for($i=0; $i<count($success); $i++) {
                            $pid = $success[$i]['id'];
                            $amount = $success[$i]['amount'];
                            $selectsql = mysqli_query($conn,"SELECT sum(amount) as tot FROM payment_table WHERE date(payment_date)='$date'");
                            while ($row1 = mysqli_fetch_assoc($selectsql)) {
                              $total = $row1['tot'];
                              } ?>
												<tr>
													<td><?php echo $i+1; ?></td>
													<td>
														<h2 class="table-avatar">
															<?php echo $success[$i]['patient_name']; ?>
														</h2>
													</td>
													<td><?php echo date("d M Y", strtotime($success[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($success[$i]['start'])); ?></span></td>
													<td><?php echo $success[$i]['title']; ?></td>
													<td><?php echo $success[$i]['mobile_number']; ?></td>
													<td><a href="view-prescription.php?id=<?php echo $success[$i]['eid']; ?>" class="btn btn-sm bg-info-light">
																<i class="far fa-eye"></i> View
															</a></td>
													<td class="text-center"><?php echo $success[$i]['amount']; ?> ₹/-</td>
													<?php
													$sql = "Select * from payment_table where event_id='".$success[$i]['eid']."' ";
                            $select = mysqli_query($conn,$sql);
                            $row = mysqli_fetch_array($select, MYSQLI_NUM);
                            if ($row[0] <1 )
                              {
                                echo "<td><a href='bill-payment.php?id=".$success[$i]['eid']."' class='btn btn-primary'>Payment</a></td>";
                              }
                              else
                              {
                                $select1 = "Select amount, balance_amount from payment_table where event_id='".$success[$i]['eid']."' AND date(payment_date)='$date' AND doctor_id='$did' order by id asc";
                                $result = mysqli_query($conn,$select1);
                                $j=0;
                                while ($row=mysqli_fetch_assoc($result)) {
                                  $pay[$j]['balance_amount'] = $row['balance_amount'];
                                  $pay[$j]['amount'] = $row['amount'];
                                  $j++;
                                }
                                for ($j=0; $j<count($pay); $j++) { 
                                  echo "<td>Paid: ".$pay[$j]['amount']." <br> (Balance:".$pay[$j]['balance_amount'].")</td>";
                                }
                              } ?>
													
												</tr>
											<?php } ?>
											<tr>
                               <td colspan='6' style="text-align:right;"> Todays Collection : </td>
                               <td><?php if(empty($total)){ echo "₹ 0.00/-"; } else { echo "₹".$total."/-"; }?></td>
                              </tr>
											</tbody>
										</table>		
									</div>
								</div>
							</div>
						</div>
						<!-- /Completed Appointment Tab -->
				   
						<!-- Upcoming Appointment Tab -->
						<div class="tab-pane show active" id="upcoming-appointments">
							<div class="card card-table mb-0">
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-hover table-center mb-0">
											<thead>
												<tr>
													<th>SL. NO</th>
													<th>Patient Name</th>
													<th>Appt Date</th>
													<th>Purpose</th>
													<th>Email</th>
													<th>Contact No.</th>
												</tr>
											</thead>
											<tbody>
										<?php
				                        for($i=0; $i<count($coming); $i++) {?>
												<tr>
													<td><?php echo $i+1; ?></td>
													<td>
														<h2 class="table-avatar">
															<?php echo $coming[$i]['patient_name']; ?>
														</h2>
													</td>
													<td><?php echo date("d M Y", strtotime($coming[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($coming[$i]['start'])); ?></span></td>
													<td><?php echo $coming[$i]['title']; ?></td>
													<td><?php echo $coming[$i]['email']; ?></td>
													<td ><?php echo $coming[$i]['mobile_number']; ?></td>
													<!-- <td class="text-right">
														<div class="table-action">
															<a href="javascript:void(0);" class="btn btn-sm bg-success-light">
																<i class="fas fa-check"></i> Accept
															</a>
															<a href="javascript:void(0);" class="btn btn-sm bg-danger-light">
																<i class="fas fa-times"></i> Cancel
															</a>
														</div>
													</td> -->
												</tr>
											<?php } ?>
											</tbody>
										</table>		
									</div>	
								</div>	
							</div>	
						</div>
						<!-- /Upcoming Appointment Tab -->
						
					</div>
				</div>
			</div>
		</div>

						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->

		<p><a href="#" data-toggle="modal" data-target="#myModal6" data-backdrop="static" data-keyboard="false"></a></p>
    <div  id="popup">
      <div class="modal fade" id="myModal6" role="dialog">
        <div class="modal-dialog modal-sm">
           <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Congratulations,<br> You Registered Successfully. </h4>
            </div>
            <div class="modal-body">
                <div class="fg-line">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group text-center">
                        <img src="../fd_logo.png">
                        <h3>You Have Completed <?php echo $percentage."%"; ?>.</h3>
                        <h3>Please Complete Atleast 75% Of Profile.</h3>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
				<?php if($percentage >= 75){
				echo "<a href='#' class='btn btn-primary' id='ok' name='ok' data-dismiss='modal'>Ok</a>";
			}else{ ?>
            <a href="update_doctor_details.php?id=<?php echo $did; ?>" class="btn btn-primary" id="accept" name="accept">Ok</a> <?php } ?>
           </div>
          </div>
      </div>
    </div>
  </div>

  <!-- approval status -->
  <p><a href="#" data-toggle="modal" data-target="#myModal7" data-backdrop="static" data-keyboard="false"></a></p>
    <div  id="popup">
      <div class="modal fade" id="myModal7" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 500px;">
           <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;"> </h4>
            </div>
            <div class="modal-body">
                <div class="fg-line">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group text-center">
                        <img src="../fd_logo.png">
                        <h3>Your profile has not be approved from FirstDoctor Team.</h3>
                        <h4>Please contact us for furthur details.
                          <br><br>Ph: 8070606026 <br> Email: firstdoctor@gmail.com</h4>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
        <?php if($doctor_status == '0'){ ?>
            <a href="doctor-profile.php?id=<?php echo $did; ?>" class="btn btn-primary btn-lg" id="accept" name="accept">Ok</a> <?php } ?>
           </div>
          </div>
      </div>
    </div>
  </div>

<?php 

$popup_result = mysqli_query($conn, "SELECT view FROM modal_popup WHERE doctor_id='$did'");
while($row = $popup_result->fetch_assoc()){
  $log = $row['view'];
}
  $chklp =mysqli_num_rows($popup_result);

if($chklp == 0 ){
  mysqli_query($conn, "INSERT INTO modal_popup(view, doctor_id) VALUES('0', '$did')");
}
// echo $payment_check;
 // echo $totpay = $previous_balance_amount + $history_balance_amount;
 // if($log == '0' && $payment_check < 1 && $total_payable >= 1 && $previousDate != $currentDate && $historyDate != $currentDate){ ?>
    <!-- Payment status -->
  <p><a href="#" data-toggle="modal" data-target="#myModal8" data-backdrop="static" data-keyboard="false"></a></p>
    <div  id="popup">
      <div class="modal fade" id="myModal8" role="dialog">
        <div class="modal-dialog modal-sm" style="width: 500px;">
           <!-- Modal content -->
          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;"> </h4>
            </div>
            <div class="modal-body">
                <div class="fg-line">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group text-center">
                        <img src="../fd_logo.png">
                        <h3>Your Outstanding Payment Bill has been generated.</h3>
                        <h4>Bill Amount = (₹) <?php echo $total_payable; ?>.00 /-
                          <br><br>Last date for payment : <?php echo date("d M Y", strtotime($lastdate)); ?></h4>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="modal-footer">
        <?php 
        
        if ($payment_check < 1 && $currentDate > $previousDate && $currentDate > $historyDate)
            { ?>
            <a href="outstanding-bill-payment.php" class="btn btn-primary btn-lg" id="accept" name="accept">Pay</a> 
          <?php }
          else
        if($payment_check < 1 && $date >= $firstdate && $date <= $lastdate){

          mysqli_query($conn, "UPDATE modal_popup SET view='1' WHERE doctor_id='$did'");

            echo "<a href='#' class='btn btn-primary btn-lg' id='ok' name='ok' data-dismiss='modal'>Ok</a>";
            echo "<a href='pay_outstanding_balance.php' class='btn btn-primary btn-lg' id='accept' name='accept'>Pay Now</a>";
            } ?>
            
           </div>
          </div>
      </div>
    </div>
  </div>
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script >
    $(document).ready(function(){
      var per = <?php echo $percentage; ?>;
      if (parseInt(per) >= 75) {
        $('#myModal6').hide();
      }else {
      $('#myModal6').modal({
    backdrop: 'static',
    keyboard: true
    });
    }

      var doctor_status = '<?php echo $doctor_status; ?>';

    if (doctor_status == '1') {
        $('#myModal7').hide();
      }else {
      $('#myModal7').modal({
    backdrop: 'static',
    keyboard: true
    });
    }

    var payment_check = '<?php echo $payment_check; ?>';
    var current_date = '<?php echo $currentDate; ?>';
    var history_date = '<?php echo $historyDate; ?>';

    if (payment_check < 1 && current_date != history_date) {
          $('#myModal8').modal({
        backdrop: 'static',
        keyboard: true
        });
      }else {
        $('#myModal8').hide();
    }
});
    </script>
		
	</body>
</html>