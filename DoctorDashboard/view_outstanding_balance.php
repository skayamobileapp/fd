<?php 
include('../connection/conn.php');
include('session_check.php');

$id = $_GET['id'];
$did = $_SESSION['doctor_details']['id'];

  $sql="SELECT d.date_time, d.amount, e.start, p.patient_name FROM doctor_outstanding_balance d INNER JOIN events e ON d.id_event=e.id INNER JOIN patient_details p ON e.patient_id=p.id WHERE d.id_doctor='$did' AND date(d.date_time)='$id'";
  $query = mysqli_query($conn,$sql);
  $viewdata =[];
  $i=0;
  while($row=mysqli_fetch_assoc($query))
  {
      $n =$i+1;
      $viewdata[$i]['date_time']=$row['date_time'];
      $viewdata[$i]['amount']=$row['amount'];
      $viewdata[$i]['patient_name']=$row['patient_name'];
      $viewdata[$i]['start']=$row['start'];
      $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../css/jquery-ui.css" rel="stylesheet">
    <link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">
    
</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
            
        <section class="col-sm-8 col-lg-9">    
            <div class="main-container">
              <h3>Out Standing Balance <a href="doctor_outstanding_balance.php" class="btn btn-primary btn-lg pull-right"> Back </a></h3><br>
              <p></p>
                <div class="table-responsive theme-table v-align-top">
                  <table class="table">
                  <thead>
                    <tr>
                      <th>SL. NO</th>
                      <th>Patient Name</th>
                      <th>Appointment Date</th>
                      <th>Payment Date</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    for ($i=0; $i<count($viewdata); $i++) {
                      $n=$i+1; ?>
                        <tr>                             
                            <td><?php echo $n;?></td>
                            <td><?php echo $viewdata[$i]['patient_name']; ?></td>
                            <td><?php echo date('d M Y h:i a',strtotime($viewdata[$i]['start'])); ?></td>
                            <td><?php echo date('d M Y H:i:a',strtotime($viewdata[$i]['date_time']));?></td>
                            <td><?php echo $viewdata[$i]['amount']; ?></td>
                        </tr>

                        <?php
                    }
                    $sql="SELECT sum(amount) as total FROM doctor_outstanding_balance WHERE date(date_time) ='$id' ";
                      $result = mysqli_query($conn,$sql);
                      while ($rows=mysqli_fetch_array($result)) { ?>
                      <tr>
                        <td colspan="5" class="text-right">Total : ₹ <?php echo $rows['total'];?>.00
                       
                      </td>
                    </tr>
                                  <?php
                                }
                        ?>              
                        </tbody>
                    </table>
                  </div>
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

<script src="../js/jquery-3.3.1.js"></script>

<link rel="stylesheet" href="../css/jquery-ui.css">
  <script src="../js/jquery-1.12.4.js"></script>
  <script src="../js/jquery-ui.js"></script>
  <script>
  $( function() {
    $("#fdate").datepicker({ dateFormat: 'dd-mm-yy' });
  });
  $( function() {
    $("#tdate").datepicker({ dateFormat: 'dd-mm-yy' });
  });
  </script>
</body>

</html>