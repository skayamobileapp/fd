<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');

$did = $_SESSION['doctor_details']['id'];

$select = "SELECT e.*, c.city, c.state FROM events_list e inner join cities c on c.id = e.city where e.id_doctor='$did' and date(e.event_todatetime) > CURRENT_DATE order by e.event_fromdatetime ASC";

$resultcity = mysqli_query($conn,$select);

  $i=0;
  $events = array();

   while ($row=mysqli_fetch_assoc($resultcity))
  {
     $event_id = $row['id'];

     $row['interest_no'] = "0";
     
     $j = 0;

    $sqlGetNo = "SELECT * from event_customer where id_eventlist = '$event_id' ";

     $resultNo          = $conn->query($sqlGetNo);

    while ($row_interest = $resultNo->fetch_assoc())
    {
      //echo json_encode("gfgf");exit;
      if($row_interest['interest_no'] == null)
      {
        $row_interest['interest_no'] = 0; 
      }

      $j = $j + $row_interest['interest_no'];
    }
     
    $n = number_format($j, 0, '', ',');
     $row['interest_no'] = $n;
     
     
     $row['event_fromdatetime']   = date("d M Y", strtotime($row['event_fromdatetime']));
     $row['event_todatetime']   = date("d M Y", strtotime($row['event_todatetime']));
     
     $events[$i]['id'] = $row['id'];
     $events[$i]['event_name'] = $row['event_name'];
     $events[$i]['interest_no'] = $row['interest_no'];
     $events[$i]['event_desc'] = $row['event_desc'];
     $events[$i]['event_fromdatetime'] = $row['event_fromdatetime'] . " To <br> ". $row['event_todatetime'];
     $events[$i]['location'] = $row['location'] . ",<br> " . $row['city'];
     $events[$i]['filepath'] = $row['filepath'];
     $events[$i]['fd_colaboration'] = $row['fd_colaboration'];
     $i++;
  }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<!-- Page Wrapper -->
			<div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">

            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Events List <a href="add-events.php" class="btn btn-primary float-right btn-lg">+ Add Event</a></h3><br>

							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-hover table-center mb-0">
											<thead>
												<tr>
					                               <th>Event Name</th>
					                               <th>Event Date</th>
													<th>Address</th>
					                               <th>People Interested</th>
					                               <th>Fd <br>Collaboration</th>
					                               <th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php
            
								            for ($i=0; $i <count($events) ; $i++) {
								              ?>
								              <tr>
								                <td><?php echo strtoupper($events[$i]['event_name']); ?></td>
								                <!-- <td><?php echo $events[$i]['event_desc']; ?></td> -->
								                
								                <td><?php echo $events[$i]['event_fromdatetime']; ?></td>
								                <td><?php echo $events[$i]['location'] ; ?></td>
								                <td><a href="event-customers.php?id=<?php echo $events[$i]['id'];?> "><?php echo $events[$i]['interest_no']; ?> + INTERESTED</a></td>
								                <!-- <td><a href="event_customers.php?id=<?php echo $events[$i]['id'];?> ">View</a></td> -->
								                <td><?php if($events[$i]['fd_colaboration'] == 1){ echo "Yes"; } else{ echo "No"; } ?></td>
								                <td>
								                	<?php echo "<a class='btn bg-info-light pencil' href='add-events.php?id=". $events[$i]['id']."'><i class='fa fa-edit'></i></a>"; ?>
								                    <?php echo "<a class='btn bg-danger-light trash' onclick = 'javascript: delete_id($(this));return false;' href='delete_event_list.php?id=". $events[$i]['id']."'><i class='far fa-trash-alt'></i></a>"; ?>
								                </td>

								              </tr>
								              <?php

								            }
								            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>