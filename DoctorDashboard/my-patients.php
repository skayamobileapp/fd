<?php
include('../connection/conn.php');
include('session_check.php');
$did = $_SESSION['doctor_details']['id'];

$sql = "SELECT distinct(a.id), a.patient_name, a.mobile_number, a.email, a.address1 
        FROM patient_details as a,
        events as b 
        where a.id=b.patient_id 
        and b.doctor_id='$did' order by a.patient_name ASC";
$select = mysqli_query($conn,$sql);

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['patient_name'] = $row['patient_name'];
  $view[$i]['mobile_number'] = $row['mobile_number'];
  $view[$i]['email'] = $row['email'];
  $view[$i]['address1'] = substr($row['address1'], 0, 30).'...';
  $i++;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>

			<div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">
			
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Patient List 
								<a href="add-patient.php" class="btn btn-primary btn-lg float-right">+ Add Patient</a></h3><br>

								
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
													<th>Patient Name</th>
													<th>Mobile Number</th>
													<th>Email</th>
													<th>Address</th>
													<!-- <th>Appt Date</th> -->
													<!-- <th>Payment Status</th> -->
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php

            for ($i=0; $i <count($view) ; $i++) { 
              ?>
              <tr>
                <td><?php echo strtoupper($view[$i]['patient_name']); ?></td>
                <td><?php echo $view[$i]['mobile_number']; ?></td>
                <td><?php echo strtoupper($view[$i]['email']); ?></td>
                <td><?php echo $view[$i]['address1'] ; ?></td>
                <td>
                  <!-- <?php echo "<a class='action-link edit' href='patient_registration.php?id=". $view[$i]['id']."'></a>"; ?> -->

                    <?php echo "<a class='btn bg-danger-light trash' title='Delete' onclick = 'javascript: delete_id($(this));return false;' href='delete_patient.php?id=". $view[$i]['id']."'><i class='far fa-trash-alt'></i></a>"; ?>
                </td>

              </tr>
              <?php

            }
            ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>