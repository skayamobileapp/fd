<?php
include('../connection/conn.php');
include('session_check.php');

$id = $_GET['id'];
// $select1 = mysqli_query($conn,"SELECT * FROM patient_details WHERE id='$id' ");
// while ($row1 = mysqli_fetch_assoc($select1)) {
//   $patient_name = $row1['patient_name'];
//   $patientId = $row1['id'];
//   }

$select1 = mysqli_query($conn,"SELECT p.id as pid, p.patient_name, e.id, e.title, e.start, e.bmi FROM patient_details AS p INNER JOIN events AS e ON e.patient_id = p.id Where p.id='$id'");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $patient_name = $row1['patient_name'];
  $patientId = $row1['pid'];
  $patientBMI = $row1['bmi'];
  }

$did = $_SESSION['doctor_details']['id'];
$date = date("Y-m-d");
$select = mysqli_query($conn,"Select p.*, e.id as eid, e.title, e.start from patient_details as p INNER JOIN events as e on p.id=e.patient_id where date(e.start)<='$date' and e.status='1' and e.doctor_id='$did' and p.id='$id' order by eid desc");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['patient_name'] = $row['patient_name'];
  $view[$i]['email'] = $row['email'];
  $view[$i]['mobile_number'] = $row['mobile_number'];
  $view[$i]['start'] = $row['start'];
  $view[$i]['title'] = substr($row['title'], 0, 30).'...';
  $view[$i]['eid'] = $row['eid'];
  $view[$i]['photo'] = $row['photo'];
  $i++;
}

$sql ="SELECT * FROM med_certificate";
$result = mysqli_query($conn, $sql);
  $i = 0;
  $certList = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($certList, $row);
  }



?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Patient Appointment History &nbsp;&nbsp;<a href="print_history.php?id=<?php echo $_GET['id'];?>" class="btn btn-primary float-right btn-lg" style='margin-left:14px;'>Download Health Record</a>&nbsp;<a href="patients-summary.php" class="btn btn-primary float-right btn-lg">Back</a></h3><br>
								<div class="patient-summary-head">
						          <h4><?php echo "Patient Name : ". strtoupper($patient_name); ?> <span style="color: red;"><?php if (empty($patientBMI)) { echo "BMI : NA"; } else { echo "BMI : ". $patientBMI; } ?></span></h4>
						        </div>
								
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
													<th>SL. NO</th>
					                               <th>Patient Name</th>
					                               <th>Contact No</th>
					                               <th>Appt Purpose</th>
												   <th>Appt Date</th>
					                               <th>Prescriptions</th>
												</tr>
											</thead>
											<tbody>
												<?php

               for ($i=0; $i <count($view) ; $i++) {
                $n =$i+1;
                ?>
                <tr>
                <!-- <img src="../uploads/<?php echo $view[$i][photo]; ?>" width='100px' height="100px;" /> -->
                  <td><?php echo $n; ?></td>
                  <td title="<?php echo $img; ?>"><?php echo strtoupper($view[$i]['patient_name']) ; ?></td>
                  <td><a href="tel:<?php echo $view[$i]['mobile_number']; ?>"><?php echo $view[$i]['mobile_number']; ?></a></td>
                  <td><?php echo $view[$i]['title']; ?></td>
                  <td><?php echo date("d M Y", strtotime($view[$i]['start'])); ?> <span class="d-block text-info"><?php echo date("h.i a", strtotime($view[$i]['start'])); ?></span></td>
                 <td><?php echo "<a  class='btn btn-primary' href='view-prescriptions.php?id=". $view[$i]['eid']."&pid=".$view[$i]['id']."'> View</a>";?></td>

               </tr>
               <?php

             }
             ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>