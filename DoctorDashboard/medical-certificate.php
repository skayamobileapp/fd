<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from patient_details where id='$id' ";
    $result   = $conn->query($sql);
    $patients = $result->fetch_assoc();
   }

if ($_POST) {

  $id = $_GET['id'];
  $did = $_SESSION['doctor_details']['id'];
  
  $fromdate = date("Y-m-d", strtotime($_POST['fdate']));

  $endDate = trim($_POST['tdate']);
$startDateArray = explode('/',$endDate);
$mysqlStartDate = $startDateArray[2]."-".$startDateArray[1]."-".$startDateArray[0];
$endDate = $mysqlStartDate;

  $todate = date("Y-m-d", strtotime($_POST['tdate']));
  $descrip = $_POST['desc'];

  $sql= "INSERT INTO med_certificate(id_doctor, id_patient, from_date, to_date, description) VALUES('$did', '$id', '$fromdate', '$endDate', '$descrip')";
  $result = $conn->query($sql);
  if ($result) {
    $lastid = mysqli_insert_id($conn);
    // echo "<script>parent.location='medical_certificate_pdf.php?id=$lastid&pid=$id'</script>";
  echo "<script>alert('Successfully Generated and Downloaded Medical Certificate')</script>";

?>

  <script>
    window.open('medical_certificate_pdf.php?id=<?php echo $lastid; ?>&pid=<?php echo $id; ?>');
    </script>
  <?php
  // echo "<script>alert('fdfsd')</script>";

  echo "<script>parent.location='patients-summary.php'</script>";
  }
}


?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">

		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="patients-summmary.php">Patients Summary</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Create Medical Certificate</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Medical Certificate For Patient :</font>&nbsp; &nbsp; <b class="text-info"><?php echo strtoupper($patients['patient_name']); ?></b></h4>
									<div class="row form-row">
										<div class="col-md-6">
											<div class="form-group">
												<label> From Date <span class="text-danger"> *</span></label>
                        <div class="cal-icon">
  												<input type="text" name="fdate" id="fdate" class="form-control datetimepicker" autocomplete="off" placeholder="Select From Date">
                        </div>
											</div>
										</div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label> To Date <span class="text-danger"> *</span></label>
                        <div class="cal-icon">
                          <input type="text" name="tdate" id="tdate" class="form-control datetimepicker" autocomplete="off" placeholder="Select To Date">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label> Description <span class="text-danger"> *</span></label>
                        <input type="text" name="desc" class="form-control" id="desc" autocomplete="off" placeholder="Enter Description">
                      </div>
                    </div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="patients-summary.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- DatePicker JS -->
		
		<script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              fdate : "required",
              tdate : "required",
              desc : "required"
         
            },
            messages : {
               
                fdate : "<span> select from date</span>",
                tdate : "<span> select to date</span>",
                desc : "<span> enter description</span>"
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

		 <script type="text/javascript">
$( document ).ready(function() {

	 var idstate = '<?php  echo $clinicDetails['state_id'];?>';
	 if(idstate!='' && idstate!= null) {
		getCities();
	 }
    // getCities();
    // getspecialty();
});

        function getCities(){
          var id = $("#state_id").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#city_id").html(result);
              var idcityselected = '<?php  echo $clinicDetails['city_id'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#city_id").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

       

        function getPincode(){
          var id = $("#city_id").val();
          console.log(id);

          $.ajax({url: "getpincode.php?id="+id, success: function(result){
            $("#pincode").html(result);

             var pincodeselected = '<?php  echo $clinicDetails['pincode'];?>';
              if(pincodeselected!='' && pincodeselected!= null) {
                 $("#pincode").val(pincodeselected);
              }
          }
        });
          
        }
    </script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>