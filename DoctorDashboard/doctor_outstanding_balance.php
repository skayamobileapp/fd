<?php 
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');
 //$idadmin = $_SESSION['admin']['id'];
 // print_r($idadmin).exit();
$current_month = date('M-Y');

$did = $_SESSION['doctor_details']['id'];

// $previous_month = date("M", strtotime("-1 month"));
// $previous_year = date("Y", strtotime("-1 month"));
// $history_month = date("M", strtotime("-2 month"));
// $history_year = date("Y", strtotime("-2 month"));

   $discount_amount = '0';
    $discount_percentage = '0';
    $discount_description = '';
    $after_discount_total = '0';
    $id_subscription = '0';

$sel_get_doctor_discount = "SELECT ds.* FROM `doctor_subscription` ds inner join doctor_details dd on ds.id = dd.id_subscription where dd.id = '$did' and ds.status = '1' order by dd.id DESC limit 0,1";
  $result_discount          = $conn->query($sel_get_doctor_discount);
  
//echo json_encode($sel_get_outstanding_total_balance);exit;

  while ($row_discount = $result_discount->fetch_assoc())
  {
    $discount_percentage = $row_discount['final_packege_discount_percent'];
    $id_subscription = $row_discount['id'];
    $discount_description = $row_discount['discount_description'];
  }


$sql_select_month = "SELECT distinct(dob.month) as month FROM `doctor_outstanding_balance` dob  where dob.id_doctor = '$did'";

  $result_month          = $conn->query($sql_select_month);
  
//echo json_encode($sql_select_month);exit;
$y = array();
$i = 0;
$data_array = array();
while ($row_month = $result_month->fetch_assoc())
{
  $y[$i] = $row_month;

  $previous_date = $row_month['month'];
  
  $sel_get_outstanding_total_balance = "SELECT dob.* from `doctor_outstanding_balance` dob where dob.month = '$previous_date' and dob.id_doctor = '$did' and dob.status = '0' order by dob.id DESC limit 0,1";
    $result_total_sum          = $conn->query($sel_get_outstanding_total_balance);

  while ($row_total_sum = $result_total_sum->fetch_assoc())
  {
    //if($i == 1)
  //{
  //echo json_encode($data_array);exit;
  //}
    $data_variable['id'] = $row_total_sum['id'];
    $data_variable['total'] = $row_total_sum['total'];
    $data_variable['month'] = $row_total_sum['month'];
    
    if(isset($data_variable) && ($data_variable != null || $data_variable != ''))
    {
      array_push($data_array, $data_variable);
    }
  }
  $i++;
}
//echo json_encode($data_array);exit;
//echo json_encode($y);exit;
$history_balance_amount = $data_array[0]['total'];
$history_month = $data_array[0]['month'];

$previous_balance_amount = $data_array[1]['total'];
$previous_month = $data_array[1]['month'];

if($previous_month == null)
{
  $previous_month = '';
}
if($history_month == null)
{
  $history_month = '';
}
if($history_paid_amount == null)
{
  $history_paid_amount = '0';
}
if($history_balance_amount == null)
{
  $history_balance_amount = '0';
}
if($previous_paid_amount == null)
{
  $previous_paid_amount = '0';
}
if($previous_balance_amount == null)
{
  $previous_balance_amount = '0';
}

if($discount_description == '' || $discount_description == '0')
{
  $discount_description = 'Discount'; 
}

$minimum_payable = '0';
$total_payable = '0';
$isdiscount = '0';
$minimum_payable_after_discount = '0';

if(count($data_array) == 0) 
{
  $myArray['previous_total'] = '0';
  $myArray['previous_month'] = "";
  $myArray['previous_paid'] = "0";
  $myArray['previous_balance'] = "0";
  $myArray['history_total'] = "0";
  $myArray['history_month'] = '';
  $myArray['history_paid'] = "0";
  $myArray['history_balance'] = '0';

  $myArray['min_payable'] = "0";
  $myArray['total_payable'] = "0";
  $myArray['---'] = '---';
  $myArray['total_after_discount'] = '0';
  $myArray['discount_amount'] = $discount_amount;
  $myArray['discount_percentage'] = $discount_percentage;
  $myArray['minimum_payable_after_discount'] = $minimum_payable_after_discount;
  $myArray['discount_description'] = $discount_description;
  $myArray['isdiscount'] = $isdiscount;
  $myArray['pay_month'] = '';
  // echo json_encode($myArray);exit;
  
}

if($history_month == $current_month)
{
  $myArray['previous_total'] = '0';
  $myArray['previous_month'] = $previous_month;
  $myArray['previous_paid'] = $previous_paid_amount;
  $myArray['previous_balance'] = $previous_balance_amount;
  $myArray['history_total'] = $previous_balance_amount;
  $myArray['history_month'] = '';
  $myArray['history_paid'] = $history_paid_amount;
  $myArray['history_balance'] = '0';

  $myArray['min_payable'] = ceil($minimum_payable);
  $myArray['total_payable'] = $total_payable;
  $myArray['---'] = '---';
  $myArray['total_after_discount'] = $after_discount_total;
  $myArray['discount_amount'] = $discount_amount;
  $myArray['discount_percentage'] = $discount_percentage;
  $myArray['minimum_payable_after_discount'] = $minimum_payable_after_discount;
  $myArray['discount_description'] = $discount_description;
  $myArray['isdiscount'] = $isdiscount;
  $myArray['pay_month'] = $previous_month;

  // echo json_encode($myArray);exit;
  
  
}
//echo json_encode($data_array);exit;
if($previous_month == $current_month)
{
  $previous_month = $data_array[0]['month'];
  $previous_balance_amount = $history_balance_amount;
  $myArray['previous_total'] = $history_balance_amount;
  $history_balance_amount = '0';
  $history_month = '';
  
}
else
{
  $myArray['previous_total'] = $previous_balance_amount;
}

//echo json_encode($previous_balance_amount);exit;

//$total_payable = '888';

$total_payable = $previous_balance_amount + $history_balance_amount;


//$total_payable = '100';
//$discount_percentage = 8;

$total_payable_a = $total_payable; 

if($discount_percentage >= 1)
{
  $isdiscount = '1';
  $after_discount_total = ($total_payable * 0.01 * (100 - $discount_percentage));
  $discount_amount = $total_payable - $after_discount_total;
  $total_payable_a = $after_discount_total;
}
else  
{
  $discount_amount = $discount_amount;
  $after_discount_total = $total_payable;
  
}




$minimum_payable = ($total_payable * 0.01 * 75);
$minimum_payable_after_discount = ($total_payable_a * 0.01 * 75);

$pay_month = '';
if($previous_month != "" && $history_month != "")
{
  $pay_month = $history_month. ", ". $previous_month; 
}
if(($previous_month != "" && $history_month == ""))
{
  $pay_month = $previous_month;
}
if($previous_month == "" && $history_month != "")
{
  $pay_month = $history_month;
}


$myArray['previous_month'] = $previous_month;
$myArray['previous_paid'] = $previous_paid_amount;
$myArray['previous_balance'] = $previous_balance_amount;

$myArray['history_total'] = $history_balance_amount;
$myArray['history_month'] = $history_month;
$myArray['history_paid'] = $history_paid_amount;
$myArray['history_balance'] = $history_balance_amount;

$myArray['min_payable'] = ceil($minimum_payable);
$myArray['total_payable'] = ceil($total_payable);
$myArray['---'] = '---';
$myArray['total_after_discount'] = ceil($after_discount_total);
$myArray['discount_amount'] = ceil($discount_amount);
$myArray['discount_percentage'] = $discount_percentage;
$myArray['minimum_payable_after_discount'] = ceil($minimum_payable_after_discount);
$myArray['discount_description'] = $discount_description;
$myArray['isdiscount'] = $isdiscount;
$myArray['id_subscription'] = $id_subscription;
$myArray['pay_month'] = $pay_month;

// echo json_encode($myArray);exit;

// $months =array(1=>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec');
//         $previous_month = $months[(int)$previous_month];
//         $history_month = $months[(int)$history_month];


if($_POST)
{
  $did = $_SESSION['doctor_details']['id'];
  $fdate = date("Y-m-d", strtotime($_POST['fdate']));
  $tdate = date("Y-m-d", strtotime($_POST['tdate']));

  $sql="SELECT id, max(date_time) as dt, month, max(total) as tot, sum(amount) as totamount, count(id_event) as appointNos FROM doctor_outstanding_balance WHERE date(date_time) >='$fdate' AND date(date_time) <='$tdate' AND id_doctor='$did' GROUP BY date(date_time) ORDER BY date_time ASC";
  $query = mysqli_query($conn,$sql);

}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../css/jquery-ui.css" rel="stylesheet">

<link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">
     
<style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
            
        <section class="col-sm-8 col-lg-9">
            <div class="main-container">
              <h3>Out Standing Balance</h3>
<?php $totpay = $previous_balance_amount + $history_balance_amount;
if($totpay >= 1 && $history_month != $current_month){ ?>
               <h4><a href="pay_outstanding_balance.php">Total Outstanding Balance(₹) : <?php echo $history_balance_amount +$previous_balance_amount.".00 (";

              // if ($history_month == '') {
              // $history_month = date('M-Y', strtotime ("-1 month",strtotime ($previous_month)));
              // }
                echo $history_month."=₹ ".$history_balance_amount.".00, &nbsp";
                echo $previous_month."=₹ ".$previous_balance_amount.".00)";
              ?></a></h4> <?php } ?>
              <div class="card">
                <form action="" method="POST" id="myform">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="fdate" class="form-control" placeholder="Select Date" id="fdate" autocomplete="off" value="<?php echo $_POST['fdate']; ?>">
                        <label class="fg-label">From Date:</label>
                        <span class="error"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group fg-float">
                      <div class="fg-line fg-toggled">
                        <input type="text" name="tdate" class="form-control" placeholder="Select Date" id="tdate" autocomplete="off" value="<?php echo $_POST['tdate']; ?>">
                        <label class="fg-label">To Date:</label>
                        <span class="error"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                        <input type="submit" name="getdata" class="btn btn-primary btn-lg" id="getdata" value="GET DATA">
                    </div>
                  </div>
                </div>
                    
              </form>
              </div>
<?php
                    if (isset($query))
                        {
                            $viewdata =[];
                            $i=0;
                            while($row=mysqli_fetch_assoc($query))
                            {
                                $n =$i+1;
                                $viewdata[$i]['dt']=$row['dt'];
                                $viewdata[$i]['month']=$row['month'];
                                $viewdata[$i]['tot']=$row['tot'];
                                $viewdata[$i]['totamount']=$row['totamount'];
                                $viewdata[$i]['appointNos']=$row['appointNos'];
                                $i++;
                            } ?>
                            <!-- <h4>Outstanding Balance</h4> -->
                            <div class="table-responsive theme-table v-align-top">
                              <table class="table" id="example">
                                <thead>
                                <tr>
                                    <th>SL. NO</th>
                                    <th>Date</th>
                                    <th>Number of Appointments</th>
                                    <th>Amount(₹)</th>
                                    <th>View</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i=0; $i<count($viewdata); $i++) {
                                  $n=$i+1; ?>
                                    <tr>                             
                                        <td><?php echo $n;?></td>
                                        <td><?php echo date('d M Y',strtotime($viewdata[$i]['dt']));?></td>
                                        <td><?php echo $viewdata[$i]['appointNos']; ?></td>
                                        <td><?php echo $viewdata[$i]['totamount']; ?></td>
                                       <td ><a href="view_outstanding_balance.php?id=<?php echo date("Y-m-d", strtotime($viewdata[$i]['dt']));?>"> View List </a></td>
                                    </tr>
                                    <?php
                                }
                                $sql="SELECT sum(amount) as total FROM doctor_outstanding_balance WHERE date(date_time) >='$fdate' AND date(date_time)<='$tdate' ";
                                  $result = mysqli_query($conn,$sql);
                                  while ($rows=mysqli_fetch_array($result)) { ?>
                                    <h4 class="pull-right" style="color: #02afee;">Total : ₹ <?php echo $rows['total'];?>.00</h4>
                        </tbody>
                    </table>
                  </div>
                                  <?php
                                }
                              }
                        ?>              
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

<script src="../js/jquery-3.3.1.js"></script>

<link rel="stylesheet" href="../css/jquery-ui.css">
  <script src="../js/jquery-1.12.4.js"></script>
  <script src="../js/jquery-ui.js"></script>
  <script>
  $( function() {
    $("#fdate").datepicker({ dateFormat: 'dd-mm-yy' });
  });
  $( function() {
    $("#tdate").datepicker({ dateFormat: 'dd-mm-yy' });
  });
  </script>
  <script src="../js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function() {
    $('#example').dataTable( {
    language: {
        searchPlaceholder: "Search by Date, Month"
    }
} );
} );
</script>
</body>

</html>