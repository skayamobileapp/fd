<?php
include('../connection/conn.php');
include('session_check.php');
$did = $_SESSION['doctor_details']['id'];
$date = date("Y-m-d");

$select = mysqli_query($conn,"Select i.id, i.item_name, i.company, ic.category from items as i inner join item_categories as ic on ic.id=i.item_category_id Where id_doctor='$did' AND i.status='1' ");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['item_name'] = $row['item_name'];
  $view[$i]['company'] = $row['company'];
  $view[$i]['category'] = $row['category'];
  $i++;
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">
			
			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Consumables List <a href="add-consumable.php" class="btn btn-primary float-right btn-lg">+ Add Consumable</a></h3>
								<br>
							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
					                               <th>Sl. No</th>
					                               <th>Category</th>
													<th>Consumable Name</th>
					                               <th>Company Name</th>
					                               <th>Actions</th>
												</tr>
											</thead>
											<tbody>
												<?php

               for ($i=0; $i <count($view) ; $i++) {
                $n =$i+1;
                ?>
                <tr>

                  <td><?php echo $n; ?></td>
                  <td><?php echo ucfirst($view[$i]['category']) ; ?></td>
                  <td><?php echo ucfirst($view[$i]['item_name']) ; ?></td>
                  <td><?php echo ucfirst($view[$i]['company']) ; ?></td>
                 <td>
                  <?php echo "<a class='btn bg-info-light pencil' title='Edit' href='add-consumable.php?id=". $view[$i]['id']."'><i class='fa fa-edit'></i></a>"; ?>

                    <?php echo "<a class='btn bg-danger-light trash' title='Delete' onclick = 'javascript: delete_id($(this));return false;' href='delete_item.php?id=". $view[$i]['id']."'><i class='fa fa-trash-alt'></i></a>"; ?>
                </td>

               </tr>
               <?php

             }
             ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>