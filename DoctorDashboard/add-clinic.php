<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$did = $_SESSION['doctor_details']['id'];

$clinicDetails=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from clinic_registration where id='$id' ";
    $result   = $conn->query($sql);
    $clinicDetails = $result->fetch_assoc();
   }

if($_POST)
{
  if($_GET['id'])
    {
      $id = $_GET['id'];
      $clinic_name = $_POST['clinic_name'];
      $mobile = $_POST['mobile'];
      $email = $_POST['email'];
      $state_id = $_POST['state_id'];
      $city_id = $_POST['city_id'];
      $address = $_POST['address'];
      $pincode = $_POST['postalcode'];
      $landmark = $_POST['landmark'];
      $id_doctor = $did;
      $Profile = $_FILES['photo']['name'];
      if($Profile =="")
    {
         $Profile = $clinicDetails['photo'];
    }
    else
    {
        $Profile = $_FILES['photo']['name'];
        $img_tmp =$_FILES['photo']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($Profile);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $Profile = 'ClinicPic_'. $date . '.'. $extension;

        $file_path = $upload_path . 'ClinicPic_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
    }

      $sql = "UPDATE clinic_registration SET clinic_name='$clinic_name', mobile='$mobile', email='$email', state_id='$state_id', city_id='$city_id', address='$address', pincode='$pincode', id_doctor='$id_doctor',  profile='$Profile',landmark='$landmark' WHERE id='$id' ";

      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Clinic details updated successfully');</script>";
        echo "<script>parent.location='my-clinics.php'</script>";
      }
    }
    else
    {

  $clinic_name = $_POST['clinic_name'];
  $mobile = $_POST['mobile'];
  $email = $_POST['email'];
  $state_id = $_POST['state_id'];
  $city_id = $_POST['city_id'];
  $address = $_POST['address'];
  $pincode = $_POST['pincode'];
  $landmark = $_POST['landmark'];
  $id_doctor = $did;
  $Profile = $_FILES['photo']['name'];
      if ($Profile=="") {
        $Profile = "default.png";
      }
      else
      {
        $Profile = $_FILES['photo']['name'];
        $img_tmp =$_FILES['photo']['tmp_name'];

        $server_ip = gethostbyname(gethostname());
        $upload_url = 'http://'.$server_ip.'/'.$upload_path; 

        $upload_path = '../uploads/';
        $fileinfo = pathinfo($Profile);
        $extension = $fileinfo['extension'];
        $file_name = $fileinfo['filename'];

        $file_url = $upload_url . $file_name . '.' . $extension;

        $Profile = 'ClinicPic_'. $date . '.'. $extension;

        $file_path = $upload_path . 'ClinicPic_'. $date . '.'. $extension; 

        move_uploaded_file($img_tmp,$file_path);
      }

    $sql = "INSERT INTO clinic_registration (clinic_name, mobile, email, state_id, city_id, address, pincode, landmark, id_doctor, profile, flag)VALUES('$clinic_name', '$mobile', '$email', '$state_id','$city_id','$address', '$pincode', '$landmark','$id_doctor', '$Profile', '1')";
    $insert = mysqli_query($conn,$sql);
      if ($insert)
      {
        echo "<script>alert('Clinic details updated successfully');</script>";
        echo "<script>parent.location='my-clinics.php'</script>";
      }
   }
}

$sql   = "select id, state from states";
$result = $conn->query($sql);
$stateList = array();
while ($row = $result->fetch_assoc()) {
    array_push($stateList, $row);
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="my-clinics.php">My Clinics</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Add Clinic</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Add Clinic</h4>
									<div class="row form-row">
										
										<div class="col-md-6">
											<div class="form-group">
												<label>Clinic Name <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="clinic_name"  maxlength="100" id="clinic_name" autocomplete="off" value="<?php echo $clinicDetails['clinic_name']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label> Mobile Number <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="mobile" id="mobile" maxlength="10" autocomplete="off" value="<?php echo $clinicDetails['mobile']; ?>" >
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Email Id <span class="text-danger">*</span></label>
												<input type="email" class="form-control" name="email"  maxlength="100" id="email" autocomplete="off" value="<?php echo $clinicDetails['email']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Address <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="address" autocomplete="off" value="<?php echo $clinicDetails['address']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Landmark<span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="landmark" autocomplete="off" value="<?php echo $clinicDetails['landmark']; ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>State <span class="text-danger">*</span></label>
                                    <select name="state_id" class="form-control selitemIcon" id="state_id" onchange="getCities()">
                                        <option value=''>Select</option>
                                        <?php
                                        for ($i=0; $i<count($stateList); $i++) {  ?>
                                        <option value="<?php echo $stateList[$i]['id']; ?>" <?php if( $stateList[$i]['id'] == $clinicDetails['state_id'])  { echo "selected=selected"; }
                                        ?>><?php echo $stateList[$i]['state']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>City <span class="text-danger">*</span></label>
												<select class="form-control selitemIcon" name="city_id" id="city_id">
			                                    </select>
											</div>
										</div>
										<!-- <div class="col-md-6">
											<div class="form-group">
												<label>Gender</label>
												<select class="form-control select">
													<option>Select</option>
													<option>Male</option>
													<option>Female</option>
												</select>
											</div>
										</div> -->
										<div class="col-md-6">
											<div class="form-group">
												<label>Pincode <span class="text-danger">*</span></label>
												<input type="text" class="form-control" name="postalcode" value="<?php echo $clinicDetails['pincode']; ?>" maxlength="6">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="my-clinics.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Dropzone JS -->
		<script src="../assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              clinic_name : {
                required : true,
                accept: true
            },
              email : "required",
              address : "required",
              landmark : "required",
              state_id : "required",
              city_id : "required",
              postalcode : {
                required : true,
                number : true
              },
                
                  mobile : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10
              }
         
            },
            messages : {

                clinic_name : {
               required : "<span> enter Clinic name</span>",
               accept : "<span> enter letters only</span>"
               },
               
                email : "<span> enter email Id</span>",
                address : "<span> enter address</span>",
                landmark : "<span> enter landmark</span>",
                state_id : "<span> select state</span>",
                city_id : "<span> select city</span>",
                postalcode : {
                  required : "<span> enter pincode </span>",
                  number : "<span> enter digits only </span>"
                },
            
                 mobile : {
                    required : "<span> enter mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

		 <script type="text/javascript">
$( document ).ready(function() {

	 var idstate = '<?php  echo $clinicDetails['state_id'];?>';
	 if(idstate!='' && idstate!= null) {
		getCities();
	 }
    // getCities();
    // getspecialty();
});

        function getCities(){
          var id = $("#state_id").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#city_id").html(result);
              var idcityselected = '<?php  echo $clinicDetails['city_id'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#city_id").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

       

        function getPincode(){
          var id = $("#city_id").val();
          console.log(id);

          $.ajax({url: "getpincode.php?id="+id, success: function(result){
            $("#pincode").html(result);

             var pincodeselected = '<?php  echo $clinicDetails['pincode'];?>';
              if(pincodeselected!='' && pincodeselected!= null) {
                 $("#pincode").val(pincodeselected);
              }
          }
        });
          
        }
    </script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>