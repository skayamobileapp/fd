<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$id = $_GET['id'];

$staff=[];
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from staff where id='$id' ";
    $result   = $conn->query($sql);
    $staff = $result->fetch_assoc();
   }

$sql      = "select * from roles";
$result   = $conn->query($sql);
$roles=array();
while($row=mysqli_fetch_assoc($result)){
  array_push($roles,$row);
}

if($_POST)
{
    if($_GET['id'])
    {
      $id = $_GET['id'];
      $name = $_POST['name'];
      $email = $_POST['email'];
      $mobile = $_POST['mobile'];
      $role = $_POST['role'];
      $address = $_POST['location'];
      $image = 'default.png';
// default.png
      $sql = "UPDATE staff SET name='$name', email='$email', mobile='$mobile', address='$address', role='$role' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Staff details updated successfully');</script>";
        echo "<script>parent.location='my-staff.php'</script>";
      }
    }
    else
    {
      $doc_id = $_SESSION['doctor_details']['id'];

      $name = $_POST['name'];
      $email = $_POST['email'];
      $mobile = $_POST['mobile'];
      $role = $_POST['role'];
      $address = $_POST['location'];
      $time= date('Y-m-d');
      $image = 'default.png';

    $sql = "INSERT INTO staff(name, email, password, mobile, address, role, created_by, created_time, photo) VALUES('$name','$email', '123456', '$mobile', '$address', '$role', '$doc_id','$time', '$image')";
      $result  = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Staff added successfully');</script>";
        echo "<script>parent.location='my-staff.php'</script>";
      }
    }
}
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Select2 CSS -->
		<link rel="stylesheet" href="../assets/plugins/select2/css/select2.min.css">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
		
		<link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">

		<link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
			<?php include('main-navbar.php'); ?>

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						<div class="col-md-7 col-lg-8 col-xl-9">
						<form action="" method="post" id="form" enctype="multipart/form-data">
							<!-- Basic Information -->
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="my-staff.php">My Staff</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Add Staff</li>
                </ol>
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Add Staff</h4>
									<div class="row form-row">
										
										<div class="col-md-6">
											<div class="form-group">
												<label>Staff Name <span class="text-danger">*</span></label>
												<input type="text" name="name" class="form-control" id="name" value="<?php echo $staff['name'];?>" autocomplete="off">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label> Email Id <span class="text-danger"></span></label>
												<input type="email" name="email" class="form-control" id="email" value="<?php echo $staff['email'];?>" autocomplete="off">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Mobile Number <span class="text-danger">*</span></label>
												<input type="text" name="mobile" class="form-control" id="mobile" value="<?php echo $staff['mobile'];?>" autocomplete="off" maxlength="10">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Staff Role <span class="text-danger">*</span></label>
												<select name="role" class="form-control selitemIcon" id="role">
                                  <option value=''>SELECT</option>
                                  <?php
                                  for($i=0; $i<count($roles); $i++) {
                                    // echo $roleid = $roles[$i]['role_code'];
                                    ?>
                                    <option value="<?php echo $roles[$i]['id']; ?>" <?php if($staff['role']==$roles[$i]['id']){echo "selected=selected"; } else { echo " "; }?>><?php echo $roles[$i]['role_code']; ?></option>
                                    <?php
                                  }
                                  ?>
                                </select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label>Address<span class="text-danger">*</span></label>
												<input type="text" name="location" id="location" class="form-control" value="<?php echo $staff['address'];?>" autocomplete="off" maxlength="250">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
	
							<div class="submit-section submit-btn-bottom float-right">
                <a href="my-staff.php" class="btn btn-light">Cancel</a>
								<button type="submit" class="btn btn-primary submit-btn">Save Changes</button>
							</div>
						</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="../assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Dropzone JS -->
		<script src="../assets/plugins/dropzone/dropzone.min.js"></script>
		
		<!-- Bootstrap Tagsinput JS -->
		<script src="../assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
		
		<!-- Profile Settings JS -->
		<script src="../assets/js/profile-settings.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script src="../assets/js/jquery-1.10.2.js"></script>
    <script src="../assets/js/jquery-ui.js"></script>
    <script src="../assets/js/jquery.validate.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              name : {
                required : true,
                accept: true
            },
              email : "required",
              role : "required",
              location : "required",
                
                  mobile : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                }
         
            },
            messages : {

                name : {
               required : "<span> enter Staff name</span>",
               accept : "<span> enter letters only</span>"
               },
               
                email : "<span> enter email Id</span>",
                role : "<span> Select staff role</span>",
                location : "<span> enter landmark</span>",
            
                 mobile : {
                    required : "<span> enter mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>

		 <script type="text/javascript">
$( document ).ready(function() {

	 var idstate = '<?php  echo $clinicDetails['state_id'];?>';
	 if(idstate!='' && idstate!= null) {
		getCities();
	 }
    // getCities();
    // getspecialty();
});

        function getCities(){
          var id = $("#state_id").val();
          console.log(id);

          $.ajax({url: "getcity.php?id="+id, success: function(result){
            $("#city_id").html(result);
              var idcityselected = '<?php  echo $clinicDetails['city_id'];?>';
              if(idcityselected!='' && idcityselected!= null) {
                 $("#city_id").val(idcityselected);
                 getPincode();
              }
          }
        });
        }

       

        function getPincode(){
          var id = $("#city_id").val();
          console.log(id);

          $.ajax({url: "getpincode.php?id="+id, success: function(result){
            $("#pincode").html(result);

             var pincodeselected = '<?php  echo $clinicDetails['pincode'];?>';
              if(pincodeselected!='' && pincodeselected!= null) {
                 $("#pincode").val(pincodeselected);
              }
          }
        });
          
        }
    </script>
   
    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
		
	</body>
</html>