<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

 $doctor_name = $_SESSION['doctor_details']['password'];
 $did = $_SESSION['doctor_details']['id'];

$avalability=[];
$avalability['to_time']='&nbsp;';
$avalability['from_time']='&nbsp;';

$sql = "SELECT * 
        FROM clinic_registration
        where id_doctor='$did' and flag='1'";
$select = mysqli_query($conn,$sql);

$i = 0;
$clinicName = array();
while ($row = mysqli_fetch_assoc($select)) {
  array_push($clinicName, $row);
}


if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from doctor_avalability where id='$id' ";
    $result   = $conn->query($sql);
    $avalability = $result->fetch_assoc();
   }

if($_POST)
{

      $Day = $_POST['day'];
       $FromTime = $_POST['from_time'];
      $ToTime = $_POST['to_time'];
      $clinicid = $_POST['id_clinic'];

    if($_GET['id'])
    {
      $id = $_GET['id'];

      $sql = "UPDATE doctor_avalability SET day='$Day', from_time='$FromTime', to_time='$ToTime', id_clinic='$clinicid' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Doctor availability details updated successfully');</script>";
        echo "<script>parent.location='schedule-timings.php'</script>";
      }
    }
    else
    {

      
      $sql = "INSERT INTO doctor_avalability(day, from_time, to_time, doctor,id_clinic) VALUES('$Day', '$FromTime', '$ToTime', '$did','$clinicid')";
      $result  = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Doctor availability added successfully');</script>";
        echo "<script>parent.location='schedule-timings.php'</script>";
      }
    }
}
?>
<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="../fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="../assets/css/style.css">
    
    <link rel="stylesheet" href="../assets/css/bootstrap-datetimepicker.min.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.min.js"></script>
      <script src="../assets/js/respond.min.js"></script>
    <![endif]-->
  
  </head>
  <body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
    <?php include('main-navbar.php'); ?>
      
      <!-- Page Content -->
      <div class="content">
        <div class="container-fluid">

          <div class="row">
            <?php include('sidebar.php'); ?>

            <div class="col-md-7 col-lg-8 col-xl-9">
              <div class="card">
                <div class="card-header">
                  <h4 class="card-title mb-0">Doctor Schedule Timings</h4>
                  <hr>
                  <form action="" method="POST" id="myform" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group">
                              <label >Day <span class="text-danger"> *</span></label>
                              <select class="form-control selitemIcon" name="day" id="day">
                                <option value=" ">Select Day</option>
                                <option value="monday" <?php if($avalability['day']=='monday'){ echo "selected";}?>>Monday</option>
                                <option value="tuesday" <?php if($avalability['day']=='tuesday'){ echo "selected";}?>>Tuesday</option>
                                <option value="wednesday" <?php if($avalability['day']=='wednesday'){ echo "selected";}?>>Wednesday</option>
                                <option value="thursday" <?php if($avalability['day']=='thursday'){ echo "selected";}?>>Thursday</option>
                                <option value="friday" <?php if($avalability['day']=='friday'){ echo "selected";}?>>Friday</option>
                                <option value="saturday" <?php if($avalability['day']=='saturday'){ echo "selected";}?>>Saturday</option>
                                <option value="sunday" <?php if($avalability['day']=='sunday'){ echo "selected";}?>>Sunday</option>
                              </select>
                          </div>                            
                        </div>

                        <div class="col-sm-3">
                          <div class="form-group">
                              <label>Clinic Name <span class="text-danger"> *</span></label>
                              <select class="form-control selitemIcon" name="id_clinic" id="id_clinic">
                  <option value="">SELECT CLINIC </option>

                                <?php for($i=0;$i<count($clinicName);$i++) { ?>
                              <option value="<?php echo $clinicName[$i]['id'];?>" <?php if($avalability['id_clinic']==$clinicName[$i]['id']){ echo "selected";}?>><?php echo $clinicName[$i]['clinic_name'];?></option>

                                <?php } ?>
                               
                              </select>
                          </div>                      
                        </div>

                       <div class="col-sm-3">
                          <div class="form-group">
                                <label>From Time<span class="text-danger"> *</span></label>
                                <input type="text" name="from_time" class="form-control timepicker" id="timepicker1" value="<?php echo $avalability['from_time']?>" autofill='false'/>
                          </div>
                        </div>  
                        <div class="col-sm-3">
                        <div class="form-group">
                                <label>To Time <span class="text-danger"> *</span></label>
                                <input type="text" name="to_time" class="form-control timepicker" id="timepicker2" style="cursor: text;" value="<?php echo $avalability['to_time']?>" />
                        </div>  
                    </div>
                    </div>
                     
                    <div class="bttn-group float-right">
                        <a href="schedule-timings.php" class="btn btn-light">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                        <!-- <a href="#" class="btn btn-primary btn-lg">Save</a> -->
                    </div>
                    </form>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>    
      <!-- /Page Content -->
       
    </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
    <script src="../assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="../assets/js/popper.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    
    <script src="../assets/js/moment.min.js"></script>
    <script src="../assets/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
    
    <!-- Custom JS -->
    <script src="../assets/js/script.js"></script>
    
  </body>
</html>