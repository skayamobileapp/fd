<?php
include('../connection/conn.php');
include('session_check.php');

date_default_timezone_set("Asia/Kolkata");
error_reporting(0);

  $balance = number_format($_GET['balance']);
  $did = $_GET['did'];
  $month = $_GET['month'];

  $sql = "SELECT date_time, amount, sum(amount) as totamount, count(id_event) as appointNos FROM doctor_outstanding_balance WHERE id_doctor='$did' AND month='$month' AND total <= $balance GROUP BY date(date_time) ORDER BY id";

$i = 0;
$viewList = array();
$result   = $conn->query($sql);
while ($row = mysqli_fetch_assoc($result)) {

  $viewList[$i]['date_time'] = date("d M Y", strtotime($row['date_time']));
  $viewList[$i]['amount'] = $row['amount'];
  $viewList[$i]['totamount'] = $row['totamount'];
  $viewList[$i]['appointNos'] = $row['appointNos'];
  $i++;
}
$sql="SELECT sum(amount) as total FROM doctor_outstanding_balance WHERE month ='$month' and id_doctor='$did' AND total <=$balance";
              $result = mysqli_query($conn,$sql);
              while ($rows=mysqli_fetch_array($result)) {
                $total = $rows['total'];
                }
                $totalA = $total-$balance;

$table =" ";

$table .="<table class='table' id='example'>
  <tr>
  <th style='text-align: center'>Date</th>
  <th style='text-align: center'>Appointments</th>
  <th style='text-align: center'>Total Amount(₹)</th>
</tr>";

 for($i=0; $i<count($viewList); $i++)
 {
    $table .= "<tr>
                <td>".$viewList[$i]['date_time']." </td>
                <td>".$viewList[$i]['appointNos']." </td>
                <td>".$viewList[$i]['totamount']." </td>
              </tr>";
}
$table .= "<tr>
                <td colspan='3' style='text-align: right; color: #2287de;'>Total Payable Amount (₹) ".$balance.".00 </td>
              </tr>";

        $table .="</table>";
           
echo $table;

?>
