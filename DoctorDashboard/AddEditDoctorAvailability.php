<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);

 $doctor_name = $_SESSION['doctor_details']['password'];
 $did = $_SESSION['doctor_details']['id'];

$avalability=[];
$avalability['to_time']='&nbsp;';
$avalability['from_time']='&nbsp;';

$sql = "SELECT * 
        FROM clinic_registration
        where id_doctor='$did' and flag='1'";
$select = mysqli_query($conn,$sql);

$i = 0;
$clinicName = array();
while ($row = mysqli_fetch_assoc($select)) {
  array_push($clinicName, $row);
}


if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from doctor_avalability where id='$id' ";
    $result   = $conn->query($sql);
    $avalability = $result->fetch_assoc();
   }

if($_POST)
{

      $Day = $_POST['day'];
       $FromTime = $_POST['from_time'];
      $ToTime = $_POST['to_time'];
      $clinicid = $_POST['id_clinic'];

    if($_GET['id'])
    {
      $id = $_GET['id'];

      $sql = "UPDATE doctor_avalability SET day='$Day', from_time='$FromTime', to_time='$ToTime', id_clinic='$clinicid' WHERE id='$id' ";
      $result = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Doctor availability details updated successfully');</script>";
        echo "<script>parent.location='view_doctor_availability.php'</script>";
      }
    }
    else
    {

      
      $sql = "INSERT INTO doctor_avalability(day, from_time, to_time, doctor,id_clinic) VALUES('$Day', '$FromTime', '$ToTime', '$did','$clinicid')";
      $result  = $conn->query($sql);
      if ($result)
      {
        echo "<script>alert('Doctor availability added successfully');</script>";
        echo "<script>parent.location='view_doctor_availability.php'</script>";
      }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="../select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom styles for time picker -->
    <link href="../library/TimePicker/mdtimepicker.css" rel="stylesheet" type="text/css">
    
</head>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
                <ol class="breadcrumb">
                  <li><a href="view_doctor_availability.php">Doctor's Clinic Schedule</a></li>
                  <li class="active"><?php if (!empty($avalability['id'])) { echo "Edit";} else { echo "Add"; } ?> Doctor's Clinic Schedule</li>
                </ol>              
               <h3 class="clearfix"><?php if (!empty($avalability['id'])) { echo "Edit";} else { echo "Add"; } ?> Doctor's Clinic Schedule</h3>               
                <div class="card">
                  <form action="" method="POST" id="myform" autocomplete="off">
                    <div class="row">
                        <div class="col-sm-3">
                          <div class="form-group fg-float">
                            <div class="fg-line fg-toggled">
                              <label class="">Day <span class="error"> *</span></label>
                              <select class="form-control selitemIcon" name="day" id="day">
                                <option value="<?php if (!empty($avalability['id'])) { echo $avalability['day'];} else { echo ""; } ?>"><?php if (!empty($avalability['id'])) { echo $avalability['day'];} else { echo "Select Day"; } ?></option>
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                                <option value="friday">Friday</option>
                                <option value="saturday">Saturday</option>
                                <option value="sunday">Sunday</option>
                              </select>
                            </div>
                          </div>                            
                        </div>

                        <div class="col-sm-3">
                          <div class="form-group fg-float">
                            <div class="fg-line fg-toggled">
                              <label class="">Clinic Name <span class="error"> *</span></label>
                              <select class="form-control selitemIcon" name="id_clinic" id="id_clinic">
								  <option value="">SELECT CLINIC </option>

                                <?php for($i=0;$i<count($clinicName);$i++) { ?>
  <option value="<?php echo $clinicName[$i]['id'];?>"><?php echo $clinicName[$i]['clinic_name'];?></option>

                                <?php } ?>
                               
                              </select>
                            </div>
                          </div>                            
                        </div>

                       <div class="col-sm-3">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" name="from_time" class="form-control timepicker" id="timepicker1" value="<?php echo $avalability['from_time']?>" autofill='falsel'/>
                                <label class="fg-label">From Time<span class="error"> *</span></label>
                            </div>
                        </div>
                        </div>  
                        <div class="col-sm-3">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                <input type="text" name="to_time" class="form-control timepicker" id="timepicker2" style="cursor: text;" value="<?php echo $avalability['to_time']?>" />
                                <label class="fg-label">To Time <span class="error"> *</span></label>
                            </div>
                        </div>  
                    </div>
                    </div>
                     
                    <div class="bttn-group">
                        <a href="view_doctor_availability.php" class="btn btn-link">Cancel</a>
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                        <!-- <a href="#" class="btn btn-primary btn-lg">Save</a> -->
                    </div>
                    </form>                                     
                </div>           
            </div>
        </section>
      </div>
    </div>

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>      
    <script src="../js/main.js"></script>

    <script src="../js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

     <script type="text/javascript">

   
    $(document).ready(function()
    {




      $('.timepicker').timepicker({
    timeFormat: 'HH:mm',
    interval: 10,
    minTime: '5:00',
    maxTime: '23:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});




         $("#myform").validate({
             rules : {

              day : "required",
              from_time : "required",
              to_time : "required"
            },
            messages : {

                day : "<span> SELECT DAY</span>",
                from_time : "<span> SELECT FROM TIME</span>",
                to_time : "<span> SELECT TO TIME</span>"
            }
        });
    });
</script>

 

    <script src="../select2/js/select2.js" ></script>
    <script src="../select2/js/select2-init.js" ></script>
</body>

</html>