<?php
include('../connection/conn.php');
include('session_check.php');
error_reporting(0);
$eventId = $_GET['id'];
$pid = $_GET['pid'];

$select1 = mysqli_query($conn,"SELECT p.id, p.patient_name, e.id, e.title, e.start, e.bmi FROM patient_details AS p INNER JOIN events AS e ON e.patient_id = p.id WHERE e.id='$eventId' ");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $patient_name = $row1['patient_name'];
  $patientId = $row1['patient_id'];
  $patientBMI = $row1['bmi'];
  }
$prescrip = mysqli_query($conn,"SELECT d.type, p.drug_name, p.drugname, p.duration, p.repeat_same, p.time_of_the_day, p.to_be_taken, p.patient_id, p.appointment_on FROM prescrip AS p INNER JOIN drug_type AS d ON p.drug_type=d.id WHERE p.event_id='$eventId'");
  $i=0;
  while ($row = mysqli_fetch_assoc($prescrip)) {
    $fetch[$i]['type'] = $row['type'];
    $fetch[$i]['drug_name'] = $row['drug_name'];
    if ($fetch[$i]['drug_name']=='Others') {
      $fetch[$i]['drug_name'] = ucfirst($row['drugname']);
    }
    $fetch[$i]['duration'] = $row['duration'];
    $fetch[$i]['repeat_same'] = $row['repeat_same'];
    $fetch[$i]['time_of_the_day'] = $row['time_of_the_day'];
    $fetch[$i]['to_be_taken'] = $row['to_be_taken'];
    $fetch[$i]['appointment_on'] = $row['appointment_on'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $lab = mysqli_query($conn,"SELECT lds.lab_name, ltn.test_name, lds.mobile, lds.email FROM labdetails ld INNER JOIN lab_details lds ON ld.lab_name=lds.id INNER JOIN lab_test_name ltn ON ld.test_name=ltn.id WHERE ld.event_id = '$eventId' ");

  $i=0;
  while ($row = mysqli_fetch_assoc($lab)) {
    $Lab[$i]['lab_name'] = $row['lab_name'];
    $Lab[$i]['test_name'] = $row['test_name'];
    $Lab[$i]['date'] = $row['date'];
    $Lab[$i]['email'] = $row['email'];
    $Lab[$i]['mobile'] = $row['mobile'];
    $i++;
  }

  $allergy = mysqli_query($conn,"SELECT a.*, atype.allergy_type AS altype, alist.allergy_name AS alname FROM event_allergies a INNER JOIN allergy_types atype ON atype.id=a.allergy_type INNER JOIN allergy_list alist ON a.allergy_name=alist.id WHERE a.event_id = '$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($allergy)) {
    $Allergy[$i]['altype'] = $row['altype'];
    $Allergy[$i]['alname'] = $row['alname'];
    $Allergy[$i]['date'] = $row['date'];
    $Allergy[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $vital = mysqli_query($conn,"SELECT * FROM vitals WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($vital)) {
    $vitals[$i]['height'] = $row['height'];
    $vitals[$i]['weight'] = $row['weight'];
    $vitals[$i]['bp'] = $row['bp'];
    $vitals[$i]['sugar'] = $row['sugar'];
    $vitals[$i]['blood'] = $row['blood'];
    $vitals[$i]['bmi'] = $row['bmi'];
    $i++;
  }

  $file = mysqli_query($conn,"SELECT * FROM fileUpload WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($file)) {
    $files[$i]['file_name'] = $row['file_name'];
    $files[$i]['uploaded_file'] = $row['uploaded_file'];
    $i++;
  }

  $referDoc = mysqli_query($conn,"SELECT d.doctor_name, d.mobile, d.email FROM doctor_details AS d INNER JOIN refer_table AS rt ON d.id=rt.refer_to WHERE event_id ='$eventId' AND is_other='0' ");
    $i=0;
if(empty($referDoc)){
  $referdoc = mysqli_query($conn,"SELECT doctor_name, doctor_mobile, doctor_address FROM refer_table WHERE event_id ='$eventId' AND is_other='1' ");
    $i=0;
  while ($row = mysqli_fetch_assoc($referDoc)) {
    $Refer[$i]['doctor_name'] = $row['doctor_name'];
    $Refer[$i]['mobile'] = $row['doctor_mobile'];
    $Refer[$i]['doctor_address'] = $row['doctor_address'];
    $i++;
  }
}
    while ($row = mysqli_fetch_assoc($referDoc)) {
    $Refer[$i]['doctor_name'] = $row['doctor_name'];
    $Refer[$i]['mobile'] = $row['mobile'];
    $Refer[$i]['email'] = $row['email'];
    $i++;
  }

  
  $complaint = mysqli_query($conn,"SELECT * FROM complaints WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($complaint)) {
    $complaints[$i]['complaint'] = $row['complaint'];
    $i++;
  }

  $observe = mysqli_query($conn,"SELECT * FROM observations WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($observe)) {
    $observes[$i]['obervation'] = $row['obervation'];
    $i++;
  }

  $diagnose = mysqli_query($conn,"SELECT * FROM diagnosis WHERE event_id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($diagnose)) {
    $diagnosis[$i]['diagnosis'] = $row['diagnosis'];
    $i++;
  }

  $amount = mysqli_query($conn,"SELECT * FROM events WHERE id='$eventId' ORDER BY id DESC LIMIT 1");
  $i=0;
  while ($row = mysqli_fetch_assoc($amount)) {
    $event[$i]['amount'] = $row['amount'];
    $i++;
  }



?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- Header -->
			<?php include('main-navbar.php'); ?>
			<!-- /Header -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
		
		<div class="row">
			<div class="col-md-12">
				<!-- <h4 class="mb-4">View Prescriptions</h4> -->
        <h3 class="clearfix">Prescription details &nbsp;&nbsp;<a href="print_prescription.php?id=<?php echo $_GET['id'];?>" class="btn btn-primary float-right btn-lg" style='margin-left:14px;'>Download Prescription</a>&nbsp; <a href="index.php" class="btn btn-primary float-right btn-lg">Back</a></h3><br>
        <div class="patient-summary-head col-md-12">
              <h4><?php echo "Patient Name : ". strtoupper($patient_name); ?> <span style="color: red;"><?php if (empty($patientBMI)) { echo "BMI : NA"; } else { echo "BMI : ". $patientBMI; } ?></span></h4>
            </div>
				<div class="appointment-tab">
				
					<!-- Prescriptions Tab -->
					<ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
						<li class="nav-item">
							<a class="nav-link active" href="#prescrip" data-toggle="tab">Prescriptions</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#lab" data-toggle="tab">Lab</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#allergy" data-toggle="tab">Allergies</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#vital" data-toggle="tab">Vitals</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#files" data-toggle="tab">Files</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#refer" data-toggle="tab">Refer doctor</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#complaints" data-toggle="tab">Complaints</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#observations" data-toggle="tab">Observations</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#diagnosis" data-toggle="tab">Diagnosis</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#amount" data-toggle="tab">Amount</a>
						</li>
					</ul>
					<!-- /Prescriptions Tab -->
					
					<div class="tab-content">
					
					<!-- Add Prescriptions Tab -->
						<div class="tab-pane show active" id="prescrip">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                      <thead>
                        <tr>
                          <th>SL. NO</th>
                            <th>Drug Type</th>
                            <th>Drug Name</th>
                            <th>Duration in Days</th>
                            <th>Repeat</th>
                            <th>Timings</th>
                            <th>Take</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        for($i=0; $i<count($fetch); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo $fetch[$i]['type']; ?></td>
                            <td><?php echo $fetch[$i]['drug_name']; ?></td>
                            <td class="text-center"><?php echo $fetch[$i]['duration']; ?></td>
                            <td><?php echo ucfirst($fetch[$i]['repeat_same']); ?></td>
                            <td><?php echo ucfirst($fetch[$i]['time_of_the_day']); ?></td>
                            <td><?php echo ucfirst($fetch[$i]['to_be_taken']); ?></td>
                          </tr>
                          <?php }
                          ?>
                      </tbody>
                    </table>    
                  </div>  
                </div>  
              </div>
						</div>

						<!-- Lab Tab -->
						<div class="tab-pane" id="lab">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                      <thead>
                        <tr>
                          <th>SL. NO</th>
                          <th>Lab Name</th>
                          <th>Test Name</th>
                          <th>Contact Number</th>
                          <th>Email Id</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        for($i=0; $i<count($Lab); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo strtoupper($Lab[$i]['lab_name']); ?></td>
                            <td><?php echo strtoupper($Lab[$i]['test_name']); ?></td>
                            <td><a href="tel:<?php echo $Lab[$i]['mobile']; ?>"><?php echo $Lab[$i]['mobile']; ?></a></td>
                            <td><a href="email:<?php echo $Lab[$i]['email']; ?>"><?php echo strtoupper($Lab[$i]['email']); ?></a></td>
                          </tr>
                          <?php }
                          ?>
                      </tbody>
                    </table>    
                  </div>  
                </div>  
              </div>
						</div>
						<!-- /Lab Tab -->
				   
						<!-- Allergies Tab -->
						<div class="tab-pane " id="allergy">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                      <thead>
                        <tr>
                          <th>SL. NO</th>
                          <th>Allergy Type</th>
                          <th>Allergy Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        for($i=0; $i<count($Allergy); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo $Allergy[$i]['altype']; ?></td>
                            <td><?php echo $Allergy[$i]['alname']; ?></td>
                          </tr>
                          <?php }
                          ?>
                      </tbody>
                    </table>    
                  </div>  
                </div>  
              </div>
						</div>
						<!-- /Allergies Tab -->

            <!-- Vitals Tab -->
            <div class="tab-pane " id="vital">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                      <thead>
                        <tr>
                          <th>Height in cm</th>
                          <th>Weight in Kg's</th>
                          <th>BP in kpa</th>
                          <th>Sugar in mmol/L</th>
                          <th>BMI Calculation</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        for($i=0; $i<count($vitals); $i++) {
                        ?>
                          <tr>
                            <td><?php echo $vitals[$i]['height']; ?> cm</td>
                            <td><?php echo $vitals[$i]['weight']; ?> kg</td>
                            <td><?php echo $vitals[$i]['bp']; ?></td>
                            <td><?php echo $vitals[$i]['sugar']; ?></td>
                            <td><?php echo $vitals[$i]['bmi']; ?></td>
                          </tr>
                          <?php }
                          ?>
                      </tbody>
                    </table>    
                  </div>  
                </div>  
              </div>
            </div>
            <!-- /Vitals Tab -->

            <!-- Files Tab -->
            <div class="tab-pane " id="files">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                      <thead>
                        <tr>
                          <th>SL. NO</th>
                          <th>File Name</th>
                          <th>File</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        for($i=0; $i<count($files); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo ucfirst($files[$i]['file_name']); ?></td>
                            <td><a href="../uploads/<?php echo $files[$i]['uploaded_file']; ?>" target="_blank"><?php echo $files[$i]['uploaded_file']; ?></a></td>
                          </tr>
                          <?php }
                          ?>
                      </tbody>
                    </table>    
                  </div>  
                </div>  
              </div>
            </div>
            <!-- /Files Tab -->

            <!-- Refer Doctor Tab -->
            <div class="tab-pane " id="refer">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover table-center mb-0">
                      <thead>
                        <tr><th>SL. NO</th>
                            <th>Doctor Name</th>
                            <th>Mobile</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                        for($i=0; $i<count($Refer); $i++) {
                          $n = $i+1;
                        ?>
                          <tr>
                            <td><?php echo $n; ?></td>
                            <td><?php echo strtoupper($Refer[$i]['doctor_name']); ?></td>
                            <td><a href="tel:<?php echo $Refer[$i]['mobile']; ?>"><?php echo $Refer[$i]['mobile']; ?></a></td>
                            <td><a href="email:<?php echo $Refer[$i]['email']; ?>"><?php echo strtoupper($Refer[$i]['email']); ?></a></td>
                          </tr>
                          <?php }
                          ?>
                      </tbody>
                    </table>    
                  </div>  
                </div>  
              </div>
            </div>
            <!-- /Refer Doctor Tab -->

            <!-- Complaints Tab -->
            <div class="tab-pane " id="complaints">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <i class="col-sm-3"> </i>
                  <div class="col-sm-6">
                          <?php
                        for($i=0; $i<count($complaints); $i++) {

                         echo $complaints[$i]['complaint'];
                        }
                          ?>  
                  </div>
                </div>  
              </div>
            </div>
            <!-- /Complaints Tab -->

            <div class="tab-pane " id="observations">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <i class="col-sm-3"> </i>
                  <div class="col-sm-6">
                          <?php
                        for($i=0; $i<count($observes); $i++) {

                         echo $observes[$i]['obervation'];
                        }
                          ?>  
                  </div>
                </div>  
              </div>
            </div>
            <!-- Observations Tab -->

            <div class="tab-pane " id="diagnosis">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <i class="col-sm-3"> </i>
                  <div class="col-sm-6">
                          <?php
                        for($i=0; $i<count($diagnosis); $i++) {

                         echo $diagnosis[$i]['diagnosis'];
                        }
                          ?>  
                  </div>
                  </div>  
                </div>  
              </div>
            <!-- /Observations Tab -->

            <div class="tab-pane " id="amount">
              <div class="card card-table mb-0">
                <div class="card-body">
                  <i class="col-sm-3"> </i>
                  <div class="col-sm-6">
                          <?php
                        for($i=0; $i<count($event); $i++) {

                         echo "Total Charge in INR  : ₹. <b style='font-size:16px'>". $event[$i]['amount']."</b>";
                        }
                          ?>  
                  </div>
                  </div>  
                </div>  
              </div>
            <!-- Diagnosis Tab -->
            
            <!-- /Diagnosis Tab -->						
					</div>

					
				</div>
			</div>
		</div>

						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
		   
		</div>
		<!-- /Main Wrapper -->

		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script type="text/javascript">

    function showtextBox() {
      $("#drugnameDiv").hide();

      var iddrug = $("#drugDose").val();
      if(iddrug =='Others') {
        $("#drugnameDiv").show();
      } else {
                $("#drugnameDiv").hide();

      }
    }
    $("#addPrescrip").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var dtype = $("#drugtype").val();
    if(dtype == 0) {
      alert("SELECT DRUG TYPE");
      return false;
    }

    var dose = $("#drugDose").val();
    if(dose == 0) {
      alert("SELECT DRUG NAME AND DOSAGE");
      return false;
    }
    var drugname = $("#drugname").val();
    var dose = $("#drugDose").val();
    if(dose== 'Others' && drugname=='') {
      alert("ENTER DRUG NAME");
      return false;
    }

    var dur = $("#duration").val();
    if(dur == '') {
      alert("ENTER DURATION");
      return false;
    }

    var repeat = $("input[name='repeat_same']:checked").val();
    var taken = $("input[name='to_be_taken']:checked").val();
    var time = [];
    $.each($("input[type='checkbox']:checked"), function(){
      time.push($(this).val());
    }); 
      var timing = time.join(",");
      $.ajax({
        // type: 'POST',
        url: 'addPrescription.php',
        data:{
          'drugType': dtype,
          'drugname':drugname,
          'drugDose': dose,
          'duration': dur,
          'taken': taken,
          'repeat': repeat,
          'time': timing,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
          $("#viewPrescrip").html(result);
             // $("#drugtype").val('0');
$("#drugtype").empty();//To reset cities
$("#drugtype").append('<?php for($i=0; $i<count($drugTypeList); $i++) { ?>' +
  '<option value="' +
  '<?php echo $drugTypeList[$i]['id']; ?>' +
  '">' +
  '<?php echo $drugTypeList[$i]['type']?>' +
  '</option>' +
  '<?php } ?>');
$("#drugDose").empty();
$("#drugDose").append('<option value="">SELECT DRUG NAME AND DOSAGE</option>');

        $("#duration").val(' ');
        $("#drugname").val(' ');
      $("#drugnameDiv").hide();
        // $("input[name='repeat_same']").val('');
        // $("input[name='to_be_taken']").val('');
      }
        });
      });
    </script>
    <script type="text/javascript">
    $("#addLab").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var labName = $("#lab_name").val();
    if(labName == '') {
      alert("SELECT LAB NAME");
      return false;
    }
    var testName = $("#test_name").val();
      $.ajax({
        // type: 'POST',
        url: 'add_labdetails.php',
        data:{
          'labName': labName,
          'testName': testName,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewLab").html(result);
        // $('#myModal2').close();
        $("#lab_name").empty();
        $("#lab_name").append('<option value="">SELECT LAB NAME</option>' 
          + '<?php for($i=0; $i<count($labList); $i++) { ?>' 
          + '<option value="' 
          + '<?php echo $labList[$i]['id']; ?>' +
            '">' 
          + '<?php echo $labList[$i]['lab_name']?>' +
            '</option>' +
            '<?php } ?>');
        $("#test_name").empty();
        $("#test_name").append('<option value="">SELECT TEST NAME</option>');
        // $("#lab_name").val('');
        // $("#test_name").val('');
      }
        });
      });
    </script>

    <script type="text/javascript">
    $("#addAllergy").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var allergy_type = $("#allergytype").val();
    if(allergy_type == '') {
      alert("SELECT ALLERGY TYPE");
      return false;
    }
    var allergy_name = $("#allergyname").val();
      $.ajax({
        // type: 'POST',
        url: 'add_allergies.php',
        data:{
          'allergy_type': allergy_type,
          'allergy_name': allergy_name,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewAllergy").html(result);
        // $("#myModal3").close();
        $("#allergytype").empty();
        $("#allergytype").append('<option value="">SELECT ALLERGY TYPE</option>' 
          + '<?php for($i=0; $i<count($allergyTypes); $i++) { ?>' 
          + '<option value="' 
          + '<?php echo $allergyTypes[$i]['id']; ?>' +
            '">' 
          + '<?php echo $allergyTypes[$i]['allergy_type']?>' +
            '</option>' +
            '<?php } ?>');
        $("#allergyname").empty();
        $("#allergyname").append('<option value="">SELECT ALLERGY NAME</option>');
        // $("#allergytype").val('');
        // $("#allergyname").val('');
      }
        });
      });
    </script>

<script type="text/javascript">
        $("#div2").hide();

      function getfirst(c1) {
        var cval = c1.value;
      if(cval =='1') {
        $("#itemdiv").show();
        $("#div2").hide();
      }
      else {
        $("#itemdiv").hide();
      }
    }
    function getnew(c2) {
        var cval = c2.value;
      if(cval =='2') {
        $("#div2").show();
        $("#itemdiv").hide();
      }
      else {
        $("#div2").hide();
        $("#itemdiv").show();
      }
    }
</script>
<script type="text/javascript">
    $("#weight").on('blur',function(){

        //BMI calculation
        var height = $("#height").val();
        var weight = $("#weight").val();
        var height = height/100;
        var height = height * height;
        var BMI = weight/height;
        var BMI = Math.round(BMI, 2);
        
        if (BMI < 15) {result = 'Very severely underweight'}
          else
        if (15 <= BMI && BMI<16) {result = 'Severely underweight'}
          else
        if (16 <= BMI && BMI<18.5) {result = 'Underweight'}
          else
        if (18.5 <= BMI && BMI<25) {result = 'Normal (healthy weight)'}
          else
        if (25 <= BMI && BMI<30) {result = 'Overweight'}
          else
        if (30 <= BMI && BMI<35) {result = 'Obese Class I (Moderately obese)'}
          else
        if (35 <= BMI && BMI<40) {result = 'Obese Class II (Severely obese)'}
          else
        if (BMI >=40) {result = 'Obese Class III (Very severely obese)'}
      
          $("#total").html("BMI : "+ BMI+" "+result);
          });
    </script>
  <script type="text/javascript">
    $("#referto").on('click',function(){
      var ch = $('input[name=choice]:checked').val();
    if (ch == '1') {
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var doctor = '<?php echo $_SESSION[doctor_details][id]; ?>';
    var referto = $("#doctor").val();
    if (referto == '') {
      alert('SELECT DOCTOR NAME');
      return false;
    }
    var refernote = $("#refer_note").val();
    // if (refernote == '') {
    //   alert('ENTER REFER NOTE');
    //   return false;
    // }
    var ch1 = $("#choice1").val();
    console.log(ch1);
      $.ajax({
        // type: 'POST',
        url: 'refer_doctor.php',
        data:{
          'referto': referto,
          'refernote': refernote,
          'doctor': doctor,
          'ch1': ch1,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewDoctor").html(result);
        // $("#myModal3").close();
        $("#doctor").val('');
      }
        });
      }

  if (ch == '2') {

    var ch2 = $("#choice2").val();
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var doctor = '<?php echo $_SESSION[doctor_details][id]; ?>';
    var dname = $("#doctor_name").val();
    if (dname == '') {
      alert('ENTER DOCTOR NAME');
      return false;
    }
    var phoneno = $("#phone_number").val();
    if (phoneno == '') {
      alert('ENTER DOCTOR PHONE NUMBER');
      return false;
    }
    var dadd = $("#doctor_address").val();
    if (dadd == '') {
      alert('ENTER DOCTOR ADDRESS');
      return false;
    }
    var refernote = $("#refer_note").val();
    // if (refernote == '') {
    //   alert('ENTER REFER NOTE');
    //   return false;
    // }
    console.log(ch2);
      $.ajax({
        // type: 'POST',
        url: 'refer_doctor.php',
        data:{
          'dname': dname,
          'refernote': refernote,
          'doctor': doctor,
          'ch2': ch2,
          'phoneno': phoneno,
          'dadd': dadd,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
        $("#viewDoctor").html(result);
        // $("#myModal3").close();
        $("#doctor_name").val('');
        $("#phone_number").val('');
        $("#doctor_address").val('');
        $("#refer_note").val('');
      }
        });
    }
});
</script>

    <script type="text/javascript">
      $(document).ready(function (e) {
          $('#addFile').on('click', function () {
              var eid = '<?php echo $EventId; ?>';
              var sid = '<?php echo $sessionid; ?>';
              var fileName = $("#file_name").val();
              if(fileName == ''){
                alert('ENTER FILE NAME');
                return false;
              }
              var file_data = $('#file').prop('files')[0];
              if(file_data == undefined){
                alert('UPLOAD FILE');
                return false;
              }
              var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('filename', fileName);
              form_data.append('sid', sid);
              form_data.append('eid', eid);

              $.ajax({
                  url: 'file_upload.php', // point to server-side PHP script 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#addfiles').html(response); // display success response from the PHP script
                      $("#file_name").val('');
                      $('#file').val('');
                  },
              });
          });
      });
    </script>

    <!-- <script type="text/javascript">
    $("#addFile").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var sid = '<?php echo $sessionid; ?>';
    var fileName = $("#file_name").val();
    var fileUploaded = $("input[type='file']").val();
      $.ajax({
        // type: 'POST',
        url: 'file_upload.php',
        data:{
          'fileName': fileName,
          'file': fileUploaded,
          'eid': eid,
          'sid': sid,
        },
        success: function(result){
         }
        });
    });

    </script> -->

    <script type="text/javascript">
    $("#addVitals").on('click',function(){
    var eid = '<?php echo $EventId; ?>';
    var height = $("#height").val();
    if(height == '') {
        alert("ENTER PATIENT HEIGHT");
        return false;
        }
    var weight = $("#weight").val();
    if(weight == '') {
        alert("ENTER PATIENT WEIGHT");
        return false;
    }
    var pulse = $("#pulse").val();
    if(pulse == '') {
        alert("ENTER PATIENT PULSE");
        return false;
    }
    var bp = $("#bp").val();
    if(bp == '') {
        alert("ENTER PATIENT BP LEVEL");
        return false;
    }
    var sugar = $("#sugar").val();
    if(sugar == '') {
        alert("ENTER PATIENT SUGAR LEVEL");
        return false;
    }
    // var blood = $("#blood").val();
      $.ajax({
        // type: 'POST',
        url: 'add_Vitals.php',
        data:{
          'height': height,
          'weight': weight,
          'pulse': pulse,
          'bp': bp,
          'sugar': sugar,
          // 'blood': blood,
          'eid': eid,
        },
        success: function(result){
        $("#viewVitals").html(result);
        $("#myModal5").close(); }
        });
    });

    </script>


 <script type="text/javascript">

        function getDosage(){
          var id = $("#drugtype").val();
          console.log(id);

          $.ajax({url: "getDosageList.php?id="+id, success: function(result){
            $("#drugDose").html(result);
          }
        });
        }

        function getAllergies(){
          var id = $("#allergytype").val();
          console.log(id);

          $.ajax({url: "getallergies.php?id="+id, success: function(result){
            $("#allergyname").html(result);
          }
        });
        }


        function getTests(){
          var id = $("#lab_name").val();
          console.log(id);

          $.ajax({url: "get_lab_tests.php?id="+id, success: function(result){
            $("#test_name").html(result);
          }
        });
        }

    </script>
		
	</body>
</html>