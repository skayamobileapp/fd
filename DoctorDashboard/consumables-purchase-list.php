<?php
include('../connection/conn.php');
include('session_check.php');
$did = $_SESSION['doctor_details']['id'];
$date = date("Y-m-d");

$select = mysqli_query($conn,"Select e.*, i.item_name from expense_item AS e INNER JOIN items AS i ON i.id=e.id_item Where e.id_doctor='$did' ");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['amount'] = $row['amount'];
  $view[$i]['item_name'] = $row['item_name'];
  $view[$i]['date'] = $row['date'];
  $view[$i]['approval_status'] = $row['approval_status'];
  $i++;
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- Datatables CSS -->
		<link rel="stylesheet" href="../admin/assets/plugins/datatables/datatables.min.css">
		
		<!--[if lt IE 9]>
			<script src="../admin/assets/js/html5shiv.min.js"></script>
			<script src="../admin/assets/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <style>

  .dataTables_filter input { width: 400px }
</style>
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>
    <body>
	
		<!-- Main Wrapper -->
        <div class="main-wrapper">
			<?php include('main-navbar.php'); ?>
		
		<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<?php include('sidebar.php'); ?>
			
			<div class="col-md-7 col-lg-8 col-xl-9 theiaStickySidebar">

			<!-- Page Wrapper -->
            <div class="page-wrapper">
                <div class="content container-fluid">

					<!-- Page Header -->
					<div class="page-header">
						<div class="row">
							<div class="col">
								<h3 class="page-title">Purchase Of Consumables List <a href="add-consumable-purchase.php" class="btn btn-primary float-right btn-lg">+ Add Purchase Of Consumable</a></h3><br>

							</div>
						</div>
					</div>
					<!-- /Page Header -->
					
					<div class="row">
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">

									<div class="table-responsive">
										<table class="datatable table table-stripped">
											<thead>
												<tr>
					                               <th>SL. NO</th>
													<th>Consumable Name</th>
					                               <th>Amount</th>
					                               <th>Date</th>
												</tr>
											</thead>
											<tbody>
												<?php

               for ($i=0; $i <count($view) ; $i++) {
                $n =$i+1;
                ?>
                <tr>
                <!-- <img src="../uploads/<?php echo $view[$i][photo]; ?>" width='100px' height="100px;" /> -->
                  <td><?php echo $n; ?></td>
                  <td><?php echo $view[$i]['item_name']; ?></td>
                  <td><?php echo $view[$i]['amount']; ?></td>
                  <td><?php echo date("d M Y", strtotime($view[$i]['date'])); ?></td>

                 <!-- <td><?php if ($view[$i]['approval_status']==1) { echo "Active"; } else{ echo "Inactive"; } ?></td>
                 <td>
                  <?php echo "<a class='action-link edit' href='add_expense_items.php?id=". $view[$i]['id']."'></a>"; ?>

                    <?php echo "<a class='action-link delete' onclick = 'javascript: delete_id($(this));return false;' href='delete_expense.php?id=". $view[$i]['id']."'></a>"; ?> 
                </td>-->

               </tr>
               <?php

             }
             ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				</div>			
			</div>
		</div>
			<!-- /Main Wrapper -->
		</div>
		
        </div>
    </div>
</div>
		<!-- /Main Wrapper -->
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="../assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Circle Progress JS -->
		<script src="../assets/js/circle-progress.min.js"></script>
		
		<!-- Custom JS -->
		<!-- <script src="../assets/js/script.js"></script> -->
		
		<!-- Slimscroll JS -->
        <script src="../admin/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
		
		<!-- Datatables JS -->
		<script src="../admin/assets/plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="../admin/assets/plugins/datatables/datatables.min.js"></script>
		
		<!-- Custom JS -->
		<script  src="../admin/assets/js/script.js"></script>
		
    </body>
</html>