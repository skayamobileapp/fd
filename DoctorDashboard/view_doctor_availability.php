<?php 
include('../connection/conn.php');
include('session_check.php');
 //$idadmin = $_SESSION['admin']['id'];
 $did= $_SESSION['doctor_details']['id'];
 // print_r($idadmin).exit();
$sql   = "select a.*,b.clinic_name from doctor_avalability as a, clinic_registration as b  where a.id_clinic=b.id and a.doctor = '$did'";

$result = $conn->query($sql);
$avalabilityList = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($avalabilityList, $row);
}
 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">

    <link href="../css/jquery-ui.css" rel="stylesheet">
    <link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">
    
</head>

<body>
<?php include('navbar.php'); ?>
    <div class="container-fluid main-wrapper">
      <div class="row">
         <?php include('menu.php'); ?>
        
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Doctor Availability<a href="AddEditDoctorAvailability.php" class="btn btn-primary pull-right btn-lg">+ Add Doctor Availability</a>
                            </h3> 
                <div class="table-responsive theme-table v-align-top">
                    <table class="table" id="avalability">
                       <thead>
                           <tr>
                               <th>DAY</th>
                               <th>Clinic Name</th>
                               <th>From</th>
                               <!-- <th>Tuesday To Time</th> -->
                               <th>To</th>
                               <th>Actions</th>

                           </tr>
                       </thead>
                       <tbody>
                        <?php foreach ($avalabilityList as $avalability) {
                        $avalabilityid = $avalability['id'];?>
                                   
                            <tr>
                                <td><?php echo strtoupper($avalability['day']); ?></td>
                                <td><?php echo strtoupper($avalability['clinic_name']); ?></td>
                                
                                <td><?php echo $avalability['from_time']; ?></td>
                                <td><?php echo $avalability['to_time']; ?></td>

                                <td><a href="AddEditDoctorAvailability.php?id=<?php echo $avalabilityid;?>" class="action-link edit" >Edit </a>
                                <a href="javascript:deleterole(<?php echo $avalabilityid;?>)" class="action-link delete" >Delete </a></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>                   
                </div>               
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>

<script src="../js/jquery-3.3.1.js"></script>
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.jqueryui.min.js"></script>

<script type="text/javascript">
       $(document).ready(function() {
        $('#avalability').DataTable();
    });

    function deleterole(avalabilityid) {
        var cnf = confirm("Do you really want to delete");
        if(cnf==true) {
            parent.location="delete_doctor_availability.php?id="+avalabilityid;
        }
    }
</script>
</body>

</html>