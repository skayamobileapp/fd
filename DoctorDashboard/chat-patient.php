<?php
include('../connection/conn.php');
include('session_check.php');


$did = $_SESSION['doctor_details']['id'];

$sql = "SELECT a.id, a.patient_name, a.mobile_number, a.email, a.photo  
        FROM patient_details as a,
        events as b 
        where a.id=b.patient_id 
        and b.doctor_id='$did' group by a.id";
$result = $conn->query($sql);
          $patientList = array();
          while ($row = $result->fetch_assoc()) {
          array_push($patientList, $row);
        }


?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="../fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="../assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="../assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="../assets/js/html5shiv.min.js"></script>
			<script src="../assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body class="">

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			
						<?php include('main-navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<?php include('sidebar.php'); ?>

						<div class="col-md-7 col-lg-8 col-xl-9">
							<br><br>
							<div class="chat-window">
							
								<!-- Chat Left -->
								<div class="chat-cont-left">
									<div class="chat-header">
										<span>Patient Chats</span>
									</div>
									<form class="chat-search">
										<div class="input-group">
											<div class="input-group-prepend">
												<i class="fas fa-search"></i>
											</div>
											<input type="text" class="form-control" placeholder="Search" name="search" id="search" onkeyup="getpatient()" autocomplete="off">
										</div>
									</form>
									<div class="chat-users-list">
										<div class="chat-scroll">
											<?php
		                            for($i=0; $i<count($patientList); $i++)
		                            {
		                              $count1=0;
		                              $notesql= "SELECT * FROM messages where doctor_id='$did' AND sent_by='patient' AND doctor_flag='0' AND patient_id='".$patientList[$i]['id']."' ";
		                                $result=mysqli_query($conn, $notesql);
		                                $count1=mysqli_num_rows($result);
		                              ?>
											<a href="#" class="media" onclick="get_prev(this);" id="<?php echo $patientList[$i]['id']; ?>">
												<div class="media-img-wrap">
													<div class="avatar avatar">
														<img src="../uploads/<?php echo $patientList[$i]['photo']; ?>" alt="" class="avatar-img rounded-circle">
													</div>
												</div>
												<div class="media-body">
													<div>
														<div class="user-name"><?php echo $patientList[$i]['patient_name']; ?></div>
														<div class="user-last-chat"><?php echo $patientList[$i]['mobile_number']; ?></div>
													</div>
													<div>
														<div class="last-chat-time block"> &nbsp;</div>
														<?php if($count1>0) { echo "
														<div class='badge badge-success badge-pill'>$count1</div>";
														} ?>
													</div>
												</div>
											</a>
										<?php } ?>
										</div>
									</div>
								</div>
								<!-- /Chat Left -->
                                <input type="hidden" name="patient_id" id="patient_id" value="">

								
								<!-- Chat Right -->
								<div class="chat-cont-right">

									<div id="previous"></div>
										
									<div class="chat-footer">
										<div class="input-group">
											<!-- <div class="input-group-prepend">
												<div class="btn-file btn">
													<i class="fa fa-paperclip"></i>
													<input type="file">
												</div>
											</div> -->
											<input type="text" class="input-msg-send form-control" placeholder="Type something" name="answer" id="answer">
											<div class="input-group-append">
												<button type="button" class="btn msg-send-btn" name="save" id="save"><i class="fab fa-telegram-plane"></i></button>
											</div>
										</div>
									</div>
								</div>
								<!-- /Chat Right -->
								
							</div>
						</div>
					</div>
					<!-- /Row -->

				</div>

			</div>		
			<!-- /Page Content -->
		</div>
		<!-- /Main Wrapper -->
		
		<!-- jQuery -->
		<script src="../assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="../assets/js/popper.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		
        <script src="../assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Custom JS -->
		<script src="../assets/js/script.js"></script>

		<script type="text/javascript">

        function get_prev(id){
          var pid = $(id).attr("id");
          var did = '<?php echo $did; ?>';
          console.log(did);
          console.log(pid);
          $("#patient_id").val(pid);

          $.ajax({
            url: 'get_questions.php',
            data:{
                  'did': did,
                  'pid': pid,
        },
            success: function(result){
            $("#previous").html(result);
          }
        });
        }
      </script>
      <script type="text/javascript">
    
        function getpatient(){
          var find = $("#search").val();
          console.log(find);

          $.ajax({
            url: 'get_patients.php',
            data:{
                  'find': find,
        },
            success: function(result){
            $("#result").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript">
    $("#save").on('click',function(){
    var aid = $('#answer').val();
    var did = '<?php echo $did; ?>';
    var pid = $("#patient_id").val();
    console.log(did);
    console.log(pid);
    console.log(aid);
      $.ajax({
        // type: 'POST',
        url: 'send_message.php',
        data:{
          'aid': aid,
          'did': did,
          'pid': pid,
        },
        success: function(result){
        $("#previous").html(result);
        $('#answer').val('');
      }
        });
      });
    </script>
		
	</body>
</html>