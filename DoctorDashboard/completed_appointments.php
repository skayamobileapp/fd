<?php
include('../connection/conn.php');
include('session_check.php');
$select = mysqli_query($conn,"SELECT * FROM patient_details");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select)) {
  $view[$i]['id'] = $row['id'];
  $view[$i]['patient_name'] = $row['patient_name'];
  $view[$i]['mobile_number'] = $row['mobile_number'];
  $view[$i]['email'] = $row['email'];
  $view[$i]['address1'] = $row['address1'];
  $i++;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>First Doctor</title>
    <link rel="icon" href="../fd_logo.png">

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/main.css" rel="stylesheet">
    
</head>
<script type="text/javascript">
  function delete_id(id)
  {
   var conf = confirm('Are you sure want to delete this record?');
   if(conf)
    window.location=anchor.attr("href");
}
</script>

<body>     
    <?php include('navbar.php'); ?>

    <div class="container-fluid main-wrapper">
      <div class="row">
        <aside class="col-sm-4 col-lg-3 sidebar">
          <?php include('menu.php'); ?>
                
            <ul class="sidebar-menu clearfix">
                <li><a href="index.php" class="dashboard">Dashboard</a></li>
               <!--  <li><a href="AdminHospitalDetails.php" class="hospital">Hospitals</a></li> -->
                <li ><a href="today_appointments.php" class="patient">Today Appointments</a></li>
                <li><a href="appointmentReschedule.php" class="patient">Reschedule Appointment</a></li>
                <li class="active"><a href="completed_appointments.php" class="patient">Completed Appointments</a></li>
                <li ><a href="view_patient_details.php" class="patient">Patients</a></li>
                <li ><a href="view_patient_summary.php" class="patient">Patient Summary</a></li>
                <li ><a href="view_doctor_availability.php" class="doctor">Doctor's Clinic Schedule</a></li>
                <li><a href="patient_bill_summary.php" class="patient">Patient Bill Summary</a></li>
                <?php include('list.php') ?>
            </ul>     
        </aside>
        <section class="col-sm-8 col-lg-9">          
            <div class="main-container">
               <h3 class="clearfix">Today's Completed Appointments </h3>               
                <div class="table-responsive theme-table v-align-top">
                    <table class="table">
                       <thead>
                           <tr>
                            <th>SL. NO</th>
                            <th>Patient Name</th>
                            <th>Appointment Reason</th>
                            <th>Appointment DateTime</th>
                            <th>Mobile</th>
                            <th>Email</th>
                            <th>View Presciptions</th>
                            <!-- <th>View Presciptions</th> -->
                           </tr>
                       </thead>
                        <tbody>
                           <?php
            
            for ($i=0; $i <count($view) ; $i++) { 
              ?>
              <tr>

              </tr>
              <?php

            }
            ?>                                                                                                           
                        </tbody>
                    </table>                   
                </div>               
            </div>
        </section>
      </div>
    </div>    
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>      
</body>

</html>