<?php
include('connection/conn.php');
error_reporting(0);

$is_search = '0';
$city = $_GET['city'];
$type = $_GET['type'];
$name = $_GET['name'];

if($type == 'doctor'){
    
    $is_search = '1';
	$view = array();
$doctorSql = "SELECT d.id, d.doctor_name, d.photo, d.email, d.mobile, d.experiance, d.address, s.specialty, c.city as cityname, c.state as statename FROM doctor_details d Left JOIN specialties s ON d.specialty=s.id INNER JOIN cities c ON d.city=c.id WHERE d.doctor_name like '%$name%' or d.specialty like '%$name%' ";
$result1 = mysqli_query($conn, $doctorSql);
while($row=mysqli_fetch_assoc($result1)){
array_push($view, $row);
}
}
else
if($type == 'clinic'){
    $is_search = '1';
	$doctorSql = "SELECT c FROM clinic_registration c Where c.city_id='$city' AND c.clinic_name like '%$name%'";
		$result1 = mysqli_query($conn, $doctorSql);
		$searchList = array();
while($row=mysqli_fetch_assoc($result1)){
array_push($searchList, $row);
}
}
else
if($type == 'speciality'){
    $is_search = '1';
	$view = array();
$doctorSql = "SELECT d.id, d.doctor_name, d.photo, d.email, d.mobile, d.experiance, d.address, s.specialty, c.city as cityname, c.state as statename FROM doctor_details d Left JOIN specialties s ON d.specialty=s.id INNER JOIN cities c ON d.city=c.id WHERE s.specialty like '%$name%' or d.specialty like '%$name%' ";
$result1 = mysqli_query($conn, $doctorSql);
while($row=mysqli_fetch_assoc($result1)){
array_push($view, $row);
}
}
else{
	$view = array();
$doctorSql = "SELECT d.id, d.doctor_name, d.photo, d.email, d.mobile, d.experiance, d.address, s.specialty, c.city as cityname, c.state as statename FROM doctor_details d INNER JOIN specialties s ON d.specialty=s.id INNER JOIN cities c ON d.city=c.id ";
$result1 = mysqli_query($conn, $doctorSql);
while($row=mysqli_fetch_assoc($result1)){
array_push($view, $row);
}
}

    $resnum = mysqli_num_rows($result1);

    $sql  = "select id, city from cities order by city";
$result = $conn->query($sql);
$cityList = array();
while ($row = $result->fetch_assoc()) {
    array_push($cityList, $row);
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Select2 CSS -->
		<link href="select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- Fancybox CSS -->
		<link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
		<?php include('navbar.php'); ?>
			<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-8 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Search</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title"><?php echo $resnum; ?> matches found</h2>
						</div>
						<div class="col-md-4 col-12 d-md-block d-none">
							<div class="sort-by">
								<span class="sort-title">Sort by</span>
								<span class="sortby-fliter">
									<select class="select">
										<option>Select</option>
										<option class="sorting">Rating</option>
										<option class="sorting">Popular</option>
										<option class="sorting">Latest</option>
										<option class="sorting">Free</option>
									</select>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->

			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Search Filter -->
							<div class="card search-filter">
								<div class="card-header">
									<h4 class="card-title mb-0">Search Filter</h4>
									<?php if($is_search == '')
									{
									    ?>
									<h5 class="card-title mb-0">Searched For Filter : <?php $city . ", " . $type ?></h5>"
									<?php
									}
									?>
								</div>
								<div class="card-body">
								<div class="filter-widget">
								<div class="form-group">
									<select name="city" class="form-control selitemIcon" id="city">
										<option value="">Select location</option>
										<?php
		                                for ($i=0; $i<count($cityList); $i++) {  ?>
		                                <option value="<?php echo $cityList[$i]['id']; ?>"><?php echo $cityList[$i]['city']; ?></option>
		                                <?php 
		                                }
		                                ?>
									</select>
								</div>
								<div class="form-group">
									<select name="type" class="form-control selitemIcon" id="type">
										<option value="">Select Type</option>
										<option value="doctor">Doctor</option>
										<option value="clinic">Clinic</option>
										<option value="speciality">Speciality</option>
									</select>
								</div>
								<div class="form-group">
									<input type="text" class="form-control search-location" name="name" id="name" placeholder="Search Here">
									<span class="form-text">Ex : Doctors or Clinics or Specialities</span>
								</div>
								</div>
									<div class="btn-search">
										<button type="button" class="btn btn-block" onclick="callSearch()">Search</button>
									</div>	
								</div>
							</div>
							<!-- /Search Filter -->
							
						</div>
						
						<div class="col-md-12 col-lg-8 col-xl-9">
							<?php for($i=0; $i<count($view); $i++){
                            $roleid = $view[$i]['id']; ?>
							<!-- Doctor Widget -->
							<?php $view[$i]['doctor_name']; ?>
							<div class="card">
								<div class="card-body">
									<div class="doctor-widget">
										<div class="doc-info-left">
											<div class="doctor-img">
												<a href="doctor-profile.php?id=<?php echo $view[$i]['id']; ?>">
													<img src="uploads/<?php echo $view[$i]['photo']; ?>" class="img-fluid" alt="User Image">
												</a>
											</div>
											<div class="doc-info-cont">
												<h4 class="doc-name"><a href="doctor-profile.php?id=<?php echo $view[$i]['id']; ?>"><?php echo ucwords($view[$i]['doctor_name']); ?></a></h4>
												<p class="doc-speciality"><?php echo ucfirst($view[$i]['address']); ?></p>
												<h5 class="doc-department"><?php echo $view[$i]['specialty']; ?></h5>
												<div class="rating">
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star filled"></i>
													<i class="fas fa-star"></i>
													<span class="d-inline-block average-rating"></span>
												</div>
												<div class="clinic-details">
													<p class="doc-location"><i class="fas fa-map-marker-alt"></i> <?php echo $view[$i]['cityname']; ?>, <?php echo $view[$i]['statename']; ?>, India</p>
												</div>
												<!-- <div class="clinic-services">
													<span>Dental Fillings</span>
													<span> Whitneing</span>
												</div> -->
											</div>
										</div>
										<div class="doc-info-right">
											<div class="clini-infos">
												<ul>
													<li><i class="fas fa-mobile"></i> <?php echo $view[$i]['mobile']; ?> </li>
													<li><i class="fa fa-envelope"></i> <?php echo $view[$i]['email']; ?> </li>
													<li><i class="fas fa-map-marker-alt"></i> <?php echo $view[$i]['cityname']; ?> , <?php echo $view[$i]['statename']; ?></li>

												</ul>
											</div>
											<div class="clinic-booking">
												<a class="view-pro-btn" href="doctor-profile.php?id=<?php echo $view[$i]['id']; ?>">View Profile</a>
												<a class="apt-btn" href="booking.php?id=<?php echo $view[$i]['id']; ?>">Book Appointment</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Doctor Widget -->

						<?php } ?>
						

							<!-- <div class="load-more text-center">
								<a class="btn btn-primary btn-sm" href="javascript:void(0);">Load More</a>	
							</div>	 -->
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
   <?php include('footer.php'); ?>

		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="select2/js/select2.js" ></script>
	    <script src="select2/js/select2-init.js" ></script>
		
		<!-- Datetimepicker JS -->
		<script src="assets/js/moment.min.js"></script>
		<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Fancybox JS -->
		<script src="assets/plugins/fancybox/jquery.fancybox.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		<script type="text/javascript">
			function clearAll() {
				$("input[type=checkbox]").prop('checked', false);
			}
		</script>
		<script type="text/javascript">
            function callSearch(){
            var city = $("#city").val();
            var type = $("#type").val();
            var name = $("#name").val();

              parent.location="search.php?city="+city+"&type="+type+"&name="+name;
              //   success: function(result){
              //   $("#div1").html(result);
              // }
            }
        </script>
		
	</body>
</html>