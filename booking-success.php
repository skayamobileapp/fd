<?php 
include('connection/conn.php');

$did = $_GET['did'];
$slotid = $_GET['slot_id'];
$purpose = $_GET['purpose'];

$sql = "select * from doctor_details where id ='$did'";
$result = $conn->query($sql);
$doctor_data = $result->fetch_assoc();

                $slot_query= "SELECT * FROM timeslots WHERE id='$slotid' ORDER BY date";
                $slot_result = $conn->query($slot_query);
                $row = $slot_result->fetch_assoc();
                $date = $row['date'];
                $time = $row['time'];
                $slot_id = $row['id'];
                $id_clinic = $row['id_clinic'];
                $datetime  = $date." ".$time;
                $etime = strtotime($row['time']);
                // $startTime = date("H:i", strtotime('-30 minutes', $etime));
                $endTime = date("H:i", strtotime('+30 minutes', $etime));
                $enddatetime = $date." ".$endTime;
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<?php include('navbar.php'); ?>
			
			<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Booking</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Booking</h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content success-page-cont">
				<div class="container-fluid">
				
					<div class="row justify-content-center">
						<div class="col-lg-6">
						
							<!-- Success Card -->
							<div class="card success-card">
								<div class="card-body">
									<div class="success-cont">
										<i class="fas fa-check"></i>
										<h3>Appointment booked Successfully!</h3>
										<p>Appointment booked with <strong><?php echo $doctor_data['doctor_name']; ?></strong><br> on <strong><?php echo date("d M Y", strtotime($date)); ?> <?php echo date("h:iA", strtotime($time)); ?> to <?php echo date("h:iA", strtotime($endTime)); ?></strong></p>
										<a href="index.php" class="btn btn-primary view-inv-btn">OK</a>
									</div>
								</div>
							</div>
							<!-- /Success Card -->
							
						</div>
					</div>
					
				</div>
			</div>		
			<!-- /Page Content -->
   
			<?php include('footer.php'); ?>
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		
	</body>
</html>