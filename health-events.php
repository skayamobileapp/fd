<?php
include('connection/conn.php');
$currentDate = date('Y-m-d');


if($_POST) {
  $idcity = $_POST['city'];
  $eventName = $_POST['name']; //date(event_todatetime)>=$currentDate
  $sql = "SELECT e.id, e.event_name, e.event_desc, e.event_fromdatetime, e.location, e.event_todatetime, e.filepath, e.state, c.city, c.state, e.id_doctor, d.doctor_name, d.email, d.mobile from events_list e INNER JOIN cities c ON e.city=c.id INNER JOIN doctor_details d ON e.id_doctor=d.id WHERE date(e.event_todatetime)>=CURRENT_TIMESTAMP AND e.city='$idcity' AND e.event_name like '%$eventName%' ORDER BY e.event_fromdatetime ASC";
  $result = mysqli_query($conn, $sql);
  $events_lists = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($events_lists, $row);
   }
}
   else
   {

   	$sql = "SELECT e.id, e.event_name, e.event_desc, e.event_fromdatetime, e.location, e.event_todatetime, e.filepath, e.state, c.city, c.state, e.id_doctor, d.doctor_name, d.email, d.mobile from events_list e INNER JOIN cities c ON e.city=c.id INNER JOIN doctor_details d ON e.id_doctor=d.id WHERE date(e.event_todatetime)>=CURRENT_TIMESTAMP ORDER BY e.event_fromdatetime ASC";
  $result = mysqli_query($conn, $sql);
  $i = 0;
  $events_lists = array();
  while ($row = mysqli_fetch_assoc($result)) {
    array_push($events_lists, $row);
   }

   }
   if (empty($events_lists)) {
     $message = "<h3 class='text-center'>Will keep you posted on events in your city</h3";
   }

$sql  = "select id, city from cities order by city";
$result = $conn->query($sql);
$cityList = array();
while ($row = $result->fetch_assoc()) {
    array_push($cityList, $row);
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Datetimepicker CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- Fancybox CSS -->
		<link rel="stylesheet" href="assets/plugins/fancybox/jquery.fancybox.min.css">
		
		<link rel="stylesheet" href="assets/plugins/select2/css/select2.min.css">

		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">

		<link href="select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
		<?php include('navbar.php'); ?>
			<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-8 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.php">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Search</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title"> matches found</h2>
						</div>
						<div class="col-md-4 col-12 d-md-block d-none">
							<div class="sort-by">
								<span class="sort-title">Sort by</span>
								<span class="sortby-fliter">
									<select class="form-control">
										<option>Select</option>
										<option class="sorting">Rating</option>
										<option class="sorting">Popular</option>
										<option class="sorting">Latest</option>
										<option class="sorting">Free</option>
									</select>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Search Filter -->
							<div class="card search-filter">
								<div class="card-header">
									<h4 class="card-title mb-0">Search Filter</h4>
								</div>
								<div class="card-body">
								<div class="filter-widget">
									<div style="padding-left: 11em;">
										<a href="#" onclick="clearAll()">Clear all</a>
									</div>
									<!-- <div class="cal-icon">
										<input type="text" class="form-control datetimepicker" placeholder="Select Date">
									</div> -->
								</div>
								<div class="filter-widget">
									<h4>Search Event By</h4>
									<div>
										<label class="custom_check">
											<input type="checkbox" name="select_specialist" >
											<span class="checkmark"></span> Popular
										</label>
									</div>
									<div>
										<label class="custom_check">
											<input type="checkbox" name="select_specialist" >
											<span class="checkmark"></span> Latest 
										</label>
									</div>
									<div>
										<label class="custom_check">
											<input type="checkbox" name="select_specialist">
											<span class="checkmark"></span> Free
										</label>
									</div>
								</div>
									<div class="btn-search">
										<button type="button" class="btn btn-block">Search</button>
									</div>	
								</div>
							</div>
							<!-- /Search Filter -->
							
						</div>
						
						<div class="col-md-12 col-lg-8 col-xl-9">

							<div class="card">
								<div class="card-body">
									<div class="doctor-widget">
										<div class="doc-info-left">
											<div class="doc-info-cont">
												<div class="search-box">
							<form action="" method="post">
								<div class="form-group search-location">
									<select name="city" class="form-control selitemIcon" id="city">
										<option value="">Search location</option>
										<?php
		                                for ($i=0; $i<count($cityList); $i++) {  ?>
		                                <option value="<?php echo $cityList[$i]['id']; ?>"><?php echo $cityList[$i]['city']; ?></option>
		                                <?php
		                                }
		                                ?>
									</select>
									<span class="form-text">Based on your Location</span>
								</div>
								<div class="form-group">
									<input type="text" class="form-control search-info" name="name" id="name" placeholder="Search Event">
									<span class="form-text">Based on Name</span>
								</div>
								<button type="submit" class="btn btn-primary search-btn" style="background: #20c0f3;"><i class="fas fa-search"></i> <span>Search</span></button>
							</form>
						</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<?php for($i=0; $i<count($events_lists); $i++){
                            $roleid = $view[$i]['id']; ?>
							<!-- Doctor Widget -->
							<?php $events_lists[$i]['doctor_name']; ?>
							<div class="card">
								<div class="card-body">
									<div class="doctor-widget">
										<div class="doc-info-left">
											<div class="doctor-img">
												 <a href="uploads/<?php echo $events_lists[$i]['filepath']; ?>" data-fancybox="gallery">
													<img src="uploads/<?php echo $events_lists[$i]['filepath']; ?>" class="img-fluid" alt="User Image">

												</a>
											</div>
											<div class="doc-info-cont">
												<h4 class="doc-name"><a href="#"><?php echo ucwords($events_lists[$i]['event_name']); ?></a></h4>
												<div class="clinic-services">
													<span>Fast Filling</span>
													<br>
												</div>
												<h5 class="doc-department"><br
													><a href="doctor-profile.php?id=<?php echo $events_lists[$i]['id_doctor']; ?>">
													Event by : <?php echo $events_lists[$i]['doctor_name'];?></a></h5>
												
												<div class="clinic-details">
													<p class="doc-location"><i class="fas fa-map-marker-alt" title="locations"></i> <?php echo ucfirst($events_lists[$i]['location']); ?></p>
												</div>
											</div>
										</div>
										<div class="doc-info-right">
											<div class="clini-infos">
												<ul>
													<li><i class="fas fa-mobile"></i> <?php echo $events_lists[$i]['mobile']; ?> </li>
													<li><i class="fa fa-envelope"></i> <?php echo $events_lists[$i]['email']; ?> </li>
													<li><i class="fas fa-map-marker-alt"></i> <?php echo $events_lists[$i]['city']; ?> , <?php echo $events_lists[$i]['state']; ?></li>

												</ul>
											</div>
											<div class="clinic-booking">
												<a class="apt-btn" href="#" data-toggle="modal" data-target="#myModal2" onclick="fillEVent(<?php echo $events_lists[$i]['id']; ?>)">Interested</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Doctor Widget -->

						<?php } ?>
						

							<!-- <div class="load-more text-center">
								<a class="btn btn-primary btn-sm" href="javascript:void(0);">Load More</a>	
							</div>	 -->
						</div>
					</div>

				</div>

			</div>

    <input type="hidden" name="event_id" id="event_id" value="" />

			<!-- /Page Content -->
   <?php include('footer.php'); ?>

		</div>

<div id="popup">
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog modal-md" style="width: 70%;">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Contact Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <input type="text" name="contact_name" class="form-control" id="contact_name" placeholder="Contact Name">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                      <input type="text" name="contact_number" class="form-control" id="contact_number" placeholder="Contact Number">
                  </div>
                </div>
              </div>
               <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                      <input type="text" name="contact_email" class="form-control" id="contact_email" placeholder="Email Id">
                  </div>
                </div>
               
              </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button"  data-dismiss="modal" class="btn btn-primary btn-lg" id="addMe" name="addMe">Submit</button>
      </div>
    </div>
  </div>
</div>
</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Sticky Sidebar JS -->
        <script src="assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
        <script src="assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>
		
		<!-- Select2 JS -->
		<script src="assets/plugins/select2/js/select2.min.js"></script>
		
		<!-- Datetimepicker JS -->
		<script src="assets/js/moment.min.js"></script>
		<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Fancybox JS -->
		<script src="assets/plugins/fancybox/jquery.fancybox.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		<script type="text/javascript">
			function clearAll() {
				$("input[type=checkbox]").prop('checked', false);
			}
		</script>
		<script type="text/javascript">
    $("#addMe").on('click',function(){
    var event_id = $("#event_id").val();
    var contact_name = $("#contact_name").val();
    var contact_number = $("#contact_number").val();
    var contact_email = $("#contact_email").val();
      $.ajax({
        // type: 'POST',
        url: 'add_customer_for_event.php',
        data:{
          'contact_name': contact_name,
          'contact_number': contact_number,
          'contact_email': contact_email,
          'event_id': event_id,
        },
        success: function(result){
           alert("Thanks for showing interest");
           $('#myModal2').hide();
      }
        });
      });


    function fillEVent(id) {
      $("#event_id").val(id);
    }
    </script>

		<script src="select2/js/select2.js" ></script>
    <script src="select2/js/select2-init.js" ></script>
		
	</body>
</html>