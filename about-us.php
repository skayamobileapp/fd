<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  
  </head>
  <body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>
      <!-- Breadcrumb -->
      <!-- <div class="breadcrumb-bar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-12 col-12">
              <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Terms and Conditions</li>
                </ol>
              </nav>
              <h2 class="breadcrumb-title">Terms and Conditions</h2>
            </div>
          </div>
        </div>
      </div> -->
      <!-- /Breadcrumb -->
      
      <!-- Page Content -->
      <div class="content">
        <div class="container">
          <div class="row">
                <div class="col-sm-6">
                    <h1>About Us</h1>
                    <div class="join-us-content">
                      <p class="text-justify"> Digitization has transformed our abilities to pursue all aspects of our lives, including the ability to provide and receive healthcare. Once within the absolute physical domain, where one need to spend a lot of time to access, meet and interact with healthcare providers, services can now be accessed at the click of a button.</p>

                      <p class="text-justify">Healthcare system, especially in a huge and diverse country like ours, cannot, and should not, remain fragmented. For enhancing efficiencies within, what is surely, the largest healthcare system in the world, we need a holistic ecosystem where all stakeholders are connected and accessible to each other. This will also help us utilize our limited resources efficiently, leading to better provision of healthcare services.</p>

                      <p class="text-justify">Digitization of patient data is a cost-effective way to gain access to the medical history and prescribed treatment regimen of the patient by the network of healthcare providers. This can lead to seamless and swift delivery of services, leading to thwarting of life-threatening events.</p>                      
                    </div>
                </div>
                
                <div class="col-sm-6 text-center">
                    <img src="img/about.svg" alt="Discover top doctors" class="img-responsive">
                </div>
                <div class="col-xs-12">
                  <div class="join-us-content">
                    <p  class="text-justify">The network should also be afforded a platform that will make delivery of healthcare services effective. A PHR-enabled platform that includes patient management systems can be a blessing for small and large hospitals, individual caregivers and patients and their relatives. At FirstDoctor, we envision a holistic platform that provides nodes of service to each stakeholder within the healthcare ecosystem to participate effectively and deliver service efficiently.</p>                 

                    <p  class="text-justify">FirstDoctor is a venture by Gateway Healthtech Fidelity Pvt. Ltd., India’s largest technology enabled, omni-channel aggregator of healthcare services that also includes wellness and happiness, custom spas and salons, fitness and sports and fashion and beauty to encompass all aspects of human well-being.  </p>                         
                  </div>             
                </div>
            </div>                    

                <div class="row text-center">
                    <div class="col-sm-4">
                       <div class="about-callout">
                        <h3>Our Mission</h3>
                        <p style="text-align: center;">We endeavour to create a new-age, patient-centric healthcare ecosystem with a network of
healthcare providers and patients that will enhance accessibility, allow for seamless and
hassle-free interactions and simplify health delivery solutions by leveraging data- and
medical technology.</p>                       
                       </div>
                    </div>
                    <div class="col-sm-4">
                       <div class="about-callout">
                        <h3>Our Vision</h3>
                        <p style="text-align: center;">We envision a holistic healthcare ecosystem within which adequate healthcare services are
accessible equally to all strata of society.</p>                    
                       </div>                   
                    </div>
                    <div class="col-sm-4">
                       <div class="about-callout">
                        <h3>Our Values</h3>
                        <p style="text-align: center;">At the core of all our efforts is our firm belief in the integrity of life. We believe that life is
precious and all attempts must be made to keep it healthy and full of vigour and vitality in
order to experience its richness and fullness. We believe that access to healthcare services
should not be a privilege and it should be affordable. We value honesty and transparency in
all dealings and we seek to connect like-minded stakeholders to transform the healthcare
system in the country.</p>                      
                       </div>                   
                    </div>                                
                </div>
        </div>

      </div>    
      <!-- /Page Content -->
   
      <?php include('footer.php'); ?>
    </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="assets/js/script.js"></script>
    
  </body>
</html>