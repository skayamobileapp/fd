<?php 
include('connection/conn.php');

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from doctor_details where email = '$id'";
    $result   = $conn->query($sql);
    while ($row = mysqli_fetch_array($result))
    {
        $Firstname = $row['first_name'];
        $Lastname = $row['last_name'];
        $Fullname = $Firstname." ".$Lastname ;
        $did = $row['id'];

    }

	if (!empty($id))
	{
        $to =  $id;
        $name = $Fullname;
        $update_id = $did;
        $link = "<a href='http://rvvlsi.com/firstdoctor/set_new_doctor_password.php?id=$update_id'>click Here</a>";
        $subject = "Set New Password - FirstDoctor";
        $message = "
        <table>
        <tr>
        <td>Dear $name</td>
        </tr>
        <tr>
        <td>Welcome to <b>First Doctor</b></td>
        </tr>
        <tr>
        <td>Here is link to set new password for First Doctor click below. <br>$link</br></td>
        </tr>
        <tr>
        <td><br/><br/>Regards,</td>
        </tr>
        <tr>
        <td><img src='http://rvvlsi.com/firstdoctor/img/phr_logo.png' width='100px' height='50px'></td>
        </tr>
        <tr>
        <td>First Doctor team</td>
        </tr>
        </table>";


        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <no-reply@firstdoctor.app>' . "\r\n";

        mail($to,$subject,$message,$headers);

    $table = "<script>alert('link sent to your mail')</script>";
    echo $table;
        }
    }

        // echo "<script>alert('password setup link sent to your mail')</script>";
	// else
	// {
	// 	$sql = "SELECT * FROM patient_details WHERE email = '$id' ";
	// 	$result = $conn->query($sql);
	// 	$patList = array();
	// 	while ($row = $result->fetch_assoc())
	// 	{
	// 		array_push($patList, $row);
	// 	}
 //    }


?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body class="account-page">

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
	<?php include('navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					
					<div class="row">
						<div class="col-md-8 offset-md-2">
							
							<!-- Account Content -->
							<div class="account-content">
								<div class="row align-items-center justify-content-center">
									<div class="col-md-7 col-lg-6 login-left">
										<img src="assets/img/login-banner.png" class="img-fluid" alt="Login Banner">	
									</div>
									<div class="col-md-12 col-lg-6 login-right">
										<div class="login-header">
											<h3>Forgot Password?</h3>
											<p class="small text-muted">Enter your email to get a password reset link</p>
										</div>
										
										<!-- Forgot Password Form -->
										<form action="doctor_login.php">
											<div class="form-group form-focus">
												<input type="email" class="form-control floating">
												<label class="focus-label">Email</label>
											</div>
											<div class="text-right">
												<a class="forgot-link" href="doctor_login.php">Remember your password?</a>
											</div>
											<button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Reset Password</button>
										</form>
										<!-- /Forgot Password Form -->
										
									</div>
								</div>
							</div>
							<!-- /Account Content -->
							
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
   
		<?php include('footer.php'); ?>
		   
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		
	</body>
</html>