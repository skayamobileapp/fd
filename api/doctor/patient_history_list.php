<?php
include('../../connection/conn.php');

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');
$patientid = $_GET['patient_id'];

$select = mysqli_query($conn,"Select p.*, e.id as eid, e.title, e.start, e.amount, dd.doctor_name from patient_details as p INNER JOIN events as e on p.id=e.patient_id inner join doctor_details dd on e.doctor_id = dd.id where date(e.start)<='$date' and e.status='1'  and p.id='$patientid' order by eid desc");

$i = 0;
$view = array();
while ($row = mysqli_fetch_assoc($select))
{
	$amount = number_format($row['amount'], 2, '.', ',');
	
	//$row['start'] = "Date : ". date("d-m-Y H:i", strtotime($row['start']));
	$row['start'] = date("d-m-Y H:i", strtotime($row['start']));
	$row['amount'] = $amount;
  array_push($view,$row);

}

echo json_encode($view);exit;
?>