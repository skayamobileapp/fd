<?php
include('../../connection/conn.php');


date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');

$doctorid = $_GET['doctor_id'];

$patient_id = array();

//$select_id = "SELECT distinct(p.id) as patient_id, p.patient_name, p.mobile_number, p.email, p.address1, p.photo from patient_details as p INNER JOIN events as e on p.id=e.patient_id INNER JOIN payment_table pt on pt.event_id = e.id left join cities ci on ci.id = p.city where date(e.start) <= '$date' and e.status='1'  and e.doctor_id='$doctorid' and pt.amount > '0' order by e.end DESC";

$select_id = "SELECT distinct(p.id) as patient_id, p.patient_name, p.mobile_number, p.email, p.address1, p.photo, p.social_security_or_identity_num, p.card_type from patient_details as p INNER JOIN events as e on p.id=e.patient_id left join cities ci on ci.id = p.city where e.doctor_id='$doctorid' order by p.patient_name ASC";


$result_getid          = $conn->query($select_id);

$i = 0;
while ($row = $result_getid->fetch_assoc())
{
	if($row['social_security_or_identity_num'] == null)
	{
		$row['social_security_or_identity_num'] = "";
	}
	if($row['card_type'] == null)
	{
		$row['card_type'] = "Basic";
	}
	$patientid = $row['patient_id'];

	$patient_id[$i]['id'] = $row['patient_id'];
	$patient_id[$i]['patient_name'] = $row['patient_name'];
	$patient_id[$i]['mobile_number'] = $row['mobile_number'];
	$patient_id[$i]['email'] = $row['email'];
	$patient_id[$i]['address1'] = $row['address1'];
	$patient_id[$i]['social_security_or_identity_num'] = $row['social_security_or_identity_num'];
	$patient_id[$i]['card_type'] = $row['card_type'];
	$patient_id[$i]['photo'] = $row['photo'];
	
	//$select_date = "SELECT e.id, e.start, e.amount from events e where e.patient_id ='$patientid' and e.status = '1' order by e.start DESC";
	
	$select_date = "SELECT e.id, e.start, e.amount from events e where e.patient_id ='$patientid' order by e.start ASC";

	$result_date          = $conn->query($select_date);
	

	while ($rowdate = $result_date->fetch_assoc())
	{
		if($rowdate['amount'] == null)
		{
			$patient_id[$i]['amount'] = "0";
		}
		
		else
		{
			$patient_id[$i]['amount'] = $rowdate['amount'];
		}
			
		$patient_id[$i]['start'] =  date("d-m-Y H:i", strtotime($rowdate['start']));
		
	}
	
	$i++;
}




echo json_encode($patient_id);exit;

?>