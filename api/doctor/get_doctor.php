<?php
include('../../connection/conn.php');
$did = $_GET['doctor_id'];
 $sql = "SELECT dd.*, stat.state, cit.city, stre.name as stream, spec.specialty, subspe.name as sub_specialty  FROM doctor_details dd  left join states stat on stat.id = dd.state left join cities cit on cit.id = dd.city left join stream stre on dd.stream = stre.id left join specialties spec on dd.specialty = spec.id left join sub_specialties subspe on subspe.id = dd.sub_specialty WHERE dd.id = '$did' ";
$result = $conn->query($sql);
$doctorArray = array();
while ($row = $result->fetch_assoc())
{
	if($row['date_of_birth'] == null)
	{
		$row['date_of_birth'] = "";
	}
	else
	{
		$row['date_of_birth'] = date("Y-m-d", strtotime($row['date_of_birth']));
	}
	
	if($row['date_of_birth'] == null)
	{
		$row['dob'] = "";
	}
	else
	{
		$row['dob'] = date("d-m-Y", strtotime($row['date_of_birth']));
	}
	
	if($row['experiance'] == null)
	{
		$row['experiance'] = "";
	}
	
	if($row['stream'] == null)
	{
		$row['stream'] = "";
	}

	if($row['specialty'] == null)
	{
		$row['specialty'] = "";
	}

	if($row['sub_specialty'] == null)
	{
		$row['sub_specialty'] = "";
	}

	if($row['telephone'] == null)
	{
		$row['telephone'] = "";
	}

	if($row['mobile'] == null)
	{
		$row['mobile'] = "";
	}

	if($row['cell_number'] == null)
	{
		$row['cell_number'] = "";
	}
	
	if($row['alter_number'] == null)
	{
		$row['alter_number'] = "";
	}

	if($row['address'] == null)
	{
		$row['address'] = "";
	}

	if($row['pincode'] == null)
	{
		$row['pincode'] = "";
	}

	if($row['state'] == null)
	{
		$row['state'] = "";
	}

	if($row['city'] == null)
	{
		$row['city'] = "";
	}

	if($row['practice_contact'] == null)
	{
		$row['practice_contact'] = "";
	}

	if($row['clinic_address'] == null)
	{
		$row['clinic_address'] = "";
	}
	
    array_push($doctorArray, $row);
}

//   $view['code'] = "200";
//   $view['result'] = $doctorArray;

$data = $doctorArray[0];
if($data == null)
{
	$data = array();
}

echo json_encode($data);exit;
?>