<?php
include('../../connection/conn.php');

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');

$from_date = $_POST['from_date'];
$to_date = $_POST['to_date'];
$doctor_id = $_POST['doctor_id'];


function returnBetweenDates( $startDate, $endDate )
{
    $startStamp = strtotime($startDate);
    $endStamp   = strtotime($endDate);

    if( $endStamp > $startStamp )
	{
        while( $endStamp >= $startStamp )
		{

            $dateArr[] = date( 'Y-m-d', $startStamp );

            $startStamp = strtotime( ' +1 day ', $startStamp );

        }
        return $dateArr;    
    }else
	{
        return $startDate;
    }
}

$view = returnBetweenDates( $from_date, $to_date );

//echo json_encode($view);exit;
 //adds an element

$myArray = array();


$myArray[0]['date'] = "Date";
$myArray[0]['date_sql'] = "";
$myArray[0]['number'] = "Appointments";
$myArray[0]['total'] = "Total (₹)";
$myArray[0]['sum'] = "";


$i = 1;
foreach($view as $date)
{
	$no = 0.0;
	$total = 0.00;
	$sum = 0.00;
	
	$sel_get_outstanding = "SELECT count(id) as no FROM `doctor_outstanding_balance` where date(date_time) = '$date' and id_doctor = '$doctor_id'";
	$resultNo          = $conn->query($sel_get_outstanding);
	
	
	while ($row_interest = $resultNo->fetch_assoc())
	{

		$no =  $row_interest['no'];
	}
	
	$sel_get_outstanding_total = "SELECT sum(amount) as total FROM `doctor_outstanding_balance` where date(date_time) = '$date' and id_doctor = '$doctor_id'";
	 //order by id DESC limit 0,1
	
	$result_total          = $conn->query($sel_get_outstanding_total);
	
	
	while ($row_total = $result_total->fetch_assoc())
	{

		$total = number_format($row_total['total'], 2, '.', ',');
	}
	if($total == null)
	{
		$total =  "0.00";
	}
	
	
	$sel_get_outstanding_total_sum = "SELECT sum(amount) as sum FROM `doctor_outstanding_balance` where date(date_time) >= '$from_date' and date(date_time) <= '$to_date' and id_doctor = '$doctor_id'";
	 //order by id DESC limit 0,1
	
	$result_total_sum          = $conn->query($sel_get_outstanding_total_sum);
	
	
	while ($row_total_sum = $result_total_sum->fetch_assoc())
	{
		$row_total_sum['sum'] = number_format($row_total_sum['sum'], 2, '.', ',');

		$sum = "Total(₹) : ".  $row_total_sum['sum'];
	}
	if($sum == null)
	{
		$sum =  "0";
	}
	
	//$row['payment_date'] =  date("d-m-Y H:i", strtotime($row['payment_date']));
	
	$myArray[$i]['date'] = date("d-m-Y", strtotime($date));
	$myArray[$i]['date_sql'] = $date;
	$myArray[$i]['number'] = $no;
	$myArray[$i]['total'] = $total;
	$myArray[$i]['sum'] = $sum;
	$i++;	
}




 echo json_encode($myArray);exit;

?>