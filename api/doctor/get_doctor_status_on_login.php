<?php
include('../../connection/conn.php');

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');
$current_month = date('M-Y');



$doctor_id = $_POST['doctor_id'];
$previous_month = $_POST['previous_month'];
$previous_year = $_POST['previous_year'];

$app_update_status = '0';
$app_update_link = '';

$months =array(1=>'Jan', 2=>'Feb', 3=>'Mar', 4=>'Apr', 5=>'May', 6=>'Jun', 7=>'Jul', 8=>'Aug', 9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec');
        $previous_month = $months[(int)$previous_month];
		$previous_date = $previous_month."-".$previous_year;



$data['doctor_profile_status'] = "0";
$data['doctor_approval_status'] = "0";
$payment_status = "1";
$payment_balance_amount = '0';
$payment_due_month = '0';

$select_payment_status = "SELECT * FROM `doctor_outstanding_balance` where month = '$previous_date' and id_doctor = '$doctor_id' and status = '0' order by id DESC limit 0,1";
$result_payment_status          = $conn->query($select_payment_status);

while ($row_payment_status = $result_payment_status->fetch_assoc())
{
	$payment_balance_amount = $row_payment_status['total'];
	$payment_due_month = $row_payment_status['month'];
	$status = $row_payment_status['status'];
	if($status == '0')
	{
		$payment_status = '0';
	}
	else
	{
		$payment_status = '1';	
	}
}



$select_app_link = "SELECT * FROM `app_link` where name = 'Doctor' and status = '1'";
$result_app_link          = $conn->query($select_app_link);

while ($row = $result_app_link->fetch_assoc())
{
	$data['app_link'] = $row['link'];
	$app_update_status = $row['update_available'];
	$app_update_link = $row['update_app_link'];
}


$select_profile_status = "SELECT * FROM `doctor_details` where id = '$doctor_id' limit 0,1";

$result_profile_status       = $conn->query($select_profile_status);

while ($row_profile_status = $result_profile_status->fetch_assoc())
{
	$data['doctor_profile_status'] = $row_profile_status['profile_status'];
	$data['doctor_approval_status'] = $row_profile_status['approval_status'];
}




$discount_amount = '0';
		$discount_percentage = '0';
		$discount_description = '';
		$after_discount_total = '0';
		$id_subscription = '0';

$sel_get_doctor_discount = "SELECT ds.* FROM `doctor_subscription` ds inner join doctor_details dd on ds.id = dd.id_subscription where dd.id = '$doctor_id' and ds.status = '1' order by dd.id DESC limit 0,1";
	$result_discount          = $conn->query($sel_get_doctor_discount);
	
//echo json_encode($sel_get_outstanding_total_balance);exit;


	while ($row_discount = $result_discount->fetch_assoc())
	{
		$discount_percentage = $row_discount['final_packege_discount_percent'];
		$id_subscription = $row_discount['id'];
		$discount_description = $row_discount['discount_description'];
	}


	$sql_select_month = "SELECT distinct(dob.month) as month FROM `doctor_outstanding_balance` dob  where dob.id_doctor = '$doctor_id'";
	$result_month          = $conn->query($sql_select_month);
	
//echo json_encode($sql_select_month);exit;

$data_array = array();
while ($row_month = $result_month->fetch_assoc())
{

	$previous_date = $row_month['month'];
	
	$sel_get_outstanding_total_balance = "SELECT dob.* from `doctor_outstanding_balance` dob where dob.month = '$previous_date' and dob.id_doctor = '$doctor_id' and dob.status = '0' order by dob.id DESC limit 0,1";
	//echo json_encode($sel_get_outstanding_total_balance);exit;
	$result_total_sum          = $conn->query($sel_get_outstanding_total_balance);

	while ($row_total_sum = $result_total_sum->fetch_assoc())
	{
		$data_variable['total'] = $row_total_sum['total'];
		$data_variable['month'] = $row_total_sum['month'];
		if($data_variable != null)
		{
			array_push($data_array, $data_variable);
		}
	}


}
$previous_month = '';
$history_balance_amount = $data_array[0]['total'];
$history_month = $data_array[0]['month'];

$previous_balance_amount = $data_array[1]['total'];
$previous_month = $data_array[1]['month'];

//echo json_encode($data_array);exit;
if($previous_month == null)
{
	$previous_month = '';
}
if($history_month == null)
{
	$history_month = '';
}
if($history_paid_amount == null)
{
	$history_paid_amount = '0';
}
if($history_balance_amount == null)
{
	$history_balance_amount = '0';
}
if($previous_paid_amount == null)
{
	$previous_paid_amount = '0';
}
if($previous_balance_amount == null)
{
	$previous_balance_amount = '0';
}

if($discount_description == '' || $discount_description == '0')
{
	$discount_description = 'Discount';	
}

$minimum_payable = '0';
$total_payable = '0';
$isdiscount = '0';
//echo json_encode($current_month);exit;
//echo json_encode($data_array);exit;

if(count($data_array) == 0) 
{
	$data['app_link'] = $data['app_link'];
	$data['doctor_profile_status'] = $data['doctor_profile_status'];
	$data['doctor_approval_status'] = $data['doctor_approval_status'];
	$data['doctor_payment_status'] = $payment_status;
	$data['doctor_payment_balance_amount'] = ceil($after_discount_total);
	$data['doctor_payment_due_month'] = $previous_month;
	$data['app_update_status'] = $app_update_status;
	$data['app_update_link'] = $app_update_link;
	$data['block'] = '0';

echo json_encode($data);exit;
}
	
if($history_month == $current_month)
{
	$data['app_link'] = $data['app_link'];
	$data['doctor_profile_status'] = $data['doctor_profile_status'];
	$data['doctor_approval_status'] = $data['doctor_approval_status'];
	$data['doctor_payment_status'] = $payment_status;
	$data['doctor_payment_balance_amount'] = ceil($after_discount_total);
	$data['doctor_payment_due_month'] = $previous_month;
	$data['app_update_status'] = $app_update_status;
	$data['app_update_link'] = $app_update_link;
	$data['block'] = '0';

echo json_encode($data);exit;
}
//echo json_encode($previous_month);exit;
//echo json_encode($payment_status);exit;
//$total_payable = '888';
//echo json_encode($history_balance_amount);exit;
if($current_month == $previous_month)
{
	$tot = $history_balance_amount;
	if($discount_percentage >= 1)
	{
		$isdiscount = '1';
		$after_discount_total = ($tot * 0.01 * (100 - $discount_percentage));
		$discount_amount = $tot - $after_discount_total;
		$total_payable_a = $after_discount_total;
	}
	else	
	{
		$discount_amount = $discount_amount;
		$after_discount_total = $tot;
	}
	
	$data['app_link'] = $data['app_link'];
	$data['doctor_profile_status'] = $data['doctor_profile_status'];
	$data['doctor_approval_status'] = $data['doctor_approval_status'];
	$data['doctor_payment_status'] = $payment_status;
	$data['doctor_payment_balance_amount'] = ceil($after_discount_total);
	$data['doctor_payment_due_month'] = $history_month;
	$data['app_update_status'] = $app_update_status;
	$data['app_update_link'] = $app_update_link;
	$data['block'] = '0';
	echo json_encode($data);exit;
}

$total_payable = $previous_balance_amount + $history_balance_amount;
//$total_payable = '100';
//$discount_percentage = 8;


$total_payable_a = $total_payable; 

if($discount_percentage >= 1)
{
	$isdiscount = '1';
	$after_discount_total = ($total_payable * 0.01 * (100 - $discount_percentage));
	//echo json_encode($after_discount_total);exit;
	$discount_amount = $total_payable - $after_discount_total;
	$total_payable_a = $after_discount_total;
}
else	
{
	$discount_amount = $discount_amount;
	$after_discount_total = $total_payable;
}




$minimum_payable = ($total_payable * 0.01 * 75);
$minimum_payable_after_discount = ($total_payable_a * 0.01 * 75);


if($after_discount_total > 0)
{
	$payment_status = '0';
}
//echo json_encode($previous_month);exit;
//$previous_month = 'Nov-2019';
//echo json_encode($total_payable);exit;
$pay_month = '';
if($previous_month != "" && $history_month != "")
{
	$pay_month = $history_month. ", ". $previous_month;	
}
if(($previous_month != "" && $history_month == ""))
{
	$pay_month = $previous_month;
}
if($previous_month == "" && $history_month != "")
{
	$pay_month = $history_month;
}

$data['app_link'] = $data['app_link'];
$data['doctor_profile_status'] = $data['doctor_profile_status'];
$data['doctor_approval_status'] = $data['doctor_approval_status'];
$data['doctor_payment_status'] = $payment_status;
$data['doctor_payment_balance_amount'] = ceil($after_discount_total);
$data['doctor_payment_due_month'] = $pay_month;
$data['app_update_status'] = $app_update_status;
$data['app_update_link'] = $app_update_link;
$data['block'] = '1';



echo json_encode($data);exit;
?>