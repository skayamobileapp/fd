<?php
include('../../connection/conn.php');
$eventId = $_GET['id'];

 $select1 = mysqli_query($conn,"SELECT p.doctor_name,e.start FROM doctor_details AS p INNER JOIN events AS e ON e.doctor_id = p.id WHERE e.id='$eventId' ");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $doctor_name = $row1['doctor_name'];
  $examinedDate = date('d-M-Y',strtotime($row1['start']));
  }


$select1 = mysqli_query($conn,"SELECT p.id, p.patient_name, e.id, e.title, e.start FROM patient_details AS p INNER JOIN events AS e ON e.patient_id = p.id WHERE e.id='$eventId' ");
while ($row1 = mysqli_fetch_assoc($select1)) {
  $patient_name = $row1['patient_name'];
  }
$prescrip = mysqli_query($conn,"SELECT d.type, p.drug_name, p.duration, p.repeat_same, p.time_of_the_day, p.to_be_taken, p.patient_id, p.appointment_on FROM prescrip AS p INNER JOIN drug_type AS d ON p.drug_type=d.id WHERE p.event_id='$eventId'");
  $i=0;
  while ($row = mysqli_fetch_assoc($prescrip)) {
    $fetch[$i]['type'] = $row['type'];
    $fetch[$i]['drug_name'] = $row['drug_name'];
    $fetch[$i]['duration'] = $row['duration'];
    $fetch[$i]['repeat_same'] = $row['repeat_same'];
    $fetch[$i]['time_of_the_day'] = $row['time_of_the_day'];
    $fetch[$i]['to_be_taken'] = $row['to_be_taken'];
    $fetch[$i]['appointment_on'] = $row['appointment_on'];
    $fetch[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $lab = mysqli_query($conn,"SELECT lds.lab_name, ltn.test_name, lds.mobile, lds.email FROM labdetails ld INNER JOIN lab_details lds ON ld.lab_name=lds.id INNER JOIN lab_test_name ltn ON ld.test_name=ltn.id WHERE ld.event_id = '$eventId' ");

  $i=0;
  while ($row = mysqli_fetch_assoc($lab)) {
    $Lab[$i]['lab_name'] = $row['lab_name'];
    $Lab[$i]['test_name'] = $row['test_name'];
    $Lab[$i]['date'] = $row['date'];
    $Lab[$i]['email'] = $row['email'];
    $Lab[$i]['mobile'] = $row['mobile'];
    $i++;
  }

  $allergy = mysqli_query($conn,"SELECT a.*, atype.allergy_type AS altype, alist.allergy_name AS alname FROM event_allergies a INNER JOIN allergy_types atype ON atype.id=a.allergy_type INNER JOIN allergy_list alist ON a.allergy_name=alist.id WHERE a.event_id = '$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($allergy)) {
    $Allergy[$i]['altype'] = $row['altype'];
    $Allergy[$i]['alname'] = $row['alname'];
    $Allergy[$i]['date'] = $row['date'];
    $Allergy[$i]['patient_id'] = $row['patient_id'];
    $i++;
  }

  $vital = mysqli_query($conn,"SELECT * FROM vitals WHERE event_id='$eventId'");
  $i=0;
  while ($row = mysqli_fetch_assoc($vital)) {
    $height = $row['height'];
    $weight = $row['weight'];
    $bp = $row['bp'];
    $sugar = $row['sugar'];
    $bmi = $row['bmi'];
    $i++;
  }

  $file = mysqli_query($conn,"SELECT * FROM fileUpload WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($file)) {
    $files[$i]['file_name'] = $row['file_name'];
    $files[$i]['uploaded_file'] = $row['uploaded_file'];
    $i++;
  }

  $referDoc = mysqli_query($conn,"SELECT d.doctor_name, d.mobile, d.email FROM doctor_details AS d INNER JOIN refer_table AS rt ON d.id=rt.refer_to WHERE event_id ='$eventId' ");
    $i=0;
    while ($row = mysqli_fetch_assoc($referDoc)) {
    $Refer[$i]['doctor_name'] = $row['doctor_name'];
    $Refer[$i]['mobile'] = $row['mobile'];
    $Refer[$i]['email'] = $row['email'];
    $i++;
  }

  
  $complaint = mysqli_query($conn,"SELECT * FROM complaints WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($complaint)) {
    $complaints = $row['complaint'];
    $i++;
  }

  $observe = mysqli_query($conn,"SELECT * FROM observations WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($observe)) {
    $observes = $row['obervation'];
    $i++;
  }

  $diagnose = mysqli_query($conn,"SELECT * FROM diagnosis WHERE event_id='$eventId' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($diagnose)) {
    $diagnosis = $row['diagnosis'];
    $i++;
  }

$currentDate = date('d-M-Y');
          $file_data="<br/>
          <table width='100%' style='text-align:center;font-size: 16pt;color:#1e88e5'>
   <tr>
    <th>Prescription Details</th>
  </tr>
  
   
   </table>

<table width='100%'>
   <tr>
    <th style='text-align:left;'>Patient Name :$patient_name</th>
    <th style='text-align:right;'>Examined By Doctor : $doctor_name</th>
  </tr>
   <tr>
     <th  style='text-align:left;'>Mobile Number :</th>
    <th style='text-align:right;'>Examined On  : $examinedDate</th>

   </tr>
   <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   </table>
   <table width='100%'>
   <tr>
   <td colspan='5' align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Vitals </td>
   </tr>
   <tr>
   <td>Weight </td>
   <td> Height</td>
   <td>BP </td>
   <td> Sugar</td>
   <td> BMI</td>

   <tr>
   <tr>
  <td>$weight</td>
   <td> $height</td>
   <td>$bp </td>
   <td> $sugar</td>
   <td> $bmi</td>
   </tr>
</table> ";


  $file_data.="<table width='100%'>
   <tr>
   <td colspan='5'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='5'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Prescription </td>
   </tr>
   </table>
   <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Drug Type </th>
   <th> Drug Name</th>
   <th>Duration in Days </th>
   <th> Repeat</th>
   <th> Timings </th>
   <th> Meal</th>

   </tr>";
   for($i=0;$i<count($fetch);$i++) {
$type = $fetch[$i][type];
$drug_name = $fetch[$i][drug_name];
$duration = $fetch[$i][duration];
$repeat_same = $fetch[$i][repeat_same];
$time_of_the_day = $fetch[$i][time_of_the_day];
$to_be_taken = $fetch[$i][to_be_taken];
  $file_data.="<tr>
   <td>$type </td>
   <td>$drug_name</td>
   <td>$duration </td>
   <td>$repeat_same</td>
   <td>$time_of_the_day</td>
   <td>$to_be_taken</td>

   </tr>";

   }
    $file_data.="</table>




 <table width='100%'>
  <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='2'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Allergies </td>
   </tr>
</table>
 <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Allergy Type </th>
   
   <th> Allergy Name</th>

   </tr> ";
   for($i=0;$i<count($Allergy);$i++) {


     $altype = $Allergy[$i]['altype'];
    $alname = $Allergy[$i]['alname'];

    $file_data.="<tr>
   <td>$altype</td>
   
   <td>$alname</td>

   </tr>";

   }

   $file_data.="
</table>



<table width='100%'>
  <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='2'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Diagnostic Lab </td>
   </tr>
  </table>
 <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Lab Name </th>
   <th> Test Name </th>

   </tr>";

   for($i=0;$i<count($Lab);$i++) {


     $lab_name = $Lab[$i]['lab_name'];
    $test_name = $Lab[$i]['test_name'];

    $file_data.="<tr>
   <td>$lab_name</td>
   
   <td>$test_name</td>

   </tr>";
 }


   $file_data.="
</table>


<table width='100%'>
  <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='2'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Refered Doctors </td>
   </tr>
   </table>
 <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Doctor Name </th>
 <th>Mobile </th>
  <th>Email </th>
   </tr>";
   for($i=0;$i<count($Refer);$i++) {
    $doctor_name = $Refer[$i]['doctor_name'];
    $mobile = $Refer[$i]['mobile'];
    $email = $Refer[$i]['email'];
 $file_data.="<tr>
   <td>$doctor_name</td>
      <td>$mobile</td>

   <td>$email</td>

   </tr>";
 }
   $file_data.="</table>
<table width='100%' style='page-break-inside:avoid;padding-top:20px;'>
<tr>
   <td colspan='1'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='1'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Complaints </td>
   </tr>
   <tr>
   <td>$complaints</td>

   </tr>
   <tr>
   <td colspan='1'> 
   <hr/> </td>
   </tr>
</table>
<br/>
<table width='100%' style='page-break-inside:avoid;padding-top:20px;'>
   <tr>
   <td colspan='1'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Diagnosis </td>
   </tr>
   <tr>
   <td>$diagnosis</td>

   </tr>
   <tr>
   <td colspan='1'> 
   <hr/> </td>
   </tr>
</table>
<table width='100%' style='page-break-inside:avoid;padding-top:20px;'>
   <tr>
   <td colspan='1'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Observations </td>
   </tr>
   <tr>
   <td>$observes</td>

   </tr>
   <tr>
   <td colspan='1'> 
   <hr/> </td>
   </tr>
</table>";
// print_r($files);exit;
// $logo = '/var/www/html/firstdoctor/uploads/logo.jpg';
   for($i=0;$i<count($files);$i++) {
    $file_name = $files[$i]['file_name'];
    $uploaded_file = '/var/www/html/firstdoctor/uploads/'.$files[$i]['uploaded_file']; 
 $file_data.="<table width='100%' page-break='false' style='page-break-inside:avoid;padding-top:20px;' >
            <tr>
               <th align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'>$file_name <br/>
               <p style='font-size:14px;color:#1e88e5'><a href='$uploaded_file' target='_blank'>Click here to view original file </a></th>
            </tr>
            <tr>
            <td><img src='$uploaded_file'></td>
            </tr>
            </table>";
 }
  

$file_data.="<table width='100%'>
   <tr>
   <td><br/>
   </td>
   </tr>
   <tr>
   <td>------------------------------------------------ <br> (Sign Here)</td>

   </tr>
   <tr>
   <td>$doctor_name</td>

   </tr>
    <tr>
   <td>Date : $currentDate</td>

   </tr>

</table>

";
include("../../library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetHeader("<table>
                  <tr>
            <td><img src='$logo' height='32px;'>
                   </td>
                  </tr>
                  </table>");

$mpdf->SetFooter('<div>Powered by firstDoctor</div>');
// echo $file_data;exit;

$mpdf->WriteHTML($file_data);

$mpdf->Output('medical_certificate.pdf', 'D');
exit;

?>
