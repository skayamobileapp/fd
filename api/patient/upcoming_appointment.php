<?php
include('../../connection/conn.php');

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');


$patient_id = $_GET['patient_id'];
 
 
 
 $sql = "Select d.*, e.id as eid, e.title, e.start, e.id_time_slots, e.id_clinic, c.city, c.state, spe.specialty, d.specialty, cr.clinic_name, cr.mobile as clinic_mobile, cr.address as clinic_address, cr.landmark, cr.pincode as clinic_pincode from doctor_details as d INNER JOIN events as e on d.id=e.doctor_id left join specialties spe on spe.id = d.specialty inner join clinic_registration cr on cr.id = e.id_clinic inner join cities c on cr.city_id = c.id
 where e.patient_id='$patient_id' and e.status='0' and e.start >= '$date' order by e.start ASC";

//echo json_encode($sql);exit;

$result = mysqli_query($conn,$sql);
$i=0;
$success = array();
while ($row=mysqli_fetch_assoc($result)) {
  $success[$i]['id'] = $row['id'];
  $success[$i]['doctor_name'] = $row['doctor_name'];
  $success[$i]['photo'] = $row['photo'];
	$success[$i]['id_time_slots'] = $row['id_time_slots'];
  $success[$i]['mobile'] = $row['mobile'];
  $success[$i]['start'] = date("d-m-Y H:i", strtotime($row['start']));
	$success[$i]['qualification'] = $row['qualification'];
  $success[$i]['email'] = $row['email'];
  $success[$i]['title'] = $row['title'];
  $success[$i]['eid'] = $row['eid'];
	$success[$i]['specialty'] = $row['specialty'];
		$success[$i]['address'] = $row['address'];
		$success[$i]['pincode'] = $row['pincode'];
		$success[$i]['state'] = $row['state'];
		$success[$i]['city'] = $row['city'];
		$success[$i]['experiance'] = $row['experiance'];
	$success[$i]['id_clinic'] = $row['id_clinic'];
	$success[$i]['clinic_name'] = $row['clinic_name'];
	$success[$i]['clinic_address'] = $row['clinic_address'];
	if($row['clinic_pincode'] == null)
	{
		$row['clinic_pincode'] = '';
	}
	if($row['clinic_landmark'] == null)
	{
		$row['clinic_landmark'] = '';
	}
	$success[$i]['clinic_pincode'] = $row['clinic_pincode'];
	$success[$i]['clinic_landmark'] = $row['clinic_landmark'];
	$success[$i]['clinic_mobile'] = $row['clinic_mobile'];
	
  $i++;
}



echo json_encode($success);exit;
?>