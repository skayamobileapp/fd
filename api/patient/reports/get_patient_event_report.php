<?php
include('../../../connection/conn.php');


date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d');

$event_id = $_POST['event_id'];
$patient_id = $_POST['patient_id'];



$file_data.="
<table width='100%' style='text-align:center;font-size: 16pt;color:#1e88e5;padding-top:20px;' >
  <tr>
    <th>Health Record</th>
  </tr>
</table>";





 //$sql_patient_doc_info = "SELECT e.title, e.start, e.amount, cr.clinic_name, cr.email as clinic_email, cr.mobile as clinic_mobile, pd.patient_name, pd.mobile_number as patient_mobile, pd.email as patient_email, pd.address1 as patient_address, dd.doctor_name, dd.email as doctor_email, dd.mobile as doctor_mobile FROM `events` as e, patient_details as pd, doctor_details as dd, clinic_registration as cr WHERE e.patient_id = pd.id AND e.doctor_id = dd.id AND e.id_clinic = cr.id AND e.id = '$event_id' ";

$sql_patient_doc_info = "SELECT e.title, e.start, e.amount, pd.patient_name, pd.mobile_number as patient_mobile, pd.email as patient_email, pd.address1 as patient_address, dd.doctor_name, dd.email as doctor_email, dd.mobile as doctor_mobile FROM `events` as e, patient_details as pd, doctor_details as dd WHERE e.patient_id = pd.id AND e.doctor_id = dd.id AND e.id = '$event_id' ";


$result_patient_doctor = $conn->query($sql_patient_doc_info);
$data_patient_doctor = array();
while ($row_patient_doctor = $result_patient_doctor->fetch_assoc())
{
    $patient_name = $row_patient_doctor['patient_name'];
    $doctor_name = $row_patient_doctor['doctor_name'];
    $patient_mobile = $row_patient_doctor['patient_mobile'];
    $examined_date = date("m-d-Y H:i", strtotime($row_patient_doctor['start'])); 

}






$sql_prescription = mysqli_query($conn,"select pre.*, pre.drug_type as drug_type, dt.type as type  from prescrip pre inner join drug_type dt on dt.id = pre.drug_type where pre.event_id='$event_id'");
$i = 0;
$data_prescription = array();
while ($row_prescription = mysqli_fetch_assoc($sql_prescription))
{
  if($row_prescription['drug_name'] == 'Other' || $row_prescription['drug_name'] == 'Others')
  {
    $row_prescription['drug_name'] = $row_prescription['drugname'];
  }
  
  $row_prescription['duration'] = $row_prescription['duration'].' Days';

  array_push($data_prescription,$row_prescription);

}


//echo json_encode($data_prescription);exit;

$select_lab = mysqli_query($conn,"select a.*,b.*,c.* from labdetails as a, lab_details as b, lab_test_name as c where a.lab_name=b.id and a.test_name=c.id and a.event_id='$event_id'");
$i = 0;
$data_lab = array();
while ($row_lab = mysqli_fetch_assoc($select_lab)) {
  array_push($data_lab,$row_lab);

}




$sql_allergies = mysqli_query($conn,"SELECT a.*,b.*,c.* from event_allergies as a, allergy_list as b, allergy_types as c where a.allergy_name=b.id and b.allergy_idtype=c.id and a.event_id='$event_id'");
$i = 0;
$data_allergies = array();
while ($row_allergies = mysqli_fetch_assoc($sql_allergies))
{
  array_push($data_allergies,$row_allergies);

}





$select_vitals = mysqli_query($conn,"select * from vitals where event_id='$event_id'");

$weight['weight'] = "NA";
$height['height'] = "NA";
$bp['bp'] = "NA";
$sugar['sugar'] = "NA";
$bmi['bmi'] = "NA";
$pulse['pulse'] = "NA";

while ($row_vitals = mysqli_fetch_assoc($select_vitals))
{
    $weight = $row_vitals['weight'];
    $height = $row_vitals['height'];
    
    
    $bmi = ($weight / ($height * $height)) * 10000;
  
  $bmi = number_format($bmi, 2, '.', ',');
      
  $weight = $row_vitals['weight'] . " kg";
  $height = $row_vitals['height']. " cm";
  $bp = $row_vitals['bp']. " mmHg";
  $sugar = $row_vitals['sugar'] . " mg"."/"."dL";
  $bmi = $bmi . " kg/m2";
  $pulse = $row_vitals['pulse'] . " bpm";
 
}



 $sql_referal_doctor = "SELECT rd.*, dd.experiance, dd.photo, dd.doctor_name, dd.qualification, dd.doctor_name, dd.mobile, dd.email, c.city  FROM refer_table rd inner join doctor_details as dd on rd.refer_to = dd.id inner join cities c on c.id = dd.city where rd.event_id = '$event_id'";
$result_referal_doctor = $conn->query($sql_referal_doctor);
$data_referal_doctor = array();
while ($row_referal_doctor = $result_referal_doctor->fetch_assoc()) {
    array_push($data_referal_doctor,$row_referal_doctor);
}


  $sql_complaint = mysqli_query($conn,"SELECT * FROM complaints WHERE event_id='$event_id' order by id DESC");

  $data_complaint['name'] = 'NA';

  while ($row_complaint = mysqli_fetch_assoc($sql_complaint)) {
    $complaint = $row_complaint['complaint'];
  }
if($complaint == '' || $complaint == null)
{
	$complaint = 'NA';
}



 $sql_observation = mysqli_query($conn,"SELECT * FROM observations WHERE event_id='$event_id'");

    $data_observation['name'] = 'NA';

  while ($row_observation = mysqli_fetch_assoc($sql_observation))
  {
    $observations = $row_observation['obervation'];
  }
if($observations == '' || $observations == null)
{
	$observations = 'NA';
}


 $sql_diagnose = mysqli_query($conn,"SELECT * FROM diagnosis WHERE event_id='$event_id'");

 $data_diagnose['name'] = 'NA';

  while ($row_diagnose = mysqli_fetch_assoc($sql_diagnose))
  {
    $diagnosis = $row_diagnose['diagnosis'];
  }
if($diagnosis == '' || $diagnosis == null)
{
	$diagnosis = 'NA';
}


$logo = $_SERVER['DOCUMENT_ROOT']."/uploads/logo.png";

$currentDate = date('d_M_Y_H_i_s');


$file_data.="
          
<table width='100%'>
   <tr>
    <th style='text-align:left;'>Patient Name :$patient_name</th>
    <th style='text-align:right;'>Examined By Doctor : $doctor_name</th>
  </tr>
   <tr>
     <th  style='text-align:left;'>Mobile Number : $patient_mobile</th>
    <th style='text-align:right;'>Examined On  : $examined_date</th>

   </tr>
   <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   </table>
   <table width='100%'>
   <tr>
   <td colspan='6' align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Vitals </td>
   </tr>
   <tr>
   <td>Weight </td>
   <td> Height</td>
   <td>BP </td>
   <td> Sugar</td>
   <td> PULSE</td>
   <td> BMI</td>

   <tr>
   <tr>
  <td>$weight</td>
   <td> $height</td>
   <td>$bp </td>
   <td> $sugar</td>
   <td> $pulse</td>
   <td> $bmi</td>
   </tr>
</table> ";


  $file_data.="<table width='100%'>
   <tr>
   <td colspan='5'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='5'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Prescription </td>
   </tr>
   </table>
   <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Drug Type </th>
   <th> Drug Name</th>
   <th>Duration in Days </th>
   <th> Repeat</th>
   <th> Timings </th>
   <th> Meal</th>

   </tr>";

   for($i=0;$i<count($data_prescription);$i++)
   {

    $type = $data_prescription[$i]['type'];
    $drug_name = $data_prescription[$i]['drug_name'];
    $duration = $data_prescription[$i]['duration'];
    $repeat_same = $data_prescription[$i]['repeat_same'];
    $time_of_the_day = $data_prescription[$i]['time_of_the_day'];
    $to_be_taken = $data_prescription[$i]['to_be_taken'];


   $file_data.="<tr>
   <td>$type </td>
   <td>$drug_name</td>
   <td>$duration </td>
   <td>$repeat_same</td>
   <td>$time_of_the_day</td>
   <td>$to_be_taken</td>

   </tr>";
  }


  $file_data.="
 </table>

 <table width='100%'>
  <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='2'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Allergies </td>
   </tr>
</table>
 <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Allergy Type </th>
   
   <th> Allergy Name</th>

   </tr> ";
   for($i=0;$i<count($data_allergies);$i++)
   {


     $allergy_type = $data_allergies[$i]['allergy_type'];
    $allergy_name = $data_allergies[$i]['allergy_name'];

    $file_data.="
    <tr>
     <td>$allergy_type</td>
     <td>$allergy_name</td>
   </tr>";

   }



     $file_data.="


</table>

<table width='100%'>
  <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='2'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Diagnostic Lab </td>
   </tr>
  </table>
 <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Lab Name </th>
   <th> Test Name </th>

   </tr>";

   for($i=0;$i<count($data_lab);$i++)
   {


     $lab_name = $data_lab[$i]['lab_name'];
    $test_name = $data_lab[$i]['test_name'];

    $file_data.="
    <tr>
     <td>$lab_name</td>
     <td>$test_name</td>
   </tr>";
  }



     $file_data.="
</table>


<table width='100%'>
  <tr>
   <td colspan='2'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='2'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Refered Doctors </td>
   </tr>
   </table>
 <table width='100%' border='1' style='border-collapse:collapse;'>
   <tr>
   <th>Doctor Name </th>
 <th>Mobile </th>
  <th>Email </th>
   </tr>";
   for($i=0;$i<count($data_referal_doctor);$i++)
   {
    $doctor_name = $data_referal_doctor[$i]['doctor_name'];
    $mobile = $data_referal_doctor[$i]['mobile'];
    $email = $data_referal_doctor[$i]['email'];


 $file_data.="<tr>
   <td>$doctor_name</td>
      <td>$mobile</td>

   <td>$email</td>

   </tr>";
 }



  $file_data.="</table>

<table width='100%'>
<tr>
   <td colspan='1'>
    <hr/>
   </td>
   </tr>
   <tr>
   <td colspan='1'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Complaints </td>
   </tr>
   <tr>
   <td>$complaint</td>

   </tr>
   <tr>
   <td colspan='1'> 
   <hr/> </td>
   </tr>
</table>

<table width='100%'>
   <tr>
   <td colspan='1'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Diagnosis </td>
   </tr>
   <tr>
   <td>$diagnosis</td>

   </tr>
   <tr>
   <td colspan='1'> 
   <hr/> </td>
   </tr>
</table>

<table width='100%'>
   <tr>
   <td colspan='1'  align='left'  style='font-size:16pt;font-weight:bold;color:#1e88e5'> Observations </td>
   </tr>
   <tr>
   <td>$observations</td>

   </tr>
   <tr>
   <td colspan='1'> 
   <hr/> </td>
   </tr>
</table>";



$logo = $_SERVER['DOCUMENT_ROOT']."/uploads/logo.png";

        
include("../../../library/mpdf60/mpdf.php");


$mpdf=new mPDF();
$mpdf->SetHeader("<table>
                  <tr>
            <td><img src='$logo' height='32px;'>
                   </td>
                  </tr>
                  </table>");

$filename = $patient_name. "_" .$currentDate;

$doc_root = $_SERVER['DOCUMENT_ROOT'] ."/download/". $filename.".pdf";

$mpdf->SetFooter('<div>Powered by firstDoctor</div>');
$mpdf->WriteHTML($file_data);   
$mpdf->Output($doc_root,'F');
    chmod($doc_root,0777);
$download = "http://firstdoctor.net/download/". $filename.".pdf";
  $data['status'] = 200;
$data['message'] = $download;
$data['name'] = $filename.".pdf";

echo json_encode($data);exit;

/*$filename = "dasdas.pdf";
        $mpdf = new Mpdf(); 
        $mpdf->setTitle('Sample PDF');
        $mpdf->setFooter('{PAGENO}');
        $mpdf->setAuthor('Admin');
        $mpdf->setCreator('FIMS');
        $mpdf->setSubject('Report');
        $mpdf->setKeywords('report,form');    
        $mpdf->WriteHTML($file_data);    
        $path = $_SERVER['DOCUMENT_ROOT']."/download/" . $filename;
    chmod($_SERVER['DOCUMENT_ROOT']."/download/" . $filename,0777);
    $mpdf->Output($filename.'.pdf','D');
$download = "http://firstdoctor.net/download/". $filename;
  $data['status'] = 200;
$data['message'] = $download;

echo json_encode($data);exit;*/

?>