<?php
session_start();
include('connection/conn.php');
include('encryption.php');
include('decryption.php');
  // $doctorId = $_GET['did'];

$mobNum = decryption($_GET['id']);

$sql = mysqli_query($conn,"SELECT * FROM patient_details Where mobile_number='$mobNum' ");
while ($row = mysqli_fetch_array($sql))
{
        $verifyOtp = $row['otp'];
}

if ($_POST)
{
  $id = decryption($_GET['id']);
  $reqfrom = decryption($_GET['reqfrom']);
	$reqfor = decryption($_GET['reqfor']);

$sql = "SELECT * FROM patient_details Where mobile_number='$id' ";
$result = $conn->query($sql);
$patientList = array();
while ($row = $result->fetch_assoc())
{
  $_SESSION['patient_details'] = $row;
  array_push($patientList, $row);
}

  $otp = $_POST['otp'];
  if ($verifyOtp == $otp && $reqfrom == 'booking')
  {
      echo "<script>alert('verification successful')</script>";
      echo "<script>parent.location ='booking.php?id=$reqfor'</script>";
  }
  else
  if ($verifyOtp == $otp )
  {
      echo "<script>alert('verification successful')</script>";
      echo "<script>parent.location ='PatientDashboard/index.php'</script>";
  }
  else{
      echo "<script>alert('Incorrect otp!!')</script>";
  }
}
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body class="account-page">

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					
					<div class="row">
						<div class="col-md-8 offset-md-2">
							
							<!-- Login Tab Content -->
							<div class="account-content">
								<div class="row align-items-center justify-content-center">
									<div class="col-md-7 col-lg-6 login-left">
										<img src="assets/img/login-banner.png" class="img-fluid" alt="Doccure Login">	
									</div>
									<div class="col-md-12 col-lg-6 login-right">
										<div class="login-header">
											<h3>OTP Verification <span></span></h3>
										</div>
									    <form action="" method="POST" id="loginform">
											<div class="form-group">
								                <p>Check Your Email or Mobile Text Message</p>
								                <input name="otp" type="text" class="form-control" placeholder="Enter OTP" maxlength="7" autocomplete="off" required>
								            </div> 

											<button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Verify</button>
										</form>
									</div>
								</div>
							</div>
							<!-- /Login Tab Content -->
								
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->

			<div class="container" id="popup">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header"><br>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Set New Password Now</h4>
        </div>
        <div class="modal-body">
          <div class="form-group fg-float">
            <div class="fg-line">
              <input type="email" name="emailid" class="form-control" id="emailid" required style="width: 100%; " autocomplete="off">
              <label class="fg-label">Enter Your E-MailId / Mobile Number</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" data-dismiss="modal" id="setNow" onclick="setPassword()">Submit</button>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- FOR DOCTOR -->

		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		
	</body>
</html>