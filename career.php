<!DOCTYPE html> 
<html lang="en">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
  
  </head>
  <body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>
      <!-- Breadcrumb -->
      <!-- <div class="breadcrumb-bar">
        <div class="container-fluid">
          <div class="row align-items-center">
            <div class="col-md-12 col-12">
              <nav aria-label="breadcrumb" class="page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Terms and Conditions</li>
                </ol>
              </nav>
              <h2 class="breadcrumb-title">Terms and Conditions</h2>
            </div>
          </div>
        </div>
      </div> -->
      <!-- /Breadcrumb -->
      
      <!-- Page Content -->
      <div class="content">
        <div class="container">
          <div class="row aboutus">
                <div class="col-sm-12">
                    <h1>Reach your True Potential</h1>
                    <div class="join-us-content">

                        <p class="text-justify">firstDoctor is a tremendous success, and we attribute it to the vision, innovation, anticipation, dedication and perseverance of our team. We are one of the fastest growing healthcare technology company in India</p>
                      <p class="text-justify">Our goal has always been to share our success by attracting, developing and retaining top talent in a health care industry. And if you join our team you will experience our commitment and dedication. If you have the energy and inventiveness of a true leader and believe you can inspire success, then submit your resume to us today, send your resume to <b>hr@firstdoctor.net</b> or call at <b>+91 83 90 74 95 20</b>

</p>

                      <p class="text-justify">Proactive personalities with enthusiasm, outstanding communication and interpersonal skills with a proven success record are invited to join our team.

</p>

                      <p class="text-justify">firstdoctor offers an exciting work environment with attractive remuneration package commensurate with successful candidate’s qualifications and experience.</p>                      

                      <p class="text-justify">firstdoctor is an equal opportunity employer. Only qualified candidates will be notified.

</p>

 <h1>Below are our openings</h1>
 <div id="accordion">
  <h3>Sales Executive (PM/EMR/RCM)</h3>
  <div class="pricing-content">
        <p><strong>Responsibilities</strong></p>
    <ul>
        <li>Close sales of the firstdoctor solution with medical practices in defined region and specialty.</li>
        <li>Responsible for daily new business prospecting through cold calling and lead nurturing for small, medium, and large medical practices</li>
        <li>Drive the sales cycle through qualification, discovery, planning, promoting, demonstrating, referencing and closing.</li>
        <li>Analyze prospective customer needs and provide firstdoctor product and services information</li>
        <li>Educate and set expectations regarding the firstdoctor solution, from demonstration to delivery to ongoing optimal usage.</li>
        <li>Arranges and conducts site visits, conference calls, and pre-demonstration sessions</li>
        <li>Execute sales leveraging marketing materials, videos, portal demonstrations and proposal documents</li>
        <li>Maintains ongoing industry and product knowledge, adapting to changes in the market/competition</li>
        <li>Communicate and collaborate with the Team Lead on established goals and objectives</li>
        <li>Attend industry trade shows and conferences several times annually. Overnight travel required</li>
        <li>Travels regionally - 50% as required by position</li>
    </ul>
    <p><strong>Requirements</strong></p>
    <ul>
        <li>1+ years of field sales experience selling Practice Management, EMR, and/or Revenue Cycle Management.</li>
        <li>Minimum of Bachelor’s degree preferred.</li>
        <li>Proven sales producer; must prove sales track record of achieving sales quotas.</li>
        <li>Sharp communication skills are required – verbal, written, and product demonstration.</li>
        <li>Ability to present concepts and demonstrate software functionality using persuasive sales techniques.</li>
        <li>Must possess hunter and new business development mentality</li>
        <li>Good negotiation, conflict management & customer service skills</li>
        <li>Should be already comfortable in medical and health IT terminology.</li>
        <li>Working knowledge of the healthcare IT industry itself – PM, EMR, EDI, RCM, eRx tools, a plus.</li>
        <li>Working knowledge of medical practice workflow and operational issues, a plus</li>
    </ul>
    <p style="margin-top:20px;">If you meet the above requirements, please submit your resume to <a href="mailto:hr@firstdoctor.net">hr@firstdoctor.net</a> for immediate consideration.  firstdoctor Healthcare is an equal opportunity employer.  Only short-listed candidates will be notified. </p>

  </div>
  <h3>EMR Implementation Specialist/EMR Trainer</h3>
  <div class="pricing-content">
    <p style="margin-top:20px;"><strong>Responsibilities</strong></p>
    <ul>
        <li>Provide overall implementation of firstdoctor EMR; develop and oversee implementation project milestones</li>
        <li>Evaluate clients’ workflows, documents and procedures for implementation and customized training</li>
        <li>Management of clients’ expectations and relationships</li>
        <li>Client training on firstdoctor’s practice management/EMR application, including training providers, practice front office and billing staff</li>
        <li>On-site support and issues triage/resolution for clients during implementation and specifically when clients “go live”</li>
        <li>Support of project managers on assessing site operations in multi-site client operations</li>
        <li>Other projects that could range and vary in type, but may include on-site staff support for clients on firstdoctor processes (e.g., working client work queues) to specific assignments</li>
    </ul>
    <p style="margin-top:20px;"><strong>Requirements</strong></p>
    <ul>
        <li>Minimum of Bachelors Degree (Engineering, Computer Science, Healthcare Administration, Business, Communications preferred)</li>
        <li>Must have 1-2 years of work experience in EMR/EHR implemention in a healthcare setting, project and implementation management, training, account management and support, customer service department, consulting environment</li>
        <li>Working knowledge of physician billing, clinical, operational workflows for both in-patient and out-patient settings environment is a plus</li>
        <li>Ability to use individual discretion while dealing with clients</li>
        <li>Possess multi-tasking skills and works well under pressure with tight deadlines</li>
        <li>Strong ability to identify potential issues and participate in their resolution</li>
        <li>Sound analytical and problem solving skills</li>
        <li>Strong computer literacy and the comfort, ability and desire to advance technically and understand industry standards</li>
        <li>Self-motivated, detail oriented and team-player attitude</li>
        <li>Excellent verbal and written communication skills and telephone etiquette</li>
        <li>Basic understanding of connectivity and interfacing</li>
        <li>Travel requirements are 40% of the time</li>
        <li>Must possess our core values of Integrity, Innovation, and Teamwork</li>
    </ul>
    <p style="margin-top:20px;">If you meet the above requirements, please submit your resume to <a href="mailto:hr@firstdoctor.net">hr@firstdoctor.net</a> for immediate consideration.  firstdoctor Healthcare is an equal opportunity employer.  Only short-listed candidates will be notified. </p>

  </div>
  <h3>Accounts Management Executive</h3>
  <div class="pricing-content">
    <p style="margin-top:20px;"><strong>Responsibilities</strong></p>
    <ul>
        <li>Manage, organize and monitor progress of tasks assigned.</li>
        <li>Ensure team members work to resolve issues/verify deliverables.</li>
        <li>Provide status reporting of team activities and keep supervisors informed of task accomplishment, issues and status.</li>
        <li>Document tasks performed on a daily basis.</li>
        <li>To audit practices, finding and resolving neglected areas.</li>
        <li>To analyze clearing house, ERA and EOB rejections and providing viable solutions where possible.</li>
        <li>To coordinate with respective AR team leads for effective practice management.</li>
        <li>Correction of assigned errors in TD.</li>
        <li>Thorough verification of EDI files and coordination with the corporate office on paper claims printing in case of any issue.</li>
        <li>Perform job roles in compliance with IS Policies and Procedures and acceptable usage of asset and services guidelines.</li>
        <li>Regulatory, contractual and IS Policy violations are reported immediately to supervisors or appropriate authorities for rectification and disciplinary actions.</li>
        <li>Must possess expert-level knowledge of full Revenue Cycle Management.</li>
        <li>Ability to effectively initiate and manage multi-disciplined projects. Up-to-date knowledge of reimbursement regulations and compliance issues of third party payers (Insurance companies).</li>
        <li>Good communication and analytical skills.</li>
        <li>Capable of managing accounts receivable with multiple third party managed care contracts.</li>
        <li>Strong professional demeanor with the inherent quality to rapidly establish credibility and rapport with clinicians, managers, customers and external groups</li>
        <li>Ability to identify opportunities and solutions for process improvement along with the skills of planning and implementation of these solutions.</li>

    </ul>
    <p style="margin-top:20px;"><strong>Requirements</strong></p>
    <ul>
        <li>Bachelor’s Degree in Commerce</li>
    </ul>
    <p style="margin-top:20px;">If you meet the above requirements, please submit your resume to <a href="mailto:work@firstdoctor.com">work@firstdoctor.com</a> for immediate consideration.  firstdoctor Healthcare is an equal opportunity employer.  Only short-listed candidates will be notified. </p>

  </div>
  
</div>

                    </div>
                </div>
                
               
              

</div>
        </div>

      </div>    
      <!-- /Page Content -->
   
      <?php include('footer.php'); ?>
    </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="assets/js/script.js"></script>
    
  </body>
</html>