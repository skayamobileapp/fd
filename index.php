<?php
include('connection/conn.php');
error_reporting(0);
// $session_id = session_id();

$doctorSql = "SELECT d.id, d.doctor_name, d.photo, d.email, d.mobile, d.experiance, d.address, s.specialty, c.city as cityname, c.state as statename FROM doctor_details d Left JOIN specialties s ON d.specialty=s.id INNER JOIN cities c ON d.city=c.id limit 0,5";
$view = array();
$result1 = mysqli_query($conn, $doctorSql);
while($row=mysqli_fetch_assoc($result1)){
array_push($view, $row);
}

  if (empty($view)) {
     $message = "<h3 class='text-center'>Doctor Not Found</h3";
   }


$sql  = "select id, city from cities order by city";
$result = $conn->query($sql);
$cityList = array();
while ($row = $result->fetch_assoc()) {
    array_push($cityList, $row);
}

$sql   = "select id, specialty from specialties order by specialty ";
$result = $conn->query($sql);
$specialtyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($specialtyList, $row);
}

$sql   = "select count(id) as totalDoctorCount from doctor_details";
$result = $conn->query($sql);
$doctorList = array();
while ($row = $result->fetch_assoc()) {
  $doctorList['totalDoctorCount'] = $row['totalDoctorCount'];
}

$sql   = "select count(id) as totalLabCount from lab_details";
$result = $conn->query($sql);
$labList = array();
while ($row = $result->fetch_assoc()) {
  $labList['totalLabCount'] = $row['totalLabCount'];
}

$sql   = "select count(id) as totalEventCount from events_list";
$result = $conn->query($sql);
$eventList = array();
while ($row = $result->fetch_assoc()) {
  $eventList['totalEventCount'] = $row['totalEventCount'];
}

$sql   = "select count(id) as totalDrugCount from pharmacy_registration";
$result = $conn->query($sql);
$drugList = array();
while ($row = $result->fetch_assoc()) {
  $drugList['totalDrugCount'] = $row['totalDrugCount'];
}

$sql  = "select * from blogs";
$result = $conn->query($sql);
$blogList = array();
while ($row = $result->fetch_assoc()) {
    array_push($blogList, $row);
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<title>Firstdoctor</title>
		
		<!-- Favicons -->
		<link type="image/x-icon" href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">

		<link href="select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body>

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
			<?php include('navbar.php'); ?>
			<!-- Home Banner -->
			<section class="section section-search">
				<div class="container-fluid">
					<div class="banner-wrapper">
						<div class="banner-header text-center">
							<h2><b>INDIA'S NO. 1 </b><br>PLATFORM FOR EVERYTHING ABOUT HEALTH</h2>
							<p><br>Discover the Best <b>Medical Practices In The City</b>.</p>
						</div>
                         
						<!-- Search -->
						<div class="search-box">
							<form action="" method="">
								<div class="form-group search-location">
									<select name="city" class="form-control selitemIcon" id="city">
										<option value="">Select location</option>
										<?php
		                                for ($i=0; $i<count($cityList); $i++) {  ?>
		                                <option value="<?php echo $cityList[$i]['id']; ?>"><?php echo $cityList[$i]['city']; ?></option>
		                                <?php
		                                }
		                                ?>
									</select>
									<span class="form-text">Based on your Location</span>
								</div>
								<div class="form-group search-location">
									<select name="type" class="form-control selitemIcon" id="type">
										<option value="">Select Type</option>
										<option value="doctor">Doctor</option>
										<option value="clinic">Clinic</option>
										<option value="speciality">Speciality</option>
									</select>
									<span class="form-text">Based on Type</span>
								</div>
								<div class="form-group">
									<input type="text" class="form-control search-location" name="name" id="name" placeholder="Search Here">
									<span class="form-text">Ex : Doctors or Clinics or Specialities</span>
								</div>
								<button type="button" onclick="callSearch()" class="btn btn-primary search-btn"><i class="fas fa-search"></i> <span>Search</span></button>
							</form>
						</div>
						<!-- /Search -->
						
					</div>
				</div>
			</section>
			<!-- /Home Banner -->
			  
			<!-- Clinic and Specialities -->
			<section class="section section-specialities">
				<div class="container-fluid">
					<div class="section-header text-center">
						<h2>Integrated Customer Centric Services</h2>
						<!-- <p class="sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> -->
					</div>
					<div class="row justify-content-center">
						<div class="col-md-9">
							<!-- Slider -->
							<div class="specialities-slider slider">
							
								<!-- Slider Item -->
								<div class="speicality-item text-center">
									<div class="speicality-img">
										<!--<img src="assets/img/specialities/specialities-01.png" class="img-fluid" alt="Speciality">-->
										<img src="assets/img/custom_by_vk/p_m_01.png" class="img-fluid" alt="Speciality">
										<span><i class="fa fa-circle" aria-hidden="true"></i></span>
									</div>
									<!--<p>Urology</p>-->
									<p>Patient Management</p>
									<!-- Hided For DEscription Of Patient Management Add-->
								<!--<p> Search For Best Medical Practice In Your City, Manage appointments, follow ups and get daily notification of your schedule. Close the communication gap, automated appointment and follow ups reminders.</p>-->
								</div>	
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								<div class="speicality-item text-center">
									<div class="speicality-img">
										<!--<img src="assets/img/specialities/specialities-02.png" class="img-fluid" alt="Speciality">-->
										<img src="assets/img/custom_by_vk/p_m_02.png" class="img-fluid" alt="Speciality">
										<span><i class="fa fa-circle" aria-hidden="true"></i></span>
									</div>
									<!--<p>Neurology</p>-->
									<p>Medical Records</p>
								</div>							
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								<div class="speicality-item text-center">
									<div class="speicality-img">
										<!--<img src="assets/img/specialities/specialities-03.png" class="img-fluid" alt="Speciality">-->
										<img src="assets/img/custom_by_vk/p_m_03.png" class="img-fluid" alt="Speciality">
										<span><i class="fa fa-circle" aria-hidden="true"></i></span>
									</div>	
									<!--<p>Orthopedic</p>-->
									<p>Progress Notes</p>
								</div>							
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								<div class="speicality-item text-center">
									<div class="speicality-img">
										<!--<img src="assets/img/specialities/specialities-04.png" class="img-fluid" alt="Speciality">-->
										<img src="assets/img/custom_by_vk/p_m_04.png" class="img-fluid" alt="Speciality">
										<span><i class="fa fa-circle" aria-hidden="true"></i></span>
									</div>	
									<!--<p>Cardiologist</p>	-->
									<p>Medical Billing</p>	
								</div>							
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								<div class="speicality-item text-center">
									<div class="speicality-img">
										<!--<img src="assets/img/specialities/specialities-05.png" class="img-fluid" alt="Speciality">-->
										<img src="assets/img/custom_by_vk/p_m_05.png" class="img-fluid" alt="Speciality">
										<span><i class="fa fa-circle" aria-hidden="true"></i></span>
									</div>	
									<!--<p>Dentist</p>-->
									<p>Patient Acquisition</p>
								</div>							
								<!-- /Slider Item -->
								
								<div class="speicality-item text-center">
									<div class="speicality-img">
										<!--<img src="assets/img/specialities/specialities-05.png" class="img-fluid" alt="Speciality">-->
										<img src="assets/img/custom_by_vk/p_m_06.png" class="img-fluid" alt="Speciality">
										<span><i class="fa fa-circle" aria-hidden="true"></i></span>
									</div>	
									<!--<p>Dentist</p>-->
									<p>Doctor Hub</p>
								</div>							
								<!-- /Slider Item -->
								
							</div>
							<!-- /Slider -->
							
						</div>
					</div>
				</div>   
			</section>	 
			<!-- Clinic and Specialities -->
		  
			<!-- Popular Section -->
			<section class="section section-doctor">
				<div class="container-fluid">
				   <div class="row">
						<div class="col-lg-4">
							<div class="section-header ">
								<h2>Book An Appointment</h2>
								<p> </p>
							</div>
							<div class="about-content">
								<p>   Search For Best Medical Practice In Your City, Manage appointments, follow ups and get daily notification of your schedule. Close the communication gap, automated appointment and follow ups reminders.</p>	
								<p>   Get Your Health Records on a Single Click When Ever & Where Ever. Search a Diagnostic, Best Doctor and Laboratory Near You.</p>					
								<a href="javascript:;">Read More..</a>
							</div>
						</div>
						<div class="col-lg-8">
							<div class="doctor-slider slider">
							
							<?php for($i=0; $i<count($view); $i++){ ?>
								<!-- Doctor Widget -->
								<div class="profile-widget">
								    <!-- Hided Photo Instructions By Ritesh Sir-->
									<!--<div class="doc-img">-->
									<!--	<a href="doctor-profile.php?id=<?php echo $view[$i]['id'];?>">-->
									<!--		<img class="img-fluid" alt="User Image" src="uploads/<?php echo $view[$i]['photo'];?>" height="530px">-->
									<!--	</a>-->
									<!--	<a href="javascript:void(0)" class="fav-btn">-->
									<!--		<i class="far fa-bookmark"></i>-->
									<!--	</a>-->
									<!--</div>-->
									<div class="pro-content">
										<h3 class="title">
											<a href="doctor-profile.php?id=<?php echo $view[$i]['id'];?>"><?php echo $view[$i]['doctor_name'];?></a>
											<!-- <i class="fas fa-check-circle verified"></i> -->
										</h3>
										<p class="speciality"><?php echo $view[$i]['specialty'];?></p>
										<div class="rating">
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<i class="fas fa-star filled"></i>
											<span class="d-inline-block average-rating"></span>
										</div>
										<ul class="available-info">
											<li>
												<i class="fas fa-map-marker-alt"></i> <?php echo $view[$i]['cityname'];?>, <?php echo $view[$i]['statename'];?>
											</li>
											<li>
												<i class="far fa-clock"></i> Available on Fri, 22 Mar
											</li>
										</ul>
										<div class="row row-sm">
											<div class="col-6">
												<a href="doctor-profile.php?id=<?php echo $view[$i]['id'];?>" class="btn view-btn">View Profile</a>
											</div>
											<div class="col-6">
												<a href="booking.php?id=<?php echo $view[$i]['id'];?>" class="btn book-btn">Book Now</a>
											</div>
										</div>
									</div>
								</div>
								<!-- /Doctor Widget -->
							<?php } ?>
								
							</div>
						</div>
				   </div>
				</div>
			</section>
			<!-- /Popular Section -->
		   
		   <!-- Availabe Features -->
		   <section class="section section-features">
				<div class="container-fluid">
				   <div class="row">
						<div class="col-md-5 features-img">
							<!--<img src="assets/img/features/feature.png" class="img-fluid" alt="Feature">-->
							<img src="assets/img/custom_by_vk/free_health_checkup_feature_ration.png" class="img-fluid" alt="Feature">
							
						</div>
						<div class="col-md-7">
							<div class="section-header">	
								<h2 class="mt-2">Find The Health Camps Near You & Register.</h2>
								<!--<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>-->
							</div>	
							<div class="features-slider slider">
								<!-- Slider Item -->
								<div class="feature-item text-center">
									<!--<img src="assets/img/features/feature-01.jpg" class="img-fluid" alt="Feature">-->
									<img src="assets/img/custom_by_vk/blood_donation.jpg" class="img-fluid" alt="Feature">
									<p>Blood Donation</p>
								</div>
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								
							<div class="feature-item text-center">
								<img src="assets/img/custom_by_vk/vision.svg" class="img-fluid" alt="Feature">
									<p>Eye Checkup</p>
								</div>
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								<div class="feature-item text-center">
									<img src="assets/img/custom_by_vk/diabetes.svg" class="img-fluid" alt="Feature">
									<p>Diabetic Awareness</p>
								</div>
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								
								<div class="feature-item text-center">
								<img src="assets/img/custom_by_vk/cardiac.svg" class="img-fluid" alt="Feature">
									<p>Healthy Heart</p>
								</div>
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
								
								<div class="feature-item text-center">
								    <img src="assets/img/custom_by_vk/run_health.svg" class="img-fluid" alt="Feature">
									<p>Run For A Health</p>
								</div>
								<!-- /Slider Item -->
								
								<!-- Slider Item -->
							
								<div class="feature-item text-center">
									<!--<img src="assets/img/features/feature-06.jpg" class="img-fluid" alt="Feature">-->
								<img src="assets/img/custom_by_vk/chakra_yoga.svg" class="img-fluid" alt="Feature">
									<p>Yoga For Soal</p>
								</div>
								<!-- /Slider Item -->
							</div>
						</div>
				   </div>
				</div>
			</section>		
			<!-- /Availabe Features -->
			
			<!-- Blog Section -->
		   <section class="section section-blogs">
				<div class="container-fluid">
				
					<!-- Section Header -->
					<div class="section-header text-center">
						<h2>Blogs</h2>
					</div>
					<!-- /Section Header -->
					
					<div class="row blog-grid-row">
						<?php for ($i=0; $i <count($blogList); $i++) { ?>
						<div class="col-md-6 col-lg-3 col-sm-12">
						
							<!-- Blog Post -->
							<div class="blog grid-blog">
								<div class="blog-image">
									<a href="blog-detail.php?of=<?php echo $blogList[$i]['link_name']; ?>"><img class="img-fluid" src="<?php echo $blogList[$i]['image']; ?>" alt="Post Image"></a>
								</div>
								<div class="blog-content">
									<ul class="entry-meta meta-item">
										<li>
											<div class="post-author">
												<!-- <a href="doctor-profile.html"><img src="assets/img/doctors/doctor-thumb-01.jpg" alt="Post Author"> <span>Dr. Ruby Perrin</span></a> -->
											</div>
										</li>
										<li><i class="far fa-clock"></i> <?php echo date("d M Y", strtotime($blogList[$i]['created_date'])); ?></li>
									</ul>
									<h3 class="blog-title" title="<?php echo $blogList[$i]['title']; ?>"><a href="blog-detail.php?of=<?php echo $blogList[$i]['link_name']; ?>"><?php echo substr($blogList[$i]['title'], 0, 40)."..."; ?></a></h3>
									<p class="mb-0"><?php echo substr($blogList[$i]['description'], 0, 100)."..."; ?></p>
								</div>
							</div>
							<!-- /Blog Post -->
							
						</div>
						
					<?php } ?>
					</div>
					<div class="view-all text-center"> 
						<!-- <a href="blog-list.html" class="btn btn-primary">View All</a> -->
					</div>
				</div>
			</section>
			<!-- /Blog Section -->			
			
		<?php include('footer.php'); ?>   
	   </div>
	   <!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Slick JS -->
		<script src="assets/js/slick.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>

		<script type="text/javascript">
            function callSearch(){
            var city = $("#city").val();
            var type = $("#type").val();
            var name = $("#name").val();

              parent.location="search.php?city="+city+"&type="+type+"&name="+name;
              //   success: function(result){
              //   $("#div1").html(result);
              // }
            }
        </script>

		<script src="select2/js/select2.js" ></script>
    <script src="select2/js/select2-init.js" ></script>
		
	</body>
</html>