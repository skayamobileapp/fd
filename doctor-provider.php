<!DOCTYPE html> 
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Firstdoctor</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    
    <!-- Favicons -->
    <link href="fd_logo.png" rel="icon">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
    <link rel="stylesheet" href="assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">

      h4{
        size: 6px;
      }
      p{
        font-size: 13px;
      }

    </style>
  
  </head>
  <body>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
    
      <?php include('navbar.php'); ?>
      
      <!-- Page Content -->
      <div class="content">
        <div class="container">

        <h1 class="text-center primary-title">For Providers</h1>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/service_phr.svg" width="80px">
                    <h4 style="color: #3c663e;">EHR - Patient</h4>
                    <p>Enhance quality, efficiency and productivity</p>
                </div>
            </div>


             <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/service_phr.svg" width="80px">
                    <h4 style="color: #3c663e;">EHR - Practice</h4>
                    <p>The latest evolution of our core EHR is
transforming healthcare — through comprehensive documentation, in-depth understanding, and industry-leading value and satisfaction.</p>
                </div>
            </div>


            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green" >
                    <img src="img/service_homecare.svg" width="80px">
                    <h4 style="color: #3c663e;">Patient Engagement</h4>
                    <p><b>Better Engagement = Better Healthcare.</b>
                Patients need easy and comprehensive access to all that you offer. Start with Patient Portal, healow apps, and online booking to establish a strong online presence</p>
                </div>
            </div>

             <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col pink">
                    <img src="img/service_nextgen_testing.svg" width="80px">
                    <h4>Cloud Data</h4>
                    <p>You’re on your own database, your system performance will never be affected by another practice’s upgrades or large search queries. You can also schedule upgrades when they’re convenient for you.</p>
                </div>
            </div>


<div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="new-services-col navy-blue">
                        <img src="img/service_pharmacy.svg" width="80px">
                        <h4 style="color: #5f329b;">Pharmacy Management System</h4>
                        <p>Accredited pharmacies will help provide required medication to patients to doorstep.</p>
                    </div>
                </div>
            
<div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/service_diagnostic.svg" width="80px">
                    <h4 style="color: #3c663e;">Diagnostic Management System</h4>
                    <p>Accredited path labs on the FirstDoctor platform for quick testing and reporting at
affordable rates.</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/diacom.png" width="80px">
                    <h4 style="color: #3c663e;">Digital Imaging and Communications in Medicine</h4>
                    <p>Digital Imaging and Communications in Medicine is a standard protocol for the management and transmission of medical images and related data and is used in many healthcare facilities</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col green">
                    <img src="img/pacs.png" width="80px">
                    <h4 style="color: #3c663e;">picture archiving and communication system</h4>
                    <p>A picture archiving and communication system (PACS) is a medical imaging technology which provides economical storage and convenient access to images from multiple modalities (source machine types).</p>
                </div>
            </div>

            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col pink">
                    <img src="img/service_post_care_treatment.svg" width="80px">
                    <h4 style="color: #ff3655;">Patient Portal</h4>
                    <p>Let your patients subscribe to valuable electronic tools and self service options</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="new-services-col orange">
                    <img src="img/service_ai.svg" width="80px">
                    <h4 style="color: #ed8e0e;">Out Patient Department Bills</h4>
                    <p>Reduce denials, increase collections and fix revenue leaks</p>
                </div>
            </div>
                                                                         
        </div>
        </div>

      </div>    
      <!-- /Page Content -->
   
      <?php include('footer.php'); ?>
    </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="assets/js/script.js"></script>
    
  </body>
</html>