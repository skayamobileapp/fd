<?php
include('connection/conn.php');

error_reporting(0);
  // $doctorId = $_GET['id'];

if(isset($_POST['save']))
{
  $doctorId = $_GET['id'];

  $fname = $_POST['first_name'];
  $midname = $_POST['middle_name'];
  $lname = $_POST['last_name'];
  $patientName = $fname." ".$midname." ".$lname;
  $patientPhoneNumber = $_POST['patient_phone_number'];
  $patientMobileNumber = $_POST['mobile'];
  $email = $_POST['email_address'];
  $address1 = $_POST['address1'];
  $dateOfBirth = $_POST['dob'];
  $card = $_POST['card'];
  $socialSecurityIdentity = $_POST['social_security_identity'];
  $gender = $_POST['gender'];
  $maritalStatus = $_POST['martial_status'];
  $pincode = $_POST['pincode'];
  $state = $_POST['state'];
  $city = $_POST['city'];
  $patientEmployer = $_POST['patient_employer'];
  $employmentStatus = $_POST['employment_status'];
  $emergencyContact = $_POST['emergency_contact'];
  $relationshipToPatient = $_POST['relationship_to_patient'];
  $address2 = $_POST['address2'];
  $password = $_POST['password'];

  $recieveotp = $_POST['recieveotp'];
  $telphone = $_POST['Telcheck'];
  $teldata = implode(",", $telphone);

  $Profile = $_FILES['photo']['name'];
    $img_tmp =$_FILES['photo']['tmp_name'];
    move_uploaded_file($img_tmp,"uploads/".$Profile);

  $otp = rand(0000000,9999999);

  $check ="SELECT * FROM patient_details WHERE email='$email' ";
  $checkresult = mysqli_query($conn,$check);
  $chklp =mysqli_num_rows($checkresult);

  if ($chklp=='') {

    $insert = mysqli_query($conn,"INSERT INTO patient_details(patient_name, first_name, middle_name, last_name, patient_phone_number, mobile_number, email, address1, date_of_birth, card_type, social_security_or_identity_num, gender, martial_status, pincode, state, city, patient_employer, employment_status, emergency_contact, relationship_to_patient, address2, password, telephone, receive_otp, photo, otp, status)VALUES('$patientName', '$fname', '$midname', '$lname', '$patientPhoneNumber','$patientMobileNumber','$email', '$address1', '$dateOfBirth','$card','$socialSecurityIdentity', '$gender', '$maritalStatus', '$pincode', '$state', '$city','$patientEmployer','$employmentStatus', '$emergencyContact', '$relationshipToPatient', '$address2', '$password', '$teldata', '$recieveotp', 'default.png', '$otp', '0')");

    
    if ($insert) 
    {

        $last_id = mysqli_insert_id($conn);
        $date = date('Y-m-d');
        // $EndDate = date('Y')+48000;
        $etime = strtotime(date('Y-m-d'));
        $expiryDate = date("Y-m-d", strtotime('+1 year', $etime));
          mysqli_query($conn,"INSERT INTO card_history(patient_id, card_type, renewal_date, expiry_date, status) VALUES('$last_id', '$card', '$date', '$expiryDate', '1')");
        
        $to =  $email;
$subject = "OTP for FirstDoctor";

$message = "
<table>
<tr>
<td>Dear $patientName</td>
</tr>
<tr>
<td>Welcome to <b>First Doctor</b></td>
</tr>
<tr>
<td>Your OTP verification code is <b>$otp</b>, Please enter the verification code to proceed further.</td>
</tr>
<tr>
<td><br/><br/>Regards,</td>
</tr>
<tr>
<td><img src='http://rvvlsi.com/firstdoctor/img/phr_logo.png' width='100px' height='50px'></td>
</tr>
<tr>
<td>First Doctor team</td>
</tr>
</table>

";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <no-reply@firstdoctor.app>' . "\r\n";

mail($to,$subject,$message,$headers);

// Sms otp verification code
$mobileno = $patientMobileNumber;
$message = urlencode("Your OTP verification code is ".$otp." ");

$ch = curl_init();

$url = "http://simplysmsit.com/submitsms.jsp?user=HEALTH&key=a9f3bd5447XX&mobile=$mobileno&message=$message&senderid=FSTDOC&accusage=1";

// set URL and other appropriate options
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

// grab URL and pass it to the browser
curl_exec($ch);

// close cURL resource, and free up system resources
curl_close($ch);

        echo "<script>parent.location = 'patient-otp-checker.php?id=$last_id'</script>";
        
    }
  }
  else
  {
   $message= "<div class='alert alert-danger'>THIS EMAIL Id ALREADY EXIST<button type='button' class='btn pull-right' data-dismiss='alert'>X</button></div>";
  }
}

?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">

    <!-- Datetimepicker CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->

		<style type="text/css">
			
	.error{
      text-transform: uppercase;
      position: relative;
      color: #a94442;
    }
		</style>
	
	</head>
	<body class="account-page">

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
		<?php include('navbar.php'); ?>	
			<!-- Page Content -->

			<form action="" method="POST" id="form" enctype="multipart/form-data">
    <div class="container col-sm-10">
       <div class="row">
           <section class="col-sm-10 col-lg-12">          
            <div class="main-container">
				<div class="login-header col-sm-6">
	               <h2 style="color: #2287de; font-family: 'SourceSansPro-Bold'; padding-bottom: 10px;">Patient Subscription Form</h2>
	           </div>

                <div class="card" style="padding: 20px;">
                  <div class="row">
                    <div class="form-group">
                      <div class="col-sm-10 col-sm-offset-2">
                        <?php echo $message; ?>
                      </div>
                    </div>                  
                  </div>

                    <div class="row" >
                        <div class="col-sm-6">
                        <div class="form-group">
                                <input type="text" class="form-control floating" name="first_name" id="first_name" maxlength="50" autocomplete="off" value="<?php echo $_POST['first_name']; ?>" placeholder="First Name">
                        </div>                            
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                                <input type="text" class="form-control" name="middle_name" id="middle_name" maxlength="50" autocomplete="off" value="<?php echo $_POST['middle_name']; ?>" placeholder="Middle Name">
                        </div>                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
	                        <div class="form-group">
                                <input type="text" class="form-control" name="last_name" maxlength="50" autocomplete="off" value="<?php echo $_POST['last_name']; ?>" placeholder="Last Name">
	                        </div>           
                        </div>
                        <div class="col-sm-6">
	                        <div class="form-group">
                                <input type="email" class="form-control" name="email_address" maxlength="30" autocomplete="off" value="<?php echo $_POST['email']; ?>" placeholder="Email Address">
	                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
	                        <div class="form-group">
                                <input type="password" class="form-control" name="password" maxlength="10" autocomplete="off" value="<?php echo $_POST['password']; ?>" placeholder="Create Password">
	                        </div>         
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="gender" id="gender" class="form-control">
                                	<option value="">Select Gender</option>
                                	<option value="Male">MALE</option>
                                	<option value="Female">FEMALE</option>
                                	<option value="Other">OTHER</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                          <div class="cal-icon">
                            <input type="text" class="form-control datetimepicker" name="dob" id="dob" autocomplete="off" value="<?php echo $_POST['dob']; ?>" placeholder="Date of Birth">
                          </div>
                        </div>
                      </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="mobile" maxlength="10" autocomplete="off" value="<?php echo $_POST['mobile']; ?>" placeholder="Patient's Mobile Number">
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="checkbox" name="terms" id="terms" class="terms" required>
                                <a href="#" data-toggle="modal" data-target="#myPrivacy">I agree to the terms of Service.</a>
                            </div>
                        </div>
                    </div>

                    <div class="bttn-group " style="padding: 20px;">
                        <button class="btn btn-primary btn-lg float-right" name="save" id="save"> Register </button>
                    </div>                                       
                </div>
                </div>
            </section>
        </div>
    </div>
</form>


			<!-- /Page Content -->
			<?php include('footer.php'); ?>	   
		</div>

		<div class="modal fade" id="myPrivacy" role="dialog">
            <div class="modal-dialog model-md" style="width: 90%;">
                <div class="modal-content">
                    <div class="modal-body">
                    	<h1 style="color: #09aded;">FirstDoctor – OPD SaaS Terms and Conditions</h1>
                        <button type="button" class="btn btn-light" data-dismiss="modal">X</button>
                        <?php include('getPrivacyService.php'); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>

    <!-- Datetimepicker JS -->
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>

		<script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/jquery-ui.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>

		<script type="text/javascript">
    $(document).ready(function()
    {

         $("#form").validate({
             rules : {

              first_name : {
                required : true,
                accept: true
            },
              speciality : "required",
              sub_speciality : "required",
              postal_address : "required",
              email_address : "required",
              gender : "required",
              password : "required",
              institution : "required",
              state : "required",
              city : "required",
              ima_no : "required",
              licence : "required",
              clinic_address : "required",
              fellow_ship : "required",
              dob : "required",
              recieveotp : "required",
              Telcheck : "required",
              // photo : "required",
              terms : "required",
              newterms : "required",

            degree_name : {
                required : true,
                // accept: true
            },
            last_name : {
                required : true,
                accept: true
            },
                
                  mobile : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
                years_of_practice : {
                    required : true,
                    number : true,
                    maxlength : 3

                },
                pincode : {
                    required : true,
                    // number : true,
                    // minlength : 6,
                    // maxlength : 6

                },
                telephone_details : {
                    required : true,
                    number : true,
                    minlength : 11,
                    maxlength : 11

                },
                practice_contact : {
                    required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
                alter_phone : {
                  required : true,
                    number : true,
                    minlength : 10,
                    maxlength : 10

                },
                cell_number : {
                  required : true,
                   number : true,
                    minlength : 10,
                    maxlength : 10

                },
         
            },
            messages : {

                first_name : {
               required : "<span> enter first name</span>",
               accept : "<span> enter letters only</span>"
               },
               
               last_name : {
                   required : "<span> enter last name</span>",
                   accept : "<span> enter letters only</span>"
               },
                speciality : "<span> select speciality</span>",
                 sub_speciality : "<span> select sub-speciality</span>",
                postal_address : "<span> enter postal address</span>",
                email_address : "<span> enter email address </span>",
                gender : "<span> select gender </span>",
                password : "<span> enter password</span>",
                 state : "<span> select state </span>",
                city : "<span> select city</span>",
                 institution : "<span> enter institution</span>",
                 ima_no : "<span> enter indian medical assoc number</span>",
                 licence : "<span> enter licence number</span>",
                 clinic_address : "<span> enter clinic address</span>",
                 fellow_ship : "<span> enter fellowship number</span>",
                 dob : "<span>select date of birth</span>",
                 recieveotp : "<span>Choose any one</span>",
                 Telcheck : "<span>Choose atleast one</span>",
                 photo : "<span>Upload profile photo</span>",
                 terms : "<span>Check to accept terms</span>",
                 newterms : "<span>Check to accept payment terms</span>",

                degree_name : {
               required : "<span> enter highest educational qualification</span>",
               accept : "<span> enter letters only</span>"
           },

            
                 mobile : {
                    required : "<span> enter mobile number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                years_of_practice : {
                    required : "<span> enter total professional experience</span>",
                    number : "<span> enter number only</span>",
                    maxlength : "<span> enter max 3 digit  number</span>"
                },
                 pincode : {
                    required : "<span> enter pincode</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 6 digit number</span>",
                    maxlength : "<span> enter max 6 digit number</span>"
                },
                telephone_details : {
                    required : "<span> enter telephone number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 11 digit number</span>",
                    maxlength : "<span> enter max 11 digit  number</span>"
                },
                 practice_contact : {
                    required : "<span> enter practice contact number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                alter_phone : {
                    required : "<span> enter alternative number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                },
                cell_number : {
                    required : "<span> enter cell phone number</span>",
                    number : "<span> enter number only</span>",
                    minlength : "<span> enter 10 digit number</span>",
                    maxlength : "<span> enter max 10 digit  number</span>"
                }
            }
        });
    });

    $(function(){
    $("#termservice").change(function(){
        $('.terms').prop('checked', $(this).prop('checked'));
        $('#myPrivacy').modal('toggle');

    });
});
$(function(){
    $("#termservice").change(function(){
      $('#myPrivacy').modal('toggle');
    });
});
</script>

</body>
</html>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
		
	</body>
</html>