<?php
session_start();
include('connection/conn.php');

if($_POST)
{
    $userType = $_POST['role'];
    if($userType == 'doctor')
    {
      $email = $_POST['email'];
      $password = $_POST['password'];
      
        $sql = "SELECT * FROM doctor_details WHERE email='$email' AND password='$password' AND status=1";
        $result  = $conn->query($sql);
        $doctorList = array();
        while ($row = $result->fetch_assoc())
        {
          $_SESSION['doctor_details'] = $row;
          array_push($doctorList, $row);
        }
      }
    else
    if ($userType == 'staff') {
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        $sql = "SELECT * FROM staff WHERE email='$email' AND password='$password' ";
        $result  = $conn->query($sql);
        $staffList = array();
        while ($row = $result->fetch_assoc())
        {
          $_SESSION['staff'] = $row;
          array_push($staffList, $row);
        }
      }

    if(empty($doctorList) && empty($staffList))
    {
      echo "<script>alert('Please enter the valid username and password');</script>";
      echo "<script>parent.location='doctor_login.php'</script>";
      exit;
    }
    else
    if($doctorList)
    {
      echo "<script>parent.location='DoctorDashboard/index.php'</script>";
      exit;
    }
    else
    {
      echo "<script>parent.location='StaffDashboard/index.php'</script>";
      exit;
    }
}
?>
<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Firstdoctor</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		
		<!-- Favicons -->
		<link href="fd_logo.png" rel="icon">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		
		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
		<link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
		
		<!-- Main CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.min.js"></script>
			<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	
	</head>
	<body class="account-page">

		<!-- Main Wrapper -->
		<div class="main-wrapper">
		
<?php include('navbar.php'); ?>
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					
					<div class="row">
						<div class="col-md-8 offset-md-2">
							
							<!-- Login Tab Content -->
							<div class="account-content">
								<div class="row align-items-center justify-content-center">
									<div class="col-md-7 col-lg-6 login-left">
										<img src="assets/img/login-banner.png" class="img-fluid" alt="Doccure Login">	
									</div>
									<div class="col-md-12 col-lg-6 login-right">
										<div class="login-header">
											<h3>Login <span></span></h3>
										</div>
										<form action="" method="POST" id="loginform">
											<div class="form-group form-focus">
												<input type="email" name="email" autocomplete="off" class="form-control floating" required>
												<label class="focus-label">Email</label>
											</div>
											<div class="form-group form-focus">
												<input name="password" type="password" class="form-control floating" autocomplete="off" required>
												<label class="focus-label">Password</label>
											</div>
											<!-- <div class="form-group form-focus"> -->
								              <!-- <p class="label-text">Select Role</p> -->
								              <div class="radio-container">
								                <label class="radio-inline radio-box-style">
								                  <input type="radio" name="role" id="doctor" value="doctor" required><span class="check-radio"></span> Doctor
								                </label>
								                <label class="radio-inline radio-box-style">
								                  <input type="radio" name="role" id="staff" value="staff"><span class="check-radio"></span> Staff
								                </label>
								              </div>
								            <!-- </div> -->
											<div class="text-right">
												<a class="forgot-link" href="forgot-password.php">Forgot Password ?</a>
											</div>
											<button class="btn btn-primary btn-block btn-lg login-btn" type="submit">Login</button>
											<div class="login-or">
												<span class="or-line"></span>
												<span class="span-or">or</span>
											</div>
											<div class="row form-row social-login">
												<div class="col-6">
													<a href="#" class="btn btn-facebook btn-block"><i class="fab fa-facebook-f mr-1"></i> Login</a>
												</div>
												<div class="col-6">
													<a href="#" class="btn btn-google btn-block"><i class="fab fa-google mr-1"></i> Login</a>
												</div>
											</div>
											<div class="text-center dont-have">Don’t have an account? <a href="doctor_register.php">Register</a></div>
										</form>
									</div>
								</div>
							</div>
							<!-- /Login Tab Content -->
								
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->
   
			<?php include('footer.php'); ?>
		</div>
		<!-- /Main Wrapper -->
	  
		<!-- jQuery -->
		<script src="assets/js/jquery.min.js"></script>
		
		<!-- Bootstrap Core JS -->
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		
		<!-- Custom JS -->
		<script src="assets/js/script.js"></script>
		
	</body>
</html>